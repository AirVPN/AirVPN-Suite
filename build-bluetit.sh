#!/bin/sh

#
# Build dynamic binary for bluetit
#
# Version 1.1 - ProMIND
#

if [ `uname` = "Darwin" ]; then
    echo "Bluetit for macOS is not currently available."

    exit 1
fi

BASE_NAME=bluetit
SSL_LIB_TYPE=OPENSSL

INC_DIR=..
OPENVPN3=${INC_DIR}/openvpn3-airvpn
ASIO=${INC_DIR}/asio

DBUS_INCLUDE=$(pkg-config --cflags --libs dbus-1)
DBUS_LIBS=$(pkg-config --cflags --libs dbus-1)

SOURCES="src/bluetit.cpp \
         src/dbusconnector.cpp \
         src/localnetwork.cpp \
         src/dnsmanager.cpp \
         src/netfilter.cpp \
         src/rcparser.cpp \
         src/optionparser.cpp \
         src/base64.cpp \
         src/airvpntools.cpp \
         src/airvpnmanifest.cpp \
         src/airvpnserver.cpp \
         src/airvpnuser.cpp \
         src/countrycontinent.cpp \
         src/airvpnservergroup.cpp \
         src/cipherdatabase.cpp \
         src/airvpnserverprovider.cpp \
         src/wireguardclient.cpp \
         src/semaphore.cpp \
         src/execproc.c \
         src/loadmod.c
        "

BIN_FILE=${BASE_NAME}

case $SSL_LIB_TYPE in
    OPENSSL)
        SSL_DEF=-DUSE_OPENSSL
        SSL_LIB_LINK="-lssl -lcrypto"
        break
        ;;

    MBEDTLS)
        SSL_DEF=-DUSE_MBEDTLS
        SSL_LIB_LINK="-lmbedtls -lmbedx509 -lmbedcrypto"
        break
        ;;

    *)
        echo "Unsupported SSL Library type ${SSL_LIB_TYPE}"
        exit 1
        ;;
esac

COMPILE="g++ -fwhole-program -Ofast -Wall -Wno-sign-compare -Wno-unused-parameter -std=c++17 -flto=4 -Wl,--no-as-needed -Wunused-local-typedefs -Wunused-variable -Wno-shift-count-overflow -pthread ${SSL_DEF} -DMAKING_BLUETIT -DUSE_ASIO -DASIO_STANDALONE -DASIO_NO_DEPRECATED -I${ASIO}/asio/include -DHAVE_LZ4 -I${OPENVPN3} -I${OPENVPN3}/openvpn ${DBUS_INCLUDE} -I/usr/include/libxml2 ${SOURCES} obj/wireguard.o -lcryptopp -lcurl -lxml2 ${SSL_LIB_LINK} -llz4 -lz -llzma -lzstd ${DBUS_LIBS} -o ${BIN_FILE}"

echo $COMPILE

$COMPILE

strip ${BIN_FILE}
