#!/bin/sh

#
# AirVPN suite installation script
#
# Version 1.3.0 - 30 May 2023
#
# Written by ProMIND
#

get_bin_path()
{
    binpath="/bin /sbin /usr/bin /usr/sbin /usr/local/bin /usr/local/sbin"

    local cmdpath=$2
    eval $cmdpath=$1

    for cpath in $binpath; do
        tbin="${cpath}/${1}"

        if [ -f ${tbin} ]; then
            eval $cmdpath=$tbin
        fi
    done
}

get_dbus_path()
{
    tpath="/etc/dbus-1/system.d /usr/local/etc/dbus-1/system.d /usr/share/dbus-1/system.d /usr/local/share/dbus-1/system.d"

    for dpath in $tpath; do
        if [ -d ${dpath} ] && [ "${dbuspath}" = "" ]; then
            dbuspath=$dpath
        fi
    done
}

get_bin_path useradd useradd_bin
get_bin_path groupadd groupadd_bin
get_bin_path usermod usermod_bin
get_bin_path passwd passwd_bin

dbuspath=""

get_dbus_path

airvpn_user=airvpn
airvpn_group=airvpn

echo
echo "AirVPN suite installation script"
echo

if [ `id -u` != "0" ]; then
    echo "You need root privilegs to install AirVPN suite"
    echo

    exit 1
fi

read -p "Do you want to install AirVPN Suite? [y/n] " yn

if [ "${yn}" = "n" ]; then
    echo
    echo "Installation aborted"

    exit 1
fi

INIT_TYPE="unknown"

echo

if [ `cat < /proc/1/comm` = "systemd" ]; then
    INIT_TYPE=systemd
    echo "System is using systemd"
elif [ `cat < /proc/1/comm` = "init" ]; then
    INIT_TYPE=init
    echo "System is using SysV-style init"
else
    echo "This system is not running either systemd or SysV-style init"
    echo "Installation aborted"
    echo

    exit 1
fi

if [ "${dbuspath}" = "" ]; then
    echo
    echo "ERROR: D-Bus is not properly configured or not available"
    echo "Installation aborted"
    echo

    exit 1
else
    echo
    echo "D-Bus directory is ${dbuspath}"
    echo
fi

if [ "${INIT_TYPE}" = "systemd" ]; then
    systemctl --quiet is-active bluetit.service

    if [ $? -eq 0 ]; then
        systemctl stop bluetit.service

        sleep 5
    fi
else
    /etc/init.d/bluetit stop 2&>1 > /dev/null
fi

echo "Installing bluetit to /sbin"

cp bin/bluetit /sbin/bluetit

if [ -d "/usr/local/bin" ]; then
    BINDIR=/usr/local/bin
elif [ -d "/usr/bin" ]; then
    BINDIR=/usr/bin
else
    BINDIR=/bin
fi

echo "Installing goldcrest to ${BINDIR}"

cp bin/goldcrest $BINDIR

echo "Installing hummingbird to ${BINDIR}"

cp bin/hummingbird $BINDIR

echo "Installing bluetit configuration files"

if [ ! -d "/etc/airvpn" ]; then
    mkdir /etc/airvpn
fi

chmod 770 /etc/airvpn

cp etc/airvpn/airvpn-manifest.xml /etc/airvpn
cp etc/airvpn/connection_priority.txt /etc/airvpn
cp etc/airvpn/connection_sequence.csv /etc/airvpn
cp etc/airvpn/country_continent.csv /etc/airvpn
cp etc/airvpn/country_names.csv /etc/airvpn
cp etc/airvpn/continent_names.csv /etc/airvpn

if [ ! -f "/etc/airvpn/bluetit.rc" ]; then
    cp etc/airvpn/bluetit.rc /etc/airvpn
fi

chmod 660 -R /etc/airvpn

echo "Installing D-Bus configuration files"

cp etc/dbus-1/system.d/* $dbuspath

chmod 644 $dbuspath/org.airvpn.*

if [ "${INIT_TYPE}" = "systemd" ]; then
    # systemd

    echo "Installing systemd bluetit.service unit"

    cp etc/systemd/system/bluetit.service /etc/systemd/system

    echo

    read -p "Do you want to enable bluetit.service unit? [y/n] " yn

    if [ "${yn}" = "y" ]; then
        systemctl enable bluetit.service

        if [ $? -eq 0 ]; then
            echo "Bluetit service enabled"
        else
            echo "Cannot enable Bluetit service"
            echo "Run 'systemctl status bluetit.service' for more information"
            echo

            exit 1
        fi
    fi

    echo

    read -p "Do you want to start Bluetit service now? [y/n] " yn

    if [ "${yn}" = "y" ]; then
        systemctl stop bluetit.service
        systemctl daemon-reload
        systemctl start bluetit.service

        if [ $? -eq 0 ]; then
            echo "Bluetit service started"
        else
            echo "Cannot start Bluetit service"
            echo "Run 'systemctl status bluetit.service' for more information"
            echo

            exit 1
        fi
    fi

else
    # SysV-style init

    echo "Installing bluetit SysV-style init script"

    cp etc/init.d/bluetit /etc/init.d

    echo

    read -p "Do you want to start bluetit at boot? [y/n] " yn

    if [ "${yn}" = "y" ]; then
        which chkconfig > /dev/null 2>&1

        if [ $? -eq 0 ]; then
            chkconfig --add bluetit
        else
            ln -s /etc/init.d/bluetit /etc/rc0.d/K60bluetit
            ln -s /etc/init.d/bluetit /etc/rc1.d/K60bluetit
            ln -s /etc/init.d/bluetit /etc/rc2.d/K60bluetit
            ln -s /etc/init.d/bluetit /etc/rc3.d/S90bluetit
            ln -s /etc/init.d/bluetit /etc/rc4.d/K60bluetit
            ln -s /etc/init.d/bluetit /etc/rc5.d/S90bluetit
            ln -s /etc/init.d/bluetit /etc/rc6.d/K60bluetit
        fi
    fi

    echo

    read -p "Do you want to start Bluetit daemon now? [y/n] " yn

    if [ "${yn}" = "y" ]; then
        /etc/init.d/bluetit start

        if [ $? -eq 0 ]; then
            echo "Bluetit service started"
        else
            echo "Cannot start Bluetit daemon"
            echo "Run 'cat < /var/log/syslog | grep bluetit' for more information"
            echo

            exit 1
        fi
    fi

fi

id $airvpn_user > /dev/null 2>&1

if [ $? -ne 0 ]; then
    echo

    read -p "Do you want to create ${airvpn_user} user? [y/n] " yn

    if [ "${yn}" = "y" ]; then
        $useradd_bin -m $airvpn_user

        if [ $? -eq 0 ]; then
            echo "Please set a password for user ${airvpn_user}"

            $passwd_bin $airvpn_user
        fi
    fi
fi

grep -q ^$airvpn_group /etc/group

if [ $? -ne 0 ]; then
    echo

    read -p "Do you want to create ${airvpn_group} group? [y/n] " yn

    if [ "${yn}" = "y" ]; then
        $groupadd_bin $airvpn_group

        if [ $? -ne 0 ]; then
            echo "ERROR: Cannot create group ${airvpn_group}"
            echo

            exit 1
        fi
    fi
fi

id $airvpn_user > /dev/null 2>&1

if [ $? -eq 0 ]; then
    grep -q ^$airvpn_group /etc/group

    if [ $? -eq 0 ]; then
        grep ^$airvpn_group /etc/group | cut -d: -f4 | grep -q $airvpn_user
 
        if [ $? -eq 1 ]; then
            $usermod_bin -a -G $airvpn_group $airvpn_user > /dev/null 2>&1

            if [ $? -eq 0 ]; then
                echo

                echo "User ${airvpn_user} added to group ${airvpn_group}"
            else
                echo "ERROR: Cannot add user ${airvpn_user} to group ${airvpn_group}"
                echo

                exit 1
            fi
        fi
    fi
fi

echo

if [ "${INIT_TYPE}" = "systemd" ]; then
    systemctl reload dbus.service
else
    /etc/init.d/dbus reload
fi

echo "Done."
