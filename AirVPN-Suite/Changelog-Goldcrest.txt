Changelog for Goldcrest

Version 1.3.0 - 1 June 2023

- [ProMIND] added run control directive "air-vpn-type"
- [ProMIND] --air-info --air-server now reports OpenVPN and WireGuard VPN specifications
- [ProMIND] --air-info --air-server now reports Perfect Secrecy Forward (PFS) availability
- [ProMIND] --air-info --air-server and --air-country now reports the respective information timestamps
- [ProMIND] signal_handler(): in case of termination in progress, ignore termination signals
- [ProMIND] added connection statistics to the log after "event_disconnected"


*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*


Version 1.2.1 - 9 December 2022

- [ProMIND] production release


*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*


Version 1.2.1 RC 1 - 30 November 2022

- [ProMIND] updated all dependencies and libraries
- [ProMIND] package is now realeased both for OpenSSL 3.0 and OpenSSL 1.1.x (legacy)
- [ProMIND] "bluetit-status" option now shows the status of network lock and connection


*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*


Version 1.2.0 - 22 March 2022

- [ProMIND] production release


*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*


Version 1.2.0 RC 3 - 17 March 2022

- [ProMIND] Update connection statistics to the latest Bluetit specifications


*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*


Version 1.2.0 RC 2 - 8 March 2022

- [ProMIND] Added --list-data-ciphers option
- [ProMIND] Added server information summary to statistics output
- [ProMIND] Normalized (extended) bool values for options allowuaf, compress and network-lock


*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*


Version 1.2.0 RC 1 - 15 February 2022

- [ProMIND] Reassigned short option "Q" to long option "air-key-load"


*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*


Version 1.2.0 Beta 1 - 7 February 2022

- [ProMIND] Removed ipv6 command line option and replaced with allowuaf option (Allow Unused Address Families) in order to comply to the new OpenVPN3 specifications
- [ProMIND] Added OpenVPN copyright information and SSL library information to the welcome message
- [ProMIND] Changed usage() in order to use the new normalized option format


*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*


Version 1.1.0 - 4 June 2021

- [ProMIND] Production release


*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*


Version 1.1.0 RC 4 - 14 May 2021

- [ProMIND] Added SIGUSR2 handler for VPN reconnection
- [ProMIND] Added --reconnect option


*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*


Version 1.1.0 RC 3 - 16 April 2021

- [ProMIND] Release Candidate 3


*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*


Version 1.1.0 RC 2 - 14 April 2021

- [ProMIND] Release Candidate 2


*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*


Version 1.1.0 RC 1 - 7 April 2021

- [ProMIND] Release Candidate 1


*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*


Version 1.1.0 Beta 2 - 2 April 2021

- [ProMIND] Updated base classes
- [ProMIND] Added line option --bluetit-stats


*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*


Version 1.0.0 - 7 January 2021

- [ProMIND] Production release
