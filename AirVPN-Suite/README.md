# AirVPN Suite for Linux

#### AirVPN's free and open source OpenVPN 3 suite based on AirVPN's OpenVPN 3 library fork

### Version 1.3.0 - Release date 1 June 2023


**Main features:**

* Bluetit: lightweight D-Bus controlled system daemon providing full
  connectivity to AirVPN servers and generic OpenVPN servers

* Goldcrest: Bluetit client, allowing full integration with AirVPN servers,
  users, keys, profiles as well as generic OpenVPN servers

* Hummingbird: lightweight and standalone client for generic OpenVPN server
  connection

* Linux x86-64, arm7l and arm64 (Raspberry) support

* Full integration with systemd, SysVStyle-init and chkconfig

* No heavy framework required, no GUI

* Tiny RAM footprint

* Lightning fast

* Based on [OpenVPN 3 library fork by AirVPN](https://github.com/AirVPN/openvpn3-airvpn)
  with tons of critical bug fixes from the main branch, new cipher support
  and never seen before features

* ChaCha20-Poly1305 cipher support on both Control and Data Channel providing
  great performance boost on ARM, Raspberry PI and any Linux-based platform not
  supporting AES-NI. *Note:* ChaCha20 support for Android had been already
  implemented in [our free and open source Eddie Android edition](https://airvpn.org/forums/topic/44201-eddie-android-edition-24-released-chacha20-support/)

* Robust leaks prevention through Network Lock based either on iptables,
  nftables or pf through automatic detection

* Proper handling of DNS push by VPN servers, working with resolv.conf as well
  as any operational mode of systemd-resolved additional features


## Contents

* [Introduction](#introduction)
* [Installing AirVPN Suite for Linux](#installing-airvpn-suite-for-linux)
  * [Requirements](#requirements)
  * [Install AirVPN Suite](#install-airvpn-suite)
  * [Note about Hummingbird and SELinux](#note-about-hummingbird-and-selinux)
  * [Note on Checksum Files](#note-on-checksum-files)
* [AirVPN Bluetit daemon](#airvpn-bluetit-daemon)
  * [Bluetit configuration](#bluetit-configuration)
    * [Run control file](#run-control-file)
    * [User policy](#user-policy)
  * [Bluetit default connection values](#bluetit-default-connection-values)
  * [Controlling Bluetit daemon](#controlling-bluetit-daemon)
    * [systemd](#systemd)
    * [SysVStyle-init](#sysvstyle-init)
  * [Logging](#logging)
  * [Automatic connection at boot or startup time](#automatic-connection-at-boot-or-startup-time)
* [Goldcrest client](#goldcrest-client)
  * [Goldcrest configuration](#goldcrest-configuration)
  * [Note on AirVPN username and password](#note-on-airvpn-username-and-password)
  * [Controlling Goldcrest client](#controlling-goldcrest-client)
  * [Using Goldcrest with AirVPN Servers](#using-goldcrest-with-airvpn-servers)
  * [Using Goldcrest with Generic OpenVPN servers](#using-goldcrest-with-generic-openvpn-servers)
* [Running the Hummingbird Client](#running-the-hummingbird-client)
  * [Start a connection](#start-a-connection)
  * [Stop a connection](#stop-a-connection)
  * [Start a connection with a specific cipher](#start-a-connection-with-a-specific-cipher)
  * [Disable the network filter and lock](#disable-the-network-filter-and-lock)
  * [Ignore the DNS servers pushed by the VPN server](#ignore-the-dns-servers-pushed-by-the-vpn-server)
  * [Controlling Hummingbird](#controlling-hummingbird)
  * [Note on macOS and UDP](#note-on-macos-and-udp)
* [Network Filter and Lock](#network-filter-and-lock)
  * [Using Network Filter and Lock with Bluetit](#using-network-filter-and-lock-with-bluetit)
* [DNS Management in Linux](#dns-management-in-linux)
* [DNS Management in macOS](#dns-management-in-macos)
* [Recover Your Network Settings](#recover-your-network-settings)
  * [Special Note for Bluetit](#special-note-for-bluetit)
* [Building AirVPN Suite from Sources](#building-airvpn-suite-from-sources)
  * [Build Linux Dynamic Binary](#build-linux-dynamic-binary)
  * [Build Linux - ARM Static Binary](#build-linux-arm-static-binary)


-------------------------------------------------------------------------------


## Introduction

AirVPN Suite is a collection of applications providing VPN connectivity both to
AirVPN servers and to generic OpenVPN systems. The Suite provides two different
operational methods giving specific ways to connect to a VPN server and
according to the system in use. The Suite is made of three applications:
Bluetit, Goldcrest and Hummingbird. It should however be noted full integration
and functionality with AirVPN infrastructure and servers is primarily provided by
Bluetit and Goldcrest, whereas Hummingbird is to be considered as a standalone
program to connect any OpenVPN server, including AirVPN's. AirVPN users are of
course advised to install and use Bluetit and Goldcrest in their computers in order
to have both full access and functionality to AirVPN server infrastructure and to
get a better security model provided by the specific client/server architecture of
these two components.

Hummingbird, while it can be used to connect to any AirVPN server, is to be
considered a standalone and portable tool for connecting to any OpenVPN server
by providing a valid OpenVPN config file.

AirVPN Suite provides the following components:

* **Bluetit**: D-Bus controlled system daemon with OpenVPN connectivity
* **Goldcrest**: Command line client for Bluetit
* **Hummingbird**: Standalone command line OpenVPN client

All the components of AirVPN Suite are based on OpenVPN 3 AirVPN version 3.8.4.


## Installing AirVPN Suite for Linux

AirVPN Suite is distributed both in binary and source code forms. Both the
binary version and the complete source code is available in the official
[gitlab repository](https://gitlab.com/AirVPN/AirVPN-Suite). For more information,
feedback and latest news, please refer to [AirVPN forum](https://airvpn.org/forums/)
and related threads


### Requirements

The AirVPN Suite supports both systemd and SysVStyle-init Linux distributions on
the x86-64, ARM32 and ARM64 architectures.

AirVPN Suite requires and runs in the following architectures and systems:

**Linux with OpenSSL 3.0.x**

* x86-64 (64 bit), ARM 32 or ARM 64 bit CPU
* OpenSSL 3.0
* tar
* sha512sum (optional)
* ldd (optional)

**Linux with OpenSSL 1.1.1x (legacy)**

* x86-64 (64 bit), ARM 32 or ARM 64 bit CPU
* OpenSSL 1.1.x
* tar
* sha512sum (optional)
* ldd (optional)

Each component of the suite requires specific dependencies to be properly
installed, configured and running in your system. It is very likely most of the
dependencies are already available and properly configured in your system as
they are part of any modern Linux distribution. In case a specific dependency is
not already available in your system, you need to install it by using each
distribution's specific tool for installing packages. Each component requires
the following dependencies to be available and installed in your system:

**Bluetit**

* D-Bus
* libxml2
* libssl (OpenSSL 3.0.x, OpenSSL 1.1.1x for legacy installation)

**Goldcrest**

* D-Bus

**Hummingbird**

* libssl (OpenSSL 3.0.x, OpenSSL 1.1.1x for legacy installation)


Before installing the suite, you need to download the distribution package and
checksum file for your specific system and in particular:

* **Linux x86-64 - OpenSSL 3.0.x **

  [AirVPN-Suite-x86_64-1.3.0.tar.gz](https://gitlab.com/AirVPN/AirVPN-Suite/blob/master/binary/AirVPN-Suite-x86_64-1.3.0.tar.gz)

  [AirVPN-Suite-x86_64-1.3.0.tar.gz.sha512](https://gitlab.com/AirVPN/AirVPN-Suite/blob/master/binary/AirVPN-Suite-x86_64-1.3.0.tar.gz.sha512)

* **Linux x86-64 legacy - OpenSSL 1.1.1x**

  [AirVPN-Suite-x86_64-legacy-1.3.0.tar.gz](https://gitlab.com/AirVPN/AirVPN-Suite/blob/master/binary/AirVPN-Suite-x86_64-legacy-1.3.0.tar.gz)

  [AirVPN-Suite-x86_64-legacy-1.3.0.tar.gz.sha512](https://gitlab.com/AirVPN/AirVPN-Suite/blob/master/binaryAirVPN-Suite-x86_64-legacy-1.3.0.tar.gz.sha512)

* **Raspberry Pi OS - Raspberry Pi OS legacy - Linux ARM 32 bit - OpenSSL 1.1.1x**

  [AirVPN-Suite-armv7l-legacy-1.3.0.tar.gz](https://gitlab.com/AirVPN/AirVPN-Suite/blob/master/binary/AirVPN-Suite-armv7l-legacy-1.3.0.tar.gz)

  [AirVPN-Suite-armv7l-legacy-1.3.0.tar.gz.sha512](https://gitlab.com/AirVPN/AirVPN-Suite/blob/master/binary/AirVPN-Suite-armv7l-legacy-1.3.0.tar.gz.sha512)

* **Raspberry PiOS 64 bit - Linux ARM 64 bit - OpenSSL 1.1.1x**

  [AirVPN-Suite-aarch64-legacy-1.3.0.tar.gz](https://gitlab.com/AirVPN/AirVPN-Suite/blob/master/binary/AirVPN-Suite-aarch64-legacy-1.3.0.tar.gz)

  [AirVPN-Suite-aarch64-legacy-1.3.0.tar.gz.sha512](https://gitlab.com/AirVPN/AirVPN-Suite/blob/master/binary/AirVPN-Suite-aarch64-legacy-1.3.0.tar.gz.sha512)

* **Linux ARM 64 bit - OpenSSL 3.0.x**

  [AirVPN-Suite-aarch64-1.3.0.tar.gz](https://gitlab.com/AirVPN/AirVPN-Suite/blob/master/binary/AirVPN-Suite-aarch64-1.3.0.tar.gz)

  [AirVPN-Suite-aarch64-1.3.0.tar.gz.sha512](https://gitlab.com/AirVPN/AirVPN-Suite/blob/master/binary/AirVPN-Suite-aarch64-1.3.0.tar.gz.sha512)


## Install AirVPN Suite

AirVPN Suite comes with an installation Bourne shell script (sh) which tries to
determine your system type and environment in order to properly run the correct
installation procedure. In particular, AirVPN Suite can be used both in systemd
and SysVStyle-init systems and the installation script takes care of both
environments and properly configures the system.

The installation script can also create a proper user and group in order to let
Goldcrest client communicate and use Bluetit daemon. Specifically, the Bluetit
daemon can be operated and used only by users belonging to the group `airvpn`.
The installation script will prompt for both `airvpn` user and group to be created
and, of course, it is up to the superuser to properly manage user and group
creation. The creation of both `airvpn` user and group is optional, however the
superuser is advised to add at least one user to the group `airvpn` in order to
use and communicate with Bluetit daemon.

Superuser is of course granted the use and management of all AirVPN Suite
components. Please note the installation script needs root privileges.

To install AirVPN Suite, please do the following steps:

* Download AirVPN Suite tarball according to your system and to the links provided
  above.

* [optional] Download the corresponding checksum file according to the links
  provided above. This file is required to check the integrity of the above tar
  archive. It is not mandatory but it is strongly advised to download this file
  and check the tar archive integrity

* Open a terminal window and login as root by issuing the command `su -l`. In
  case your system does not allow root login (such as Ubuntu) you can use `sudo`
  or enable root login.

* [optional] Check the integrity of the tar archive by issuing this command:
  `sha512sum --check <checksum_file.sha512>`

* [optional] Make sure the command responds with `<package_name>: OK`

* Change your current directory to a convenient place, such as your home
  directory. This can be done by issuing the command `cd ~`

* Extract the tar archive by issuing this command in your terminal window:
  `tar xvf <package_name`

* A new directory will be created: `AirVPN-Suite`

* Move into this new directory with command `cd AirVPN-Suite`

* [optional] Check dynamic library availability. Issue the ldd command
  for each program of the suite:

  * `ldd bin/bluetit`

  * `ldd bin/goldcrest`

  * `ldd bin/hummingbird`

  Make sure all the required dynamic libraries are available. No line of the
  output must contain "not found"

* AirVPN Suite is now ready to be installed

* Make sure to login as `root` either by opening a root terminal or by issuing
  the command `su -l`

* Run the installation script: `sh ./install.sh`

* In case your system does not permit root login, you can run the installation
  script with `sudo sh ./install.sh`. The user from which it is issued this
  command must be in the "sudoers" list.

* The script will configure your system according to the init style in use
  (systemd or SysVStyle-init) and will copy the programs in their specific
  locations:

  * `bluetit` will be copied in `/sbin`

  * `goldcrest` and `hummingbird` will be copied in `/usr/local/bin` or
    `/usr/bin` or `/bin` according to directory availability and in this
    specific order. **Please note hummingbird needs root privileges to run. Your
    user must therefore be included in your system's "sudoers" (depending on
    specific Linux distribution)**

* Depending on your system, the script will ask you whether you want to start
  the Bluetit daemon at boot, therefore by making it always available to clients
  and to connect any OpenVPN server, including AirVPN functionality and
  connectivity.

  * **systemd** If you want Bluetit to be started at boot, you need to answer
    "yes" to both "Do you want to enable bluetit.service unit?" and "Do you want
    to start Bluetit service now?"

  * **SysVStyle-init** If you want Bluetit to be started at boot, you need to
    answer "yes" to "Do you want to start bluetit at boot?". Please note the
    SysVStyle-init bluetit script can also be used with `chkconfig` and the
    installation script will use it in case it is available in your system.

* Bluetit daemon is accessible and used only by users belonging to the group
  `airvpn` or by `root`. In this regard, the installation script will ask
  whether it should create, in case it is not already defined in your system,
  the user `airvpn`. Creating such a user is not mandatory and it is however on
  the responsibility of `root` to properly manage and create users and the
  `airvpn` group in order to comply to Bluetit policy.

* The installation script will also ask whether the group `airvpn` should be
  created and in case it is not already defined in your system. In case you do
  not want the script to create the group `airvpn`, be advised `root` must
  however create it and add to this group all the users allowed to run
  `goldcrest` and to interact with Bluetit daemon.
  In the scenario of a standard installation, only users belonging to the group
  `airvpn` are allowed to use and access Bluetit. The system administrator can
  of course change this behavior by changing and properly setting the policy of
  both D-Bus and Bluetit and under his or her own responsibility. To set or
  change D-Bus and Bluetit policy, please refer to the specific section below.

* By default, the installation script will add the `airvpn` user to the `airvpn`
  group.

* AirVPN Suite is now ready to be used.


### Note about Hummingbird and SELinux

The user is always and strongly advised to use Bluetit daemon and to control it
by using one of its clients, such as Goldcrest. In case the user wants to run
Hummingbird from a systemd unit and the system has SELinux, in order to make it
start at boot time, you should note SELinux will prevent nft to be called from
Hummingbird because access to stdin is forbidden by the default policy. In this
specific case, Hummingbird will be unable to enforce Network Lock rules via nft
and you need to create a specific SELinux exception in order to do that.


### Note on Checksum Files

We do strongly suggest you to check the integrity of the distribution `tar.gz`
file in order to make sure you are installing the suite created and fully
supported by AirVPN. Please read [CHECKSUM.md](https://gitlab.com/AirVPN/AirVPN-Suite/blob/master/CHECKSUM.md)
for more information and to get the checksum codes created and guaranteed by
AirVPN.


## AirVPN Bluetit daemon

AirVPN Bluetit daemon is the core component of the suite providing full support
to connectivity and functionality with AirVPN server architecture and infrastructure.
It also provides three levels of security and access control by allowing the
superuser to define specific policies within Bluetit.

Bluetit also provides full connectivity with any or generic OpenVPN server by
using a working OpenVPN profile or configuration file. OpenVPN connectivity is
implemented with [OpenVPN 3 AirVPN library](https://github.com/AirVPN/openvpn3-airvpn)
forked from the master project and actively maintained by AirVPN developers.

Bluetit is a daemon designed and developed with the aim of providing a
lightweight system service with a low impact on the overall consumption of
system resources. The daemon is accessed through a D-Bus interface by providing
specific methods and interface in order to give full support to OpenVPN connection
and AirVPN functionality, including - but not limited to - quick automatic connection
to the best AirVPN server for any specific location as well as any AirVPN server.

It also allows the connection to any or generic OpenVPN server by means of
profile or configuration files. This means Bluetit can also be used with any
OpenVPN infrastructure and not limited to AirVPN's. As a matter of fact, AirVPN
support and functionality can be disabled by the superuser, therefore having
Bluetit working as a generic OpenVPN component.


### Bluetit configuration

Bluetit allows the superuser to configure the functionality of the daemon,
including specific access and use policies, in three different levels and each
controlling a specific behavior as well as restricting the connections to
particular servers. Bluetit control and access is provided through these three
methods which will be covered in detail in the below and relative sections.

* System user policy: the access to the daemon can be granted only to specific
  users or groups

* D-Bus policy both for daemon and client. This allows a "fine grained" access
  control by allowing or denying specific users or groups

* Bluetit run control file


AirVPN Bluetit uses a set of files to control its behavior and, in particular,
to ensure full support to all of AirVPN functions and services. The
configuration files used by Bluetit are stored in `/etc/airvpn` and this folder
is created by the installation script, in case it does not exist. In this
directory is also stored the `bluetit.lock` file (actually, a PID file)
controlling the execution of the daemon. Please note the installation script
sets `/etc/airvpn` mode to `0770` that is `rwxrwx---`. This means only `root`
and users belonging to its group can read, write and access the content of this
directory.


#### Run control file

Bluetit daemon can be controlled by specific settings defined in the run control
file `bluetit.rc` stored in `/etc/airvpn`. In this file can be defined specific
rules and settings in order to control the functionality of the daemon, in
particular, what services and functions are allowed, limited or denied by the
superuser.

AirVPN Suite comes with a `bluetit.rc` template in which are defined only the
options needed to ensure AirVPN support, therefore allowing AirVPN connectivity
"out of the box".

The run control file provides for the following options and types. The options
defined as "list" can be used multiple times within `bluetit.rc` and each entry
can have as a value both a string (with no quotes) or a list of strings in the
form of `value1,value2,value3,...`.

* **bootserver**: (list) URL of AirVPN bootstrap server. In order to enable
  AirVPN support there must be at least one bootserver defined.

* **rsaexponent**: (string) RSA exponent of AirVPN public key. This option must
  be set in order to enable AirVPN support.

* **rsamodulus**: (string) RSA modulus of AirVPN public key. This option must be
  set in order to enable AirVPN support.

* **airconnectatboot**: (string) Define whether Bluetit should connect to an AirVPN
  server at boot or startup time. Possible values: off, quick, server, country.
  Default: off

* **networklockpersist**: (string) Define the network lock method to be used for the
  whole bluetit session. When turned on, traffic is allowed to and from the network only
  in case bluetit is connected to a VPN. You should therefore consider activating this
  directive carefully in case bluetit is started at boot time. In this specific case,
  you should be aware no traffic will be permitted soon after bluetit startup and this
  will affect all services and processes subsequently started and requiring network access.
  Also consider this directive takes priority over networklock directive which it will
  be therefore ignored. Network lock is an exclusive AirVPN feature in order to prevent
  data leakage in case of accidental disconnection by keeping the network traffic on a
  locked state. Network locking is done by a specific set of firewall rules allowing
  traffic to and from the connected server only. Possible values: on (automatic),
  iptables, nftable, pf or off. The "on" value automatically detects the firewall
  system in use. Default: off

* **airusername**: (string) Valid AirVPN username to be used for AirVPN connection
  at boot or startup time. Default: empty

* **airpassword**: (string) Password associated to the airusername to be used for
  AirVPN connection at boot or startup time. **Important**: password is stored in
  this directive and in the .rc file in plain characters. This is something you should
  however consider although bluetit.rc, by default, can be accessed and read by root
  user only. Default: empty

* **airvpntype**: (string) VPN type to be used for AirVPN connections. Possible
  values: wireguard, openvpn. Default: openvpn

* **airkey**: (string) Name of AirVPN key associated to airusername. It must be
  an existing key and previously defined in the Client Area of AirVPN website.
  Please note this value is case sensitive and it must be therefore exactly entered
  the way it has been defined in the Client Area. In case no key is provided,
  automatic connection will use the first key defined for airusername. Default: empty

* **airserver**: (string) Name of AirVPN server to be used for boot connection by
  means of airconnectatboot directive. In this specific case, airconnectatboot must
  be assigned to "server" option. Default: empty

* **aircountry**: (string) Name of AirVPN country to be used for boot connection by
  means of airconnectatboot directive. In this specific case, airconnectatboot must
  be assigned to "country" option. When this mode is enabled, AirVPN connection will
  be established with the best performing server of the specified country.
  Default: empty

* **airproto**: (string) Protocol to be used for automatic boot connection. Possible
  values: udp, tcp. Default: udp

* **airport**: (integer) Port number to be used for automatic boot connection.
  Default: 443

* **aircipher**: (string) Cipher algorithm name to be used for automatic boot
  connection. Default: empty

* **airipv6**: (on/off) Enable or disable IPv6 for automatic boot connection.
  Default: off

* **air6to4**: (on/off) Enable or disable IPv6 over IVPv4 for automatic boot
  connection. Default: off

* **manifestupdateinterval**: (integer)	Interval between AirVPN manifest updates
  in minutes. Default: 15

* **airwhiteserverlist**: (list) Name of AirVPN servers allowed and available to
  connection. In case this option is defined, only the specified AirVPN servers
  can be connected by the clients. Default: empty

* **airblackserverlist**: (list) Name of AirVPN servers explicitly forbidden and
  denied to connection. In case this option is defined, the specified AirVPN
  servers cannot be connected by the clients in any way. Default: empty

* **airwhitecountrylist**: (list) Define the countries allowed and available to
  connection. In case this option is defined, only AirVPN server located in the
  specified countries can be connected by the clients. Countries must be
  specified with their corresponding ISO code. Default: empty

* **airblackcountrylist**: (list) Define the countries explicitly forbidden and
  denied to connection. In case this option is defined, all the AirVPN server
  located in the specified countries cannot be connected by the clients.
  Countries must be specified with their corresponding ISO code. Default: empty

* **forbidquickhomecountry**: (on/off) Never allow quick connection to connect
  servers in the country where your computer is located at, including those
  specified in the white list (option "airwhiteserverlist"). For security reasons,
  AirVPN strongly advises to set this option on. Default: on

* **allowuservpnprofiles**: (on/off) Allow Bluetit's clients (therefore, users) to
provide custom VPN profiles. Default: off

* **country**: (string) ISO code of the country where your computer is located at
  (home country). In case this option is not used, Bluetit will determine the home
  country by contacting AirVPN's ipleak.net site through a secure connection. The
  home country is needed in order to let AirVPN quick and automatic connection to
  properly choose the best server location, therefore ensuring the best traffic
  performance. Default: empty
  
  **Important note for users connecting from Russian Federation**: Because of the
  large extension of Russian Federation, covering a quite large range of longitudes,
  please be aware that this country has been actually divided into two areas.
  Specifically, use **R1** in case your system is located in the Western area of
  Russian Federation, **R2** in case it is located in the Eastern area. For your
  reference, the division point is exactly located at 103.851959 decimal longitude.
  This is essential for the best server determination in case of AirVPN quick
  (automatic) connection. In fact, users located in the Western area of the
  Russian Federation (R1) are preferably connected to European AirVPN servers,
  whereas those in the Eastern area (R2) are connected to Asian AirVPN servers.
  This ensures the best performance for the whole Russian Federation. In case
  you set this option to **RU**, connection will be established with the best
  European AirVPN server.

* **remote**: (list) Define the OpenVPN servers allowed to connection. Values
  can be both valid IP addresses or URL. This option also affects AirVPN
  connectivity and it will allow connection only to AirVPN servers which IP
  addresses are defined in this option. Default: empty

* **proto**: (list) Define the protocols allowed and available to connection for
  both AirVPN and OpenVPN servers. Possible values are udp and tcp. Default:
  empty

* **port**: (list) Define the port numbers allowed and available to connection
  for both AirVPN and OpenVPN servers. Default: empty

* **tunpersist**: (on/off) Enable or disable tunnel persistence. In case it is
 enabled the tun device is kept active in case of VPN connection loss or pause.
 This usually prevents traffic leaks during reconnection or accidental
 disconnection. Default: on

* **cipher**: (list) Define the cipher algorithms allowed to connection for both
  AirVPN and OpenVPN servers. Default: empty

* **maxconnretries**: (integer) Define the maximum number of retries for any VPN
  connection with the exception of AirVPN quick connection, that is connection to
  AirVPN servers, automatic connection at boot to an AirVPN specific server and to
  a generic OpenVPN profile. Default: 10

* **tcpqueuelimit**: (integer) Define the maximum number of queued TCP output
  packets. In case this value is too small, it is very likely the queue becomes
  frequently full therefore leading to data loss. This is particularly true for
  high speed connections. Default: 8192

* **ncpdisable**: (yes/no) Control whether the Negotiable Crypto Parameters
  (NCP) is enabled or disabled. NCP is essential in order to let the OpenVPN
  client force a specific cipher algorithm in case of OpenVPN servers prior to
  2.5 version. Default: on

* **networklock**: (string) Define the network lock method to be used during the
  connection. This directive is ignored in case networklockpersist is defined.
  Network lock is an exclusive AirVPN feature in order to prevent data leakage in
  case of accidental disconnection by keeping the network traffic on a locked
  state. Network locking is done by a specific set of firewall rules allowing
  traffic to and from the connected server only. Possible values: on (automatic),
  iptables, nftable, pf or off. The "on" value automatically detects the firewall
  system in use. Default: on

* **ignorednspush**: (yes/no) Define whether Bluetit should ignore the DNS
  setting pushed by the server or not. In case DNS push is ignored, connection
  will use current system DNS configuration. Default: no

* **timeout**: (integer) Connection timeout in seconds. Default: 0 (retry
  indefinitely)

* **compress**: (string) Compression mode. Possible values: yes, allow
  compression on both uplink and downlink; asym - allow compression on downlink
  only; no - support compression stubs only. Default: no

* **tlsversionmin**: (string) Minimum TLS version override. Possible values:
  disabled - don't specify a minimum; default - use TLS version defined in
  profile; tls_1_0 - use TLS 1.0 minimum; tls_1_1 - use TLS 1.1 minimum; tls_1_2
  - use TLS 1.2 minimum. Default: default

* **proxyhost**: (string) Proxy IP address or URL. Default: empty

* **proxyport**: (integer) Proxy port number. Default: empty

* **proxyusername**: (string) Proxy user name. Default: empty

* **proxypassword**: (string) Proxy user password. Default: empty

* **proxybasic**: (yes/no) Allow HTTP basic auth for proxy connection. Default: no


#### User policy

Bluetit user policy is provided by means of two separate levels: system user
policy and D-Bus configuration, the latter providing a *fine grained* control
over both groups and users. Bluetit can in fact be accessed only through its
D-Bus interface exposing methods for controlling both AirVPN functionality and
VPN connectivity. Clients and users can therefore use Bluetit only in case a
specific and proper D-Bus policy has been defined for them.

The default policy provided by the default configuration files found in the
standard distribution package (that is the one directly distributed by AirVPN
only) permits Bluetit access only to users belonging to the system group
`airvpn`. This restriction is essentially defined by D-Bus policy and, by
default, it permits access to Bluetit only to users belonging to this group.

By using D-Bus configuration mechanism, the superuser can change this default
policy, including granting or denying access to specific users and groups, under
his or her own responsibility.

System user policy is controlled by the usual system commands (i.e. `useradd`,
`usermod` and `addgroup`) whereas D-Bus configuration files are stored in
`/etc/d-bus1/system.d` and controlled by these specific files:

* `org.airvpn.server.conf`: D-Bus policy for Bluetit daemon (server)

* `org.airvpn.client.conf`: D-Bus policy for all clients

By changing the content of these files, the superuser `root` can change and
define Bluetit access policy according to the specific need of the environment
and system. In case one of these files is changed, as well as when a user or
group is changed or added, please remember to reload D-Bus configuration in
order to make them effective. (systemd: "`systemctl reload dbus.service`" -
SysVStyle-init: "`/etc/init.d/dbus reload`")


### Bluetit default connection values

Bluetit uses specific default values applied to any VPN connection if not
otherwise specified both by the client (i.e. `goldcrest`) or the run control
file `bluetit.rc`.

These default values are always applied regardless of the VPN connection type,
either to any AirVPN server or generic server connected via a custom OpenVPN
configuration file. In this specific latter case, you should consider some of
the directives of the OpenVPN configuration file will be always overridden by
Bluetit default values, saved the case they are explicitly set with the relative
and corresponding Bluetit options through a client (i.e. `goldcrest` command
line options).

The following values are used as default for these specific connection parameters:

* `proto`: UDP
* `port`: 443
* TLS mode: Crypt (only for AirVPN connections)
* `cipher`: Server. This means the choice of the cipher algorithm is determined
  by the connected server configuration.


### Controlling Bluetit daemon

Running and stopping Bluetit daemon depends on the system in use. Although
Bluetit can be run by directly invoking it from a terminal or shell (i.e.
entering `/sbin/bluetit`) it is advised to control it by using either systemd
tools or SysVStyle-init script. **Please note Bluetit daemon requires `root`
privileges to run.**

When Bluetit starts, it creates the "lock" file `bluetit.lock` in `/etc/airvpn`.
This file is actually a PID file and is also used to check whether Bluetit is
running or has ended abnormally. In case you are sure Bluetit is not running,
you can delete this file in order to let Bluetit start again.
  
Bluetit daemon uses the following system signals to control specific actions.
You can send a signal to Bluetit by using the `kill` command from a terminal.

* **SIGTERM**, **SIGINT**, **SIGPIPE** and **SIGHUP**: Stop and terminate
  Bluetit daemon. In case Bluetit is connected to a server, the connection will
  be terminated. No VPN connectivity is therefore available and no client will be
  able to use it.

* **SIGUSR1**: This is actually a toggle signal to be used to both pause and
  resume VPN connection. In case Bluetit is connected to a VPN server, the
  connection is paused, whereas in case it is paused, VPN connection will be
  resumed. When the VPN connection is paused, tunnel device status is controlled
  according to `tunpersist` option. Also consider that pausing and resuming a
  connection is allowed only in case TUN persistence is enabled.

* **SIGUSR2**: Reconnect (restart) the current VPN connection. Reconnecting a
  connection is allowed only in case TUN persistence is enabled.


#### systemd

In a systemd controlled system, Bluetit can be controlled by using the
`systemctl` tool and with the usual options. The systemd unit name controlling
Bluetit is `bluetit.service`. Below are described the relevant commands to be used
for controlling Bluetit daemon.

* `systemctl enable bluetit.service`: Enable Bluetit to run at boot time. This
  makes Bluetit available and running whenever you turn your system on or restart
  it.

* `systemctl disable bluetit.service`: Disable Bluetit run at boot time. In case
  you want to use Bluetit, you need to explicitly start it with the command below.

* `systemctl start bluetit.service`: Start Bluetit daemon for the current session.

* `systemctl stop bluetit.service`: Stop Bluetit daemon. In case Bluetit is
  connected to a server, the connection will be terminated. No VPN connectivity is
  therefore available and no client will be able to use it.

* `systemctl restart bluetit.service`: Restart Bluetit daemon. In case Bluetit
  is connected to a server, the connection will be terminated and will not be
  resumed.

* `systemctl status bluetit.service`: Show the running status of Bluetit daemon.


#### SysVStyle-init

In a SysVStyle-init system, Bluetit can be controlled by using the `bluetit`
script found in `/etc/init.d`. The script is compatible with `chkconfig` and, by
default, the installation script will configure Bluetit daemon to start at run
levels 3 and 5 only. By default, start priority is assigned to `90` whereas stop
priority is `60`. Below are listed the relevant commands to control Bluetit
daemon in a SysVStyle-init system.

* `/etc/init.d/bluetit start`: Start Bluetit daemon for the current session.

* `/etc/init.d/bluetit stop`: Stop Bluetit daemon. In case Bluetit is connected
  to a server, the connection will be terminated. No VPN connectivity is therefore
  available and no client will be able to use it.

* `/etc/init.d/bluetit restart`: Restart Bluetit daemon. In case Bluetit is
  connected to a server, the connection will be terminated and will not be
  resumed.

* `/etc/init.d/bluetit status`: Show the running status of Bluetit daemon.


### Logging

The whole activity of Bluetit daemon is logged to the system log, that is to
`syslog`, including requests from clients, errors and information messages which
may be useful to the superuser `root` to better understand the daemon activity.

All log messages sent to `syslog` are tagged as `bluetit` and can be therefore
filtered according to this key. Not all log messages are sent to the client,
therefore in case of critical errors or to better understand the cause of
errors, superuser `root` should only rely on `syslog` messages instead of client
messages. System relevant messages, as well as internal and sensitive
information are sent to the `syslog` only, whereas to the client are sent user
related messages only, including message errors relevant to user activity.

Bluetit log can be browsed according to the standard tools and logging
facilities in use on each specific system or Linux distribution.


## Automatic connection at boot or startup time

Bluetit can start an automatic connection to any AirVPN server at boot time or
startup time. To use this feature, Bluetit needs to be started either via systemd
or SysVinit-Style scripting facilities, depending on your system.

Automatic connection at boot or startup time can be done in many ways usually
allowed for any standard AirVPN connection mode. To enable automatic connection
at boot or startup time, you need to properly configure bluetit.rc file by using
the directives implemented for this specific purpose.

Bluetit provides for the following automatic connection modes:

**quick**: Start a quick connection by selecting the best performing server at
  the moment of connection and according to your home country
  
**server**: Start a connection to a specific AirVPN server. This mode requires
  the directive airserver to be properly assigned to a valid, existing and available
  AirVPN server
  
**country**: Start a connection to the best performing AirVPN server at the moment
  of connection of a specific AirVPN country. This mode requires  the directive
  aircountry to be properly assigned to a valid and existing AirVPN country

All the above modes requires the directives airusername and airpassword to be
assigned to a valid AirVPN username and relative password. The automatic boot or
startup connection will in fact be started by using these credentials. In this regard,
the connection can also be started by using a specific AirVPN key associated to the
user. The connection key must be specified with the airkey directive. Please note
this directive is case sensitive, the key name therefore must be exactly entered
the way it has been defined in the Client Area of AirVPN website. In case no
airkey is provided, automatic boot connection will use the first key associated to
the user in the Client Area.

The automatic connection at boot or startup time can also be optionally started with
a specific protocol and port. In this case, is is needed to properly use the directives
airproto and airport with the desired values.

When started, the automatic connection can be controlled by using goldcrest client.
For this specific purpose, in fact, the client provides three options to be used to
stop (disconnect), pause or resume the connection. Please note that connection pausing
or resuming work only in case the connection has been started with tun persistence mode
enabled.

Please note that in case you want to control Bluetit automatic connection with the
below commands, no other Bluetit client must be running.


#### Stop the automatic connection

>`$ goldcrest --disconnect`


#### Pausing the automatic connection

>`$ goldcrest --pause`


#### Resuming the automatic connection

>`$ goldcrest --resume`


## Goldcrest client

Goldcrest is the command line Bluetit client allowing full functionality and
control both for any OpenVPN and AirVPN server. In order to use Goldcrest, the
user must belong to the group `airvpn` which is optionally created by the
installation script. For your convenience, the installation script can 
create the `airvpn` user specifically meant to be used for this
purpose. The system administrator can of course add any other user to the group
`airvpn` in order to grant other users the use of Bluetit daemon trough the
Goldcrest client. In this regard, please refer to the Bluetit configuration
sections above.

Goldcrest can be controlled by invoking it with command line options as well as
configuring it by properly editing its configuration file as described below.
Below are listed all the command line options supported by Goldcrest:

>`Goldcrest - AirVPN Bluetit Client 1.3.0 - 1 June 2023`  
>__
>`usage: ./goldcrest <options> [config-file]`  
>__
>`--help, -h                    : Show the help page`  
>`--version, -v                 : Show version info`  
>`--bluetit-status              : Show Bluetit status and exit`  
>`--bluetit-stats               : Show Bluetit connection statistics and exit`  
>`--air-connect, -O             : Start AirVPN connection`  
>`--air-server, -S              : AirVPN server name <pattern>`  
>`--air-country, -Z             : AirVPN country or continent <pattern|country ISO code|continent code>`  
>`--air-vpn-type, -f            : VPN type for AirVPN connection <wireguard|openvpn>`  
>`--air-tls-mode, -T            : TLS mode for AirVPN connection <auto|auth|crypt>`  
>`--air-ipv6, -V                : IPv6 mode for AirVPN connection <on|off>`  
>`--air-6to4, -B                : Enable IPv6 over IPv4 for AirVPN connection <on|off>`  
>`--air-user, -U                : AirVPN user name`  
>`--air-password, -P            : AirVPN user password`  
>`--air-key, -F                 : AirVPN user key <name>`  
>`--air-key-list, -K            : AirVPN user key list`  
>`--air-key-load, -Q            : Load (use) AirVPN user key from local file <file_name>`  
>`--air-save, -W                : Save an AirVPN server, country profile or user key to local file <file_name>`  
>`--air-info, -I                : AirVPN info (requires --air-server or --air-country)`  
>`--air-list, -L                : AirVPN server or country list <pattern>`  
>`--air-white-server-list, -G   : AirVPN white server list <list>`  
>`--air-black-server-list, -M   : AirVPN black server list <list>`  
>`--air-white-country-list, -J  : AirVPN white country list <list>`  
>`--air-black-country-list, -X  : AirVPN black country list <list>`  
>`--response, -r                : Static response`  
>`--dc, -D                      : Dynamic challenge/response cookie`  
>`--cipher, -C                  : Encrypt packets with specific cipher algorithm <alg>`  
>`--list-data-ciphers           : List supported data ciphers`  
>`--server, -s                  : Server override`  
>`--proto, -p                   : Protocol override <udp|tcp>`  
>`--port, -R                    : Port override`  
>`--tcp-queue-limit, -l         : Size of TCP packet queue <1-65535> default 8192`  
>`--allowuaf, -6                : Allow unused address families <yes|no|default>`  
>`--ncp-disable, -n             : Disable negotiable crypto parameters`  
>`--network-lock, -N            : Network filter and lock mode <on|iptables|nftables|pf|off> default on`  
>`--ignore-dns-push, -i         : Ignore DNS push request and use system DNS settings`  
>`--timeout, -t                 : Connection timeout <seconds>`  
>`--compress, -c                : Compression mode <yes|no|asym>`  
>`--pk-password, -z             : Private key password`  
>`--tvm-override, -m            : tls-version-min override <disabled|default|tls_1_x>`  
>`--tcprof-override, -o         : tls-cert-profile override <legacy|preferred|...>`  
>`--proxy-host, -y              : HTTP proxy hostname/IP`  
>`--proxy-port, -q              : HTTP proxy port`  
>`--proxy-username, -u          : HTTP proxy username`  
>`--proxy-password, -w          : HTTP proxy password`  
>`--proxy-basic, -b             : Allow HTTP basic auth`  
>`--alt-proxy, -A               : Enable alternative proxy module`  
>`--cache-password, -H          : Cache password`  
>`--no-cert, -x                 : Disable client certificate`  
>`--def-keydir, -k              : Default key direction <bi|0|1>`  
>`--ssl-debug                   : SSL debug level`  
>`--auto-sess, -a               : Request autologin session`  
>`--auth-retry, -Y              : Retry connection on auth failure`  
>`--persist-tun, -j             : Keep TUN interface open across reconnections`  
>`--peer-info, -e               : Peer information key/value list in the form K1=V1,K2=V2,...`  
>`--gremlin, -g                 : Gremlin information <send_delay_ms|recv_delay_ms|send_drop_prob|recv_drop_prob>`  
>`--epki-ca                     : Simulate external PKI certificate supporting intermediate/root certificates`  
>`--epki-cert                   : Simulate external PKI certificate`  
>`--epki-key                    : Simulate external PKI private key`  
>`--gui-version, -E             : Set custom gui version <text>`  
>`--recover-network             : Recover network settings after a crash or unexpected exit`  
>`--disconnect                  : Disconnect the VPN session, including Bluetit boot connection`  
>`--pause                       : Pause the VPN connection, including Bluetit boot connection`  
>`--resume                      : Resume a paused VPN connection, including Bluetit boot connection`  
>`--reconnect                   : Reconnect the VPN connection, including Bluetit boot connection`  
>__
>`Open Source Project by AirVPN (https://airvpn.org)`  
>__
>`Linux and macOS design, development and coding by ProMIND`  
>__
>`Special thanks to the AirVPN community for the valuable help,`  
>`support, suggestions and testing.`  


All the above options beginning with `--air` are specifically meant to be used
with AirVPN server and infrastructure, having no effect when connecting to a
generic OpenVPN server by using a config file. This is also true when Goldcrest
is used for connecting to an AirVPN server with a config file either created and
saved with Goldcrest or the Config Generator available in AirVPN web site.

The remaining options can affect connectivity of both AirVPN and generic OpenVPN
servers, therefore allowing the user to override both the connection directives
of the optional config file or the default parameters of both Goldcrest and
Bluetit. In this regard, Bluetit configuration and options cannot be overridden
in any way by Goldcrest and, in this sense, Bluetit configuration sets the
connectivity limit for all clients and what can be done when connecting to any
VPN server, either being an OpenVPN or AirVPN one.


### Goldcrest configuration

The configuration of Goldcrest is meant to set a specific group of default
options to be used at each connection, both to a generic OpenVPN and AirVPN
server. The values specified in the configuration file can be overridden by
command line options and, in this sense, they always take precedence over the
configuration file.

The configuration file location changes according to each specific system and,
in particular, whether the home directory complies to freedesktop.org's XDG
standard layout (X Desktop Group) and whether `.config`directory is available 
in the user's home directory.

In case `.config` directory is available, Goldcrest configuration file is stored
in `~/.config/goldcrest.rc` otherwise it is always found in `~/.goldcrest.rc`.
In the latter case, please note the `.` (dot) at the beginning of the file
name, actually making it a "hidden file".

When Goldcerst is run for the first time, it will create a template
configuration file in case it is not found in the user's home directory. The
options available in the configuration file have the same name of command line
options. In this regard, you should note the double dash `--` must not be used
in the configuration file and not all the command line options can be used in
the configuration file.

The configuration file provides for the following options and types. The options
defined as "list" can be used multiple times within the configuration file and
each entry can have as a value both a string (with no quotes) or a list of
strings in the form of `value1,value2,value3,...`.

* **air-server**: (string) Default AirVPN server to be used in any AirVPN
  connection. Default: empty

* **air-tls-mode**: (auto/auth/crypt) Default tls mode for AirVPN connection.
  Default: empty

* **air-ipv6**: (on/off) Enable or disable IPv6 for AirVPN connection.
  Default: off

* **air-6to4**: (on/off) Enable or disable IPv6 over IVPv4 for AirVPN connection.
  Default: off

* **air-user**: (string) Default AirVPN username. Default: empty

* **air-password**: (string) Default AirVPN username password. **Please note
  password is written in this file as plain, therefore it is visible
  to anyone editing the configuration file. For security reason, the user
  is advised to not store the user password in the configuration file and to
  enter it at each use. For more information on AirVPN user access, please
  see below.** Default: empty

* **air-key**: (string) Default AirVPN user key to be used for all AirVPN
  operations and as defined in the "client area" in AirVPN web site.
  Default: empty
  
* **cipher**: (string) Default cipher algorithm name for all VPN connections.
  Default: empty

* **proto**: (udp/tcp) Default protocol for all VPN connections. Default: empty

* **server**: (string) Default server IP address or URL to be used for generic
  OpenVPN connection. Default: empty

* **port**: (number) Default port number for all VPN connections. Default: empty

* **tcp-queue-limit**: (integer) Define the maximum number of queued TCP output
  packets. In case this value is too small, it is very likely the queue becomes
  frequently full therefore leading to data loss. This is particularly true for
  high speed connections. Default: 8192

* **ncp-disable**: (yes/no) Control whether the Negotiable Crypto Parameters
  (NCP) is enabled or disabled by default. NCP is essential in order to let the
  OpenVPN client force a specific cipher algorithm in case of OpenVPN servers
  prior to 2.5 version. Default: on

* **network-lock**: (string) Define the network lock method to be used during the
  connection. Network lock is an exclusive AirVPN feature in order to prevent
  data leakage in case of accidental disconnection by keeping the network
  traffic on a locked state. Network locking is done by a specific set of
  firewall rules allowing traffic to and from the connected server only.
  Possible values are: on (automatic), iptables, nftable, pf or off. The "on"
  value automatically detects the firewall system in use. Default: on

* **ignore-dns-push**: (yes/no) Define whether the connection process should ignore
  the DNS setting pushed by the server of not. In case DNS push is ignored, connection
  will use current system DNS configuration. Default: no

* **allowuaf**: (yes/no/default) Allow unused address families. Default: default

* **timeout**: (integer) Connection timeout in seconds. Default: 0 (retry
  indefinitely)

* **compress**: (string) Compression mode. Possible values: yes, allow compression
  on both uplink and downlink; asym - allow compression on downlink only; no - support
  compression stubs only. Default: no

* **proxy-host**: (string) Proxy IP address or URL. Default: empty

* **proxy-port**: (integer) Proxy port number. Default: empty

* **proxy-username**: (string) Proxy user name. Default: empty

* **proxy-password**: (string) Proxy user password. Default: empty

* **proxy-basic**: (yes/no) Allow HTTP basic auth for proxy connection. Default: no

* **alt-proxy**: (yes/no) enable alternative proxy module. Default: no

* **persist-tun**: (on/off) Enable or disable tunnel persistence. In case it is
 enabled the tun device is kept active in case of VPN connection loss or pause.
 This usually prevents traffic leaks during reconnection or accidental
 disconnection. Default: on

* **conn-stat-interval**: (integer) Interval time in seconds for connection
  statistics logging. When set to 0, connection statistics logging is disabled.
  Default: 60 seconds


### Note on AirVPN username and password

When accessing AirVPN user functions or connecting to a server, Goldcrest
requires a valid AirVPN username and password in order to properly login to
AirVPN infrastructure. In particular, whenever Goldcrest is used with the
options `--air-connect`, `--air-key`, `--air-key-list`, `--air-save` and
`--air-key-load`, it is mandatory to provide AirVPN credentials, that is
a valid username and password.

There are many ways to provide AirVPN credentials to Goldcrest and specifically:

* Configuration file (options `air-user` and `air-password`)

* Command line options (`--air-user` and `--air-password`)

* Keyboard input


Please note command line options have the highest priority, therefore, in case
credentials are stored in the configuration file and Goldcrest is run with command
line options `--air-user` or `--air-password`, this will explicitly override
the value defined in the configuration file.

In case credentials, or one of the two parameters, that is username or password,
is not provided either in the configuration file or command line options, Goldcrest
will ask the user to enter them with the keyboard. In this specific case, username
will be echoed to the output while it is being typed, whereas the password will not
be echoed on the terminal for obvious security reasons.

In case AirVPN credentials are stored in the configuration file, please be aware of
the fact they are kept as plain characters, therefore perfectly readable to anyone
having access to the configuration file. For this reason, in order to ensure the best
safeguarding of AirVPN credentials, it is advised to always enter them from the
keyboard when Goldcrest asks the user to do so.


### Controlling Goldcrest client

Goldcrest uses the following system signals to control specific actions.
You can send a signal to Goldcrest by using the `kill` command from a terminal.

* **SIGTERM**, **SIGINT**, **SIGPIPE**, **SIGHUP**: Disconnect the active VPN connection

* **SIGUSR1**: This is actually a toggle signal to be used to both pause and
  resume VPN connection. In case Goldcrest is connected to a VPN server, the
  connection is paused, whereas in case it is paused, VPN connection will be
  resumed. When the VPN connection is paused, tunnel device status is controlled
  according to `--persist-tun` option. Also consider that pausing and resuming a
  connection is allowed only in case TUN persistence is enabled.

* **SIGUSR2**: Reconnect (restart) the current VPN connection. Reconnecting a
  connection is allowed only in case TUN persistence is enabled.


### Using Goldcrest with AirVPN Servers

Goldcrest provides full functionality with AirVPN infrastructure to registered and
active users. The client allows the user to both start and stop a connection to
any AirVPN server as well as fetching information about the user, user key, servers
and countries.

The user can therefore arbitrarily start a VPN connection to a specific AirVPN server
and with specific connection parameters - such as protocol, cipher algorithm and port -
as well as starting a quick (automatic) connection by letting Bluetit to choose the
best server in order to ensure the best traffic performance.

Below are provided some examples on how using Goldcrest with AirVPN. Some of them
require the user to provide valid AirVPN credentials by using the methods described
above.


#### List AirVPN servers

List all AirVPN servers which name, country or location contain the pattern "co"

>`$ goldcrest --air-list --air-server co`

List all AirVPN servers located in Germany

>`$ goldcrest --air-list --air-server germany`

or

>`$ goldcrest --air-list --air-server de`

List all AirVPN servers

>`$ goldcrest --air-list --air-server all`


#### List AirVPN countries

List all AirVPN countries which name or ISO code contain the pattern "se"

>`$ goldcrest --air-list --air-country SE`

List a specific AirVPN country

>`$ goldcrest --air-list --air-country Sweden`

List a specific all AirVPN countries

>`$ goldcrest --air-list --air-country all`


#### Request information about a specific AirVPN server

>`$ goldcrest --air-info --air-server diadema`


#### Request information about a specific AirVPN country

>`$ goldcrest --air-info --air-country japan`

or

>`$ goldcrest --air-info --air-country nl`


#### Request information about a specific AirVPN continent

>`$ goldcrest --air-info --air-country europe`

or

>`$ goldcrest --air-info --air-country eur`


Continent codes and names are defined as follow:

>`Code  Name`  
>`----  ----------------`  
>`AFR   Africa`  
>`AME   America`  
>`ASI   Asia`  
>`EARTH Earth`  
>`EUR   Europe`  
>`NAM   North America`  
>`OCE   Oceania`  
>`SAM   South America`  

**Please note** the statistics returned by `--air-info` and `--air-list`
options, when used with `--air-country`, include the available and active
AirVPN servers only. This may differ from what it is shown in the server
page of AirVPN website, where all the servers making the whole network are
considered, no matter of their operational status.


#### List user keys

>`$ goldcrest --air-key-list`


#### Save user key to a local file

>`$ goldcrest --air-save mykey.asc --air-key my_key`


#### Generate and save an OpenVPN config file for an AirVPN server

>`$ goldcrest --air-save comae.ovpn --air-server comae`

Generate an OpenVPN config file for a specific user key

>`$ goldcrest --air-save comae.ovpn --air-server comae --air-key my_key`


#### Generate and save an OpenVPN config file for an AirVPN country

>`$ goldcrest --air-save usa.ovpn --air-country US`

>`$ goldcrest --air-save spain.ovpn --air-country spain`

Generate an OpenVPN config file for a specific user key

>`$ goldcrest --air-save belgium.ovpn --air-country belgium --air-key my_key`


#### Start a quick (automatic) connection to AirVPN

>`$ goldcrest --air-connect`

Start a quick connection with a specific key

>`$ goldcrest --air-connect --air-key my_key`

Start a quick connection with a specific protocol and port

>`$ goldcrest --air-connect --proto tcp --port 80`

Start a quick connection with a specific cipher algorithm

>`$ goldcrest --air-connect --cipher CHACHA20-POLY1305 --ncp-disable`


#### Start a connection to a specific AirVPN server

>`$ goldcrest --air-connect --air-server orion`

Start a connection with a specific key

>`$ goldcrest --air-connect --air-server mebsuta --air-key my_key`

Start a connection with a specific protocol and port

>`$ goldcrest --air-connect --air-server luhman --proto udp --port 443`

Start a connection with a specific cipher algorithm

>`$ goldcrest --air-connect --air-server comae --cipher AES-128-GCM --ncp-disable`


#### Start a connection to a specific AirVPN country

>`$ goldcrest --air-connect --air-country canada`

Start a connection with a specific key

>`$ goldcrest --air-connect --air-country ukraine --air-key my_key`

Start a connection with a specific protocol and port

>`$ goldcrest --air-connect --air-country ch --proto udp --port 443`

Start a connection with a specific cipher algorithm

>`$ goldcrest --air-connect --air-country spain --cipher AES-256-GCM --ncp-disable`


#### Stop a connection

Type `CTRL+C` in the terminal window where Goldcrest is running. The client
will ask Bluetit daemon to initiate the disconnection process and to restore
your original network settings according to your options.


### Using Goldcrest with Generic OpenVPN servers

Goldcrest can also be used to connect to any generic OpenVPN server by
providing a valid OpenVPN config file or profile. When connecting to a
generic OpenVPN server, that is by providing an OpenVPN config file,
all the AirVPN options `--air-*` will be ignored and have no effect.

To connect a generic OpenVPN server, you need to provide the config
file name at the end of command line options. For example

>`goldcrest server.ovpn`

will start a connection to the OpenVPN server defined in the `server.ovpn`
config file. This mode can also be used to connect an AirVPN server
defined in a config file generated and saved either with Goldcrest (i.e.
`goldcrest --air-save airvpn-orion-server.ovpn --air-server orion`) or
created with the config generator available in the client area of AirVPN
web site.

User can override the values defined in the config file by using command
line options. For example to override cipher, port and protocol:

>`goldcrest --cipher CHACHA20-POLY1305 --port 443 --proto tcp server.ovpn`

When used in this mode, Goldcrest becomes equivalent to Hummingbird in
functionality, however ensuring a better security model offered by Bluetit
daemon. You should in fact consider while Goldcrest can be used by any
user of the system and authorized by root and bluetit policy, Hummingbird
needs root privilege to run.

For this reason, it is always advised and recommended to use Goldcrest and
Bluetit on any computer or system whenever possible. In this regard, Hummingbird
is meant to be a standalone client and mainly used for specific and special
purposes, such as in portable systems and conditions.


# Running the Hummingbird Client

Hummingbird is the standalone AirVPN client allowing the user to connect to
any generic OpenVPN server by providing a valid OpenVPN config file or profile,
including AirVPN servers.

However, Hummingbird does not support any direct integration with AirVPN
infrastructure, saved the case of config files previously generated. In case
you need full functionality and integration with AirVPN, you need to properly
install and use Bluetit daemon and Goldcrest client.

To use Hummingbird, you need `root` privileges. Run `hummingbird` and display
its help in order to become familiar with its options. From your terminal window
issue this command:

>`sudo ./hummingbird --help`

After having entered your root account password, `hummingbird` responds with:

>`Hummingbird - OpenVPN3 Client 1.3.0 - 1 June 2023`  
>__
>`OpenVPN core 3.8.4 AirVPN linux x86_64 64-bit`  
>`Copyright (C) 2012-2022 OpenVPN Inc. All rights reserved.`  
>`OpenSSL 3.0.8 7 Feb 2023`  
>__
>`usage: hummingbird [options] <config-file>`  
>__
>`--help, -h            : show this help page`  
>`--version, -v         : show version info`  
>`--eval, -e            : evaluate profile only (standalone)`  
>`--username, -u        : username`  
>`--password, -p        : password`  
>`--response, -r        : static response`  
>`--dc, -D              : dynamic challenge/response cookie`  
>`--cipher, -C          : encrypt packets with specific cipher algorithm (alg)`  
>`--list-data-ciphers   : list supported data ciphers`  
>`--proto, -P           : protocol override (udp|tcp)`  
>`--server, -s          : server override`  
>`--port, -R            : port override`  
>`--tcp-queue-limit, -l : size of TCP packet queue (1-65535, default 8192)`  
>`--ncp-disable, -n     : disable negotiable crypto parameters`  
>`--network-lock, -N    : network filter and lock mode (on|iptables|nftables|pf|off, default on)`  
>`--gui-version, -E     : set custom gui version (text)`  
>`--ignore-dns-push, -i : ignore DNS push request and use system DNS settings`  
>`--allowuaf, -6        : allow unused address families (yes|no|default)`  
>`--timeout, -t         : timeout`  
>`--compress, -c        : compression mode (yes|no|asym)`  
>`--pk-password, -z     : private key password`  
>`--tvm-override, -M    : tls-version-min override (disabled, default, tls_1_x)`  
>`--tcprof-override, -X : tls-cert-profile override (legacy, preferred, etc.)`  
>`--proxy-host, -y      : HTTP proxy hostname/IP`  
>`--proxy-port, -q      : HTTP proxy port`  
>`--proxy-username, -U  : HTTP proxy username`  
>`--proxy-password, -W  : HTTP proxy password`  
>`--proxy-basic, -b     : allow HTTP basic auth`  
>`--alt-proxy, -A       : enable alternative proxy module`  
>`--cache-password, -H  : cache password`  
>`--no-cert, -x         : disable client certificate`  
>`--def-keydir, -k      : default key direction ('bi', '0', or '1')`  
>`--ssl-debug           : SSL debug level`  
>`--auto-sess, -a       : request autologin session`  
>`--auth-retry, -Y      : retry connection on auth failure`  
>`--persist-tun, -j     : keep TUN interface open across reconnects`  
>`--peer-info, -I       : peer info key/value list in the form K1=V1,K2=V2,...`  
>`--gremlin, -G         : gremlin info (send_delay_ms, recv_delay_ms, send_drop_prob, recv_drop_prob)`  
>`--epki-ca             : simulate external PKI cert supporting intermediate/root certs`  
>`--epki-cert           : simulate external PKI cert`  
>`--epki-key            : simulate external PKI private key`  
>`--recover-network     : recover network settings after a crash or unexpected exit`  
>__
>`Open Source Project by AirVPN (https://airvpn.org)`  
>__
>`Linux and macOS design, development and coding by ProMIND`  
>__
>`Special thanks to the AirVPN community for the valuable help,`  
>`support, suggestions and testing.`  


Hummingbird needs a valid OpenVPN profile in order to connect to a server. You
can create an OpenVPN profile by using the config generator available at AirVPN
website in your account's [Client Area](https://airvpn.org/generator/)


#### Start a connection

>`sudo ./hummingbird your_openvpn_file.ovpn`


#### Stop a connection

Type `CTRL+C` in the terminal window where hummingbird is running. The client
will initiate the disconnection process and will restore your original network
settings according to your options.


#### Start a connection with a specific cipher

>`sudo ./hummingbird --ncp-disable --cipher CHACHA20-POLY1305 your_openvpn_file.ovpn`

**Please note**: in order to properly work, the server you are connecting to
must support the cipher specified with the `--cipher` option.


#### Disable the network filter and lock

>`sudo ./hummingbird --network-lock off your_openvpn_file.ovpn`


#### Ignore the DNS servers pushed by the VPN server

>`sudo ./hummingbird --ignore-dns-push your_openvpn_file.ovpn`


**Please note**: the above options can be combined together according to their
use and function.


#### Controlling Hummingbird

Hummingbird uses the following system signals to control specific actions.
You can send a signal to Hummingbird by using the `kill` command from a terminal.

* **SIGTERM**, **SIGINT**, **SIGPIPE**, **SIGHUP** : Disconnect the active VPN
  connection

* **SIGUSR1**: This is actually a toggle signal to be used to both pause and
  resume VPN connection. In case Goldcrest is connected to a VPN server, the
  connection is paused, whereas in case it is paused, VPN connection will be
  resumed. When the VPN connection is paused, tunnel device status is controlled
  according to `--persist-tun` option. Also consider that pausing and resuming a
  connection is allowed only in case TUN persistence is enabled.

* **SIGUSR2**: Reconnect (restart) the current VPN connection. Reconnecting a
  connection is allowed only in case TUN persistence is enabled.


#### Note on macOS and UDP

Because of some architectural specifications and implementation in macOS, in
particular in case of high speed traffic - a condition becoming more and more
common in modern internet technology and infrastructure - Hummingbird may
warn the user about shortage of buffer space, specifically when connected with
the UDP protocol.

This condition is signaled by Hummingbird with the below messages in the log:

>`UDP send exception: send: No buffer space available`  
>`ERROR: NETWORK_SEND_ERROR`  

This error is caused by the maximum size set in macOS for network sockets, a value
usually small and unsuited for modern high speed networks, specifically in case the
VPN is connected with the UDP protocol. The solution to this problem consists in
increasing the maximum allowed size for socket buffers and, in case the problem
persists, to increase the number of mbuf clusters.

In this regard, it should be considered each mbuf cluster in macOS has a fixed size
of 2Kb and the maximum socket buffer size cannot be greater than 1/16 of the total
memory allocated by the system for mbuf clusters.

For example, in case the number of mbuf clusters is set to 32768 (this is likely
to be the default value in most macOS systems), the total memory allocated for
this purpose is 32768 * 2Kb, therefore 64Mb. The maximum socket buffer size is
hence set to 1/16 of 64Mb, therefore 4Mb.

To find out the current values set in your system, you can use the following
commands. **Please note**, you need to be `root` in order to manipulate these
settings according to the commands provided below.

>`sysctl -a kern.ipc.nmbclusters`  

and

>`sysctl -a kern.ipc.maxsockbuf`  

in order to respectively get the current values set for the number of mbuf
clusters and the maximum socket buffer size.

In case the current number of mbuf clusters set in your systems allows to do so,
you can change the maximum socket buffer size by issuing the following command:

>`sysctl -w kern.ipc.maxsockbuf=<size_in_bytes>`  

For example, to set the maximum socket buffer size to 16Mb, this can be done
with the below command:

>`sysctl -w kern.ipc.maxsockbuf=16777216`  

The above value is accepted by the system only in case the number of mbuf
clusters is equal to or greater than 131072 (131072 * 2Kb = 268435456 = 256Mb)

**Important note:** by using `sysctl` command as described above, the value is
set for the current session only and it is not permanent. This means when the
machine is restarted or powered on, the above values will be reset to their
respectively default values and according to the macOS version in use. To make
these changes permanent and properly set up at system start-up, please refer to
the procedures provided for the specific macOS version in use.

In case the current `kern.ipc.nmbclusters` value is not enough to properly
increase `kern.ipc.maxsockbuf`, you can permanently change the value of `ncl`
boot argument by issuing this command:

>`nvram boot-args="ncl=<number_of_mbuf_clusters>"`  

Please remember each mbuf cluster equates to 2Kb of RAM, therefore in case you
want to set the size of memory used for mbuf clusters to 256Mb, therefore allowing
a maximum value for `kern.ipc.maxsockbuf` of 16Mb, you can multiply this value
(in bytes) by 16 then divided by 2048 (2Kb in bytes), therefore yielding the
number of mbuf clusters.

16777216 * 16 = 268435456 (256Mb)

268435456 / 2048 = 131072

to make this value permanent you the need to issue the command:

>`nvram boot-args="ncl=131072"`  

then restart your system:

>`shutdown -r now`  

Upon reboot you are now allowed to set `kern.ipc.maxsockbuf` to 16Mb.


## Network Filter and Lock

AirVPN Suite network filter and lock natively uses `iptables`, `iptables-legacy`,
`nftables` and `pf` in order to provide a "best effort leak prevention". Bluetit
and Hummingbird will automatically detect and use the infrastructure available on
your system.

You can also override this default behavior by manually selecting your preferred
firewall by using `--network-lock` option (`networklock` in Bluetit configuration
file) which defaults to `on` and, in this specific case, it will be automatically
detected and use the firewall installed on your system by using this specific
priority: `iptables-legacy`, `iptables`, `nftables` and finally `pf`.

In case you want to force the use of a specific firewall, you can do that by
specifying its name in the `--network-lock` option (`networklock` in Bluetit
configuration file). For example, in case you want to force nftables, you can specify
`--network-lock nftables`. Please note the firewall must be properly installed on your
system.

Also note in case both `iptables` and `iptables-legacy` are installed on your system,
`iptables-legacy` will be used.

**Note on nftables**: Nftables rules created and issued by AirVPN Suite follow the
specification and behavior of nftables version 0.9. In case you detect nftables
errors or it seems to not be working properly, please check nftables installed
on your system and make sure it is compatible with 0.9 specifications.

**Please note**: Linux services `firewalld` and `ufw` may interfere with the
AirVPN Suite's network filter and lock and you are strongly advised to not issue
any firewall related command while the VPN connection is active.


### Using Network Filter and Lock with Bluetit

The Network Filter and Lock in Bluetit can strongly affect the whole network traffic
of yuor system, in particular in case it is going to be enabled is persistent mode.
When turned on at boot time with the `networklockpersist` directive in Bluetit
configuration file, traffic is allowed to and from the external network (i.e. any
non-local subnet) only in case Bluetit is connected to a VPN. Local network traffic is
however allowed despite of Network Filter and Lock status or whether the system is
connected to a VPN or not. You should therefore consider activating this directive
carefully in case Bluetit is started at boot time. In this specific case, you should
be aware no traffic will be permitted soon after Bluetit startup and this will affect
all services and processes subsequently started and requiring external network access.
Also consider `networklockpersist` directive takes priority over `networklock` directive
which will be therefore ignored.

You should also consider that enabling the Network Filter and Lock at boot time will
forbid any external network traffic and this also includes Bluetit in case it is not
connected to a VPN. This restriction also applies to AirVPN Manifest update and the
initial query to AirVPN's ipleak.net in order to determine the system real country.

In these specific cases, be aware AirVPN Manifest update is suspended until a new
connection to an AirVPN server is started, whereas, as for system country detection,
you need to explicitly set it with the `country` directive in Bluetit run control file.
In case no country is defined in Bluetit run control file and Network Filer and Lock is
enabled at boot, AirVPN quick connection will consider the whole AirVPN server
infrastructure and will connect to the "best worldwide server" at that specific time.
This means quick connection will not consider the specific area in which your system is
located and will connect to a server which could be too far away or not the best for
yuor local and specific geographic condition.

In case Network Filter and Lock is started in persistent mode, that is enabled at
Bluetit startup, be aware it will stay enabled as long as Bluetit is running and it is
not provided any direct procedure or command to disable it or turn it off. However, the
system administrator `root` can of course change the firewall rules set by Bluetit
- therefore directly affecting Network Filter and Lock - by using the specific system
tools to operate the firewall. In this specific case you should be aware Bluetit will
enable Network Filter and Lock after the VPN is disconnected and will override any
firewall rule or specification set by `root` meanwhile.


## DNS Management in Linux

AirVPN Suite currently supports both `resolv.conf` and `systemd-resolved`
service. It is also aware of Network Manager, in case it is running. While any
of the AirVPN Suite components are running, you are strongly advised to not issue
any resolved related command (such as `resolvectl`) or change the `resolv.conf`
file in order to make sure the system properly uses DNS pushed by the VPN server.
**Please note**: DNS system settings are not changed in case the client has been
started with `--ignore-dns-push` (`ignorednspush` in Bluetit configuration file).
In this specific case, the connection will use your system DNS.

Furthermore, please note that if your network interfaces are managed by Network
Manager, DNS settings might be changed under peculiar circumstances during a VPN
connection, even when DNS push had been previously accepted.


## Recover Your Network Settings

In case Bluetit or Hummingbird crash or are killed by the user (i.e. ``kill -9 `pidof hummingbird` ``)
as well as in case of system reboot while the connection is active, the system
may keep and use some or all of the netwrok settings determined by the previous
session, therefore your network connection might not work as expected, every
connection might be refused and the system might seem to be "network locked".
To restore and recover your system network, you can use the client (Goldcrest or
Hummingbird) with the `--recover-network` option.

>`goldcrest --recover-network`

or

>`sudo hummingbird --recover-network`

Please note in case of crash or unexpected exit, when you subsequently run
goldcrest or hummingbird you will be warned about the unexpected exit and will
require you to run it again with the `--recover-network` option. They will also
refuse to start any connection until the network has been properly restored and
recovered.

### Special Note for Bluetit

In case Bluetit has ended abnormally, it can happen DNS and firewall rules are
set to reflect the last connected VPN server and network connectivity will not
be available. In other words, no traffic is allowed from and to the system. In
such a specific case, you need to do the following in order to recover your
system network connectivity.

* Log in as `root`

* In case the file `bluetit.lock` is present in `/etc/airvpn`, check whether
  Bluetit is running (i.e. `ps -aux | grep bluetit`)

* In case Bluetit is not running, delete the lock file (i.e. `rm /etc/airvpn/bluetit.lock`)
  and start Bluetit

* Use a Bluetit client, such as `goldcrest` and send it the `recover-network`
  option (i.e. `goldcrest --recover-network`)

* Bluetit will try to recover network connectivity by restoring the system
  configuration



# Building AirVPN Suite from Sources

In order to build Bluetit, Goldcrest and Hummingbird from sources, you need the
following dependencies:

* [OpenVPN 3 AirVPN fork](https://github.com/AirVPN/openvpn3-airvpn) (at least version 3.8.4)
* [asio](https://github.com/chriskohlhoff/asio)
* [OpenSSL](https://www.openssl.org/source/) or [mbedTLS](https://tls.mbed.org/download)
* [LZ4 library](https://github.com/lz4/lz4)
* [LZMA library](https://www.7-zip.org/sdk.html)
* GCC development suite

Clone `AirVPN-Suite` repository into your computer:

>`git clone https://gitlab.com/AirVPN/AirVPN-Suite`

Move into the project's directory:

>`cd AirVPN-Suite`


## Build Linux Dynamic Binaries

Edit `build-bluetitd.sh`, `build-goldcerest.sh` and `build-hummingbird.sh`
scripts and set `INC_DIR`, `OPENVPN3` and `ASIO` variables according to your
system configuration

Run the build shell script:

* Bluetit: `sh build-bluetit.sh`

* Goldcrest: `sh build-goldcrest.sh`

* Hummingbird: `sh build-hummingbird.sh`

The scripts will compile their corresponding binary component in the current directory.


## Build Linux - ARM Static Binary

Edit `build-bluetit-static.sh`, `build-goldcrest-static.sh` and `build-hummingbird-static.sh`
scripts and set `INC_DIR`, `OPENVPN3` and `ASIO` variables according to your system
configuration. Also set `STATIC_LIB_DIR` and `SSL_LIB_DIR` according to your system architecture.

Run the build shell script:

* Bluetit: `sh build-bluetit-static.sh`

* Goldcrest: `sh build-goldcrest-static.sh`

* Hummingbird: `sh build-hummingbird-static.sh`

The script will create their corresponding static binary component according to your
system and will also create the associated distribution compressed tarball file
in the current directory. To install the binary in your system, please refer to
the installation instructions provided above.


***

AirVPN Suite and its components Bluetit, Goldcerst and Hummingbird is an open source
project by [AirVPN](https://airvpn.org)

Linux and macOS design, development and coding by ProMIND

Special thanks to the AirVPN community for the valuable help, support,
suggestions and testing.

OpenVPN is Copyright (C) 2012-2022 OpenVPN Inc. All rights reserved.

AirVPN Suite and its components are released and licensed under the
[GNU General Public License Version 3 (GPLv3)](https://gitlab.com/AirVPN/AirVPN-Suite/blob/master/LICENSE.md)

