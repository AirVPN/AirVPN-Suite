#!/bin/sh

#
# Build dynamic binary for goldcrest
#
# Version 1.1 - ProMIND
#

if [ `uname` = "Darwin" ]; then
    echo "Goldcrest for macOS is not currently available."

    exit 1
fi

BASE_NAME=goldcrest

DBUS_INCLUDE=$(pkg-config --cflags --libs dbus-1)
DBUS_LIBS=$(pkg-config --cflags --libs dbus-1)

SOURCES="src/goldcrest.cpp \
         src/dbusconnector.cpp \
         src/airvpntools.cpp \
         src/rcparser.cpp
        "

BIN_FILE=${BASE_NAME}

COMPILE="g++ -fwhole-program -Ofast -Wall -Wno-sign-compare -Wno-unused-parameter -std=c++17 -flto=4 -Wl,--no-as-needed -Wunused-local-typedefs -Wunused-variable -Wno-shift-count-overflow ${DBUS_INCLUDE} -I/usr/include/libxml2 ${SOURCES} ${DBUS_LIBS} -lpthread -o ${BIN_FILE}"

echo $COMPILE

$COMPILE

strip ${BIN_FILE}
