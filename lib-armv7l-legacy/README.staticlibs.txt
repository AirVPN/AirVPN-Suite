In order to compile the static version of the suite for armv7l, you need to
provide libcryptopp.a and libcurl.a in this directory.

CryptoPP: https://www.cryptopp.com/#download
cURL: https://curl.se/download.html
