# AirVPN Suite for Linux

#### AirVPN's free and open source OpenVPN 3 client based on AirVPN's OpenVPN 3 library fork

### Version 1.3.0 - Release date 1 June 2023


## Note on Checksum Files

AirVPN Suite is an open source project and, as such, its source code can be
downloaded, forked and modified by anyone who wants to create a derivative
project or build it on his or her computer. This also means the source code can
be tampered or modified in a malicious way, therefore creating a binary version
of the suite which may act harmfully, destroy or steal your data, redirecting
your network traffic and data while pretending to be the "real" AirVPN Suite
client genuinely developed and supported by AirVPN.

For this reason, we cannot guarantee forked, modified and custom compiled
versions of AirVPN Suite to be compliant to our specifications, development and
coding guidelines and style, including our security standards. These projects,
of course, may also be better and more efficient than our release, however we
cannot guarantee or provide help for the job of others.

You are therefore strongly advised to check and verify the checksum codes found
in the `.sha512` files to exactly correspond to the ones below, that is, the
checksum we have computed from the sources and distribution files directly
compiled and built by AirVPN. This will make you sure about the origin and
authenticity of the suite. Please note the files contained in the distribution
tarballs are created from the very source code available in the master branch of
the [official AirVPN Suite's repository](https://gitlab.com/AirVPN/AirVPN-Suite).


### Checksum codes for Version 1.3.0

The checksum codes contained in files `AirVPN-Suite-<arch>-1.3.0.<archive>.sha512`
must correspond to the codes below in order to prove they are genuinely created and
distributed by AirVPN.


## Linux x86_64

`AirVPN-Suite-x86_64-1.3.0.tar.gz`:
2e95f97be1dd3e279c97510a7ee39d7734030d1d20aca77187d6470c0a057ffedc91d75dc341931ba551818d526a88d342065758a25c6d9dc3ea15e84ba4aa79


## Linux x86_64 Legacy

`AirVPN-Suite-x86_64-legacy-1.3.0.tar.gz`:
54672997b3e0221db1464490b4ece755b0c27b4a45728749a559f134fb5e2f503473c5b3773cf5dfce4aece51acc91261c463fc2f262ce1f14ed6a58a8dce966


## Linux ARM64

`AirVPN-Suite-aarch64-1.3.0.tar.gz`:
25c98fafa3002848b6d285ac5778cdb3d2e42780d9423bc27f212f0f1a2ba9e8359cbbbbe538c031e24468755c3daf1bec723446845a6dbed6dee271a556c15a


## Linux ARM32 Legacy

`AirVPN-Suite-armv7l-legacy-1.3.0.tar.gz`:
e8ac5e6bd0add850ca19fd5929620423021fd687a01d58c172724bfc1161c4b7ad4179404e6f130070a970e04377705f7556bfadb25254df117eb1af0289354f


## Linux ARM64 Legacy

`AirVPN-Suite-aarch64-legacy-1.3.0.tar.gz`:
65f470e8a5cb864e13e154d40abf6b6ba3a48daee2285319e551480d3ba0dfa23b35881c224719ce1f700ed4788645e03dfd0cb8e914c2c761a58f45049a40b7
