/*
 * wireguardclient.cpp
 *
 * This file is part of the AirVPN Suite for Linux and macOS.
 * Copyright (C) 2019-2023 AirVPN (support@airvpn.org) / https://airvpn.org
 *
 * Developed by ProMIND
 *
 * This is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with the AirVPN Suite. If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "include/wireguardclient.hpp"
#include "include/airvpntools.hpp"

#if defined(MAKING_BLUETIT)

extern void global_syslog(const std::string &s);

#endif

extern void global_log(const std::string &s);

WireGuardClient::WireGuardClient()
{
    privateKey = "";
    publicKey = "";
    presharedKey = "";
    endPointPort = -1;
    persistentKeepalive = -1;
    customMTU = -1;
    
    dnsAddress.clear();
    ipAddress.clear();
    allowedIpAddress.clear();

    std::string wireGuardProfile;
    wireGuardProfile = "";
}

WireGuardClient::~WireGuardClient()
{
}

bool WireGuardClient::setConfiguration(std::string configuration)
{
    std::vector<std::string> row, item;
    bool result = true;
    IPAddress ipEntry;
    ConfigSection configSection = SECTION_UNDEFINED;

    if(configuration.empty() == true)
        return false;

    wireGuardProfile = configuration;

    row = AirVPNTools::split(wireGuardProfile, "\n");

    for(std::string line : row)
    {
        line = AirVPNTools::trim(line);

        if(line.empty() == false && line[0] != '#')
        {
            line = AirVPNTools::replaceAll(line, "=", " ");
            line = AirVPNTools::replaceAll(line, ",", " ");
            line = AirVPNTools::replaceAll(line, " +", " ");
            line = AirVPNTools::replaceAll(line, "\t+", " ");

            item = AirVPNTools::split(line, " ");

            item[0] = AirVPNTools::toLower(item[0]);

            if(item[0] == "[interface]")
            {
                configSection = SECTION_INTERFACE;
            }
            else if(item[0] == "[peer]")
            {
                configSection = SECTION_PEER;
            }
            else if(item[0] == "privatekey")
            {
                if(configSection == SECTION_INTERFACE)
                    privateKey = item[1];
                else
                {
                    log("Error in WireGuard configuration: 'PrivateKey' directive must be in 'Interface' section.");

                    result = false;
                }
            }
            else if(item[0] == "address")
            {
                if(configSection == SECTION_INTERFACE)
                {
                    for(int i = 1; i < item.size(); i++)
                    {
                        ipEntry = LocalNetwork::parseIpSpecification(item[i]);

                        if(ipEntry.address.empty() == false)
                        {
                            ipEntry.prefixLength = (ipEntry.family == IPFamily::IPv4) ? 32 : 128;

                            ipAddress.push_back(ipEntry);
                        }
                    }
                }
                else
                {
                    log("Error in WireGuard configuration: 'Address' directive must be in 'Interface' section.");

                    result = false;
                }
            }
            else if(item[0] == "dns")
            {
                if(configSection == SECTION_INTERFACE)
                {
                    for(int i = 1; i < item.size(); i++)
                    {
                        ipEntry = LocalNetwork::parseIpSpecification(item[i]);

                        if(ipEntry.address.empty() == false)
                        {
                            ipEntry.prefixLength = (ipEntry.family == IPFamily::IPv4) ? 32 : 128;

                            dnsAddress.push_back(ipEntry);
                        }
                    }
                }
                else
                {
                    log("Error in WireGuard configuration: 'DNS' directive must be in 'Interface' section.");

                    result = false;
                }
            }
            else if(item[0] == "mtu")
            {
                if(configSection == SECTION_INTERFACE)
                    customMTU = std::stoi(item[1]);
                else
                {
                    log("Error in WireGuard configuration: 'MTU' directive must be in 'Interface' section.");

                    result = false;
                }
            }
            else if(item[0] == "publickey")
            {
                if(configSection == SECTION_PEER)
                    publicKey = item[1];
                else
                {
                    log("Error in WireGuard configuration: 'PublicKey' directive must be in 'Peer' section.");

                    result = false;
                }
            }
            else if(item[0] == "presharedkey")
            {
                if(configSection == SECTION_PEER)
                    presharedKey = item[1];
                else
                {
                    log("Error in WireGuard configuration: 'PresharedKey' directive must be in 'Peer' section.");

                    result = false;
                }
            }
            else if(item[0] == "persistentkeepalive")
            {
                if(configSection == SECTION_PEER)
                    persistentKeepalive = std::stoi(item[1]);
                else
                {
                    log("Error in WireGuard configuration: 'PersistentKeepAlive' directive must be in 'Peer' section.");

                    result = false;
                }
            }
            else if(item[0] == "allowedips")
            {
                if(configSection == SECTION_PEER)
                {
                    for(int i = 1; i < item.size(); i++)
                    {
                        ipEntry = LocalNetwork::parseIpSpecification(item[i]);

                        if(ipEntry.address.empty() == false)
                            allowedIpAddress.push_back(ipEntry);
                    }
                }
                else
                {
                    log("Error in WireGuard configuration: 'AllowedIPs' directive must be in 'Peer' section.");

                    result = false;
                }
            }
            else if(item[0] == "endpoint")
            {
                if(configSection == SECTION_PEER)
                {
                    std::vector<std::string> endPointSpec = AirVPNTools::split(item[1], ":");

                    if(endPointSpec.size() > 0)
                    {
                        endPoint = LocalNetwork::parseIpSpecification(endPointSpec[0]);

                        if(endPointSpec.size() > 1)
                            endPointPort = std::stoi(endPointSpec[1]);
                        else
                            endPointPort = 0;
                    }
                    else
                    {
                        endPoint.address= "";
                        endPoint.family = IPFamily::IPv4;
                        endPoint.prefixLength = 0;

                        endPointPort = 0;
                    }
                }
                else
                {
                    log("Error in WireGuard configuration: 'Endpoint' directive must be in 'Peer' section.");

                    result = false;
                }
            }
            else
            {
                log("Error in WireGuard configuration: unrecognized directive '" + item[0] + "'.");

                result = false;
            }
        }
    }

    return result;
}

void WireGuardClient::log(std::string s)
{
    if(s.empty() == false)
        global_log(s);
}

void WireGuardClient::log(std::ostringstream os)
{
    log(os.str());
}

void WireGuardClient::syslog(std::string s)
{
#if defined(MAKING_BLUETIT)

    if(s.empty() == false)
        global_syslog(s);

#else

    if(s.empty() == false)
        global_log(s);

#endif
}

void WireGuardClient::syslog(std::ostringstream os)
{
    syslog(os.str());
}
