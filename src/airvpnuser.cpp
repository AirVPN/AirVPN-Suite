/*
 * airvpnuser.cpp
 *
 * This file is part of the AirVPN Suite for Linux and macOS.
 * Copyright (C) 2019-2022 AirVPN (support@airvpn.org) / https://airvpn.org
 *
 * Developed by ProMIND
 *
 * This is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with the AirVPN Suite. If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <fstream>
#include <sstream>
#include <syslog.h>
#include "include/airvpnuser.hpp"

extern void global_log(const std::string &s);

AirVPNUser::AirVPNUser(const std::string &userName, const std::string &password)
{
    airVpnTools = new AirVPNTools();

    initializeUserData();

    airVPNUserName = userName;
    airVPNUserPassword = password;

    loadUserProfile();
}

AirVPNUser::~AirVPNUser()
{
}

void AirVPNUser::initializeUserData()
{
    airVPNUserName = "";
    airVPNUserPassword = "";
    airVPNUserCurrentProfile = "";

    initializeUserProfileData();
}

void AirVPNUser::initializeUserProfileData()
{
    userIsValid = false;
    userLoginFailed = true;
    expirationDate = "";
    daysToExpiration = 0;
    certificateAuthorityCertificate = "";
    tlsAuthKey = "";
    tlsCryptKey = "";
    sshKey = "";
    sshPpk = "";
    sslCertificate = "";
    wireguardPublicKey = "";

    userKey.clear();
}

std::string AirVPNUser::getUserIP()
{
    return userIP;
}

void AirVPNUser::setUserIP(const std::string &ip)
{
    userIP = ip;
}

std::string AirVPNUser::getUserCountry()
{
    return userCountry;
}

void AirVPNUser::setUserCountry(const std::string &country)
{
    userCountry = AirVPNTools::toUpper(country);
}

float AirVPNUser::getUserLatitude()
{
    return userLatitude;
}

void AirVPNUser::setUserLatitude(float l)
{
    userLatitude = l;
}

float AirVPNUser::getUserLongitude()
{
    return userLongitude;
}

void AirVPNUser::setUserLongitude(float l)
{
    userLongitude = l;
}

bool AirVPNUser::isUserValid()
{
    return userIsValid;
}

std::string AirVPNUser::getExpirationDate()
{
    return airVPNSubscriptionExpirationDate;
}

void AirVPNUser::setExpirationDate(const std::string &e)
{
    airVPNSubscriptionExpirationDate = e;
}

int AirVPNUser::getDaysToExpiration()
{
    return daysToExpiration;
}

void AirVPNUser::setDaysToExpiration(int d)
{
    daysToExpiration = d;
}

std::string AirVPNUser::getCertificateAuthorityCertificate()
{
    return certificateAuthorityCertificate;
}

void AirVPNUser::setCertificateAuthorityCertificate(const std::string &c)
{
    certificateAuthorityCertificate = c;
}

std::string AirVPNUser::getTlsAuthKey()
{
    return tlsAuthKey;
}

void AirVPNUser::setTlsAuthKey(const std::string &t)
{
    tlsAuthKey = t;
}

std::string AirVPNUser::getTlsCryptKey()
{
    return tlsCryptKey;
}

void AirVPNUser::setTlsCryptKey(const std::string &t)
{
    tlsCryptKey = t;
}

std::string AirVPNUser::getSshKey()
{
    return sshKey;
}

void AirVPNUser::setSshKey(const std::string &s)
{
    sshKey = s;
}

std::string AirVPNUser::getSshPpk()
{
    return sshPpk;
}

void AirVPNUser::setSshPpk(const std::string &s)
{
    sshPpk = s;
}

std::string AirVPNUser::getSslCertificate()
{
    return sslCertificate;
}

void AirVPNUser::setSslCertificate(const std::string &s)
{
    sslCertificate = s;
}

std::string AirVPNUser::getWireGuardPublicKey()
{
    return wireguardPublicKey;
}

void AirVPNUser::setWireGuardPublicKey(const std::string &s)
{
    wireguardPublicKey = s;
}

std::map<std::string, AirVPNUser::UserKey> AirVPNUser::getUserKeys()
{
    return userKey;
}

void AirVPNUser::setUserKeys(const std::map<std::string, AirVPNUser::UserKey> &u)
{
    userKey = u;
}

AirVPNUser::UserKey AirVPNUser::getUserKey(const std::string &name)
{
    UserKey key;

    if(userKey.empty())
        return key;

    std::map<std::string, UserKey>::iterator it = userKey.find(name);

    if(it != userKey.end())
        key = it->second;

    return key;
}

void AirVPNUser::setUserKey(const std::string &name, const UserKey &u)
{
    userKey.insert(std::make_pair(name, u));
}

void AirVPNUser::addUserKey(const std::string &name, const UserKey &key)
{
    if(name.empty() || !getUserKey(name).name.empty())
        return;

    userKey.insert(std::make_pair(name, key));
}

std::vector<std::string> AirVPNUser::getUserKeyNames()
{
    std::vector<std::string> keyNames;

    if(userKey.size() > 0)
    {
        for(std::map<std::string, UserKey>::iterator it = userKey.begin(); it != userKey.end(); it++)
            keyNames.push_back(it->first);
    }

    return keyNames;
}

AirVPNUser::UserProfileType AirVPNUser::getUserProfileType()
{
    return userProfileType;
}

std::string AirVPNUser::getUserName()
{
    return airVPNUserName;
}

void AirVPNUser::setUserName(const std::string &u)
{
    airVPNUserName = u;
}

std::string AirVPNUser::getUserPassword()
{
    return airVPNUserPassword;
}

void AirVPNUser::setUserPassword(const std::string &p)
{
    airVPNUserPassword = p;
}

std::string AirVPNUser::getCurrentProfile()
{
    return airVPNUserCurrentProfile;
}

std::string AirVPNUser::getFirstProfileName()
{
    std::string name = "";

    if(userKey.size() > 0)
    {
        std::map<std::string, UserKey>::iterator it = userKey.begin();
        
        name = it->first;
    }

    return name;
}

void AirVPNUser::setCurrentProfile(const std::string &p)
{
    airVPNUserCurrentProfile = p;
}

AirVPNUser::Error AirVPNUser::getUserProfileError()
{
    return userProfileError;
}

std::string AirVPNUser::getUserProfileErrorDescription()
{
    return userProfileErrorDescription;
}

AirVPNUser::UserLocationStatus AirVPNUser::detectUserLocation()
{
    UserLocationStatus result;
    std::map<std::string, std::string> userLocation;
    std::map<std::string, std::string>::iterator it;

    userLocation = airVpnTools->detectLocation();

    if(userLocation.size() > 0)
    {
        it = userLocation.find("status");
        
        if(it->second == AirVPNTools::LOCATION_STATUS_OK)
        {
            userLocationStatusError = it->second;

            it = userLocation.find("ip");
            userIP = it->second;

            it = userLocation.find("country");
            userCountry = AirVPNTools::toUpper(it->second);

            it = userLocation.find("latitude");
            
            try
            {
                userLatitude = std::stod(it->second);
            }
            catch(std::exception &e)
            {
                userLatitude = 0.0;
            }

            it = userLocation.find("longitude");

            try
            {
                userLongitude = std::stod(it->second);
            }
            catch(std::exception &e)
            {
                userLongitude = 0.0;
            }

            result = SET;
        }
        else
        {
            userIP = "";
            userCountry = "";
            userLatitude = 0.0;
            userLongitude = 0.0;

            userLocationStatusError = "ERROR: Cannot detect user location: " + it->second;

            result = UNKNOWN;
        }
    }
    else
    {
        userIP = "";
        userCountry = "";
        userLatitude = 0.0;
        userLongitude = 0.0;
        
        userLocationStatusError = "ipleak.net returned an empty document";

        result = UNKNOWN;
    }

    return result;
}

void AirVPNUser::loadUserProfile()
{
    airVpnUserProfileDocument = nullptr;
    std::ostringstream osstring;
    std::vector<std::string> bootstrapServerList = AirVPNManifest::getBootStrapServerUrlList();
    std::string rsaPublicKeyModulus = AirVPNManifest::getRsaPublicKeyModulus();
    std::string rsaPublicKeyExponent = AirVPNManifest::getRsaPublicKeyExponent();

    if(airVPNUserName.empty())
    {
        userProfileError = AirVPNUser::Error::INVALID_USER;
        userProfileErrorDescription = "AirVPN Username is empty";
        userProfileType = UserProfileType::NOT_SET;
        userIsValid = false;
        
        return;
    }

    if(airVPNUserPassword.empty())
    {
        userProfileError = AirVPNUser::Error::INVALID_USER;
        userProfileErrorDescription = "AirVPN User password is empty";
        userProfileType = UserProfileType::NOT_SET;
        userIsValid = false;
        
        return;
    }

    if(rsaPublicKeyModulus.empty() || rsaPublicKeyExponent.empty() || bootstrapServerList.empty())
    {
        userProfileError = AirVPNUser::Error::AIRVPN_PARAMETERS_ERROR;
        userProfileErrorDescription = "Invalid AirVPN bootstrap server parameters";
        userProfileType = UserProfileType::NOT_SET;
        userIsValid = false;
        
        return;
    }

    airVpnTools->setRSAModulus(rsaPublicKeyModulus);
    airVpnTools->setRSAExponent(rsaPublicKeyExponent);

    airVpnTools->resetBootServers();

    for(std::string server : bootstrapServerList)
        airVpnTools->addBootServer(server);

    airVpnTools->resetAirVPNDocumentRequest();
    airVpnTools->setAirVPNDocumentParameter("login", airVPNUserName);
    airVpnTools->setAirVPNDocumentParameter("password", airVPNUserPassword);

#if defined(__linux__)
    airVpnTools->setAirVPNDocumentParameter("system", "linux");
#elif __APPLE__
    airVpnTools->setAirVPNDocumentParameter("system", "osx");
#endif

    airVpnTools->setAirVPNDocumentParameter("act", "user");

    AirVPNTools::AirVPNServerError res = airVpnTools->requestAirVPNDocument();

    if(res == AirVPNTools::AirVPNServerError::OK)
    {
        userProfileDocument = airVpnTools->getRequestedDocument();
        AirVPNUser::Error error = processXmlUserProfile();

        if(error == AirVPNUser::Error::OK)
        {
            userProfileError = AirVPNUser::Error::OK;
            userProfileErrorDescription = "OK";
            userProfileType = UserProfileType::FROM_SERVER;
        }
        else
        {
            userProfileError = AirVPNUser::Error::PARSE_ERROR;
            userProfileErrorDescription = "";

            switch(error)
            {
                case INVALID_USER:
                {
                    userProfileErrorDescription += "Invalid username or password";
                }
                break;
                
                case CANNOT_RETRIEVE_PROFILE:
                {
                    userProfileErrorDescription += "Cannot retrieve user profile from server";
                }
                break;
                
                case EMPTY_PROFILE:
                {
                    userProfileErrorDescription += "Server returned an empty user profile";
                }
                break;
                
                case NULL_XML_DOCUMENT:
                {
                    userProfileErrorDescription += "Server returned an unknown user profile";
                }
                break;
                
                case PARSE_ERROR:
                {
                    userProfileErrorDescription += "Failed to parse. Malformed user profile.";
                }
                break;
                
                default:
                {
                    userProfileErrorDescription += "Failed to parse. Unknown error.";
                }
                break;
            }

            userProfileDocument = "";
            userProfileType = UserProfileType::NOT_SET;
            userIsValid = false;
        }
    }
    else
    {
        userProfileError = AirVPNUser::Error::INVALID_USER;
        userProfileErrorDescription = airVpnTools->getDocumentRequestErrorDescription();
        userProfileType = UserProfileType::NOT_SET;
        userIsValid = false;
    }
}

AirVPNUser::Error AirVPNUser::processXmlUserProfile()
{
    std::string value;
    xmlNode *node = nullptr;
    
    if(userProfileDocument.empty())
        return EMPTY_PROFILE;

    cleanupXmlParser();

    airVpnUserProfileDocument = xmlReadMemory(userProfileDocument.c_str(), userProfileDocument.size(), "noname.xml", NULL, 0);

    if(airVpnUserProfileDocument == nullptr)
        return NULL_XML_DOCUMENT;

    rootNode = xmlDocGetRootElement(airVpnUserProfileDocument);

    initializeUserProfileData();

    node = AirVPNTools::findXmlNode((xmlChar *)"user", rootNode);

    if(node != NULL)
    {
        if(AirVPNTools::getXmlAttribute(node, "message_action", "").empty())
            userIsValid = true;
        else
        {
            userIsValid = false;

            userProfileErrorDescription = AirVPNTools::getXmlAttribute(node, "message_action", "");
            
            return INVALID_USER;
        }

        if(AirVPNTools::getXmlAttribute(node, "login", "") != airVPNUserName)
        {
            userProfileErrorDescription = "ERROR: User name in profile data does not match to current user";
            
            userIsValid = false;

            return INVALID_USER;
        }
        
        expirationDate = AirVPNTools::getXmlAttribute(node, "expirationdate", "");
        certificateAuthorityCertificate = AirVPNTools::convertXmlEntities(AirVPNTools::getXmlAttribute(node, "ca", ""));
        tlsAuthKey = AirVPNTools::convertXmlEntities(AirVPNTools::getXmlAttribute(node, "ta", ""));
        tlsCryptKey = AirVPNTools::convertXmlEntities(AirVPNTools::getXmlAttribute(node, "tls_crypt", ""));
        sshKey = AirVPNTools::convertXmlEntities(AirVPNTools::getXmlAttribute(node, "ssh_key", ""));
        sshPpk = AirVPNTools::convertXmlEntities(AirVPNTools::getXmlAttribute(node, "ssh_ppk", ""));
        sslCertificate = AirVPNTools::convertXmlEntities(AirVPNTools::getXmlAttribute(node, "ssl_crt", ""));
        wireguardPublicKey = AirVPNTools::convertXmlEntities(AirVPNTools::getXmlAttribute(node, "wg_public_key", ""));

    }

    if(AirVPNTools::findXmlNode((xmlChar *)"keys", rootNode) != NULL)
    {
        node = rootNode;

        while(node != NULL)
        {
            node = AirVPNTools::findXmlNode((xmlChar *)"key", node);

            if(node != NULL)
            {
                value = AirVPNTools::getXmlAttribute(node, "name", "");

                if(!value.empty())
                {
                    UserKey localUserKey;
                    
                    localUserKey.name = value;
                    localUserKey.certificate = AirVPNTools::convertXmlEntities(AirVPNTools::getXmlAttribute(node, "crt", ""));
                    localUserKey.privateKey = AirVPNTools::convertXmlEntities(AirVPNTools::getXmlAttribute(node, "key", ""));
                    localUserKey.wireguardPrivateKey = AirVPNTools::convertXmlEntities(AirVPNTools::getXmlAttribute(node, "wg_private_key", ""));
                    localUserKey.wireguardPresharedKey = AirVPNTools::convertXmlEntities(AirVPNTools::getXmlAttribute(node, "wg_preshared", ""));
                    localUserKey.wireguardIPv4 = AirVPNTools::convertXmlEntities(AirVPNTools::getXmlAttribute(node, "wg_ipv4", ""));
                    localUserKey.wireguardIPv6 = AirVPNTools::convertXmlEntities(AirVPNTools::getXmlAttribute(node, "wg_ipv6", ""));
                    localUserKey.wireguardDnsIPv4 = AirVPNTools::convertXmlEntities(AirVPNTools::getXmlAttribute(node, "wg_dns_ipv4", ""));
                    localUserKey.wireguardDnsIPv6 = AirVPNTools::convertXmlEntities(AirVPNTools::getXmlAttribute(node, "wg_dns_ipv6", ""));

                    addUserKey(value, localUserKey);
                }
                
                node = node->next;
            }
        }
    }
    
    return OK;
}

std::string AirVPNUser::getOpenVPNProfile(const std::string &userKeyName, std::string server, int port, const std::string &proto, const std::string &tlsMode, const std::string &cipher, bool connectIPv6, bool mode6to4, bool createCountryProfile, const std::string &countryCode, const std::string &customProfile)
{
    std::ostringstream openVpnProfile;
    std::string serverProto, serverTlsMode, manifestDirectives;

    if(customProfile.empty() && (userKeyName.empty() || userKey.find(userKeyName) == userKey.end()))
    {
        global_log("ERROR: AirVPNUser: Wrong profile name " + userKeyName);

        return "";
    }

    if(server.empty())
    {
        global_log("ERROR: AirVPNUser: server is empty");

        return "";
    }

    if(port < 1 || port > 65535)
    {
        global_log("ERROR: AirVPNUser: illegal port number " + std::to_string(port));

        return "";
    }

    serverProto = AirVPNTools::toLower(proto);

    if(serverProto != "udp" && serverProto != "tcp" && serverProto != "udp6" && serverProto != "tcp6")
    {
        global_log("ERROR: AirVPNUser: illegal prptocol " + proto);

        return "";

    }

    serverTlsMode = AirVPNTools::toLower(tlsMode);

    if(serverTlsMode != "tls-auth" && serverTlsMode != "tls-crypt")
    {
        global_log("ERROR: AirVPNUser: illegal tls mode " + tlsMode);

        return "";
    }

    if(createCountryProfile)
        server = AirVPNTools::getAirVpnCountryHostname(countryCode, serverTlsMode, connectIPv6);

    openVpnProfile.str("");

    openVpnProfile << "client" << std::endl << "dev tun" << std::endl;

    openVpnProfile << "remote " << server << " " << port << std::endl;

    openVpnProfile << "proto " << proto << std::endl;

    openVpnProfile << "push-peer-info" << std::endl;

    openVpnProfile << "setenv UV_IPV6 ";

    if(mode6to4 == true || connectIPv6 == true)
        openVpnProfile << "yes";
    else
        openVpnProfile << "no";

    openVpnProfile << std::endl;

    openVpnProfile << "resolv-retry infinite" << std::endl;
    openVpnProfile << "nobind" << std::endl;
    openVpnProfile << "persist-key" << std::endl;
    openVpnProfile << "persist-tun" << std::endl;
    openVpnProfile << "verb 3" << std::endl;

    if(proto == "udp")
        openVpnProfile << "explicit-exit-notify 5" << std::endl;

    openVpnProfile << "remote-cert-tls server" << std::endl;

    if(cipher == "SERVER")
        openVpnProfile << "data-ciphers AES-256-GCM:AES-128-GCM" << std::endl;
    else
    {
        openVpnProfile << "data-ciphers " << cipher << std::endl;
        openVpnProfile << "ncp-disable" << std::endl;
    }

    if(tlsMode == "tls-crypt")
        openVpnProfile << "auth SHA512" << std::endl;

    if(tlsMode == "tls-auth")
        openVpnProfile << "key-direction 1" << std::endl;

    if(certificateAuthorityCertificate.empty())
    {
        global_log("ERROR: AirVPNUser: Certificate authority certificate (ca) is empty");

        return "";
    }

    manifestDirectives = AirVPNManifest::getServerOpenVpnDirectives(server);
    
    if(manifestDirectives.empty() == false)
        openVpnProfile << manifestDirectives;

    if(customProfile.empty())
    {
        openVpnProfile << "<ca>" << std::endl << certificateAuthorityCertificate << std::endl << "</ca>" << std::endl;

        std::map<std::string, UserKey>::iterator it = userKey.find(userKeyName);
        
        if(it != userKey.end())
        {
            UserKey profile = it->second;

            openVpnProfile << "<cert>" << std::endl << profile.certificate << std::endl << "</cert>" << std::endl;

            openVpnProfile << "<key>" << std::endl << profile.privateKey << std::endl << "</key>" << std::endl;
        }

        if(tlsMode == "tls-auth")
            openVpnProfile << "<tls-auth>" << std::endl << tlsAuthKey << std::endl << "</tls-auth>" << std::endl;
        else if(tlsMode == "tls-crypt")
            openVpnProfile << "<tls-crypt>" << std::endl << tlsCryptKey << std::endl << "</tls-crypt>" << std::endl;
    }
    else
        openVpnProfile << std::endl << customProfile;

    return openVpnProfile.str();
}

std::string AirVPNUser::getWireGuardProfile(const std::string &userKeyName, std::string server, int port, int listenPort, int fwMark, const std::string &allowedIPs, int persistentKeepalive, bool connectIPv6, bool mode6to4, bool createCountryProfile, const  std::string &countryCode)
{
    UserKey userProfile;
    std::string wireguardProfile = "", address = "", dns = "";

    if(userKeyName.empty() || userKey.find(userKeyName) == userKey.end())
    {
        global_log("ERROR: AirVPNUser: Wrong profile name " + userKeyName);

        return "";
    }

    if(server.empty())
    {
        global_log("ERROR: AirVPNUser: server is empty");

        return "";
    }

    if(port < 1 || port > 65535)
    {
        global_log("ERROR: AirVPNUser: illegal port number " + std::to_string(port));

        return "";
    }

    if(createCountryProfile)
    {
        server = AirVPNTools::toLower(countryCode);

        if(connectIPv6)
            server += ".ipv6";

        server += ".vpn.airdns.org";
    }

    userProfile = userKey.find(userKeyName)->second;

    wireguardProfile = "[Interface]\n";

    address = "";

    if(connectIPv6 == false || mode6to4 == true)
        address = userProfile.wireguardIPv4;

    if(connectIPv6 == true || mode6to4 == true)
    {
        if(address.empty() == false)
            address += ", ";
        
        address += userProfile.wireguardIPv6;
    }

    wireguardProfile += "Address = " + address;

    wireguardProfile += "\nPrivateKey = " + userProfile.wireguardPrivateKey + "\n";

    dns = "";

    if(connectIPv6 == false || mode6to4 == true)
        dns = userProfile.wireguardDnsIPv4;

    if(connectIPv6 == true || mode6to4 == true)
    {
        if(dns.empty() == false)
            dns += ", ";
        
        dns += userProfile.wireguardDnsIPv6;
    }

    wireguardProfile += "DNS = " + dns;

    wireguardProfile += "\n";

    if(listenPort > 0)
    {
        wireguardProfile += "ListenPort = ";
        wireguardProfile += std::to_string(listenPort);
        wireguardProfile += "\n";
    }

    if(fwMark > 0)
    {
        wireguardProfile += "FwMark = ";
        wireguardProfile += std::to_string(fwMark);
        wireguardProfile += "\n";
    }

    wireguardProfile += "\n[Peer]\n";

    wireguardProfile += "PublicKey = ";
    wireguardProfile += wireguardPublicKey;
    wireguardProfile += "\n";

    wireguardProfile += "PresharedKey = ";
    wireguardProfile += userProfile.wireguardPresharedKey;
    wireguardProfile += "\n";

    wireguardProfile += "Endpoint = ";

    if(server.find(':') !=std::string::npos)
        wireguardProfile += "[";

    wireguardProfile += server;

    if(server.find(':') !=std::string::npos)
        wireguardProfile += "]";

    wireguardProfile += ":";
    wireguardProfile += std::to_string(port);
    wireguardProfile += "\n";

    wireguardProfile += "AllowedIPs = ";
    wireguardProfile += allowedIPs;
    wireguardProfile += "\n";

    wireguardProfile += "PersistentKeepalive = ";
    wireguardProfile += std::to_string(persistentKeepalive);
    wireguardProfile += "\n";

    return wireguardProfile;
}

std::string AirVPNUser::getUserLocationStatusError()
{
    return userLocationStatusError;
}

void AirVPNUser::cleanupXmlParser()
{
    if(airVpnUserProfileDocument == nullptr)
        return;

    xmlFreeDoc(airVpnUserProfileDocument);
}

size_t AirVPNUser::curlWriteCallback(void *data, size_t size, size_t nmemb, void *userp)
{
    ((std::string*)userp)->append((char*)data, size * nmemb);

    return size * nmemb;
}
