/*
 * dbusconnector.cpp
 *
 * This file is part of the AirVPN Suite for Linux and macOS.
 * Copyright (C) 2019-2022 AirVPN (support@airvpn.org) / https://airvpn.org
 *
 * Developed by ProMIND
 *
 * This is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with the AirVPN Suite. If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <sstream>
#include <iostream>
#include <chrono>
#include <thread>
#include <locale.h>
#include <langinfo.h>
#include <string.h>
#include "include/dbusconnector.hpp"

DBusConnector::DBusConnector(const std::string &interface, const std::string &bus)
{
    dbus_threads_init_default();

    dbus_error_init(&dbusError);

    if(interface == "")
        throw(DBusConnectorException("DBusConnector: empty interface name"));

    if(bus == "")
        throw(DBusConnectorException("DBusConnector: empty bus name"));

    interfaceName = interface;
    busName = bus;

    setlocale(LC_ALL, "");
    setlocale(LC_CTYPE, NULL);
    localeEncoding = nl_langinfo(CODESET);

    if(localeEncoding.find("UTF-8") == std::string::npos)
    {
        isEncodingNeeded = true;
        
        iconvToUTF8 = iconv_open("UTF-8", localeEncoding.c_str());
        iconvToLocale = iconv_open(std::string(localeEncoding+"//TRANSLIT").c_str(), "UTF-8");
        
        iconvOut = (char *)malloc(ICONV_OUT_LEN);
    }
    else
        isEncodingNeeded = false;

    if(!connect())
        throw(DBusConnectorException("DBusConnector: cannot connect to D-Bus"));
}

DBusConnector::~DBusConnector()
{
    if(isEncodingNeeded == true)
    {
       iconv_close(iconvToUTF8);
       iconv_close(iconvToLocale);
       
       free(iconvOut);
    }

    disconnect();

    dbus_shutdown();
}

void DBusConnector::disconnect()
{
    if(dbusConnection != nullptr)
    {
        dbus_connection_flush(dbusConnection);

        dbus_connection_close(dbusConnection);
    }
}

bool DBusConnector::connect()
{
    std::ostringstream os;
    int dbusRetval;

    if(dbusConnection != nullptr)
        disconnect();

    if(busName == "")
        throw(DBusConnectorException("DBusConnector: empty bus name"));

    dbusConnection = dbus_bus_get_private(DBUS_BUS_SYSTEM, &dbusError);

    if(dbus_error_is_set(&dbusError))
    {
        os.str("");

        os << "DBusConnector: connection failed. D-Bus is not available - " << dbusError.message;

        dbus_error_free(&dbusError);

        throw(DBusConnectorException(os.str()));

        return false;
    }

    if(!dbusConnection)
    {
        throw(DBusConnectorException("DBusConnector: error while connecting to D-Bus"));

        return false;
    }

    dbusRetval = dbus_bus_request_name(dbusConnection, busName.c_str(), DBUS_NAME_FLAG_REPLACE_EXISTING, &dbusError);

    if(dbus_error_is_set(&dbusError))
    {
        os.str("");

        os << "DBusConnector: request name error \"" << busName << "\" - " << dbusError.message;;

        dbus_error_free(&dbusError);

        throw(DBusConnectorException(os.str()));

        return false;
    }

    if(dbusRetval != DBUS_REQUEST_NAME_REPLY_PRIMARY_OWNER)
    {
        os.str("");

        os << "DBusConnector: not primary owner (" << dbusRetval << ")";

        throw(DBusConnectorException(os.str()));

        return false;
    }

    return true;
}

bool DBusConnector::isDBusConnectionAvailable()
{
    if(dbusConnection == nullptr)
    {
        throw(DBusConnectorException("DBusConnector: dbusConnection is null"));

        return false;
    }

    if(!dbus_connection_get_is_connected(dbusConnection))
    {
        try
        {
            if(!connect())
            {
                throw(DBusConnectorException("DBusConnector: connection is not available"));

                return false;
            }
        }
        catch(DBusConnectorException &e)
        {
            throw(e);

            return false;
        }
    }

    return true;
}

bool DBusConnector::readWriteDispatch(int timeout_millis)
{
    if(!isDBusConnectionAvailable() || timeout_millis < 0)
        return false;

    std::this_thread::sleep_for(std::chrono::milliseconds(timeout_millis));

    return dbus_connection_read_write_dispatch(dbusConnection, 0);
}

DBusMessage *DBusConnector::popMessage()
{
    DBusMessage *message;

    if(!isDBusConnectionAvailable())
        return nullptr;

    lock();

    message = dbus_connection_pop_message(dbusConnection);

    unlock();

    return message;
}

bool DBusConnector::isMethod(DBusMessage *dbusMessage, const std::string &method)
{
    bool retval;

    if(!isDBusConnectionAvailable() || method == "")
        return false;

    retval = dbus_message_is_method_call(dbusMessage, interfaceName.c_str(), method.c_str());

    return retval;
}

bool DBusConnector::callMethod(const std::string &bus, const std::string &path, const std::string &method, const std::vector<std::string> &item)
{
    DBusMessage *dbusMessage = nullptr;
    DBusMessageIter iter;
    char *strPointer;

    if(!isDBusConnectionAvailable())
        return false;

    lock();

    if((dbusMessage = dbus_message_new_method_call(bus.c_str(), path.c_str(), interfaceName.c_str(), method.c_str())) == NULL)
    {
        unlock();

        throw(DBusConnectorException("DBusConnector: cannot create method call. (Out of memory)"));

        return false;
    }
    else
    {
        dbus_message_iter_init_append(dbusMessage, &iter);

        for(std::string arg : item)
        {
            arg = stringToUTF8(arg);

            strPointer = (char *)arg.c_str();

            if(!dbus_message_iter_append_basic(&iter, DBUS_TYPE_STRING, &strPointer))
            {
                unlock();

                throw(DBusConnectorException("DBusConnector: cannot append argument to method. (Out of memory)"));

                return false;
            }
        }

        dbus_message_set_no_reply(dbusMessage, TRUE);

        if(!dbus_connection_send(dbusConnection, dbusMessage, NULL))
        {
            unlock();

            throw(DBusConnectorException("DBusConnector: error while sending method request. (Out of memory)"));

            return false;
        }

        dbus_connection_flush(dbusConnection);

        dbus_message_unref(dbusMessage);
    }

    unlock();

    return true;
}

DBusMessage *DBusConnector::callMethodWithReply(const std::string &bus, const std::string &path, const std::string &method, const std::vector<std::string> &item)
{
    DBusMessage *dbusMessage = nullptr, *methodReply = nullptr;
    DBusMessageIter iter;
    DBusPendingCall *pendingReturn;
    char *strPointer;

    if(!isDBusConnectionAvailable())
        return nullptr;

    lock();

    if((dbusMessage = dbus_message_new_method_call(bus.c_str(), path.c_str(), interfaceName.c_str(), method.c_str())) == NULL)
    {
        unlock();

        throw(DBusConnectorException("DBusConnector: cannot create method call. (Out of memory)"));

        return nullptr;
    }
    else
    {
        dbus_message_iter_init_append(dbusMessage, &iter);

        for(std::string arg : item)
        {
            arg = stringToUTF8(arg);

            strPointer = (char *)arg.c_str();

            if(!dbus_message_iter_append_basic(&iter, DBUS_TYPE_STRING, &strPointer))
            {
                unlock();

                throw(DBusConnectorException("DBusConnector: cannot append argument to method. (Out of memory)"));

                return nullptr;
            }
        }

        if(!dbus_connection_send_with_reply(dbusConnection, dbusMessage, &pendingReturn, -1))
        {
            unlock();

            throw(DBusConnectorException("DBusConnector: cannot send method with reply"));

            return nullptr;
        }

        if(pendingReturn == nullptr)
        {
            unlock();

            throw(DBusConnectorException("DBusConnector: reply from method is null"));

            return nullptr;
        }

        dbus_connection_flush(dbusConnection);

        dbus_message_unref(dbusMessage);

        dbus_pending_call_block(pendingReturn);

        if((methodReply = dbus_pending_call_steal_reply(pendingReturn)) == nullptr)
        {
            unlock();

            throw(DBusConnectorException("DBusConnector: steal reply is null. (Out of memory)"));

            return nullptr;
        }

        dbus_pending_call_unref(pendingReturn);
    }

    unlock();

    return methodReply;
}

bool DBusConnector::replyToMessage(DBusMessage *dbusMessage, DBusResponse response)
{
    DBusMessageIter dbusMessageIter;
    DBusMessage *dbusMessageReply;
    char *strPointer;

    if(!isDBusConnectionAvailable())
        return false;

    if(response.getResponse() == "")
        return false;

    lock();

    if((dbusMessageReply = dbus_message_new_method_return(dbusMessage)) == NULL)
    {
        unlock();

        throw(DBusConnectorException("DBusConnector: cannot create method return message. (Out of memory)"));

        return false;
    }

    dbus_message_iter_init_append(dbusMessageReply, &dbusMessageIter);

    std::string arg = response.toString();

    strPointer = (char *)arg.c_str();

    if(!dbus_message_iter_append_basic(&dbusMessageIter, DBUS_TYPE_STRING, &strPointer))
    {
        unlock();

        throw(DBusConnectorException("DBusConnector: cannot append item. (Out of memory)"));

        return false;
    }

    if(!dbus_connection_send(dbusConnection, dbusMessageReply, NULL))
    {
        unlock();

        throw(DBusConnectorException("DBusConnector: cannot send reply. (Out of memory)"));

        return false;
    }

    dbus_connection_flush(dbusConnection);

    dbus_message_unref(dbusMessageReply);

    unlock();

    return true;
}

bool DBusConnector::replyToMessage(DBusMessage *dbusMessage, const std::vector<std::string> &item)
{
    DBusMessageIter dbusMessageIter;
    DBusMessage *dbusMessageReply;
    char *strPointer;

    if(!isDBusConnectionAvailable())
        return false;

    lock();

    if((dbusMessageReply = dbus_message_new_method_return(dbusMessage)) == NULL)
    {
        unlock();

        throw(DBusConnectorException("DBusConnector: cannot create method return message. (Out of memory)"));

        return false;
    }

    dbus_message_iter_init_append(dbusMessageReply, &dbusMessageIter);

    for(std::string arg : item)
    {
        arg = stringToUTF8(arg);

        strPointer = (char *)arg.c_str();

        if(!dbus_message_iter_append_basic(&dbusMessageIter, DBUS_TYPE_STRING, &strPointer))
        {
            unlock();

            throw(DBusConnectorException("DBusConnector: cannot append item. (Out of memory)"));

            return false;
        }
    }

    if(!dbus_connection_send(dbusConnection, dbusMessageReply, NULL))
    {
        unlock();

        throw(DBusConnectorException("DBusConnector: cannot send reply. (Out of memory)"));

        return false;
    }

    dbus_connection_flush(dbusConnection);

    dbus_message_unref(dbusMessageReply);

    unlock();

    return true;
}

bool DBusConnector::replyToMessage(DBusMessage *dbusMessage, int value)
{
    DBusMessageIter dbusMessageIter;
    DBusMessage *dbusMessageReply;

    if(!isDBusConnectionAvailable())
        return false;

    lock();

    if((dbusMessageReply = dbus_message_new_method_return(dbusMessage)) == NULL)
    {
        unlock();

        throw(DBusConnectorException("DBusConnector: cannot create method return message. (Out of memory)"));

        return false;
    }

    dbus_message_iter_init_append(dbusMessageReply, &dbusMessageIter);

    if(!dbus_message_iter_append_basic(&dbusMessageIter, DBUS_TYPE_INT32, &value))
    {
        unlock();

        throw(DBusConnectorException("DBusConnector: cannot append iter. (Out of memory)"));

        return false;
    }

    if(!dbus_connection_send(dbusConnection, dbusMessageReply, NULL))
    {
        unlock();

        throw(DBusConnectorException("DBusConnector: cannot send reply. (Out of memory)"));

        return false;
    }

    dbus_connection_flush(dbusConnection);

    dbus_message_unref(dbusMessageReply);

    unlock();

    return true;
}

bool DBusConnector::getArgs(DBusMessage *dbusMessage, int firstArgType, ...)
{
    dbus_bool_t retVal;
    va_list varArgs;

    if(!isDBusConnectionAvailable() || dbusMessage == NULL)
       return false;

    lock();

    va_start(varArgs, firstArgType);

    retVal = dbus_message_get_args_valist(dbusMessage, &dbusError, firstArgType, varArgs);

    va_end(varArgs);

    unlock();

    return retVal;
}

std::vector<std::string> DBusConnector::getVector(DBusMessage *dbusMessage)
{
    DBusMessageIter iter;
    std::vector<std::string> item;
    DBusBasicValue dbusValue;
    int iter_type;

    if(!isDBusConnectionAvailable() || dbusMessage == nullptr)
        return item;

    lock();

    item.clear();

    dbus_message_iter_init(dbusMessage, &iter);

    while((iter_type = dbus_message_iter_get_arg_type(&iter)) != DBUS_TYPE_INVALID)
    {
        if(iter_type == DBUS_TYPE_STRING)
        {
            dbus_message_iter_get_basic(&iter, &dbusValue);

            item.push_back(dbusValue.str);
        }

        dbus_message_iter_next(&iter);
    }

    unlock();

    return item;
}

int DBusConnector::getInt(DBusMessage *dbusMessage)
{
    int value;

    if(!isDBusConnectionAvailable() || dbusMessage == nullptr)
        return 0;

    if(getArgs(dbusMessage, DBUS_TYPE_INT32, &value, DBUS_TYPE_INVALID) == false)
        value = 0;

    return value;
}

DBusResponse *DBusConnector::getResponse(DBusMessage *dbusMessage)
{
    DBusMessageIter iter;
    DBusBasicValue dbusValue;
    DBusResponse *response = nullptr;
    std::string value = "";
    int iter_type;

    if(!isDBusConnectionAvailable() || dbusMessage == nullptr)
        return response;

    lock();

    dbus_message_iter_init(dbusMessage, &iter);

    while((iter_type = dbus_message_iter_get_arg_type(&iter)) != DBUS_TYPE_INVALID)
    {
        if(iter_type == DBUS_TYPE_STRING)
        {
            dbus_message_iter_get_basic(&iter, &dbusValue);

            value += dbusValue.str;
        }

        dbus_message_iter_next(&iter);
    }

    response = new DBusResponse();

    response->fromString(value);

    unlock();

    return response;
}

void DBusConnector::unreferenceResponse(DBusResponse *response)
{
    if(response == nullptr)
        return;

    delete response;
}

void DBusConnector::unreferenceMessage(DBusMessage *dbusMessage)
{
    if(!isDBusConnectionAvailable() || dbusMessage == nullptr)
        return;

    lock();

    dbus_connection_flush(dbusConnection);

    dbus_message_unref(dbusMessage);

    unlock();
}

std::string DBusConnector::stringToUTF8(const std::string &str)
{
    return iconvString(iconvToUTF8, str);
}

std::string DBusConnector::stringToLocale(const std::string &str)
{
    return iconvString(iconvToLocale, str);
}

std::string DBusConnector::iconvString(iconv_t iconverter, std::string str)
{
    if(isEncodingNeeded == true)
    {
        size_t inLen = str.size();
        size_t outLen;
        char *bufferOut = nullptr, *strOut =  nullptr;

        iconvIn = (char *)str.c_str();

        if(inLen > ICONV_OUT_LEN)
        {
            bufferOut = (char *)malloc(inLen * 2);

            strOut = bufferOut;
            outLen = inLen * 2;
        }
        else
        {
            strOut = iconvOut;
            bufferOut = iconvOut;
            outLen = ICONV_OUT_LEN;
        }

        memset(bufferOut, 0, outLen);

        iconv(iconverter, &iconvIn, &inLen, &bufferOut, &outLen);
        
        str = strOut;

        if(inLen > ICONV_OUT_LEN)
            free(bufferOut);
     }
    
    return str;
}

void DBusConnector::lock()
{
    if(mutex.try_lock_for(std::chrono::seconds(MUTEX_LOCK_TIMEOUT_SECONDS)) == false)
        throw(DBusConnectorException("DBusConnector: Failed to lock resource. D-Bus operation in progress."));
}

void DBusConnector::unlock()
{
    mutex.unlock();
}

// DBusResponse

DBusResponse::DBusResponse()
{
    clear();
}

DBusResponse::~DBusResponse()
{
}

void DBusResponse::clear()
{
    response = "";

    data.clear();
}

void DBusResponse::setResponse(const std::string &value)
{
    response = value;
}

std::string DBusResponse::getResponse()
{
    return response;
}

void DBusResponse::add(Item item)
{
    data.push_back(item);
}

void DBusResponse::addToItem(Item &item, const std::string &key, const std::string &value)
{
    item.insert(std::make_pair(key, value));
}

DBusResponse::Item DBusResponse::getItem(int row)
{
    if(row < 0 || row > data.size() - 1)
        return Item();

    return data[row];
}

std::string DBusResponse::getItemValue(Item item, const std::string &key)
{
    std::string value = "";

    ItemIterator it = item.find(key);

    if(it != item.end())
        value = it->second;

    return value;
}

int DBusResponse::rows()
{
    return data.size();
}

int DBusResponse::items(int row)
{
    return getItem(row).size();
}

DBusResponse::Iterator DBusResponse::begin()
{
    return data.begin();
}

DBusResponse::Iterator DBusResponse::end()
{
    return data.end();
}

DBusResponse::ItemIterator DBusResponse::begin(Item item)
{
    return item.begin();
}

DBusResponse::ItemIterator DBusResponse::end(Item item)
{
    return item.end();
}

std::string DBusResponse::itemKey(DBusResponse::ItemIterator it)
{
    return it->first;
}

std::string DBusResponse::itemValue(DBusResponse::ItemIterator it)
{
    return it->second;
}

bool DBusResponse::fromString(const std::string &str)
{
    bool responseFound = false, itemFound = false;
    std::vector<std::string> row, item, value;

    if(str.empty())
        return false;

    clear();

    row = split(str, ROW_SEPARATOR);

    for(std::string r : row)
    {
        item = split(r, ITEM_SEPARATOR);

        Item dataItem;

        for(std::string i : item)
        {
            value = split(i, VALUE_SEPARATOR);

            if(value.size() > 0)
            {
                if(value[0] == RESPONSE_KEY)
                {
                    response = value[1];

                    responseFound = true;
                }
                else
                {
                    if(value.size() == 2)
                        addToItem(dataItem, value[0], value[1]);
                    else
                        addToItem(dataItem, value[0], "");

                    itemFound = true;
                }
            }
        }

        if(dataItem.size() > 0)
            add(dataItem);
    }

    return responseFound & itemFound;
}

std::string DBusResponse::toString()
{
    std::string value = "";

    value = RESPONSE_KEY;
    value += VALUE_SEPARATOR;
    value += response;
    value += ITEM_SEPARATOR;
    value += ROW_SEPARATOR;

    for(int i = 0; i < data.size(); i++)
    {
        Item item = getItem(i);

        for(ItemIterator it = item.begin(); it != item.end(); it++)
        {
            value += it->first;
            value += VALUE_SEPARATOR;
            value += it->second;
            value += ITEM_SEPARATOR;
        }

        value += ROW_SEPARATOR;
    }

    return value;
}

std::vector<std::string> DBusResponse::split(const std::string &str, const std::string &delimeter)
{
    std::vector<std::string> items;
    int pos = 0, beg = 0;

    while((beg = str.find(delimeter, pos)) < str.size())
    {
        items.push_back(str.substr(pos, beg - pos));

        pos = beg + delimeter.size();
    }

    if(pos < str.size())
        items.push_back(str.substr(pos));

    return items;
}
