/*
 * cipherdatabase.cpp
 *
 * This file is part of the AirVPN Suite for Linux and macOS.
 * Copyright (C) 2019-2022 AirVPN (support@airvpn.org) / https://airvpn.org
 *
 * Developed by ProMIND
 *
 * This is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with the AirVPN Suite. If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "include/cipherdatabase.hpp"
#include "include/airvpntools.hpp"

std::map<std::string, int> CipherDatabase::tlsCipherDB = {};
std::map<std::string, int> CipherDatabase::tlsSuiteCipherDB = {};
std::map<std::string, int> CipherDatabase::dataCipherDB = {};

CipherDatabase::CipherDatabase()
{
}

CipherDatabase::~CipherDatabase()
{
}

void CipherDatabase::reset()
{
    tlsCipherDB.clear();
    tlsSuiteCipherDB.clear();
    dataCipherDB.clear();

    lastCode = 0;
}

void CipherDatabase::addCipherString(const std::string &ciphers, CipherType type)
{
    std::vector<std::string> cipher = AirVPNTools::split(ciphers, ":");

    for(std::string c : cipher)
        addCipher(c, type);
}

int CipherDatabase::addCipher(const std::string &cipher, CipherType type)
{
    int code = CIPHER_NOT_FOUND;

    if(cipher.empty())
        return INVALID_CIPHER;

    std::map<std::string, int> *cipherDB = getCipherDB(type);

    code = getCode(cipher, type);

    if(code == CIPHER_NOT_FOUND)
    {
        lastCode++;

        code = lastCode;

        cipherDB->insert(std::make_pair(cipher, code));
    }

    return code;
}

int CipherDatabase::getCode(const std::string &cipher, CipherType type)
{
    int code = CIPHER_NOT_FOUND;
    std::map<std::string, int> *cipherDB = getCipherDB(type);

    std::map<std::string, int>::iterator it = cipherDB->find(cipher);

    if(it != cipherDB->end())
        code = it->second;

    return code;
}

std::vector<int> CipherDatabase::getCodeArrayList(const std::string &ciphers, CipherType type)
{
    int code;
    std::vector<int> cipherList;

    if(ciphers.empty())
        return cipherList;

    std::vector<std::string> cipher = AirVPNTools::split(ciphers, ":");

    for(std::string c : cipher)
    {
        if(!c.empty())
        {
            code = getCode(c, type);

            if(code != CIPHER_NOT_FOUND)
                cipherList.push_back(code);
            else
            {
                code = addCipher(c, type);

                if(code != CIPHER_NOT_FOUND)
                    cipherList.push_back(code);
            }
        }
    }

    return cipherList;
}

std::string CipherDatabase::getMatchingCiphers(const std::string &pattern, CipherType type)
{
    std::string ciphers = "";
    std::map<std::string, int> *cipherDB = getCipherDB(type);

    for(std::map<std::string, int>::iterator it = cipherDB->begin(); it != cipherDB->end(); it++)
    {
        if(it->first.find(pattern) != std::string::npos)
        {
            if(!ciphers.empty())
                ciphers += ":";

            ciphers += it->first;
        }
    }

    return ciphers;
}

std::string CipherDatabase::getCipherNameList(CipherType type, const std::vector<int> &cipher)
{
    std::string cipherString = "", c;

    for(int code : cipher)
    {
        c = getCipher(code, type);
        
        if(!c.empty())
        {
            if(!cipherString.empty())
                cipherString += ":";

            cipherString += c;
        }
    }

    return cipherString;
}

std::vector<int> CipherDatabase::getMatchingCiphersArrayList(const std::string &pattern, CipherType type)
{
    std::vector<int> cipherList;
    std::map<std::string, int> *cipherDB = getCipherDB(type);

    for(std::map<std::string, int>::iterator it = cipherDB->begin(); it != cipherDB->end(); it++)
    {
        if(it->first.find(pattern) != std::string::npos)
            cipherList.push_back(it->second);
    }

    return cipherList;
}

std::string CipherDatabase::getCipher(int code, CipherType type)
{
    int val;
    std::string cipher = "";
    std::map<std::string, int> *cipherDB = getCipherDB(type);

    for(std::map<std::string, int>::iterator it = cipherDB->begin(); it != cipherDB->end(); it++)
    {
        val = it->second;

        if(code == val)
            cipher = it->first;
    }

    return cipher;
}

std::map<std::string, int> *CipherDatabase::getCipherDB(CipherType type)
{
    std::map<std::string, int> *cipherDB = nullptr;

    switch(type)
    {
        case TLS:
        {
            cipherDB = &tlsCipherDB;
        }
        break;

        case TLS_SUITE:
        {
            cipherDB = &tlsSuiteCipherDB;
        }
        break;

        case DATA:
        {
            cipherDB = &dataCipherDB;
        }
        break;

        default:
        {
            cipherDB = &dataCipherDB;
        }
        break;
    }

    return cipherDB;
}
