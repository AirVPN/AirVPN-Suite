/*
 * netfilter.cpp
 *
 * This file is part of the AirVPN Suite for Linux and macOS.
 * Copyright (C) 2019-2023 AirVPN (support@airvpn.org) / https://airvpn.org
 *
 * Developed by ProMIND
 *
 * This is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with the AirVPN Suite. If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <fstream>
#include <sstream>
#include <cstring>
#include <iterator>
#include <vector>
#include <algorithm>
#include <regex>
#include <unistd.h>
#include <stdio.h>
#include <sys/stat.h>
#include "include/netfilter.hpp"
#include "include/execproc.h"

NetFilter::NetFilter(const std::string &workdir, Mode mode)
{
    if(workdir == "")
        throw(NetFilterException("No working directory provided for object NetFilter"));

    if(mode == Mode::UNKNOWN)
        throw(NetFilterException("Unknown mode cannot be used for NetFilter construction"));

    workingDirectory = workdir;

    firewalldAvailable = false;
    ufwAvailable = false;
    nftablesAvailable = false;
    iptablesAvailable = false;
    iptablesLegacy = false;
    pfAvailable = false;
    pfNeedsFullFlush = true;
    networkLockEnabled = false;

    itemList.clear();

    initSystemType = AirVPNTools::getInitSystemType();

    loopbackInterface = "";

    clearIgnoredInterfaces();

    charBuffer = (char *)malloc(charBufferSize);

    if(charBuffer == NULL)
        throw(NetFilterException("Cannot allocate memory for NetFilter buffer"));

    firewalldAvailable = checkService("firewalld");

    ufwAvailable = checkService("ufw");

    // nftables

    get_exec_path(nftBinary, binpath);

    if(strcmp(binpath, "") != 0)
        nftablesAvailable = true;
    else
        nftablesAvailable = false;

    // iptables

    get_exec_path(iptablesLegacyBinary, binpath);

    if(strcmp(binpath, "") != 0)
    {
        iptablesBinary = iptablesLegacyBinary;
        iptablesSaveBinary = iptablesLegacySaveBinary;
        iptablesRestoreBinary = iptablesLegacyRestoreBinary;

        ip6tablesBinary = ip6tablesLegacyBinary;
        ip6tablesSaveBinary = ip6tablesLegacySaveBinary;
        ip6tablesRestoreBinary = ip6tablesLegacyRestoreBinary;

        iptablesAvailable = true;
        iptablesLegacy = true;
    }
    else
    {
        get_exec_path(iptablesCurrentBinary, binpath);

        if(strcmp(binpath, "") != 0)
        {
            iptablesBinary = iptablesCurrentBinary;
            iptablesSaveBinary = iptablesCurrentSaveBinary;
            iptablesRestoreBinary = iptablesCurrentRestoreBinary;

            ip6tablesBinary = ip6tablesCurrentBinary;
            ip6tablesSaveBinary = ip6tablesCurrentSaveBinary;
            ip6tablesRestoreBinary = ip6tablesCurrentRestoreBinary;

            iptablesAvailable = true;
            iptablesLegacy = false;
        }
        else
        {
            iptablesAvailable = false;
            iptablesLegacy = false;
        }
    }

    iptableFilterAvailable = false;
    iptableNatAvailable = false;
    iptableMangleAvailable = false;
    iptableSecurityAvailable = false;
    iptableRawAvailable = false;

    ip6tableFilterAvailable = false;
    ip6tableNatAvailable = false;
    ip6tableMangleAvailable = false;
    ip6tableSecurityAvailable = false;
    ip6tableRawAvailable = false;

    // pf

    get_exec_path(pfctlBinary, binpath);

    if(strcmp(binpath, "") != 0)
        pfAvailable = true;
    else
        pfAvailable = false;

    localNetwork = new LocalNetwork();

    if(localNetwork == nullptr)
        throw(NetFilterException("Cannot create a LocalNetwork object for NetFilter"));

    setMode(mode);
}

NetFilter::~NetFilter()
{
    if(filterMode != Mode::OFF)
        restore();

    if(charBuffer != NULL)
        free(charBuffer);
    
    if(localNetwork != nullptr)
        delete localNetwork;

    for(Item *i : itemList)
        delete i;
}

bool NetFilter::systemBackupExists()
{
    bool retval = false;
    std::string fileName;

    if(workingDirectory == "")
        return false;

    fileName = workingDirectory + "/" + nftablesSaveFile;

    if(access(fileName.c_str(), F_OK) == 0)
        retval = true;

    fileName = workingDirectory + "/" + iptablesSaveFile;

    if(access(fileName.c_str(), F_OK) == 0)
        retval = true;

    fileName = workingDirectory + "/" + ip6tablesSaveFile;

    if(access(fileName.c_str(), F_OK) == 0)
        retval = true;

    fileName = workingDirectory + "/" + pfSaveFile;

    if(access(fileName.c_str(), F_OK) == 0)
        retval = true;

    return retval;
}

bool NetFilter::init()
{
    bool retval = false;
    std::ostringstream os;

    if(filterMode == Mode::OFF)
    {
        global_log("Network filter and lock are disabled");

        networkLockEnabled = false;

        return true;
    }

    if(filterMode == Mode::AUTO || filterMode == Mode::UNKNOWN)
    {
        networkLockEnabled = false;

        return false;
    }

    os.str("");

    os << "Network filter and lock are using " << getModeDescription();

    global_log(os.str());

#if defined(__linux__) && !defined(__ANDROID__)

    if(filterMode == Mode::NFTABLES)
    {
        AirVPNTools::loadLinuxModule("nf_tables");
    }
    else if(filterMode == Mode::IPTABLES)
    {
        iptableFilterAvailable = AirVPNTools::loadLinuxModule("iptable_filter");
        iptableNatAvailable = AirVPNTools::loadLinuxModule("iptable_nat");
        iptableMangleAvailable = AirVPNTools::loadLinuxModule("iptable_mangle");
        iptableSecurityAvailable = AirVPNTools::loadLinuxModule("iptable_security");
        iptableRawAvailable = AirVPNTools::loadLinuxModule("iptable_raw");

        // This is needed to force the initial loading of rules at boot in distributions running under kernel 5.15.x and iptables 1,8.7

        commitAllowRule(IPFamily::IPv4, NetFilter::Hook::OUTPUT, "", NetFilter::Protocol::ANY, "", 0, "169.254.68.69", 0);
        commitRemoveAllowRule(IPFamily::IPv4, NetFilter::Hook::OUTPUT, "", NetFilter::Protocol::ANY, "", 0, "169.254.68.69", 0);

        if(localNetwork->isIPv6Enabled() == true)
        {
            ip6tableFilterAvailable = AirVPNTools::loadLinuxModule("ip6table_filter");
            ip6tableNatAvailable = AirVPNTools::loadLinuxModule("ip6table_nat");
            ip6tableMangleAvailable = AirVPNTools::loadLinuxModule("ip6table_mangle");
            ip6tableSecurityAvailable = AirVPNTools::loadLinuxModule("ip6table_security");
            ip6tableRawAvailable = AirVPNTools::loadLinuxModule("ip6table_raw");

            commitAllowRule(IPFamily::IPv6, NetFilter::Hook::OUTPUT, "", NetFilter::Protocol::ANY, "", 0, "fc00::1", 0);
            commitRemoveAllowRule(IPFamily::IPv6, NetFilter::Hook::OUTPUT, "", NetFilter::Protocol::ANY, "", 0, "fc00::1", 0);
        }
    }

    if(isFirewalldRunning())
    {
        os.str("");

        os << "WARNING: firewalld is running on this system and may interfere with network filter and lock";

        global_log(os.str());
    }

    if(isUfwRunning())
    {
        os.str("");

        os << "WARNING: ufw is running on this system and may interfere with network filter and lock";

        global_log(os.str());
    }

#endif

    switch(filterMode)
    {
        case Mode::NFTABLES:
        {
            retval = nftablesSave();
        }
        break;

        case Mode::IPTABLES:
        {
            retval = iptablesSave(IPFamily::IPv4);

            if(retval == true)
                retval = iptablesSave(IPFamily::IPv6);
            else
                retval = false;
        }
        break;

        case Mode::PF:
        {
            pfEnable();

            retval = pfSave();
        }
        break;

        default:
        {
            retval = false;
        }
        break;
    }

    networkLockEnabled = false;

    return retval;
}

bool NetFilter::restore()
{
    bool retval = true;
    std::string fileName;

    if(workingDirectory == "")
        return false;

    fileName = workingDirectory + "/" + nftablesSaveFile;

    if(access(fileName.c_str(), F_OK) == 0)
        retval &= nftablesRestore();

    fileName = workingDirectory + "/" + iptablesSaveFile;

    if(access(fileName.c_str(), F_OK) == 0)
        retval &= iptablesRestore(IPFamily::IPv4);

    fileName = workingDirectory + "/" + ip6tablesSaveFile;

    if(access(fileName.c_str(), F_OK) == 0)
        retval &= iptablesRestore(IPFamily::IPv6);

    fileName = workingDirectory + "/" + pfSaveFile;

    if(access(fileName.c_str(), F_OK) == 0)
        retval &= pfRestore();

    networkLockEnabled = false;

    return retval;
}

void NetFilter::setup(const std::string &loopbackIface)
{
    loopbackInterface = loopbackIface;

    initItemList(loopbackIface);
}

bool NetFilter::commitRules()
{
    bool retval = false;

    switch(filterMode)
    {
        case Mode::NFTABLES:
        {
            retval = nftablesCommit();
        }
        break;

        case Mode::IPTABLES:
        {
            retval = iptablesCommit(IPFamily::IPv4);

            if(retval == true && localNetwork->isIPv6Enabled() == true)
                retval = iptablesCommit(IPFamily::IPv6);
        }
        break;

        case Mode::PF:
        {
            retval = pfCommit();
        }
        break;

        default:
        {
            retval = false;
        }
        break;
    }

    networkLockEnabled = true;

    return retval;
}

bool NetFilter::addAllowRule(IPFamily ipFamily, Hook hook, const std::string &interface, Protocol protocol, const std::string &sourceIP, int sourcePort, const std::string &destinationIP, int destinationPort)
{
    Rule *r;
    int pos = ITEM_ERROR;

    checkFilterTableChain(ipFamily, hook);
    
    r = createSessionFilterRule(ipFamily, hook, interface, protocol, sourceIP, sourcePort, destinationIP, destinationPort);

    r->setCounter(true);
    r->setPolicy(Policy::ACCEPT);

    pos = insert(*r);
    
    delete r;

    return (pos != ITEM_ERROR);
}

bool NetFilter::addRejectRule(IPFamily ipFamily, Hook hook, const std::string &interface, Protocol protocol, const std::string &sourceIP, int sourcePort, const std::string &destinationIP, int destinationPort)
{
    int pos = ITEM_ERROR;
    Rule *r;

    checkFilterTableChain(ipFamily, hook);
    
    r = createSessionFilterRule(ipFamily, hook, interface, protocol, sourceIP, sourcePort, destinationIP, destinationPort);

    r->setCounter(true);
    r->setPolicy(Policy::DROP);

    pos = insert(*r);

    delete r;
    
    return (pos != ITEM_ERROR);
}

bool NetFilter::commitAllowRule(IPFamily ipFamily, Hook hook, const std::string &interface, Protocol protocol, const std::string &sourceIP, int sourcePort, const std::string &destinationIP, int destinationPort)
{
    int pos = ITEM_ERROR;
    Rule *r;

    checkFilterTableChain(ipFamily, hook);
    
    r = createSessionFilterRule(ipFamily, hook, interface, protocol, sourceIP, sourcePort, destinationIP, destinationPort);

    r->setCounter(true);
    r->setPolicy(Policy::ACCEPT);

    pos = commit(*r);

    delete r;
    
    return (pos != ITEM_ERROR);
}

bool NetFilter::commitRejectRule(IPFamily ipFamily, Hook hook, const std::string &interface, Protocol protocol, const std::string &sourceIP, int sourcePort, const std::string &destinationIP, int destinationPort)
{
    int pos = ITEM_ERROR;
    Rule *r;
    std::string rule;

    checkFilterTableChain(ipFamily, hook);
    
    r = createSessionFilterRule(ipFamily, hook, interface, protocol, sourceIP, sourcePort, destinationIP, destinationPort);

    r->setCounter(true);
    r->setPolicy(Policy::DROP);

    pos = commit(*r);

    delete r;

    return (pos != ITEM_ERROR);
}

bool NetFilter::commitRemoveAllowRule(IPFamily ipFamily, Hook hook, const std::string &interface, Protocol protocol, const std::string &sourceIP, int sourcePort, const std::string &destinationIP, int destinationPort)
{
    int pos = ITEM_ERROR;
    Rule *r;
    std::string rule;

    checkFilterTableChain(ipFamily, hook);
    
    r = createSessionFilterRule(ipFamily, hook, interface, protocol, sourceIP, sourcePort, destinationIP, destinationPort);

    r->setCounter(true);
    r->setPolicy(Policy::ACCEPT);

    pos = remove(*r);

    if(pos != ITEM_NOT_FOUND)
        delete r;

    networkLockEnabled = true;

    return (pos != ITEM_NOT_FOUND);
}

bool NetFilter::commitRemoveRejectRule(IPFamily ipFamily, Hook hook, const std::string &interface, Protocol protocol, const std::string &sourceIP, int sourcePort, const std::string &destinationIP, int destinationPort)
{
    int pos = ITEM_ERROR;
    Rule *r;
    std::string rule;

    checkFilterTableChain(ipFamily, hook);
    
    r = createSessionFilterRule(ipFamily, hook, interface, protocol, sourceIP, sourcePort, destinationIP, destinationPort);

    r->setCounter(true);
    r->setPolicy(Policy::DROP);

    pos = remove(*r);

    if(pos != ITEM_NOT_FOUND)
        delete r;

    networkLockEnabled = true;

    return (pos != ITEM_NOT_FOUND);
}

bool NetFilter::setMode(Mode mode)
{
    bool retval = false;
    std::string errorMessage;

    switch(mode)
    {
        case Mode::AUTO:
        {
            if(nftablesAvailable == true)
            {
                filterMode = Mode::NFTABLES;

                retval = true;
            }
            else if(iptablesAvailable == true)
            {
                filterMode = Mode::IPTABLES;

                retval = true;
            }
            else if(pfAvailable == true)
            {
                filterMode = Mode::PF;

                retval = true;
            }
            else
            {
                filterMode = Mode::OFF;

                throw(NetFilterException("No usable firewall found in this system"));
            }
        }
        break;

        case Mode::NFTABLES:
        {
            if(nftablesAvailable == true)
            {
                filterMode = Mode::NFTABLES;

                retval = true;
            }
            else
            {
                filterMode = Mode::OFF;

                throw(NetFilterException("nftables is not available in this system"));
            }
        }
        break;

        case Mode::IPTABLES:
        {
            if(iptablesAvailable == true)
            {
                filterMode = Mode::IPTABLES;

                retval = true;
            }
            else
            {
                filterMode = Mode::OFF;

                throw(NetFilterException("iptables or iptables legacy is not available in this system"));
            }
        }
        break;

        case Mode::PF:
        {
            if(pfAvailable == true)
            {
                filterMode = Mode::PF;

                retval = true;
            }
            else
            {
                filterMode = Mode::OFF;

                throw(NetFilterException("pf is not available in this system"));
            }
        }
        break;

        case Mode::OFF:
        {
            filterMode = Mode::OFF;

            retval = true;
        }
        break;

        default:
        {
            filterMode = Mode::OFF;

            throw(NetFilterException("Unknown firewall system"));
        }
        break;
    }

    return retval;
}

NetFilter::Mode NetFilter::getMode()
{
    return filterMode;
}

std::string NetFilter::getModeDescription(NetFilter::Mode mode)
{
    std::string description;

    switch(mode)
    {
        case Mode::AUTO:
        {
            if(nftablesAvailable == true)
                description = "nftables";
            else if(iptablesAvailable == true)
                description = iptablesBinary;
            else if(pfAvailable == true)
                description = "pf";
            else
                description = "unknown";
        }
        break;

        case Mode::NFTABLES:
        {
            description = "nftables";
        }
        break;

        case Mode::IPTABLES:
        {
            description = iptablesBinary;
        }
        break;

        case Mode::PF:
        {
            description = "pf";
        }
        break;

        default:
        {
            description = "unknown";
        }
        break;
    }

    return description;
}

std::string NetFilter::getModeDescription()
{
    return getModeDescription(filterMode);
}

bool NetFilter::addIgnoredInterface(const std::string &interface)
{
    if(std::find(ignoredInterface.begin(), ignoredInterface.end(), interface) != ignoredInterface.end())
        return false;

    ignoredInterface.push_back(interface);

    switch(filterMode)
    {
        case Mode::NFTABLES:
        {
        }
        break;

        case Mode::IPTABLES:
        {
        }
        break;

        case Mode::PF:
        {
            pfAddIgnoredInterfaces();
        }
        break;

        default:
        {
        }
        break;
    }

    return true;
}

void NetFilter::clearIgnoredInterfaces()
{
    ignoredInterface.clear();
}

bool NetFilter::checkService(const std::string &name)
{
    bool retval = false;

    if(initSystemType == AirVPNTools::InitSystemType::Systemd)
    {
        get_exec_path(systemctlBinary, binpath);

        if(strcmp(binpath, "") != 0)
        {
            if(execute_process(NULL, NULL, binpath, "is-active", "--quiet", name.c_str(), NULL) == 0)
                retval = true;
            else
                retval = false;
        }
        else
            retval = false;
    }
    else if(initSystemType == AirVPNTools::InitSystemType::SystemVinit)
    {
        char init_script[32], buf[128];

        strcpy(init_script, "/etc/init.d/");
        strcat(init_script, name.c_str());

        get_exec_path(shellBinary, binpath);

        if(execute_process(NULL, buf, binpath, init_script, "status", NULL) == 0)
            retval = true;
        else
            retval = false;
    }
    else
        retval = false;

    return retval;
}

bool NetFilter::isFirewalldRunning()
{
    return firewalldAvailable;
}

bool NetFilter::isUfwRunning()
{
    return ufwAvailable;
}

bool NetFilter::isIPTablesLegacy()
{
    return iptablesLegacy;
}

bool NetFilter::isNetworkLockAvailable()
{
    bool networkLockAvailable = true;

    if(filterMode == NetFilter::Mode::OFF || filterMode == NetFilter::Mode::UNKNOWN)
        networkLockAvailable = false;

    return networkLockAvailable;
}

bool NetFilter::isNetworkLockEnabled()
{
    return networkLockEnabled;
}

bool NetFilter::readFile(const std::string &fname, char *buffer, int size)
{
    bool retval = false;
    FILE *fp;
    struct stat fnamestat;
    int fsize;
    size_t nc;

    if(buffer == NULL || size <= 0)
        return false;

    if(access(fname.c_str(), F_OK) == 0)
    {
        stat(fname.c_str(), &fnamestat);

        fsize = fnamestat.st_size;

        if(fsize < size)
        {
            fp = fopen(fname.c_str(), "r");

            nc = fread(buffer, 1, fsize, fp);

            fclose(fp);

            if(nc > 0)
            {
                buffer[fsize] = '\0';

                retval = true;
            }
            else
                retval = false;
        }
        else
            retval = false;
    }
    else
        retval = false;

    return retval;
}

void NetFilter::initItemList(const std::string &loopbackIface)
{
    Chain *chain = nullptr;
    Rule *rule = nullptr;

    itemList.clear();

    // IPv4

    addItem(new Table(Item::FAMILY_IPV4, Table::MANGLE, Item::TARGET_NFTABLES));
    addItem(new Table(Item::FAMILY_IPV4, Table::NAT, Item::TARGET_NFTABLES));
    addItem(new Table(Item::FAMILY_IPV4, Table::FILTER, Item::TARGET_NFTABLES));

    // IPv4 Mangle

    chain = new Chain(Item::FAMILY_IPV4, Table::MANGLE, Chain::PREROUTING, Item::TARGET_NFTABLES);

    chain->setType(Chain::TYPE_FILTER);
    chain->setHook(Hook::PREROUTING);
    chain->setPriority(Chain::PRIORITY_MANGLE);
    chain->setPolicy(Policy::ACCEPT);

    addItem(chain);

    chain = new Chain(Item::FAMILY_IPV4, Table::MANGLE, Chain::INPUT, Item::TARGET_NFTABLES);

    chain->setType(Chain::TYPE_FILTER);
    chain->setHook(Hook::INPUT);
    chain->setPriority(Chain::PRIORITY_MANGLE);
    chain->setPolicy(Policy::ACCEPT);

    addItem(chain);

    chain = new Chain(Item::FAMILY_IPV4, Table::MANGLE, Chain::FORWARD, Item::TARGET_NFTABLES);

    chain->setType(Chain::TYPE_FILTER);
    chain->setHook(Hook::FORWARD);
    chain->setPriority(Chain::PRIORITY_MANGLE);
    chain->setPolicy(Policy::ACCEPT);

    addItem(chain);

    chain = new Chain(Item::FAMILY_IPV4, Table::MANGLE, Chain::OUTPUT, Item::TARGET_NFTABLES);

    chain->setType(Chain::TYPE_FILTER);
    chain->setHook(Hook::OUTPUT);
    chain->setPriority(Chain::PRIORITY_MANGLE);
    chain->setPolicy(Policy::ACCEPT);

    addItem(chain);

    chain = new Chain(Item::FAMILY_IPV4, Table::MANGLE, Chain::POSTROUTING, Item::TARGET_NFTABLES);

    chain->setType(Chain::TYPE_FILTER);
    chain->setHook(Hook::POSTROUTING);
    chain->setPriority(Chain::PRIORITY_MANGLE);
    chain->setPolicy(Policy::ACCEPT);

    addItem(chain);

    if(iptableMangleAvailable == true)
    {
        rule = new Rule(Item::FAMILY_IPV4, Table::MANGLE, Chain::NONE, Item::TARGET_IPTABLES);

        rule->setStatement("*mangle");
        rule->setPolicy(Policy::NONE);

        addItem(rule);

        rule = new Rule(Item::FAMILY_IPV4, Table::MANGLE, Chain::NONE, Item::TARGET_IPTABLES);

        rule->setStatement(":PREROUTING ACCEPT [0:0]");
        rule->setPolicy(Policy::NONE);

        addItem(rule);

        rule = new Rule(Item::FAMILY_IPV4, Table::MANGLE, Chain::NONE, Item::TARGET_IPTABLES);

        rule->setStatement(":INPUT ACCEPT [0:0]");
        rule->setPolicy(Policy::NONE);

        addItem(rule);

        rule = new Rule(Item::FAMILY_IPV4, Table::MANGLE, Chain::NONE, Item::TARGET_IPTABLES);

        rule->setStatement(":FORWARD ACCEPT [0:0]");
        rule->setPolicy(Policy::NONE);

        addItem(rule);

        rule = new Rule(Item::FAMILY_IPV4, Table::MANGLE, Chain::NONE, Item::TARGET_IPTABLES);

        rule->setStatement(":OUTPUT ACCEPT [0:0]");
        rule->setPolicy(Policy::NONE);

        addItem(rule);

        rule = new Rule(Item::FAMILY_IPV4, Table::MANGLE, Chain::NONE, Item::TARGET_IPTABLES);

        rule->setStatement(":POSTROUTING ACCEPT [0:0]");
        rule->setPolicy(Policy::NONE);

        addItem(rule);

        rule = new Rule(Item::FAMILY_IPV4, Table::MANGLE, Chain::NONE, Item::TARGET_IPTABLES);

        rule->setStatement("COMMIT");
        rule->setPolicy(Policy::NONE);

        addItem(rule);
    }

    // IPv4 NAT

    chain = new Chain(Item::FAMILY_IPV4, Table::NAT, Chain::PREROUTING, Item::TARGET_NFTABLES);

    chain->setType(Chain::TYPE_NAT);
    chain->setHook(Hook::PREROUTING);
    chain->setPriority(Chain::PRIORITY_DSTNAT);
    chain->setPolicy(Policy::ACCEPT);

    addItem(chain);

    chain = new Chain(Item::FAMILY_IPV4, Table::NAT, Chain::INPUT, Item::TARGET_NFTABLES);

    chain->setType(Chain::TYPE_NAT);
    chain->setHook(Hook::INPUT);
    chain->setPriority(Chain::PRIORITY_SRCNAT);
    chain->setPolicy(Policy::ACCEPT);

    addItem(chain);

    chain = new Chain(Item::FAMILY_IPV4, Table::NAT, Chain::OUTPUT, Item::TARGET_NFTABLES);

    chain->setType(Chain::TYPE_NAT);
    chain->setHook(Hook::OUTPUT);
    chain->setPriority(Chain::PRIORITY_DSTNAT);
    chain->setPolicy(Policy::ACCEPT);

    addItem(chain);

    chain = new Chain(Item::FAMILY_IPV4, Table::NAT, Chain::POSTROUTING, Item::TARGET_NFTABLES);

    chain->setType(Chain::TYPE_NAT);
    chain->setHook(Hook::POSTROUTING);
    chain->setPriority(Chain::PRIORITY_SRCNAT);
    chain->setPolicy(Policy::ACCEPT);

    addItem(chain);

    if(iptableNatAvailable == true)
    {
        rule = new Rule(Item::FAMILY_IPV4, Table::NAT, Chain::NONE, Item::TARGET_IPTABLES);

        rule->setStatement("*nat");
        rule->setPolicy(Policy::NONE);

        addItem(rule);

        rule = new Rule(Item::FAMILY_IPV4, Table::NAT, Chain::NONE, Item::TARGET_IPTABLES);

        rule->setStatement(":PREROUTING ACCEPT [0:0]");
        rule->setPolicy(Policy::NONE);

        addItem(rule);

        rule = new Rule(Item::FAMILY_IPV4, Table::NAT, Chain::NONE, Item::TARGET_IPTABLES);

        rule->setStatement(":INPUT ACCEPT [0:0]");
        rule->setPolicy(Policy::NONE);

        addItem(rule);

        rule = new Rule(Item::FAMILY_IPV4, Table::NAT, Chain::NONE, Item::TARGET_IPTABLES);

        rule->setStatement(":OUTPUT ACCEPT [0:0]");
        rule->setPolicy(Policy::NONE);

        addItem(rule);

        rule = new Rule(Item::FAMILY_IPV4, Table::NAT, Chain::NONE, Item::TARGET_IPTABLES);

        rule->setStatement(":POSTROUTING ACCEPT [0:0]");
        rule->setPolicy(Policy::NONE);

        addItem(rule);

        rule = new Rule(Item::FAMILY_IPV4, Table::NAT, Chain::NONE, Item::TARGET_IPTABLES);

        rule->setStatement("COMMIT");
        rule->setPolicy(Policy::NONE);

        addItem(rule);
    }

    // IPv4 Filter

    chain = new Chain(Item::FAMILY_IPV4, Table::FILTER, Chain::INPUT, Item::TARGET_NFTABLES);

    chain->setType(Chain::TYPE_FILTER);
    chain->setHook(Hook::INPUT);
    chain->setPriority(Chain::PRIORITY_FILTER);
    chain->setPolicy(Policy::DROP);

    addItem(chain);

    chain = new Chain(Item::FAMILY_IPV4, Table::FILTER, Chain::FORWARD, Item::TARGET_NFTABLES);

    chain->setType(Chain::TYPE_FILTER);
    chain->setHook(Hook::FORWARD);
    chain->setPriority(Chain::PRIORITY_FILTER);
    chain->setPolicy(Policy::DROP);

    addItem(chain);

    chain = new Chain(Item::FAMILY_IPV4, Table::FILTER, Chain::OUTPUT, Item::TARGET_NFTABLES);

    chain->setType(Chain::TYPE_FILTER);
    chain->setHook(Hook::OUTPUT);
    chain->setPriority(Chain::PRIORITY_FILTER);
    chain->setPolicy(Policy::DROP);

    addItem(chain);

    if(iptableFilterAvailable == true)
    {
        rule = new Rule(Item::FAMILY_IPV4, Table::FILTER, Chain::NONE, Item::TARGET_IPTABLES);

        rule->setStatement("*filter");
        rule->setPolicy(Policy::NONE);

        addItem(rule);

        rule = new Rule(Item::FAMILY_IPV4, Table::FILTER, Chain::NONE, Item::TARGET_IPTABLES);

        rule->setStatement(":INPUT DROP [0:0]");
        rule->setPolicy(Policy::NONE);

        addItem(rule);

        rule = new Rule(Item::FAMILY_IPV4, Table::FILTER, Chain::NONE, Item::TARGET_IPTABLES);

        rule->setStatement(":FORWARD DROP [0:0]");
        rule->setPolicy(Policy::NONE);

        addItem(rule);

        rule = new Rule(Item::FAMILY_IPV4, Table::FILTER, Chain::NONE, Item::TARGET_IPTABLES);

        rule->setStatement(":OUTPUT DROP [0:0]");
        rule->setPolicy(Policy::NONE);

        addItem(rule);
    }

    // PF policies

    rule = new Rule(Item::FAMILY_INET, Table::FILTER, Chain::NONE, Item::TARGET_PF);

    rule->setStatement("set block-policy drop");
    rule->setPolicy(Policy::NONE);

    addItem(rule);

    rule = new Rule(Item::FAMILY_INET, Table::FILTER, Chain::NONE, Item::TARGET_PF);

    rule->setStatement("set ruleset-optimization basic");
    rule->setPolicy(Policy::NONE);

    addItem(rule);

    rule = new Rule(Item::FAMILY_INET, Table::FILTER, Chain::NONE, Item::TARGET_PF);

    rule->setStatement("set skip on { " + loopbackIface + " }");
    rule->setPolicy(Policy::NONE);

    addItem(rule);

    rule = new Rule(Item::FAMILY_INET, Table::FILTER, Chain::NONE, Item::TARGET_PF);

    rule->setStatement("scrub in all");
    rule->setPolicy(Policy::NONE);

    addItem(rule);

    rule = new Rule(Item::FAMILY_INET, Table::FILTER, Chain::NONE, Item::TARGET_PF);

    rule->setStatement("block in all");
    rule->setPolicy(Policy::NONE);

    addItem(rule);

    rule = new Rule(Item::FAMILY_INET, Table::FILTER, Chain::NONE, Item::TARGET_PF);

    rule->setStatement("block out all");
    rule->setPolicy(Policy::NONE);

    addItem(rule);

    // Local IPv4 input

    rule = new Rule(Item::FAMILY_IPV4, Table::FILTER, Chain::INPUT, Item::TARGET_NFTABLES | Item::TARGET_IPTABLES);

    rule->setSourceInterface(loopbackIface);
    rule->setCounter(true);
    rule->setPolicy(Policy::ACCEPT);

    addItem(rule);

    // Accept IPv4 DHCP

    rule = new Rule(Item::FAMILY_IPV4, Table::FILTER, Chain::INPUT, Item::TARGET_ALL);

    rule->setSourceAddress({"255.255.255.255", IPFamily::IPv4, 32});
    rule->setCounter(true);
    rule->setPolicy(Policy::ACCEPT);

    addItem(rule);

    // Accept local IPv4 network

    rule = new Rule(Item::FAMILY_IPV4, Table::FILTER, Chain::INPUT, Item::TARGET_ALL);

    rule->setSourceAddress({"192.168.0.0", IPFamily::IPv4, 16});
    rule->setDestinationAddress({"192.168.0.0", IPFamily::IPv4, 16});
    rule->setCounter(true);
    rule->setPolicy(Policy::ACCEPT);

    addItem(rule);

    rule = new Rule(Item::FAMILY_IPV4, Table::FILTER, Chain::INPUT, Item::TARGET_ALL);

    rule->setSourceAddress({"10.0.0.0", IPFamily::IPv4, 8});
    rule->setDestinationAddress({"10.0.0.0", IPFamily::IPv4, 8});
    rule->setCounter(true);
    rule->setPolicy(Policy::ACCEPT);

    addItem(rule);

    rule = new Rule(Item::FAMILY_IPV4, Table::FILTER, Chain::INPUT, Item::TARGET_ALL);

    rule->setSourceAddress({"172.16.0.0", IPFamily::IPv4, 12});
    rule->setDestinationAddress({"172.16.0.0", IPFamily::IPv4, 12});
    rule->setCounter(true);
    rule->setPolicy(Policy::ACCEPT);

    addItem(rule);

    // IPv4 inbound ping
/*
    rule = new Rule(Item::FAMILY_IPV4, Table::FILTER, Chain::INPUT, Item::TARGET_NFTABLES);

    rule->setStatement("icmp type echo-request");
    rule->setCounter(true);
    rule->setPolicy(Policy::ACCEPT);

    addItem(rule);

    rule = new Rule(Item::FAMILY_IPV4, Table::FILTER, Chain::INPUT, Item::TARGET_IPTABLES);

    rule->setStatement("-p icmp -m icmp --icmp-type 8");
    rule->setCounter(true);
    rule->setPolicy(Policy::ACCEPT);

    addItem(rule);

    rule = new Rule(Item::FAMILY_IPV4, Table::FILTER, Chain::INPUT, Item::TARGET_PF);

    rule->setStatement("proto icmp");
    rule->setCounter(true);
    rule->setPolicy(Policy::ACCEPT);

    addItem(rule);
*/

    // Accept established sessions

    rule = new Rule(Item::FAMILY_IPV4, Table::FILTER, Chain::INPUT, Item::TARGET_NFTABLES);

    rule->setStatement("ct state related,established");
    rule->setCounter(true);
    rule->setPolicy(Policy::ACCEPT);

    addItem(rule);

    rule = new Rule(Item::FAMILY_IPV4, Table::FILTER, Chain::INPUT, Item::TARGET_IPTABLES);

    rule->setStatement("-m state --state RELATED,ESTABLISHED");
    rule->setCounter(true);
    rule->setPolicy(Policy::ACCEPT);

    addItem(rule);

    // Accept all tun interfaces

    rule = new Rule(Item::FAMILY_IPV4, Table::FILTER, Chain::INPUT, Item::TARGET_NFTABLES);

    rule->setSourceInterface("tun*");
    rule->setCounter(true);
    rule->setPolicy(Policy::ACCEPT);

    addItem(rule);

    rule = new Rule(Item::FAMILY_IPV4, Table::FILTER, Chain::INPUT, Item::TARGET_IPTABLES);

    rule->setSourceInterface("tun+");
    rule->setCounter(true);
    rule->setPolicy(Policy::ACCEPT);

    addItem(rule);

    // Drop everything else in input

    rule = new Rule(Item::FAMILY_IPV4, Table::FILTER, Chain::INPUT, Item::TARGET_NFTABLES | Item::TARGET_IPTABLES);

    rule->setCounter(true);
    rule->setPolicy(Policy::DROP);

    addItem(rule);

    // Accept TUN forward

    rule = new Rule(Item::FAMILY_IPV4, Table::FILTER, Chain::FORWARD, Item::TARGET_NFTABLES);

    rule->setSourceInterface("tun*");
    rule->setCounter(true);
    rule->setPolicy(Policy::ACCEPT);

    addItem(rule);

    rule = new Rule(Item::FAMILY_IPV4, Table::FILTER, Chain::FORWARD, Item::TARGET_IPTABLES);

    rule->setSourceInterface("tun+");
    rule->setCounter(true);
    rule->setPolicy(Policy::ACCEPT);

    addItem(rule);

    // Drop all the other forwarding

    rule = new Rule(Item::FAMILY_IPV4, Table::FILTER, Chain::FORWARD, Item::TARGET_NFTABLES | Item::TARGET_IPTABLES);

    rule->setCounter(true);
    rule->setPolicy(Policy::DROP);

    addItem(rule);

    // Local IPv4 output interface

    rule = new Rule(Item::FAMILY_IPV4, Table::FILTER, Chain::OUTPUT, Item::TARGET_NFTABLES | Item::TARGET_IPTABLES);

    rule->setDestinationInterface(loopbackIface);
    rule->setCounter(true);
    rule->setPolicy(Policy::ACCEPT);

    addItem(rule);

    // Accept DHCP

    rule = new Rule(Item::FAMILY_IPV4, Table::FILTER, Chain::OUTPUT, Item::TARGET_ALL);

    rule->setDestinationAddress({"255.255.255.255", IPFamily::IPv4, 32});
    rule->setCounter(true);
    rule->setPolicy(Policy::ACCEPT);

    addItem(rule);

    // Accept local IPv4 network output

    rule = new Rule(Item::FAMILY_IPV4, Table::FILTER, Chain::OUTPUT, Item::TARGET_ALL);

    rule->setSourceAddress({"192.168.0.0", IPFamily::IPv4, 16});
    rule->setDestinationAddress({"192.168.0.0", IPFamily::IPv4, 16});
    rule->setCounter(true);
    rule->setPolicy(Policy::ACCEPT);

    addItem(rule);

    rule = new Rule(Item::FAMILY_IPV4, Table::FILTER, Chain::OUTPUT, Item::TARGET_ALL);

    rule->setSourceAddress({"10.0.0.0", IPFamily::IPv4, 8});
    rule->setDestinationAddress({"10.0.0.0", IPFamily::IPv4, 8});
    rule->setCounter(true);
    rule->setPolicy(Policy::ACCEPT);

    addItem(rule);

    rule = new Rule(Item::FAMILY_IPV4, Table::FILTER, Chain::OUTPUT, Item::TARGET_ALL);

    rule->setSourceAddress({"172.16.0.0", IPFamily::IPv4, 12});
    rule->setDestinationAddress({"172.16.0.0", IPFamily::IPv4, 12});
    rule->setCounter(true);
    rule->setPolicy(Policy::ACCEPT);

    addItem(rule);

    // Allow IPv4 multicast

    rule = new Rule(Item::FAMILY_IPV4, Table::FILTER, Chain::OUTPUT, Item::TARGET_ALL);

    rule->setSourceAddress({"192.168.0.0", IPFamily::IPv4, 16});
    rule->setDestinationAddress({"224.0.0.0", IPFamily::IPv4, 24});
    rule->setCounter(true);
    rule->setPolicy(Policy::ACCEPT);

    addItem(rule);

    rule = new Rule(Item::FAMILY_IPV4, Table::FILTER, Chain::OUTPUT, Item::TARGET_ALL);

    rule->setSourceAddress({"10.0.0.0", IPFamily::IPv4, 8});
    rule->setDestinationAddress({"224.0.0.0", IPFamily::IPv4, 24});
    rule->setCounter(true);
    rule->setPolicy(Policy::ACCEPT);

    addItem(rule);

    rule = new Rule(Item::FAMILY_IPV4, Table::FILTER, Chain::OUTPUT, Item::TARGET_ALL);

    rule->setSourceAddress({"172.16.0.0", IPFamily::IPv4, 12});
    rule->setDestinationAddress({"224.0.0.0", IPFamily::IPv4, 24});
    rule->setCounter(true);
    rule->setPolicy(Policy::ACCEPT);

    addItem(rule);

    // IPv4 Simple Service Discovery Protocol address

    rule = new Rule(Item::FAMILY_IPV4, Table::FILTER, Chain::OUTPUT, Item::TARGET_ALL);

    rule->setSourceAddress({"192.168.0.0", IPFamily::IPv4, 16});
    rule->setDestinationAddress({"239.255.255.250", IPFamily::IPv4, 32});
    rule->setCounter(true);
    rule->setPolicy(Policy::ACCEPT);

    addItem(rule);

    rule = new Rule(Item::FAMILY_IPV4, Table::FILTER, Chain::OUTPUT, Item::TARGET_ALL);

    rule->setSourceAddress({"10.0.0.0", IPFamily::IPv4, 8});
    rule->setDestinationAddress({"239.255.255.250", IPFamily::IPv4, 32});
    rule->setCounter(true);
    rule->setPolicy(Policy::ACCEPT);

    addItem(rule);

    rule = new Rule(Item::FAMILY_IPV4, Table::FILTER, Chain::OUTPUT, Item::TARGET_ALL);

    rule->setSourceAddress({"172.16.0.0", IPFamily::IPv4, 12});
    rule->setDestinationAddress({"239.255.255.250", IPFamily::IPv4, 32});
    rule->setCounter(true);
    rule->setPolicy(Policy::ACCEPT);

    addItem(rule);

    // Service Location Protocol version 2 address

    rule = new Rule(Item::FAMILY_IPV4, Table::FILTER, Chain::OUTPUT, Item::TARGET_ALL);

    rule->setSourceAddress({"192.168.0.0", IPFamily::IPv4, 16});
    rule->setDestinationAddress({"239.255.255.253", IPFamily::IPv4, 32});
    rule->setCounter(true);
    rule->setPolicy(Policy::ACCEPT);

    addItem(rule);

    rule = new Rule(Item::FAMILY_IPV4, Table::FILTER, Chain::OUTPUT, Item::TARGET_ALL);

    rule->setSourceAddress({"10.0.0.0", IPFamily::IPv4, 8});
    rule->setDestinationAddress({"239.255.255.253", IPFamily::IPv4, 32});
    rule->setCounter(true);
    rule->setPolicy(Policy::ACCEPT);

    addItem(rule);

    rule = new Rule(Item::FAMILY_IPV4, Table::FILTER, Chain::OUTPUT, Item::TARGET_ALL);

    rule->setSourceAddress({"172.16.0.0", IPFamily::IPv4, 12});
    rule->setDestinationAddress({"239.255.255.253", IPFamily::IPv4, 32});
    rule->setCounter(true);
    rule->setPolicy(Policy::ACCEPT);

    addItem(rule);

    // IPv4 outbound ping
/*
    rule = new Rule(Item::FAMILY_IPV4, Table::FILTER, Chain::OUTPUT, Item::TARGET_NFTABLES);

    rule->setStatement("icmp type echo-reply");
    rule->setCounter(true);
    rule->setPolicy(Policy::ACCEPT);

    addItem(rule);

    rule = new Rule(Item::FAMILY_IPV4, Table::FILTER, Chain::OUTPUT, Item::TARGET_IPTABLES);

    rule->setStatement("-p icmp -m icmp --icmp-type 8");
    rule->setCounter(true);
    rule->setPolicy(Policy::ACCEPT);

    addItem(rule);
*/
    rule = new Rule(Item::FAMILY_IPV4, Table::FILTER, Chain::OUTPUT, Item::TARGET_PF);

    rule->setStatement("proto icmp all");
    rule->setCounter(true);
    rule->setPolicy(Policy::ACCEPT);

    addItem(rule);

    // Allow all TUN interfaces

    rule = new Rule(Item::FAMILY_IPV4, Table::FILTER, Chain::OUTPUT, Item::TARGET_NFTABLES);

    rule->setDestinationInterface("tun*");
    rule->setCounter(true);
    rule->setPolicy(Policy::ACCEPT);

    addItem(rule);

    rule = new Rule(Item::FAMILY_IPV4, Table::FILTER, Chain::OUTPUT, Item::TARGET_IPTABLES);

    rule->setDestinationInterface("tun+");
    rule->setCounter(true);
    rule->setPolicy(Policy::ACCEPT);

    addItem(rule);

    // Allow established sessions

    rule = new Rule(Item::FAMILY_IPV4, Table::FILTER, Chain::OUTPUT, Item::TARGET_NFTABLES);

    rule->setStatement("ct state established");
    rule->setCounter(true);
    rule->setPolicy(Policy::ACCEPT);

    addItem(rule);

    rule = new Rule(Item::FAMILY_IPV4, Table::FILTER, Chain::OUTPUT, Item::TARGET_IPTABLES);

    rule->setStatement("-m state --state ESTABLISHED");
    rule->setCounter(true);
    rule->setPolicy(Policy::ACCEPT);

    addItem(rule);

    // Drop everything else in output

    rule = new Rule(Item::FAMILY_IPV4, Table::FILTER, Chain::OUTPUT, Item::TARGET_NFTABLES | Item::TARGET_IPTABLES);

    rule->setCounter(true);
    rule->setPolicy(Policy::DROP);

    addItem(rule);

    // Commit everything in IPTables

    rule = new Rule(Item::FAMILY_IPV4, Table::FILTER, Chain::NONE, Item::TARGET_IPTABLES);

    rule->setStatement("COMMIT");
    rule->setPolicy(Policy::NONE);

    addItem(rule);

    // IPv6

    if(localNetwork->isIPv6Enabled() == true)
    {
        addItem(new Table(Item::FAMILY_IPV6, Table::MANGLE, Item::TARGET_NFTABLES));
        addItem(new Table(Item::FAMILY_IPV6, Table::NAT, Item::TARGET_NFTABLES));
        addItem(new Table(Item::FAMILY_IPV6, Table::FILTER, Item::TARGET_NFTABLES));

        // IPv6 Mangle

        chain = new Chain(Item::FAMILY_IPV6, Table::MANGLE, Chain::PREROUTING, Item::TARGET_NFTABLES);

        chain->setType(Chain::TYPE_FILTER);
        chain->setHook(Hook::PREROUTING);
        chain->setPriority(Chain::PRIORITY_MANGLE);
        chain->setPolicy(Policy::ACCEPT);

        addItem(chain);

        chain = new Chain(Item::FAMILY_IPV6, Table::MANGLE, Chain::INPUT, Item::TARGET_NFTABLES);

        chain->setType(Chain::TYPE_FILTER);
        chain->setHook(Hook::INPUT);
        chain->setPriority(Chain::PRIORITY_MANGLE);
        chain->setPolicy(Policy::ACCEPT);

        addItem(chain);

        chain = new Chain(Item::FAMILY_IPV6, Table::MANGLE, Chain::FORWARD, Item::TARGET_NFTABLES);

        chain->setType(Chain::TYPE_FILTER);
        chain->setHook(Hook::FORWARD);
        chain->setPriority(Chain::PRIORITY_MANGLE);
        chain->setPolicy(Policy::ACCEPT);

        addItem(chain);

        chain = new Chain(Item::FAMILY_IPV6, Table::MANGLE, Chain::OUTPUT, Item::TARGET_NFTABLES);

        chain->setType(Chain::TYPE_FILTER);
        chain->setHook(Hook::OUTPUT);
        chain->setPriority(Chain::PRIORITY_MANGLE);
        chain->setPolicy(Policy::ACCEPT);

        addItem(chain);

        chain = new Chain(Item::FAMILY_IPV6, Table::MANGLE, Chain::POSTROUTING, Item::TARGET_NFTABLES);

        chain->setType(Chain::TYPE_FILTER);
        chain->setHook(Hook::POSTROUTING);
        chain->setPriority(Chain::PRIORITY_MANGLE);
        chain->setPolicy(Policy::ACCEPT);

        addItem(chain);

        if(ip6tableMangleAvailable == true)
        {
            rule = new Rule(Item::FAMILY_IPV6, Table::MANGLE, Chain::NONE, Item::TARGET_IPTABLES);

            rule->setStatement("*mangle");
            rule->setPolicy(Policy::NONE);

            addItem(rule);

            rule = new Rule(Item::FAMILY_IPV6, Table::MANGLE, Chain::NONE, Item::TARGET_IPTABLES);

            rule->setStatement(":PREROUTING ACCEPT [0:0]");
            rule->setPolicy(Policy::NONE);

            addItem(rule);

            rule = new Rule(Item::FAMILY_IPV6, Table::MANGLE, Chain::NONE, Item::TARGET_IPTABLES);

            rule->setStatement(":INPUT ACCEPT [0:0]");
            rule->setPolicy(Policy::NONE);

            addItem(rule);

            rule = new Rule(Item::FAMILY_IPV6, Table::MANGLE, Chain::NONE, Item::TARGET_IPTABLES);

            rule->setStatement(":FORWARD ACCEPT [0:0]");
            rule->setPolicy(Policy::NONE);

            addItem(rule);

            rule = new Rule(Item::FAMILY_IPV6, Table::MANGLE, Chain::NONE, Item::TARGET_IPTABLES);

            rule->setStatement(":OUTPUT ACCEPT [0:0]");
            rule->setPolicy(Policy::NONE);

            addItem(rule);

            rule = new Rule(Item::FAMILY_IPV6, Table::MANGLE, Chain::NONE, Item::TARGET_IPTABLES);

            rule->setStatement(":POSTROUTING ACCEPT [0:0]");
            rule->setPolicy(Policy::NONE);

            addItem(rule);

            rule = new Rule(Item::FAMILY_IPV6, Table::MANGLE, Chain::NONE, Item::TARGET_IPTABLES);

            rule->setStatement("COMMIT");
            rule->setPolicy(Policy::NONE);

            addItem(rule);
        }

        // IPv6 NAT

        chain = new Chain(Item::FAMILY_IPV6, Table::NAT, Chain::PREROUTING, Item::TARGET_NFTABLES);

        chain->setType(Chain::TYPE_NAT);
        chain->setHook(Hook::PREROUTING);
        chain->setPriority(Chain::PRIORITY_DSTNAT);
        chain->setPolicy(Policy::ACCEPT);

        addItem(chain);

        chain = new Chain(Item::FAMILY_IPV6, Table::NAT, Chain::INPUT, Item::TARGET_NFTABLES);

        chain->setType(Chain::TYPE_NAT);
        chain->setHook(Hook::INPUT);
        chain->setPriority(Chain::PRIORITY_SRCNAT);
        chain->setPolicy(Policy::ACCEPT);

        addItem(chain);

        chain = new Chain(Item::FAMILY_IPV6, Table::NAT, Chain::OUTPUT, Item::TARGET_NFTABLES);

        chain->setType(Chain::TYPE_NAT);
        chain->setHook(Hook::OUTPUT);
        chain->setPriority(Chain::PRIORITY_DSTNAT);
        chain->setPolicy(Policy::ACCEPT);

        addItem(chain);

        chain = new Chain(Item::FAMILY_IPV6, Table::NAT, Chain::POSTROUTING, Item::TARGET_NFTABLES);

        chain->setType(Chain::TYPE_NAT);
        chain->setHook(Hook::POSTROUTING);
        chain->setPriority(Chain::PRIORITY_SRCNAT);
        chain->setPolicy(Policy::ACCEPT);

        addItem(chain);

        if(ip6tableNatAvailable == true)
        {
            rule = new Rule(Item::FAMILY_IPV6, Table::NAT, Chain::NONE, Item::TARGET_IPTABLES);

            rule->setStatement("*nat");
            rule->setPolicy(Policy::NONE);

            addItem(rule);

            rule = new Rule(Item::FAMILY_IPV6, Table::NAT, Chain::NONE, Item::TARGET_IPTABLES);

            rule->setStatement(":PREROUTING ACCEPT [0:0]");
            rule->setPolicy(Policy::NONE);

            addItem(rule);

            rule = new Rule(Item::FAMILY_IPV6, Table::NAT, Chain::NONE, Item::TARGET_IPTABLES);

            rule->setStatement(":INPUT ACCEPT [0:0]");
            rule->setPolicy(Policy::NONE);

            addItem(rule);

            rule = new Rule(Item::FAMILY_IPV6, Table::NAT, Chain::NONE, Item::TARGET_IPTABLES);

            rule->setStatement(":OUTPUT ACCEPT [0:0]");
            rule->setPolicy(Policy::NONE);

            addItem(rule);

            rule = new Rule(Item::FAMILY_IPV6, Table::NAT, Chain::NONE, Item::TARGET_IPTABLES);

            rule->setStatement(":POSTROUTING ACCEPT [0:0]");
            rule->setPolicy(Policy::NONE);

            addItem(rule);

            rule = new Rule(Item::FAMILY_IPV6, Table::NAT, Chain::NONE, Item::TARGET_IPTABLES);

            rule->setStatement("COMMIT");
            rule->setPolicy(Policy::NONE);

            addItem(rule);
        }

        // IPv6 Filter

        chain = new Chain(Item::FAMILY_IPV6, Table::FILTER, Chain::INPUT, Item::TARGET_NFTABLES);

        chain->setType(Chain::TYPE_FILTER);
        chain->setHook(Hook::INPUT);
        chain->setPriority(Chain::PRIORITY_FILTER);
        chain->setPolicy(Policy::DROP);

        addItem(chain);

        chain = new Chain(Item::FAMILY_IPV6, Table::FILTER, Chain::FORWARD, Item::TARGET_NFTABLES);

        chain->setType(Chain::TYPE_FILTER);
        chain->setHook(Hook::FORWARD);
        chain->setPriority(Chain::PRIORITY_FILTER);
        chain->setPolicy(Policy::DROP);

        addItem(chain);

        chain = new Chain(Item::FAMILY_IPV6, Table::FILTER, Chain::OUTPUT, Item::TARGET_NFTABLES);

        chain->setType(Chain::TYPE_FILTER);
        chain->setHook(Hook::OUTPUT);
        chain->setPriority(Chain::PRIORITY_FILTER);
        chain->setPolicy(Policy::DROP);

        addItem(chain);

        if(ip6tableFilterAvailable == true)
        {
            rule = new Rule(Item::FAMILY_IPV6, Table::FILTER, Chain::NONE, Item::TARGET_IPTABLES);

            rule->setStatement("*filter");
            rule->setPolicy(Policy::NONE);

            addItem(rule);

            rule = new Rule(Item::FAMILY_IPV6, Table::FILTER, Chain::NONE, Item::TARGET_IPTABLES);

            rule->setStatement(":INPUT DROP [0:0]");
            rule->setPolicy(Policy::NONE);

            addItem(rule);

            rule = new Rule(Item::FAMILY_IPV6, Table::FILTER, Chain::NONE, Item::TARGET_IPTABLES);

            rule->setStatement(":FORWARD DROP [0:0]");
            rule->setPolicy(Policy::NONE);

            addItem(rule);

            rule = new Rule(Item::FAMILY_IPV6, Table::FILTER, Chain::NONE, Item::TARGET_IPTABLES);

            rule->setStatement(":OUTPUT DROP [0:0]");
            rule->setPolicy(Policy::NONE);

            addItem(rule);
        }

        // Accept local ipV6 network

        rule = new Rule(Item::FAMILY_IPV6, Table::FILTER, Chain::INPUT, Item::TARGET_NFTABLES | Item::TARGET_IPTABLES);

        rule->setSourceInterface(loopbackIface);
        rule->setCounter(true);
        rule->setPolicy(Policy::ACCEPT);

        addItem(rule);

        // Reject traffic to localhost not coming from local interface

        rule = new Rule(Item::FAMILY_IPV6, Table::FILTER, Chain::INPUT, Item::TARGET_NFTABLES);

        rule->setStatement("iifname != \"" + loopbackIface + "\" ip6 saddr ::1");
        rule->setCounter(true);
        rule->setPolicy(Policy::REJECT);

        addItem(rule);

        rule = new Rule(Item::FAMILY_IPV6, Table::FILTER, Chain::INPUT, Item::TARGET_IPTABLES);

        rule->setStatement("-s ::1/128 ! -i " + loopbackIface + " -j REJECT --reject-with icmp6-port-unreachable");
        rule->setPolicy(Policy::NONE);

        addItem(rule);

        // Disable processing of any RH0 packet which could allow a ping-pong of packets

        rule = new Rule(Item::FAMILY_IPV6, Table::FILTER, Chain::INPUT, Item::TARGET_NFTABLES);

        rule->setStatement("rt type 0");
        rule->setCounter(true);
        rule->setPolicy(Policy::DROP);

        addItem(rule);

        rule = new Rule(Item::FAMILY_IPV6, Table::FILTER, Chain::INPUT, Item::TARGET_IPTABLES);

        rule->setStatement("-m rt --rt-type 0");
        rule->setCounter(true);
        rule->setPolicy(Policy::DROP);

        addItem(rule);

        // IPv6 inbound ping
/*
        rule = new Rule(Item::FAMILY_IPV6, Table::FILTER, Chain::INPUT, Item::TARGET_NFTABLES);

        rule->setStatement("icmpv6 type echo-request");
        rule->setCounter(true);
        rule->setPolicy(Policy::ACCEPT);

        addItem(rule);

        rule = new Rule(Item::FAMILY_IPV6, Table::FILTER, Chain::INPUT, Item::TARGET_NFTABLES);

        rule->setStatement("meta l4proto ipv6-icmp");
        rule->setCounter(true);
        rule->setPolicy(Policy::ACCEPT);

        addItem(rule);

        // icmpv6-type:router-advertisement - Rules which are required for your IPv6 address to be properly allocated

        rule = new Rule(Item::FAMILY_IPV6, Table::FILTER, Chain::INPUT, Item::TARGET_IPTABLES);

        rule->setStatement("-p ipv6-icmp -m icmp6 --icmpv6-type 134 -m hl --hl-eq 255");
        rule->setCounter(true);
        rule->setPolicy(Policy::ACCEPT);

        addItem(rule);

        // icmpv6-type:neighbor-solicitation - Rules which are required for your IPv6 address to be properly allocated

        rule = new Rule(Item::FAMILY_IPV6, Table::FILTER, Chain::INPUT, Item::TARGET_IPTABLES);

        rule->setStatement("-p ipv6-icmp -m icmp6 --icmpv6-type 135 -m hl --hl-eq 255");
        rule->setCounter(true);
        rule->setPolicy(Policy::ACCEPT);

        addItem(rule);

        // icmpv6-type:neighbor-advertisement - Rules which are required for your IPv6 address to be properly allocated

        rule = new Rule(Item::FAMILY_IPV6, Table::FILTER, Chain::INPUT, Item::TARGET_IPTABLES);

        rule->setStatement("-p ipv6-icmp -m icmp6 --icmpv6-type 136 -m hl --hl-eq 255");
        rule->setCounter(true);
        rule->setPolicy(Policy::ACCEPT);

        addItem(rule);

        // icmpv6-type:redirect - Rules which are required for your IPv6 address to be properly allocated

        rule = new Rule(Item::FAMILY_IPV6, Table::FILTER, Chain::INPUT, Item::TARGET_IPTABLES);

        rule->setStatement("-p ipv6-icmp -m icmp6 --icmpv6-type 137 -m hl --hl-eq 255");
        rule->setCounter(true);
        rule->setPolicy(Policy::ACCEPT);

        addItem(rule);

        rule = new Rule(Item::FAMILY_IPV6, Table::FILTER, Chain::INPUT, Item::TARGET_PF);

        rule->setStatement("proto icmp6 all");
        rule->setCounter(true);
        rule->setPolicy(Policy::ACCEPT);

        addItem(rule);
    */

        // Allow private IPv6 network

        rule = new Rule(Item::FAMILY_IPV6, Table::FILTER, Chain::INPUT, Item::TARGET_ALL);

        rule->setSourceAddress({"fe80::", IPFamily::IPv6, 10});
        rule->setDestinationAddress({"fe80::", IPFamily::IPv6, 10});
        rule->setCounter(true);
        rule->setPolicy(Policy::ACCEPT);

        addItem(rule);

        // Allow IPv6 multicast

        rule = new Rule(Item::FAMILY_IPV6, Table::FILTER, Chain::INPUT, Item::TARGET_ALL);

        rule->setSourceAddress({"ff00::", IPFamily::IPv6, 8});
        rule->setDestinationAddress({"ff00::", IPFamily::IPv6, 8});
        rule->setCounter(true);
        rule->setPolicy(Policy::ACCEPT);

        addItem(rule);

        // Accept established sessions

        rule = new Rule(Item::FAMILY_IPV6, Table::FILTER, Chain::INPUT, Item::TARGET_NFTABLES);

        rule->setStatement("ct state related,established");
        rule->setCounter(true);
        rule->setPolicy(Policy::ACCEPT);

        addItem(rule);

        rule = new Rule(Item::FAMILY_IPV6, Table::FILTER, Chain::INPUT, Item::TARGET_IPTABLES);

        rule->setStatement("-m state --state RELATED,ESTABLISHED");
        rule->setCounter(true);
        rule->setPolicy(Policy::ACCEPT);

        addItem(rule);

        // Accept all tun interfaces

        rule = new Rule(Item::FAMILY_IPV6, Table::FILTER, Chain::INPUT, Item::TARGET_NFTABLES);

        rule->setSourceInterface("tun*");
        rule->setCounter(true);
        rule->setPolicy(Policy::ACCEPT);

        addItem(rule);

        rule = new Rule(Item::FAMILY_IPV6, Table::FILTER, Chain::INPUT, Item::TARGET_IPTABLES);

        rule->setSourceInterface("tun+");
        rule->setCounter(true);
        rule->setPolicy(Policy::ACCEPT);

        addItem(rule);

        // Drop everything else in input

        rule = new Rule(Item::FAMILY_IPV6, Table::FILTER, Chain::INPUT, Item::TARGET_NFTABLES | Item::TARGET_IPTABLES);

        rule->setCounter(true);
        rule->setPolicy(Policy::DROP);

        addItem(rule);

        // Disable processing of any RH0 packet which could allow a ping-pong of packets

        rule = new Rule(Item::FAMILY_IPV6, Table::FILTER, Chain::FORWARD, Item::TARGET_NFTABLES);

        rule->setStatement("rt type 0");
        rule->setCounter(true);
        rule->setPolicy(Policy::DROP);

        addItem(rule);

        rule = new Rule(Item::FAMILY_IPV6, Table::FILTER, Chain::FORWARD, Item::TARGET_IPTABLES);

        rule->setStatement("-m rt --rt-type 0");
        rule->setCounter(true);
        rule->setPolicy(Policy::DROP);

        addItem(rule);

        // Accept TUN forward

        rule = new Rule(Item::FAMILY_IPV6, Table::FILTER, Chain::FORWARD, Item::TARGET_NFTABLES);

        rule->setSourceInterface("tun*");
        rule->setCounter(true);
        rule->setPolicy(Policy::ACCEPT);

        addItem(rule);

        rule = new Rule(Item::FAMILY_IPV6, Table::FILTER, Chain::FORWARD, Item::TARGET_IPTABLES);

        rule->setSourceInterface("tun+");
        rule->setCounter(true);
        rule->setPolicy(Policy::ACCEPT);

        addItem(rule);

        // Drop all the other forwarding

        rule = new Rule(Item::FAMILY_IPV6, Table::FILTER, Chain::FORWARD, Item::TARGET_NFTABLES | Item::TARGET_IPTABLES);

        rule->setCounter(true);
        rule->setPolicy(Policy::DROP);

        addItem(rule);

        // Allow local traffic

        rule = new Rule(Item::FAMILY_IPV6, Table::FILTER, Chain::OUTPUT, Item::TARGET_NFTABLES | Item::TARGET_IPTABLES);

        rule->setDestinationInterface(loopbackIface);
        rule->setCounter(true);
        rule->setPolicy(Policy::ACCEPT);

        addItem(rule);

        rule = new Rule(Item::FAMILY_IPV6, Table::FILTER, Chain::OUTPUT, Item::TARGET_NFTABLES);

        rule->setStatement("rt type 0");
        rule->setCounter(true);
        rule->setPolicy(Policy::DROP);

        addItem(rule);

        rule = new Rule(Item::FAMILY_IPV6, Table::FILTER, Chain::OUTPUT, Item::TARGET_IPTABLES);

        rule->setStatement("-m rt --rt-type 0");
        rule->setCounter(true);
        rule->setPolicy(Policy::DROP);

        addItem(rule);

        // Allow private IPv6 network

        rule = new Rule(Item::FAMILY_IPV6, Table::FILTER, Chain::OUTPUT, Item::TARGET_ALL);

        rule->setSourceAddress({"fe80::", IPFamily::IPv6, 10});
        rule->setDestinationAddress({"fe80::", IPFamily::IPv6, 10});
        rule->setCounter(true);
        rule->setPolicy(Policy::ACCEPT);

        addItem(rule);

        // Allow IPv6 multicast

        rule = new Rule(Item::FAMILY_IPV6, Table::FILTER, Chain::OUTPUT, Item::TARGET_ALL);

        rule->setSourceAddress({"ff00::", IPFamily::IPv6, 8});
        rule->setDestinationAddress({"ff00::", IPFamily::IPv6, 8});
        rule->setCounter(true);
        rule->setPolicy(Policy::ACCEPT);

        addItem(rule);

        // IPv6 outbound ping
/*
        rule = new Rule(Item::FAMILY_IPV6, Table::FILTER, Chain::OUTPUT, Item::TARGET_NFTABLES);

        rule->setStatement("meta l4proto ipv6-icmp");
        rule->setCounter(true);
        rule->setPolicy(Policy::ACCEPT);

        addItem(rule);

        rule = new Rule(Item::FAMILY_IPV6, Table::FILTER, Chain::OUTPUT, Item::TARGET_IPTABLES);

        rule->setStatement("-p ipv6-icmp");
        rule->setCounter(true);
        rule->setPolicy(Policy::ACCEPT);

        addItem(rule);
*/
        rule = new Rule(Item::FAMILY_IPV6, Table::FILTER, Chain::OUTPUT, Item::TARGET_PF);

        rule->setStatement("proto icmp6");
        rule->setCounter(true);
        rule->setPolicy(Policy::ACCEPT);

        addItem(rule);

        // Allow TUN

        rule = new Rule(Item::FAMILY_IPV6, Table::FILTER, Chain::OUTPUT, Item::TARGET_NFTABLES);

        rule->setDestinationInterface("tun*");
        rule->setCounter(true);
        rule->setPolicy(Policy::ACCEPT);

        addItem(rule);

        rule = new Rule(Item::FAMILY_IPV6, Table::FILTER, Chain::OUTPUT, Item::TARGET_IPTABLES);

        rule->setDestinationInterface("tun+");
        rule->setCounter(true);
        rule->setPolicy(Policy::ACCEPT);

        addItem(rule);

        // Allow established sessions

        rule = new Rule(Item::FAMILY_IPV6, Table::FILTER, Chain::OUTPUT, Item::TARGET_NFTABLES);

        rule->setStatement("ct state established");
        rule->setCounter(true);
        rule->setPolicy(Policy::ACCEPT);

        addItem(rule);

        rule = new Rule(Item::FAMILY_IPV6, Table::FILTER, Chain::OUTPUT, Item::TARGET_IPTABLES);

        rule->setStatement("-m state --state ESTABLISHED");
        rule->setCounter(true);
        rule->setPolicy(Policy::ACCEPT);

        addItem(rule);

        // Drop everything else in output

        rule = new Rule(Item::FAMILY_IPV6, Table::FILTER, Chain::OUTPUT, Item::TARGET_NFTABLES | Item::TARGET_IPTABLES);

        rule->setCounter(true);
        rule->setPolicy(Policy::DROP);

        addItem(rule);

        // Commit everything in IPTables

        rule = new Rule(Item::FAMILY_IPV6, Table::FILTER, Chain::NONE, Item::TARGET_IPTABLES);

        rule->setStatement("COMMIT");
        rule->setPolicy(Policy::NONE);

        addItem(rule);
    }

    // Block everything else in pf, just in case

    rule = new Rule(Item::FAMILY_INET, Table::FILTER, Chain::NONE, Item::TARGET_PF);

    rule->setStatement("block all");
    rule->setPolicy(Policy::NONE);

    addItem(rule);
}

int NetFilter::addItem(Item *item, int p)
{
    int pos = ITEM_ERROR;

    if(item == nullptr)
        return false;

    if(search(*item) != ITEM_NOT_FOUND)
    {
        global_log("ERROR: Item already exists (rule: " + translateItem(item, ((p == ITEM_NOT_FOUND) ? Action::ADD : Action::INSERT)) + ")");

        return ITEM_ERROR;
    }

    if(p == -1)
    {
        itemList.push_back(item);
        
        pos = itemList.size() - 1;
    }
    else
    {
        if(p < itemList.size())
        {
            itemList.insert(itemList.begin() + p, item);

            pos = p;
        }
        else
            pos = ITEM_ERROR;
    }

    return pos;
}

int NetFilter::append(Item &item)
{
    Table *table, *t;
    Chain *chain, *c;
    Rule *rule, *r;
    int pos;

    switch(item.getItemType())
    {
        case Item::Type::TABLE:
        {
            t = (Table *)&item;

            table = new Table(t->getFamily(), t->getName());

            table = t->clone();

            pos = addItem(table);
        }
        break;

        case Item::Type::CHAIN:
        {
            c = (Chain *)&item;

            chain = new Chain(c->getFamily(), c->getTable(), c->getName());

            chain = c->clone();

            pos = addItem(chain);
        }
        break;

        case Item::Type::RULE:
        {
            r = (Rule *)&item;

            rule = new Rule(r->getFamily(), r->getTable(), r->getChain());

            rule = r->clone();

            pos = addItem(rule);
        }
        break;
    }

    return pos;
}

int NetFilter::insert(Item &item)
{
    int chain_pos = ITEM_NOT_FOUND, rule_pos = ITEM_NOT_FOUND, i, pos = ITEM_NOT_FOUND;
    Chain *chain;
    Rule *rule, *r;

    if(itemList.empty() == true)
        return ITEM_ERROR;

    if(item.getItemType() == Item::Type::TABLE || item.getItemType() == Item::Type::CHAIN)
        return append(item);

    // Rule

    r = (Rule *)&item;

    for(i = 0; i < itemList.size() && rule_pos == ITEM_NOT_FOUND; i++)
    {
        switch(itemList[i]->getItemType())
        {
            case Item::Type::CHAIN:
            {
                chain = (Chain *)itemList[i];

                if(chain->getFamily() == r->getFamily() && chain->getTable() == r->getTable() && chain->getName() == r->getChain())
                    chain_pos = i;
            }
            break;

            case Item::Type::RULE:
            {
                rule = (Rule *)itemList[i];

                if(rule->getFamily() == r->getFamily() && rule->getTable() == r->getTable() && rule->getChain() == r->getChain())
                    rule_pos = i;
            }
            break;
            
            default:
            {
            }
            break;
        }
    }

    if(rule_pos != ITEM_NOT_FOUND)
        pos = rule_pos;
    else if(chain_pos != ITEM_NOT_FOUND)
        pos = chain_pos;
    else
        pos = ITEM_NOT_FOUND;
        
    if(pos != ITEM_NOT_FOUND)
    {
        rule = new Rule(r->getFamily(), r->getTable(), r->getChain());

        rule = r->clone();

        if(pos < itemList.size())
            pos = addItem(rule, pos);
        else
            pos = addItem(rule);
    }
    else
        global_log("ERROR: NetFilter::insert(): Chain not found. (rule: " + translateItem(&item, ((pos == ITEM_NOT_FOUND) ? Action::ADD : Action::INSERT)) + ")");

    return pos;
}

int NetFilter::remove(Item &item)
{
    Item *i;
    bool retval = false;
    int pos = ITEM_NOT_FOUND;
    
    pos = search(item);
    
    if(pos != ITEM_NOT_FOUND)
    {
        i = itemList[pos];

        itemList.erase(itemList.begin() + pos);

        switch(filterMode)
        {
            case Mode::NFTABLES:
            {
                retval = nftablesCommitItem(i, Action::DELETE);
            }
            break;

            case Mode::IPTABLES:
            {
                retval = iptablesCommitItem(i, Action::DELETE);
            }
            break;

            case Mode::PF:
            {
                retval = pfCommit();
            }
            break;

            default:
            {
                retval = false;
            }
            break;
        }
        
        if(retval == false)
            pos = ITEM_NOT_FOUND;

        delete i;
    }
        
    return pos;
}

int NetFilter::commit(Item &item)
{
    Action action;
    Item *newItem = nullptr;
    int pos = ITEM_ERROR;
    bool retval = false;

    pos = insert(item);

    if(pos != ITEM_ERROR)
    {
        newItem = itemList[pos];

        if(newItem->getItemType() == Item::Type::RULE)
            action = Action::INSERT;
        else
            action = Action::ADD;

        switch(filterMode)
        {
            case Mode::NFTABLES:
            {
                retval = nftablesCommitItem(newItem, action);
            }
            break;

            case Mode::IPTABLES:
            {
                retval = iptablesCommitItem(newItem, action);
            }
            break;

            case Mode::PF:
            {
                retval = pfCommit();
            }
            break;

            default:
            {
                retval = false;
            }
            break;
        }
        
        if(retval == false)
            global_log("ERROR: NetFilter::commit(): Cannot commit item. (rule: " + translateItem(newItem, ((pos == ITEM_ERROR) ? Action::ADD : Action::INSERT)) + ")");
    }
    else
        global_log("ERROR: NetFilter::commit(): Cannot insert item to the list. (rule: " + translateItem(&item, ((pos == ITEM_ERROR) ? Action::ADD : Action::INSERT)) + ")");

    return pos;
}

// nftables

void NetFilter::nftablesResetRules()
{
    nftablesRules = "";
}

std::string NetFilter::createNftablesGenericRule(IPFamily ipFamily, Hook hook, const std::string &interface, Protocol protocol, const std::string &sourceIP, int sourcePort, const std::string &destinationIP, int destinationPort)
{
    std::string ipTag, ifTag;
    std::string rule = "rule ";

    ipTag = "ip";

    if(ipFamily == IPFamily::IPv6)
    {
        if(localNetwork->isIPv6Enabled() == true)
            ipTag += "6";
        else
            return "";
    }

    rule += ipTag + " filter ";

    if(hook == Hook::INPUT)
    {
        rule += "INPUT";
        ifTag = "iifname";
    }
    else
    {
        rule += "OUTPUT";
        ifTag = "oifname";
    }

    if(interface != "")
        rule += " " + ifTag + " " + interface;

    if(sourceIP != "")
        rule += " " + ipTag + " saddr " + sourceIP;

    if(destinationIP != "")
        rule += " " + ipTag + " daddr " + destinationIP;

    if(protocol != Protocol::ANY)
    {
        rule += " ";

        if(protocol == Protocol::TCP)
            rule += "tcp";
        else
            rule += "udp";
    }

    if(sourcePort > 0)
    {
        rule += " sport ";
        rule += sourcePort;
    }

    if(destinationPort > 0)
    {
        rule += " dport ";
        rule += destinationPort;
    }

    rule += " counter";

    return rule;
}

void NetFilter::nftablesAddRule(const std::string &rule)
{
    if(rule.empty() == true)
        return;

    nftablesRules += rule;
    nftablesRules += "\n";
}

bool NetFilter::nftablesCommitRule(const std::string &rule)
{
    bool retval = false;
    char *exec_args[EXEC_MAX_ARGS];
    int n;

    if(rule.empty() == true)
        return false;

    strcpy(binpath, "");

    get_exec_path(nftBinary, binpath);

    if(strcmp(binpath, "") != 0)
    {
        retval = true;

        std::istringstream buf(rule);
        std::istream_iterator<std::string> beg(buf), end;

        std::vector<std::string> tokens(beg, end);

        n = 0;

        exec_args[n++] = binpath;

        for(auto &s : tokens)
        {
            exec_args[n++] = (char *)s.c_str();

            if(n == EXEC_MAX_ARGS)
                return false;
        }

        exec_args[n++] = NULL;

        if(execute_process_args(NULL, charBuffer, binpath, exec_args) != 0)
            retval = false;
    }
    else
        retval = false;

    return retval;
}

bool NetFilter::nftablesCommitItem(Item *item, Action action)
{
    std::string rule, command, line, handleTag = "# handle ";
    bool retval = false, handle_found = false;
    char *exec_args[EXEC_MAX_ARGS];
    std::vector<std::string> outItems;
    std::size_t tagPos, handlePos;
    int n;
    unsigned long handle = 0;

    if(item == nullptr)
        return false;

    rule = translateItemToNFTables(item, action);

    if(rule.empty() == true)
        return false;

    strcpy(binpath, "");

    get_exec_path(nftBinary, binpath);

    if(strcmp(binpath, "") != 0)
    {
        retval = true;

        std::istringstream buf(rule);
        std::istream_iterator<std::string> beg(buf), end;

        std::vector<std::string> tokens(beg, end);

        n = 0;

        exec_args[n++] = binpath;
        exec_args[n++] = (char *)"-a";
        exec_args[n++] = (char *)"-e";

        for(auto &s : tokens)
        {
            exec_args[n++] = (char *)s.c_str();

            if(n == EXEC_MAX_ARGS)
                return false;
        }

        exec_args[n++] = NULL;

        if(execute_process_args(NULL, charBuffer, binpath, exec_args) == 0)
        {
            if(action != Action::DELETE)
            {
                outItems = AirVPNTools::split(charBuffer, "\n");

                handle_found = false;

                for(n = 0; n < outItems.size() && handle_found == false; n++)
                {
                    line = AirVPNTools::toLower(outItems[n]);
    
                    tagPos = line.find(handleTag);
                    
                    if(tagPos !=std::string::npos)
                    {
                        tagPos += handleTag.length();
                        
                        handlePos = line.find_first_of("\n\t ", tagPos);

                        if(handlePos == std::string::npos)
                            handle = std::stoul(line.substr(tagPos));
                        else
                            handle = std::stoul(line.substr(tagPos, handlePos - tagPos));
                            
                        handle_found = true;
                    }
                }

                if(handle_found == true && handle > 0)
                    item->setHandle(handle);
                else
                    global_log("ERROR: Cannot get handle for nftables rule \"" + rule + "\"");
            }

            retval = true;
        }
        else
            retval = false;
    }
    else
        retval = false;

    return retval;
}

bool NetFilter::nftablesCommit()
{
    bool retval = true;
    std::vector<Item::Type> itemSequence{ Item::Type::TABLE, Item::Type::CHAIN, Item::Type::RULE };

    if(itemList.size() == 0)
        return false;

    nftablesFlush();

    for(Item::Type type : itemSequence)
    {
        for(Item *item : itemList)
        {
            if(item->getItemType() == type && item->isTargetNFTables())
                retval &= nftablesCommitItem(item, Action::ADD);
        }
    }

    return retval;
}

bool NetFilter::nftablesSave()
{
    bool retval = false;
    std::ofstream dumpFile;
    std::string fileName;

    if(workingDirectory == "")
        return false;

    strcpy(binpath, "");

    fileName = workingDirectory;
    fileName += "/";
    fileName += nftablesSaveFile;

    get_exec_path(nftBinary, binpath);

    if(strcmp(binpath, "") != 0)
    {
        if(execute_process(NULL, charBuffer, binpath, "list", "ruleset", NULL) == 0)
        {
            dumpFile.open(fileName);

            if(dumpFile.good())
            {
                dumpFile << charBuffer << std::endl;

                dumpFile.close();

                retval = true;
            }
            else
                retval = false;
        }
        else
            retval = false;
    }
    else
        retval = false;

    return retval;
}

bool NetFilter::nftablesRestore()
{
    bool retval = false;
    std::string fileName;

    if(workingDirectory == "")
        return false;

    strcpy(binpath, "");

    fileName = workingDirectory;
    fileName += "/";
    fileName += nftablesSaveFile;

    get_exec_path(nftBinary, binpath);

    if(strcmp(binpath, "") != 0 && access(fileName.c_str(), F_OK) == 0)
    {
        nftablesFlush();

        if(execute_process(NULL, NULL, binpath, "-f", fileName.c_str(), NULL) == 0)
        {
            unlink(fileName.c_str());

            retval = true;
        }
        else
            retval = false;
    }
    else
        retval = false;

    return retval;
}

bool NetFilter::nftablesFlush()
{
    bool retval = false;
    char cmdpath[64];

    strcpy(cmdpath, "");

    get_exec_path(nftBinary, cmdpath);

    if(strcmp(cmdpath, "") != 0)
    {
        retval = true;

        if(execute_process(NULL, NULL, cmdpath, "flush", "ruleset", NULL) != 0)
            retval = false;
    }
    else
        retval = false;

    return retval;
}

void NetFilter::nftablesSetup(const std::string &loopbackIface)
{
    nftablesResetRules();

    // IPv4

    nftablesAddRule("add table ip mangle");
    nftablesAddRule("add chain ip mangle PREROUTING { type filter hook prerouting priority -150; policy accept; }");
    nftablesAddRule("add chain ip mangle INPUT { type filter hook input priority -150; policy accept; }");
    nftablesAddRule("add chain ip mangle FORWARD { type filter hook forward priority -150; policy accept; }");
    nftablesAddRule("add chain ip mangle OUTPUT { type route hook output priority -150; policy accept; }");
    nftablesAddRule("add chain ip mangle POSTROUTING { type filter hook postrouting priority -150; policy accept; }");
    nftablesAddRule("add table ip nat");
    nftablesAddRule("add chain ip nat PREROUTING { type nat hook prerouting priority -100; policy accept; }");
    nftablesAddRule("add chain ip nat INPUT { type nat hook input priority 100; policy accept; }");
    nftablesAddRule("add chain ip nat OUTPUT { type nat hook output priority -100; policy accept; }");
    nftablesAddRule("add chain ip nat POSTROUTING { type nat hook postrouting priority 100; policy accept; }");
    nftablesAddRule("add table ip filter");
    nftablesAddRule("add chain ip filter INPUT { type filter hook input priority 0; policy drop; }");
    nftablesAddRule("add chain ip filter FORWARD { type filter hook forward priority 0; policy drop; }");
    nftablesAddRule("add chain ip filter OUTPUT { type filter hook output priority 0; policy drop; }");

    // Local input

    nftablesAddRule("add rule ip filter INPUT iifname \"lo\" counter accept");

    // Accept DHCP

    nftablesAddRule("add rule ip filter INPUT ip saddr 255.255.255.255 counter accept");

    // Accept local network

    nftablesAddRule("add rule ip filter INPUT ip saddr 192.168.0.0/16 ip daddr 192.168.0.0/16 counter accept");
    nftablesAddRule("add rule ip filter INPUT ip saddr 10.0.0.0/8 ip daddr 10.0.0.0/8 counter accept");
    nftablesAddRule("add rule ip filter INPUT ip saddr 172.16.0.0/12 ip daddr 172.16.0.0/12 counter accept");

    // Accept ping

    // nftablesAddRule("add rule ip filter INPUT icmp type echo-request counter accept");

    // Accept established sessions

    nftablesAddRule("add rule ip filter INPUT ct state related,established  counter accept");

    // Accept all tun interfaces

    nftablesAddRule("add rule ip filter INPUT iifname \"tun*\" counter accept");

    // Reject everything else

    nftablesAddRule("add rule ip filter INPUT counter drop");

    // Accept TUN forward

    nftablesAddRule("add rule ip filter FORWARD iifname \"tun*\" counter accept");

    // Reject all the other forwarding

    nftablesAddRule("add rule ip filter FORWARD counter drop");

    // Local output

    nftablesAddRule("add rule ip filter OUTPUT oifname \"lo\" counter accept");

    // Accept DHCP

    nftablesAddRule("add rule ip filter OUTPUT ip daddr 255.255.255.255 counter accept");

    // Accept local network

    nftablesAddRule("add rule ip filter OUTPUT ip saddr 192.168.0.0/16 ip daddr 192.168.0.0/16 counter accept");
    nftablesAddRule("add rule ip filter OUTPUT ip saddr 10.0.0.0/8 ip daddr 10.0.0.0/8 counter accept");
    nftablesAddRule("add rule ip filter OUTPUT ip saddr 172.16.0.0/12 ip daddr 172.16.0.0/12 counter accept");

    // Allow multicast

    nftablesAddRule("add rule ip filter OUTPUT ip saddr 192.168.0.0/16 ip daddr 224.0.0.0/24 counter accept");
    nftablesAddRule("add rule ip filter OUTPUT ip saddr 10.0.0.0/8 ip daddr 224.0.0.0/24 counter accept");
    nftablesAddRule("add rule ip filter OUTPUT ip saddr 172.16.0.0/12 ip daddr 224.0.0.0/24 counter accept");

    // Simple Service Discovery Protocol address

    nftablesAddRule("add rule ip filter OUTPUT ip saddr 192.168.0.0/16 ip daddr 239.255.255.250 counter accept");
    nftablesAddRule("add rule ip filter OUTPUT ip saddr 10.0.0.0/8 ip daddr 239.255.255.250 counter accept");
    nftablesAddRule("add rule ip filter OUTPUT ip saddr 172.16.0.0/12 ip daddr 239.255.255.250 counter accept");

    // Service Location Protocol version 2 address

    nftablesAddRule("add rule ip filter OUTPUT ip saddr 192.168.0.0/16 ip daddr 239.255.255.253 counter accept");
    nftablesAddRule("add rule ip filter OUTPUT ip saddr 10.0.0.0/8 ip daddr 239.255.255.253 counter accept");
    nftablesAddRule("add rule ip filter OUTPUT ip saddr 172.16.0.0/12 ip daddr 239.255.255.253 counter accept");

    // Allow ping

    // nftablesAddRule("add rule ip filter OUTPUT icmp type echo-reply counter accept");

    // Allow all TUN interfaces

    nftablesAddRule("add rule ip filter OUTPUT oifname \"tun*\" counter accept");

    // Allow established sessions

    nftablesAddRule("add rule ip filter OUTPUT ct state established  counter accept");

    // IPv6

    if(localNetwork->isIPv6Enabled() == true)
    {
        nftablesAddRule("add table ip6 mangle");
        nftablesAddRule("add chain ip6 mangle PREROUTING { type filter hook prerouting priority -150; policy accept; }");
        nftablesAddRule("add chain ip6 mangle INPUT { type filter hook input priority -150; policy accept; }");
        nftablesAddRule("add chain ip6 mangle FORWARD { type filter hook forward priority -150; policy accept; }");
        nftablesAddRule("add chain ip6 mangle OUTPUT { type route hook output priority -150; policy accept; }");
        nftablesAddRule("add chain ip6 mangle POSTROUTING { type filter hook postrouting priority -150; policy accept; }");
        nftablesAddRule("add table ip6 nat");
        nftablesAddRule("add chain ip6 nat PREROUTING { type nat hook prerouting priority -100; policy accept; }");
        nftablesAddRule("add chain ip6 nat INPUT { type nat hook input priority 100; policy accept; }");
        nftablesAddRule("add chain ip6 nat OUTPUT { type nat hook output priority -100; policy accept; }");
        nftablesAddRule("add chain ip6 nat POSTROUTING { type nat hook postrouting priority 100; policy accept; }");
        nftablesAddRule("add table ip6 filter");
        nftablesAddRule("add chain ip6 filter INPUT { type filter hook input priority 0; policy drop; }");
        nftablesAddRule("add chain ip6 filter FORWARD { type filter hook forward priority 0; policy drop; }");
        nftablesAddRule("add chain ip6 filter OUTPUT { type filter hook output priority 0; policy drop; }");

        // Accept local network

        nftablesAddRule("add rule ip6 filter INPUT iifname \"lo\" counter accept");

        // Reject traffic to localhost not coming from local interface

        nftablesAddRule("add rule ip6 filter INPUT iifname != \"lo\" ip6 saddr ::1 counter reject");

        // Disable processing of any RH0 packet which could allow a ping-pong of packets

        nftablesAddRule("add rule ip6 filter INPUT rt type 0 counter drop");
            
        // nftablesAddRule("add rule ip6 filter INPUT icmpv6 type echo-request counter accept");

        // Allow private network

        nftablesAddRule("add rule ip6 filter INPUT ip6 saddr fe80::/10 counter accept");

        // Allow multicast

        nftablesAddRule("add rule ip6 filter INPUT ip6 daddr ff00::/8 counter accept");

        // Allow ping

        // nftablesAddRule("add rule ip6 filter INPUT meta l4proto ipv6-icmp counter accept");

        // Allow established sessions

        nftablesAddRule("add rule ip6 filter INPUT ct state related,established  counter accept");

        // Allow all TUN interfaces

        nftablesAddRule("add rule ip6 filter INPUT iifname \"tun*\" counter accept");

        // Reject everything else

        nftablesAddRule("add rule ip6 filter INPUT counter drop");

        // Disable processing of any RH0 packet which could allow a ping-pong of packets

        nftablesAddRule("add rule ip6 filter FORWARD rt type 0 counter drop");

        // Allow TUN forwarding

        nftablesAddRule("add rule ip6 filter FORWARD iifname \"tun*\" counter accept");

        // Reject every other forwarding

        nftablesAddRule("add rule ip6 filter FORWARD counter drop");

        // Allow local traffic

        nftablesAddRule("add rule ip6 filter OUTPUT oifname \"lo\" counter accept");

        // Disable processing of any RH0 packet which could allow a ping-pong of packets

        nftablesAddRule("add rule ip6 filter OUTPUT rt type 0 counter drop");

        // Allow private network

        nftablesAddRule("add rule ip6 filter OUTPUT ip6 saddr fe80::/10 counter accept");

        // Allow multicast

        nftablesAddRule("add rule ip6 filter OUTPUT ip6 daddr ff00::/8 counter accept");

        // Allow ping

        nftablesAddRule("add rule ip6 filter OUTPUT meta l4proto ipv6-icmp counter accept");

        // Allow TUN

        nftablesAddRule("add rule ip6 filter OUTPUT oifname \"tun*\" counter accept");

        // Allow established sessions

        nftablesAddRule("add rule ip6 filter OUTPUT ct state established  counter accept");
    }
}

// iptables

bool NetFilter::isIptableFilterAvailable()
{
    return iptableFilterAvailable;
}

bool NetFilter::isIptableNatAvailable()
{
    return iptableNatAvailable;
}

bool NetFilter::isIptableMangleAvailable()
{
    return iptableMangleAvailable;
}

bool NetFilter::isIptableSecurityAvailable()
{
    return iptableSecurityAvailable;
}

bool NetFilter::isIptableRawAvailable()
{
    return iptableRawAvailable;
}

bool NetFilter::isIp6tableFilterAvailable()
{
    return ip6tableFilterAvailable;
}

bool NetFilter::isIp6tableNatAvailable()
{
    return ip6tableNatAvailable;
}

bool NetFilter::isIp6tableMangleAvailable()
{
    return ip6tableMangleAvailable;
}

bool NetFilter::isIp6tableSecurityAvailable()
{
    return ip6tableSecurityAvailable;
}

bool NetFilter::isIp6tableRawAvailable()
{
    return ip6tableRawAvailable;
}

void NetFilter::iptablesResetRules(IPFamily ipFamily)
{
    if(ipFamily == IPFamily::IPv4)
        iptablesRules = "";
    else
        ip6tablesRules = "";
}

std::string NetFilter::createIptablesGenericRule(Hook hook, const std::string &interface, Protocol protocol, const std::string &sourceIP, int sourcePort, const std::string &destinationIP, int destinationPort)
{
    std::string rule = "";

    if(hook == Hook::INPUT)
        rule += "INPUT";
    else
        rule += "OUTPUT";

    if(interface != "")
    {
        if(hook == Hook::INPUT)
            rule += " -i ";
        else
            rule += " -o ";

        rule += interface;
    }

    if(protocol != Protocol::ANY)
    {
        rule += " -p ";

        if(protocol == Protocol::TCP)
            rule += "tcp";
        else
            rule += "udp";
    }

    if(sourceIP != "")
    {
        rule += " -s " + sourceIP;
    }

    if(sourcePort > 0)
    {
        rule += " --sport ";
        rule += sourcePort;
    }

    if(destinationIP != "")
    {
        rule += " -d " + destinationIP;
    }

    if(destinationPort > 0)
    {
        rule += " --dport ";
        rule += destinationPort;
    }

    return rule;
}

void NetFilter::iptablesAddRule(IPFamily ipFamily, const std::string &rule)
{
    if(rule.empty() == true)
        return;

    if(ipFamily == IPFamily::IPv4)
    {
        iptablesRules += rule;
        iptablesRules += "\n";
    }
    else
    {
        ip6tablesRules += rule;
        ip6tablesRules += "\n";
    }
}

bool NetFilter::iptablesCommitRule(IPFamily ipFamily, const std::string &rule)
{
    bool retval = false;
    char *exec_args[EXEC_MAX_ARGS];
    int n;

    if(rule.empty() == true)
        return false;

    strcpy(binpath, "");

    if(ipFamily == IPFamily::IPv4)
        get_exec_path(iptablesBinary, binpath);
    else
        get_exec_path(ip6tablesBinary, binpath);

    if(strcmp(binpath, "") != 0)
    {
        retval = true;

        std::istringstream buf(rule);
        std::istream_iterator<std::string> beg(buf), end;

        std::vector<std::string> tokens(beg, end);

        n = 0;

        exec_args[n++] = binpath;

        for(auto &s : tokens)
        {
            exec_args[n++] = (char *)s.c_str();

            if(n == EXEC_MAX_ARGS)
                return false;
        }

        exec_args[n++] = NULL;

        if(execute_process_args(NULL, NULL, binpath, exec_args) != 0)
            retval = false;
    }
    else
        retval = false;

    return retval;
}

bool NetFilter::iptablesCommitItem(Item *item, Action action)
{
    std::string rule;
    bool retval = false;

    if(item == nullptr)
        return false;

    rule = translateItemToIPTables(item, action);

    if(rule.empty() == true)
        return false;

    if(item->getFamily() == Item::FAMILY_IPV4)
    {
        retval = iptablesCommitRule(IPFamily::IPv4, rule);
    }
    else if(item->getFamily() == Item::FAMILY_IPV6 && localNetwork->isIPv6Enabled() == true)
    {
        retval = iptablesCommitRule(IPFamily::IPv6, rule);
    }
    else if(item->getFamily() == Item::FAMILY_INET)
    {
        retval = iptablesCommitRule(IPFamily::IPv4, rule);
        
        if(retval == true && localNetwork->isIPv6Enabled() == true)
            retval = iptablesCommitRule(IPFamily::IPv6, rule);
    }
    else
        retval = false;
    
    return retval;
}

bool NetFilter::iptablesCommit(IPFamily ipFamily)
{
    bool retval = true, doCommit = false;
    std::string rules = "";

    if(itemList.size() == 0)
        return false;

    if(ipFamily == IPFamily::IPv6 && localNetwork->isIPv6Enabled() == false)
        return false;

    for(Item *item : itemList)
    {
        if(item->getItemType() == Item::Type::RULE && item->isTargetIPTables())
        {
            doCommit = false;

            if(item->getFamily() == Item::FAMILY_IPV4 && ipFamily == IPFamily::IPv4)
                doCommit = true;
            else if(item->getFamily() == Item::FAMILY_IPV6 && ipFamily == IPFamily::IPv6)
                doCommit = true;
            else if(item->getFamily() == Item::FAMILY_INET)
                doCommit = true;
            else
                doCommit = false;

            if(doCommit == true)
                rules += translateItemToIPTables(item, Action::ADD) + "\n";
        }
    }

    if(rules.empty() == true)
        return false;

    strcpy(binpath, "");

    if(ipFamily == IPFamily::IPv4)
        get_exec_path(iptablesRestoreBinary, binpath);
    else
        get_exec_path(ip6tablesRestoreBinary, binpath);

    if(strcmp(binpath, "") != 0)
    {
        iptablesFlush(ipFamily);

        if(execute_process((char *)rules.c_str(), NULL, binpath, NULL) == 0)
            retval = true;
        else
            retval = false;
    }
    else
        retval = false;

    return retval;
}

bool NetFilter::iptablesSave(IPFamily ipFamily)
{
    bool retval = false;
    std::ofstream dumpFile;
    std::string fileName;

    if(workingDirectory == "")
        return false;

    strcpy(binpath, "");

    fileName = workingDirectory;
    fileName += "/";

    if(ipFamily == IPFamily::IPv4)
    {
        get_exec_path(iptablesSaveBinary, binpath);

        fileName += iptablesSaveFile;
    }
    else
    {
        get_exec_path(ip6tablesSaveBinary, binpath);

        fileName += ip6tablesSaveFile;
    }

    if(strcmp(binpath, "") != 0)
    {
        if(execute_process(NULL, charBuffer, binpath, NULL) == 0)
        {
            dumpFile.open(fileName);

            if(dumpFile.good())
            {
                dumpFile << charBuffer << std::endl;

                dumpFile.close();

                retval = true;
            }
            else
                retval = false;
        }
        else
            retval = false;
    }
    else
        retval = false;

    return retval;
}

bool NetFilter::iptablesRestore(IPFamily ipFamily)
{
    bool retval = false;
    std::string backupFileName;

    if(workingDirectory == "")
        return false;

    strcpy(binpath, "");

    backupFileName = workingDirectory;
    backupFileName += "/";

    if(ipFamily == IPFamily::IPv4)
    {
        get_exec_path(iptablesRestoreBinary, binpath);

        backupFileName += iptablesSaveFile;
    }
    else
    {
        get_exec_path(ip6tablesRestoreBinary, binpath);

        backupFileName += ip6tablesSaveFile;
    }

    if(strcmp(binpath, "") != 0 && access(backupFileName.c_str(), F_OK) == 0)
    {
        if(readFile(backupFileName.c_str(), charBuffer, charBufferSize) == true)
        {
            iptablesFlush(ipFamily);

            if(execute_process(charBuffer, NULL, binpath, NULL) == 0)
            {
                unlink(backupFileName.c_str());

                retval = true;
            }
            else
                retval = false;
        }
        else
            retval = false;
    }
    else
        retval = false;

    return retval;
}

bool NetFilter::iptablesFlush(IPFamily ipFamily)
{
    bool retval = false;
    char cmdpath[64];

    strcpy(cmdpath, "");

    if(ipFamily == IPFamily::IPv4)
        get_exec_path(iptablesBinary, cmdpath);
    else
        get_exec_path(ip6tablesBinary, cmdpath);

    if(strcmp(cmdpath, "") != 0)
    {
        retval = true;

        if(iptableFilterAvailable == true)
        {
            if(execute_process(NULL, NULL, cmdpath, "-t", "filter", "-F", NULL) != 0)
                retval = false;
        }
   
        if(iptableNatAvailable == true)
        {
            if(execute_process(NULL, NULL, cmdpath, "-t", "nat", "-F", NULL) != 0)
                retval = false;
        }

        if(iptableMangleAvailable == true)
        {
            if(execute_process(NULL, NULL, cmdpath, "-t", "mangle", "-F", NULL) != 0)
                retval = false;
        }

        if(iptableRawAvailable == true)
        {
            if(execute_process(NULL, NULL, cmdpath, "-t", "raw", "-F", NULL) != 0)
                retval = false;
        }

        if(iptableSecurityAvailable == true)
        {
            if(execute_process(NULL, NULL, cmdpath, "-t", "security", "-F", NULL) != 0)
                retval = false;
        }

        if(execute_process(NULL, NULL, cmdpath, "-F", NULL) != 0)
            retval = false;

        if(execute_process(NULL, NULL, cmdpath, "-X", NULL) != 0)
            retval = false;
    }
    else
        retval = false;

    return retval;
}

void NetFilter::iptablesSetup(const std::string &loopbackIface)
{
    // IPv4

    iptablesResetRules(IPFamily::IPv4);

    if(iptableMangleAvailable == true)
    {
        iptablesAddRule(IPFamily::IPv4, "*mangle");
        iptablesAddRule(IPFamily::IPv4, ":PREROUTING ACCEPT [0:0]");
        iptablesAddRule(IPFamily::IPv4, ":INPUT ACCEPT [0:0]");
        iptablesAddRule(IPFamily::IPv4, ":FORWARD ACCEPT [0:0]");
        iptablesAddRule(IPFamily::IPv4, ":OUTPUT ACCEPT [0:0]");
        iptablesAddRule(IPFamily::IPv4, ":POSTROUTING ACCEPT [0:0]");
        iptablesAddRule(IPFamily::IPv4, "COMMIT");
    }

    if(iptableNatAvailable == true)
    {
        iptablesAddRule(IPFamily::IPv4, "*nat");
        iptablesAddRule(IPFamily::IPv4, ":PREROUTING ACCEPT [0:0]");
        iptablesAddRule(IPFamily::IPv4, ":INPUT ACCEPT [0:0]");
        iptablesAddRule(IPFamily::IPv4, ":OUTPUT ACCEPT [0:0]");
        iptablesAddRule(IPFamily::IPv4, ":POSTROUTING ACCEPT [0:0]");
        iptablesAddRule(IPFamily::IPv4, "COMMIT");
    }

    if(iptableFilterAvailable == true)
    {
        iptablesAddRule(IPFamily::IPv4, "*filter");
        iptablesAddRule(IPFamily::IPv4, ":INPUT DROP [0:0]");
        iptablesAddRule(IPFamily::IPv4, ":FORWARD DROP [0:0]");
        iptablesAddRule(IPFamily::IPv4, ":OUTPUT DROP [0:0]");
    }

    // Local input

    iptablesAddRule(IPFamily::IPv4, "-A INPUT -i " + loopbackIface + " -j ACCEPT");

    // Accept DHCP

    iptablesAddRule(IPFamily::IPv4, "-A INPUT -s 255.255.255.255/32 -j ACCEPT");

    // Accept local network

    iptablesAddRule(IPFamily::IPv4, "-A INPUT -s 192.168.0.0/16 -d 192.168.0.0/16 -j ACCEPT");
    iptablesAddRule(IPFamily::IPv4, "-A INPUT -s 10.0.0.0/8 -d 10.0.0.0/8 -j ACCEPT");
    iptablesAddRule(IPFamily::IPv4, "-A INPUT -s 172.16.0.0/12 -d 172.16.0.0/12 -j ACCEPT");

    // Accept ping

    // iptablesAddRule(IPFamily::IPv4, "-A INPUT -p icmp -m icmp --icmp-type 8 -j ACCEPT");

    // Accept established sessions

    iptablesAddRule(IPFamily::IPv4, "-A INPUT -m state --state RELATED,ESTABLISHED -j ACCEPT");

    // Accept all tun interfaces

    iptablesAddRule(IPFamily::IPv4, "-A INPUT -i tun+ -j ACCEPT");

    // Reject everything else

    iptablesAddRule(IPFamily::IPv4, "-A INPUT -j DROP");

    // Accept TUN forward

    iptablesAddRule(IPFamily::IPv4, "-A FORWARD -i tun+ -j ACCEPT");

    // Reject all the other forwarding

    iptablesAddRule(IPFamily::IPv4, "-A FORWARD -j DROP");

    // Local output

    iptablesAddRule(IPFamily::IPv4, "-A OUTPUT -o " + loopbackIface + " -j ACCEPT");

    // Accept DHCP

    iptablesAddRule(IPFamily::IPv4, "-A OUTPUT -d 255.255.255.255/32 -j ACCEPT");

    // Accept local network

    iptablesAddRule(IPFamily::IPv4, "-A OUTPUT -s 192.168.0.0/16 -d 192.168.0.0/16 -j ACCEPT");
    iptablesAddRule(IPFamily::IPv4, "-A OUTPUT -s 10.0.0.0/8 -d 10.0.0.0/8 -j ACCEPT");
    iptablesAddRule(IPFamily::IPv4, "-A OUTPUT -s 172.16.0.0/12 -d 172.16.0.0/12 -j ACCEPT");

    // Allow multicast

    iptablesAddRule(IPFamily::IPv4, "-A OUTPUT -s 192.168.0.0/16 -d 224.0.0.0/24 -j ACCEPT");
    iptablesAddRule(IPFamily::IPv4, "-A OUTPUT -s 10.0.0.0/8 -d 224.0.0.0/24 -j ACCEPT");
    iptablesAddRule(IPFamily::IPv4, "-A OUTPUT -s 172.16.0.0/12 -d 224.0.0.0/24 -j ACCEPT");

    // Simple Service Discovery Protocol address

    iptablesAddRule(IPFamily::IPv4, "-A OUTPUT -s 192.168.0.0/16 -d 239.255.255.250/32 -j ACCEPT");
    iptablesAddRule(IPFamily::IPv4, "-A OUTPUT -s 10.0.0.0/8 -d 239.255.255.250/32 -j ACCEPT");
    iptablesAddRule(IPFamily::IPv4, "-A OUTPUT -s 172.16.0.0/12 -d 239.255.255.250/32 -j ACCEPT");

    // Service Location Protocol version 2 address

    iptablesAddRule(IPFamily::IPv4, "-A OUTPUT -s 192.168.0.0/16 -d 239.255.255.253/32 -j ACCEPT");
    iptablesAddRule(IPFamily::IPv4, "-A OUTPUT -s 10.0.0.0/8 -d 239.255.255.253/32 -j ACCEPT");
    iptablesAddRule(IPFamily::IPv4, "-A OUTPUT -s 172.16.0.0/12 -d 239.255.255.253/32 -j ACCEPT");

    // Allow ping

    // iptablesAddRule(IPFamily::IPv4, "-A OUTPUT -p icmp -m icmp --icmp-type 0 -j ACCEPT");

    // Allow all TUN interfaces

    iptablesAddRule(IPFamily::IPv4, "-A OUTPUT -o tun+ -j ACCEPT");

    // Allow established sessions

    iptablesAddRule(IPFamily::IPv4, "-A OUTPUT -m state --state ESTABLISHED -j ACCEPT");

    // IPv6

    iptablesResetRules(IPFamily::IPv6);

    if(ip6tableMangleAvailable == true)
    {
        iptablesAddRule(IPFamily::IPv6, "*mangle");
        iptablesAddRule(IPFamily::IPv6, ":PREROUTING ACCEPT [0:0]");
        iptablesAddRule(IPFamily::IPv6, ":INPUT ACCEPT [0:0]");
        iptablesAddRule(IPFamily::IPv6, ":FORWARD ACCEPT [0:0]");
        iptablesAddRule(IPFamily::IPv6, ":OUTPUT ACCEPT [0:0]");
        iptablesAddRule(IPFamily::IPv6, ":POSTROUTING ACCEPT [0:0]");
        iptablesAddRule(IPFamily::IPv6, "COMMIT");
    }

    if(ip6tableNatAvailable == true)
    {
        iptablesAddRule(IPFamily::IPv6, "*nat");
        iptablesAddRule(IPFamily::IPv6, ":PREROUTING ACCEPT [0:0]");
        iptablesAddRule(IPFamily::IPv6, ":INPUT ACCEPT [0:0]");
        iptablesAddRule(IPFamily::IPv6, ":OUTPUT ACCEPT [0:0]");
        iptablesAddRule(IPFamily::IPv6, ":POSTROUTING ACCEPT [0:0]");
        iptablesAddRule(IPFamily::IPv6, "COMMIT");
    }

    if(ip6tableFilterAvailable == true)
    {
        iptablesAddRule(IPFamily::IPv6, "*filter");
        iptablesAddRule(IPFamily::IPv6, ":INPUT DROP [0:0]");
        iptablesAddRule(IPFamily::IPv6, ":FORWARD DROP [0:0]");
        iptablesAddRule(IPFamily::IPv6, ":OUTPUT DROP [0:0]");

        // Accept local network

        iptablesAddRule(IPFamily::IPv6, "-A INPUT -i " + loopbackIface + " -j ACCEPT");

        // Reject traffic to localhost not coming from local interface

        iptablesAddRule(IPFamily::IPv6, "-A INPUT -s ::1/128 ! -i " + loopbackIface + " -j REJECT --reject-with icmp6-port-unreachable");

        // Disable processing of any RH0 packet which could allow a ping-pong of packets

        iptablesAddRule(IPFamily::IPv6, "-A INPUT -m rt --rt-type 0 -j DROP");

        // icmpv6-type:router-advertisement - Rules which are required for your IPv6 address to be properly allocated

        // iptablesAddRule(IPFamily::IPv6, "-A INPUT -p ipv6-icmp -m icmp6 --icmpv6-type 134 -m hl --hl-eq 255 -j ACCEPT");

        // icmpv6-type:neighbor-solicitation - Rules which are required for your IPv6 address to be properly allocated

        // iptablesAddRule(IPFamily::IPv6, "-A INPUT -p ipv6-icmp -m icmp6 --icmpv6-type 135 -m hl --hl-eq 255 -j ACCEPT");

        // icmpv6-type:neighbor-advertisement - Rules which are required for your IPv6 address to be properly allocated

        // iptablesAddRule(IPFamily::IPv6, "-A INPUT -p ipv6-icmp -m icmp6 --icmpv6-type 136 -m hl --hl-eq 255 -j ACCEPT");

        // icmpv6-type:redirect - Rules which are required for your IPv6 address to be properly allocated

        // iptablesAddRule(IPFamily::IPv6, "-A INPUT -p ipv6-icmp -m icmp6 --icmpv6-type 137 -m hl --hl-eq 255 -j ACCEPT");

        // Allow private network

        iptablesAddRule(IPFamily::IPv6, "-A INPUT -s fe80::/10 -j ACCEPT");

        // Allow multicast

        iptablesAddRule(IPFamily::IPv6, "-A INPUT -d ff00::/8 -j ACCEPT");

        // Allow ping

        // iptablesAddRule(IPFamily::IPv6, "-A INPUT -p ipv6-icmp -j ACCEPT");

        // Allow established sessions

        iptablesAddRule(IPFamily::IPv6, "-A INPUT -m state --state RELATED,ESTABLISHED -j ACCEPT");

        // Allow all TUN interfaces

        iptablesAddRule(IPFamily::IPv6, "-A INPUT -i tun+ -j ACCEPT");

        // Reject everything else

        iptablesAddRule(IPFamily::IPv6, "-A INPUT -j DROP");

        // Disable processing of any RH0 packet which could allow a ping-pong of packets

        iptablesAddRule(IPFamily::IPv6, "-A FORWARD -m rt --rt-type 0 -j DROP");

        // Allow TUN forwarding

        iptablesAddRule(IPFamily::IPv6, "-A FORWARD -i tun+ -j ACCEPT");

        // Reject every other forwarding

        iptablesAddRule(IPFamily::IPv6, "-A FORWARD -j DROP");

        // Allow local traffic

        iptablesAddRule(IPFamily::IPv6, "-A OUTPUT -o " + loopbackIface + " -j ACCEPT");

        // Disable processing of any RH0 packet which could allow a ping-pong of packets

        iptablesAddRule(IPFamily::IPv6, "-A OUTPUT -m rt --rt-type 0 -j DROP");

        // Allow private network

        iptablesAddRule(IPFamily::IPv6, "-A OUTPUT -s fe80::/10 -j ACCEPT");

        // Allow multicast

        iptablesAddRule(IPFamily::IPv6, "-A OUTPUT -d ff00::/8 -j ACCEPT");

        // Allow ping

        // iptablesAddRule(IPFamily::IPv6, "-A OUTPUT -p ipv6-icmp -j ACCEPT");

        // Allow TUN

        iptablesAddRule(IPFamily::IPv6, "-A OUTPUT -o tun+ -j ACCEPT");

        // Allow established sessions

        iptablesAddRule(IPFamily::IPv6, "-A OUTPUT -m state --state ESTABLISHED -j ACCEPT");
    }
}

// pf

void NetFilter::pfResetRules()
{
    pfRules = "";
}

std::string NetFilter::createPfGenericRule(IPFamily ipFamily, Hook hook, const std::string &interface, Protocol protocol, const std::string &sourceIP, int sourcePort, const std::string &destinationIP, int destinationPort)
{
    std::string rule = "";

    if(hook == Hook::INPUT)
        rule += "in";
    else
        rule += "out";

    rule += " quick";

    if(interface != "")
        rule += " on " + interface;

    rule += " inet";

    if(ipFamily == IPFamily::IPv6)
    {
        if(localNetwork->isIPv6Enabled() == true)
            rule += "6";
        else
            return "";
    }

    if(protocol != Protocol::ANY)
    {
        rule += " proto ";

        if(protocol == Protocol::TCP)
            rule += "tcp";
        else
            rule += "udp";
    }

    if(sourceIP != "")
    {
        rule += " from " + sourceIP;

        if(sourcePort > 0)
        {
            rule += " port ";
            rule += sourcePort;
        }
    }

    if(destinationIP != "")
    {
        rule += " to " + destinationIP;

        if(destinationPort > 0)
        {
            rule += " port ";
            rule += destinationPort;
        }
    }

    rule += " keep state";

    return rule;
}

void NetFilter::pfAddRule(const std::string &rule)
{
    if(rule.empty() == true)
        return;

    pfRules += rule;
    pfRules += "\n";
}

bool NetFilter::pfEnable()
{
    bool retval = false;
    std::string ruleFileName, rules;
    std::ofstream outputFile;

    if(workingDirectory == "")
        return false;

    strcpy(binpath, "");

    get_exec_path(pfctlBinary, binpath);

    if(strcmp(binpath, "") != 0)
    {
        if(execute_process(NULL, NULL, binpath, "-e", NULL) == 0)
            retval = true;
        else
            retval = false;
    }
    else
        retval = false;

    return retval;
}

bool NetFilter::pfCommit()
{
    bool retval = false;
    std::string ruleFileName = "", rules = "";
    std::ofstream outputFile;

    if(itemList.size() == 0)
        return false;

    for(Item *item : itemList)
    {
        if(item->getItemType() == Item::Type::RULE && item->isTargetPF())
            rules += translateItemToPF(item, Action::ADD) + "\n";
    }

    if(rules.empty() == true)
        return false;

    if(workingDirectory == "")
        return false;

    ruleFileName = workingDirectory;
    ruleFileName += "/";
    ruleFileName += pfNetFilterRulesFile;

    if(access(ruleFileName.c_str(), F_OK) == 0)
        unlink(ruleFileName.c_str());

    strcpy(binpath, "");

    get_exec_path(pfctlBinary, binpath);

    if(strcmp(binpath, "") != 0)
    {
        pfFlush();

        outputFile.open(ruleFileName);

        if(outputFile.good())
        {
            outputFile << rules << std::endl;

            outputFile.close();

            if(execute_process(NULL, NULL, binpath, "-f", ruleFileName.c_str(), NULL) == 0)
                retval = true;
            else
                retval = false;

            unlink(ruleFileName.c_str());
        }
        else
            retval = false;
    }
    else
        retval = false;

    return retval;
}

bool NetFilter::pfSave()
{
    std::ifstream inputFile;
    std::ofstream outputFile;
    std::string outputFileName;

    if(workingDirectory == "")
        return false;

    if(access(pfConfFile.c_str(), F_OK) != 0)
        return false;

    outputFileName = workingDirectory;
    outputFileName += "/";
    outputFileName += pfSaveFile;

    inputFile.open(pfConfFile);
    outputFile.open(outputFileName);

    outputFile << inputFile.rdbuf();

    inputFile.close();
    outputFile.close();

    return true;
}

bool NetFilter::pfRestore()
{
    bool retval = false;
    std::string fileName;

    if(workingDirectory == "")
        return false;

    strcpy(binpath, "");

    fileName = workingDirectory;
    fileName += "/";
    fileName += pfSaveFile;

    get_exec_path(pfctlBinary, binpath);

    if(strcmp(binpath, "") != 0 && access(fileName.c_str(), F_OK) == 0)
    {
        pfFlushAll();

        pfNeedsFullFlush = true;

        if(execute_process(NULL, NULL, binpath, "-f", fileName.c_str(), NULL) == 0)
        {
            unlink(fileName.c_str());

            retval = true;
        }
        else
            retval = false;
    }
    else
        retval = false;

    return retval;
}

bool NetFilter::pfFlush()
{
    bool result = false;

    if(pfNeedsFullFlush == true)
        result = pfFlushAll();
    else
        result = pfFlushRules();

    return result;
}

bool NetFilter::pfFlushAll()
{
    bool retval = false;
    char cmdpath[64];

    strcpy(cmdpath, "");

    get_exec_path(pfctlBinary, cmdpath);

    if(strcmp(cmdpath, "") != 0)
    {
        if(execute_process(NULL, NULL, cmdpath, "-F", "all", NULL) == 0)
        {
            retval = true;

            pfNeedsFullFlush = false;
        }
        else
            retval = false;
    }
    else
        retval = false;

    return retval;
}

bool NetFilter::pfFlushRules()
{
    bool retval = false;
    char cmdpath[64];

    strcpy(cmdpath, "");

    get_exec_path(pfctlBinary, cmdpath);

    if(strcmp(cmdpath, "") != 0)
    {
        if(execute_process(NULL, NULL, cmdpath, "-F", "nat", NULL) != 0)
            return false;

        if(execute_process(NULL, NULL, cmdpath, "-F", "queue", NULL) != 0)
            return false;

        if(execute_process(NULL, NULL, cmdpath, "-F", "rules", NULL) != 0)
            return false;

        if(execute_process(NULL, NULL, cmdpath, "-F", "Tables", NULL) != 0)
            return false;
    }
    else
        retval = false;

    return retval;
}

void NetFilter::pfSetup(const std::string &loopbackIface)
{
    pfResetRules();

    pfAddRule("set block-policy drop");
    pfAddRule("set ruleset-optimization basic");

    pfAddRule("set skip on { " + loopbackIface + " }");

    pfAddRule("scrub in all");

    pfAddRule("block in all");
    pfAddRule("block out all");

    // IPv4

    // Local networks

    pfAddRule("pass out quick inet from 192.168.0.0/16 to 192.168.0.0/16");
    pfAddRule("pass in quick inet from 192.168.0.0/16 to 192.168.0.0/16");
    pfAddRule("pass out quick inet from 172.16.0.0/12 to 172.16.0.0/12");
    pfAddRule("pass in quick inet from 172.16.0.0/12 to 172.16.0.0/12");
    pfAddRule("pass out quick inet from 10.0.0.0/8 to 10.0.0.0/8");
    pfAddRule("pass in quick inet from 10.0.0.0/8 to 10.0.0.0/8");

    // Multicast

    pfAddRule("pass out quick inet from 192.168.0.0/16 to 224.0.0.0/24");
    pfAddRule("pass out quick inet from 172.16.0.0/12 to 224.0.0.0/24");
    pfAddRule("pass out quick inet from 10.0.0.0/8 to 224.0.0.0/24");

    // Simple Service Discovery Protocol address

    pfAddRule("pass out quick inet from 192.168.0.0/16 to 239.255.255.250/32");
    pfAddRule("pass out quick inet from 172.16.0.0/12 to 239.255.255.250/32");
    pfAddRule("pass out quick inet from 10.0.0.0/8 to 239.255.255.250/32");

    // Service Location Protocol version 2 address

    pfAddRule("pass out quick inet from 192.168.0.0/16 to 239.255.255.253/32");
    pfAddRule("pass out quick inet from 172.16.0.0/12 to 239.255.255.253/32");
    pfAddRule("pass out quick inet from 10.0.0.0/8 to 239.255.255.253/32");

    // ICMP

    pfAddRule("pass out quick inet proto icmp all");

    // IPv6

    if(localNetwork->isIPv6Enabled() == true)
    {
        // Local networks

        pfAddRule("pass out quick inet6 from fe80::/10 to fe80::/10");
        pfAddRule("pass in quick inet6 from fe80::/10 to fe80::/10");
        
        // Multicast

        pfAddRule("pass out quick inet6 from ff00::/8 to ff00::/8");
        pfAddRule("pass in quick inet6 from ff00::/8 to ff00::/8");

        // ICMP

        pfAddRule("pass out quick inet6 proto icmp6 all");
    }
}

bool NetFilter::pfAddIgnoredInterfaces()
{
    std::string pattern = "set skip on {", newStatement = "";
    int patlen = pattern.size();
    Rule *rule = nullptr, *r = nullptr;

    if(itemList.empty() == true)
        return false;

    for(int i = 0; i < itemList.size() && rule == nullptr; i++)
    {
        if(itemList[i]->getItemType() == Item::Type::RULE)
        {
            r = (Rule *)itemList[i];

            if(r->getStatement().substr(0, patlen) == pattern)
                rule = r;
        }
    }
    
    if(rule == nullptr)
        return false;

    newStatement = pattern;
    newStatement += " ";
    newStatement += loopbackInterface;

    for(std::size_t i = 0; i < ignoredInterface.size(); ++i)
    {
        newStatement += " ";
        newStatement += ignoredInterface[i];
    }

    newStatement += " }";

    rule->setStatement(newStatement);

    pfCommit();

    return true;
}

std::string NetFilter::translateItem(Item *item, Action action)
{
    int target;

    switch(filterMode)
    {
        case Mode::NFTABLES:
        {
            target = Item::TARGET_NFTABLES;
        }
        break;

        case Mode::IPTABLES:
        {
            target = Item::TARGET_IPTABLES;
        }
        break;

        case Mode::PF:
        {
            target = Item::TARGET_PF;
        }
        break;

        default:
        {
            return "";
        }
        break;
    }

    return translateItem(item, target, action);
}

std::string NetFilter::translateItem(Item *item, int target, Action action)
{
    std::string text;

    if(item == nullptr)
        return "";

    if((item->getTarget() & target) || (item->getTarget() & Item::TARGET_ALL))
    {
        if(target & Item::TARGET_NFTABLES)
            text = translateItemToNFTables(item, action);
        else if(target & Item::TARGET_IPTABLES)
            text = translateItemToIPTables(item, action);
        else if(target & Item::TARGET_PF)
            text = translateItemToPF(item, action);
        else
            text = "";
    }
    else
        text = "";

    return text;
}

std::string NetFilter::translateItemToNFTables(Item *item, Action action)
{
    std::string rule = "", command = "";
    Table *t = nullptr;
    Chain *c = nullptr;
    Rule *r = nullptr;

    if(item == nullptr)
        return "";

    if(!((item->getTarget() & Item::TARGET_NFTABLES) || (item->getTarget() & Item::TARGET_ALL)))
        return "";

    switch(action)
    {
        case Action::ADD:
        {
            command = "add";
        }
        break;

        case Action::INSERT:
        {
            command = "insert";
        }
        break;

        case Action::DELETE:
        {
            command = "delete";
        }
        break;
    }

    if(action != Action::DELETE)
    {
        if(item->getItemType() == Item::Type::TABLE)
        {
            t = (Table *)item;

            if(t->getName() == "")
                return "";

            rule = command + " table " + t->getFamily() + " " + t->getName();
        }
        else if(item->getItemType() == Item::Type::CHAIN)
        {
            c = (Chain *)item;

            if(c->getName() == Chain::NONE)
                return "";

            rule = command + " chain " + c->getFamily() + " " + c->getTable() + " " + c->getName() + " {";

            if(c->getType().empty() == false)
                rule += " type " + c->getType();

            rule += " hook ";

            switch(c->getHook())
            {
                case Hook::INPUT:
                {
                    rule += "input";
                }
                break;

                case Hook::OUTPUT:
                {
                    rule += "output";
                }
                break;

                case Hook::PREROUTING:
                {
                    rule += "prerouting";
                }
                break;

                case Hook::POSTROUTING:
                {
                    rule += "postrouting";
                }
                break;

                case Hook::FORWARD:
                {
                    rule += "forward";
                }
                break;
            }

            rule += " priority " + std::to_string(c->getPriority()) + ";";

            if(c->getPolicy() != Policy::NONE)
            {
                rule += " policy ";
                
                switch(c->getPolicy())
                {
                    case Policy::DROP:
                    {
                        rule += "drop";
                    }
                    break;

                    case Policy::REJECT:
                    {
                        rule += "reject";
                    }
                    break;

                    case Policy::ACCEPT:
                    {
                        rule += "accept";
                    }
                    break;

                    case Policy::NONE:
                    {
                        rule += "drop";
                    }
                    break;
                }
                
                rule += ";";
            }

            rule += " }";
        }
        else if(item->getItemType() == Item::Type::RULE)
        {
            r = (Rule *)item;

            rule = command + " rule " + r->getFamily() + " " + r->getTable() + " " + r->getChain();

            if(r->getStatement().empty() == false)
            {
                rule += " ";

                rule += r->getStatement();
            }
            else
            {
                if(r->getSourceInterface().empty() == false)
                {
                    if(r->getChain() == Chain::INPUT || r->getChain() == Chain::FORWARD)
                        rule += " iifname ";
                    else if(r->getChain() == Chain::OUTPUT)
                        rule += " oifname ";
                        
                    rule += "\"";
                    rule += r->getSourceInterface();
                    rule += "\"";
                }

                if(r->getSourceAddress().address != "")
                {
                    rule += " ";
                    
                    if(r->getSourceAddress().family == IPFamily::IPv4)
                        rule += "ip";
                    else
                        rule += "ip6";
                    
                    rule += " saddr " + r->getSourceAddress().address;
                    
                    if(r->getSourceAddress().prefixLength != 0)
                        rule += "/" + std::to_string(r->getSourceAddress().prefixLength);
                }

                if(r->getDestinationInterface().empty() == false)
                {
                    if(r->getChain() == Chain::INPUT || r->getChain() == Chain::FORWARD)
                        rule += " iifname ";
                    else if(r->getChain() == Chain::OUTPUT)
                        rule += " oifname ";
                        
                    rule += "\"";
                    rule += r->getDestinationInterface();
                    rule += "\"";
                }

                if(r->getDestinationAddress().address != "")
                {
                    rule += " ";
                    
                    if(r->getDestinationAddress().family == IPFamily::IPv4)
                        rule += "ip";
                    else
                        rule += "ip6";
                    
                    rule += " daddr " + r->getDestinationAddress().address;

                    if(r->getDestinationAddress().prefixLength != 0)
                        rule += "/" + std::to_string(r->getDestinationAddress().prefixLength);
                }

                if(r->getProtocol() != Protocol::ANY)
                {
                    rule += " ";

                    if(r->getProtocol() == Protocol::TCP)
                        rule += "tcp";
                    else
                        rule += "udp";
                }

                if(r->getSourcePort() > 0)
                    rule += " sport " + std::to_string(r->getSourcePort());

                if(r->getDestinationPort() > 0)
                    rule += " dport " + std::to_string(r->getDestinationPort());
            }

            if(r->isCounterSet() == true)
                rule += " counter";

            rule += " ";

            switch(r->getPolicy())
            {
                case Policy::DROP:
                {
                    rule += "drop";
                }
                break;

                case Policy::REJECT:
                {
                    rule += "reject";
                }
                break;

                case Policy::ACCEPT:
                {
                    rule += "accept";
                }
                break;

                case Policy::NONE:
                {
                    rule += "drop";
                }
                break;
            }
        }
        else
            rule = "";
    }
    else
    {
        // Delete command

        switch(item->getItemType())
        {
            case Item::Type::TABLE:
            {
                t = (Table *)item;

                rule = "delete table " + t->getFamily() + " " + t->getName();
            }
            break;

            case Item::Type::CHAIN:
            {
                c = (Chain *)item;

                rule = "delete chain " + c->getFamily() + " " + c->getTable() + " " + c->getName();
            }
            break;

            case Item::Type::RULE:
            {
                if(item->getHandle() > 0)
                {
                    r = (Rule *)item;

                    rule = "delete rule " + r->getFamily() + " " + r->getTable() + " " + r->getChain() + " handle " + std::to_string(r->getHandle());
                }
                else
                {
                    global_log("ERROR: item has no associated handle (rule: " + translateItem(item, Action::ADD) + ")");
                    
                    rule = "";
                }
            }
            break;
        }
    }

    return rule;
}

std::string NetFilter::translateItemToIPTables(Item *item, Action action)
{
    std::string rule = "", command = "";
    Rule *r = nullptr;

    if(item == nullptr)
        return "";

    if(!((item->getTarget() & Item::TARGET_IPTABLES) || (item->getTarget() & Item::TARGET_ALL)))
        return "";

    if(item->getItemType() != Item::Type::RULE)
        return "";

    switch(action)
    {
        case Action::ADD:
        {
            command = "-A";
        }
        break;

        case Action::INSERT:
        {
            command = "-I";
        }
        break;

        case Action::DELETE:
        {
            command = "-D";
        }
        break;
    }

    r = (Rule *)item;

    if(r->getChain() == Chain::NONE && r->getPolicy() == Policy::NONE && r->getStatement().empty() == false)
        return r->getStatement();

    if(r->getChain() != Chain::NONE)
        rule += command + " " + r->getChain();

    if(r->getStatement().empty() == false)
        rule += " " + r->getStatement();
    else
    {
        if(r->getSourceInterface().empty() == false)
        {
            if(r->getChain() == Chain::INPUT || r->getChain() == Chain::FORWARD || r->getChain() == Chain::PREROUTING)
                rule += " -i ";
            else
                rule += " -o ";

            rule += r->getSourceInterface();
        }

        if(r->getDestinationInterface().empty() == false)
        {
            if(r->getChain() == Chain::INPUT || r->getChain() == Chain::FORWARD || r->getChain() == Chain::PREROUTING)
                rule += " -i ";
            else
                rule += " -o ";

            rule += r->getDestinationInterface();
        }

        if(r->getProtocol() != Protocol::ANY)
        {
            rule += " -p ";

            if(r->getProtocol() == Protocol::TCP)
                rule += "tcp";
            else
                rule += "udp";
        }

        if(r->getSourceAddress().address != "")
        {
            rule += " -s " + r->getSourceAddress().address;

            if(r->getSourceAddress().prefixLength != 0)
                rule += "/" + std::to_string(r->getSourceAddress().prefixLength);
        }

        if(r->getSourcePort() > 0)
        {
            rule += " --sport ";

            rule += std::to_string(r->getSourcePort());
        }

        if(r->getDestinationAddress().address != "")
        {
            rule += " -d " + r->getDestinationAddress().address;

            if(r->getDestinationAddress().prefixLength != 0)
                rule += "/" + std::to_string(r->getDestinationAddress().prefixLength);
        }

        if(r->getDestinationPort() > 0)
        {
            rule += " --dport ";

            rule += std::to_string(r->getDestinationPort());
        }
    }

    if(r->getPolicy() != Policy::NONE)
    {
        rule += " -j ";

        switch(r->getPolicy())
        {
            case Policy::DROP:
            {
                rule += "DROP";
            }
            break;

            case Policy::ACCEPT:
            {
                rule += "ACCEPT";
            }
            break;

            default:
            {
                rule += "DROP";
            }
            break;
        }
    }

    return rule;
}

std::string NetFilter::translateItemToPF(Item *item, Action action)
{
    std::string rule = "";
    Rule *r = nullptr;

    if(item == nullptr)
        return "";

    if(!((item->getTarget() & Item::TARGET_PF) || (item->getTarget() & Item::TARGET_ALL)))
        return "";

    if(item->getItemType() != Item::Type::RULE)
        return "";

    r = (Rule *)item;

    if(r->getChain() == Chain::NONE && r->getStatement().empty() == false)
        return r->getStatement();

    if(r->getPolicy() == Policy::ACCEPT)
        rule += "pass";
    else if(r->getPolicy() == Policy::DROP || r->getPolicy() == Policy::REJECT)
        rule += "block";
    else
        return "";

    if(r->getChain() == Chain::INPUT)
        rule += " in";
    else
        rule += " out";

    if(r->isCounterSet())
        rule += " quick";

    if(r->getSourceInterface() != "")
        rule += " on " + r->getSourceInterface();

    if(r->getFamily() == Item::FAMILY_IPV4)
        rule += " inet";
    else if(r->getFamily() == Item::FAMILY_IPV6 && localNetwork->isIPv6Enabled() == true)
        rule += " inet6";
    else
        return "";

    if(r->getStatement().empty() == false)
        rule += " " + r->getStatement();
    else
    {
        if(r->getProtocol() != Protocol::ANY)
        {
            rule += " proto ";

            if(r->getProtocol() == Protocol::TCP)
                rule += "tcp";
            else
                rule += "udp";
        }

        if(r->getSourceAddress().address != "")
        {
            rule += " from " + r->getSourceAddress().address;

            if(r->getSourceAddress().prefixLength != 0)
                rule += "/" + std::to_string(r->getSourceAddress().prefixLength);

            if(r->getSourcePort() > 0)
            {
                rule += " port ";
                rule += r->getSourcePort();
            }
        }

        if(r->getDestinationAddress().address != "")
        {
            rule += " to " + r->getDestinationAddress().address;

            if(r->getDestinationAddress().prefixLength != 0)
                rule += "/" + std::to_string(r->getDestinationAddress().prefixLength);

            if(r->getDestinationPort() > 0)
            {
                rule += " port ";
                rule += r->getDestinationPort();
            }
        }

        rule += " keep state";
    }

    return rule;
}

void NetFilter::checkFilterTableChain(IPFamily ipFamily, NetFilter::Hook hook)
{
    Table *t;
    Chain *c;
    std::string familyName, tableName, chainName, hookName;
    
    switch(ipFamily)
    {
        case IPFamily::IPv4:
        {
            familyName = Item::FAMILY_IPV4;
        }
        break;

        case IPFamily::IPv6:
        {
            familyName = Item::FAMILY_IPV6;
        }
        break;

        case IPFamily::ANY:
        {
            familyName = Item::FAMILY_INET;
        }
        break;
    }

    t = new Table(familyName, Table::FILTER);
    
    if(search(*t) == ITEM_NOT_FOUND)
        append(*t);

    switch(hook)
    {
        case Hook::INPUT:
        {
            hookName = Chain::INPUT;
        }
        break;

        case Hook::OUTPUT:
        {
            hookName = Chain::OUTPUT;
        }
        break;

        case Hook::PREROUTING:
        {
            hookName = Chain::PREROUTING;
        }
        break;

        case Hook::POSTROUTING:
        {
            hookName = Chain::POSTROUTING;
        }
        break;

        case Hook::FORWARD:
        {
            hookName = Chain::FORWARD;
        }
        break;
    };

    c = new Chain(t, hookName);

    if(search(*c) == ITEM_NOT_FOUND)
        append(*c);

    delete t;
    delete c;
}

NetFilter::Rule *NetFilter::createEmptySessionFilterRule(IPFamily ipFamily, Hook hook)
{
    std::string familyName, tableName, chainName, hookName;
    
    switch(ipFamily)
    {
        case IPFamily::IPv4:
        {
            familyName = Item::FAMILY_IPV4;
        }
        break;

        case IPFamily::IPv6:
        {
            familyName = Item::FAMILY_IPV6;
        }
        break;

        case IPFamily::ANY:
        {
            familyName = Item::FAMILY_INET;
        }
        break;
    }

    switch(hook)
    {
        case Hook::INPUT:
        {
            hookName = Chain::INPUT;
        }
        break;

        case Hook::OUTPUT:
        {
            hookName = Chain::OUTPUT;
        }
        break;

        case Hook::PREROUTING:
        {
            hookName = Chain::PREROUTING;
        }
        break;

        case Hook::POSTROUTING:
        {
            hookName = Chain::POSTROUTING;
        }
        break;

        case Hook::FORWARD:
        {
            hookName = Chain::FORWARD;
        }
        break;
    };

    return new Rule(familyName, Table::FILTER, hookName, Item::TARGET_ALL, Validity::SESSION);
}

NetFilter::Rule *NetFilter::createSessionFilterRule(IPFamily ipFamily, Hook hook, const std::string &interface, Protocol protocol, const std::string &sourceIP, int sourcePort, const std::string &destinationIP, int destinationPort)
{
    Rule *r = createEmptySessionFilterRule(ipFamily, hook);

    if(hook == Hook::INPUT || hook == Hook::FORWARD || hook == Hook::PREROUTING)
        r->setSourceInterface(interface);
    else
        r->setDestinationInterface(interface);

    r->setProtocol(protocol);
    r->setSourceAddress(LocalNetwork::parseIpSpecification(sourceIP));
    r->setSourcePort(sourcePort);
    r->setDestinationAddress(LocalNetwork::parseIpSpecification(destinationIP));
    r->setDestinationPort(destinationPort);

    return r;
}

bool NetFilter::rollbackSession()
{
    bool retval = false;
    std::vector<Item::Type> itemSequence{ Item::Type::RULE, Item::Type::CHAIN, Item::Type::TABLE };

    if(itemList.size() == 0)
        return false;

    for(Item::Type type : itemSequence)
    {
        for(Item *item : itemList)
        {
            if(item->getItemType() == type && item->getValidity() == Validity::SESSION)
            {
                remove(*item);

                retval = true;
            }
        }
    }

    return retval;
}

 // NetFilter::Item

NetFilter::Item::Item(Type type, Validity v)
{
    itemType = type;
    family = "";
    name = "";
    validity = v;
    target = Item::TARGET_ALL;

    handle = 0;
}

NetFilter::Item::~Item()
{
}

NetFilter::Item::Type NetFilter::Item::getItemType() const
{
    return itemType;
}

void NetFilter::Item::setFamily(const std::string &f)
{
    if(f != Item::FAMILY_IPV4 && f != Item::FAMILY_IPV6 && f != Item::FAMILY_INET && f != Item::FAMILY_ARP && f != Item::FAMILY_BRIDGE && f != Item::FAMILY_NETDEV)
        throw NetFilterException("Invalid family \"" + f + "\"");
    
    family = f;
}

std::string NetFilter::Item::getFamily() const
{
    return family;
}

void NetFilter::Item::setName(const std::string &n)
{
    if(n.empty() && (itemType == TABLE || itemType == CHAIN))
        throw NetFilterException("Name is empty");
    
    name = n;
}

std::string NetFilter::Item::getName() const
{
    return name;
}

NetFilter::Validity NetFilter::Item::getValidity()
{
    return validity;
}

void NetFilter::Item::setHandle(unsigned long h)
{
    handle = h;
}

unsigned long NetFilter::Item::getHandle()
{
    return handle;
}

void NetFilter::Item::setTarget(int t)
{
    target = t;
}

int NetFilter::Item::getTarget()
{
    return target;
}

bool NetFilter::Item::isTargetAll()
{
    return (target & TARGET_ALL);
}

bool NetFilter::Item::isTargetNFTables()
{
    return (target & TARGET_ALL) || (target & TARGET_NFTABLES);
}

bool NetFilter::Item::isTargetIPTables()
{
    return (target & TARGET_ALL) || (target & TARGET_IPTABLES);
}

bool NetFilter::Item::isTargetPF()
{
    return (target & TARGET_ALL) || (target & TARGET_PF);
}

int NetFilter::search(NetFilter::Item &item)
{
    int position = ITEM_NOT_FOUND;

    if(itemList.empty() == true)
        return ITEM_NOT_FOUND;

    for(int i = 0; i < itemList.size() && position == ITEM_NOT_FOUND; i++)
    {
        if(itemList[i]->getItemType() == item.getItemType())
        {
            switch(itemList[i]->getItemType())
            {
                case Item::Type::TABLE:
                {
                    if(*(Table *)itemList[i] == *(Table *)&item)
                        position = i;
                }
                break;

                case Item::Type::CHAIN:
                {
                    if(*(Chain *)itemList[i] == *(Chain *)&item)
                        position = i;
                }
                break;

                case Item::Type::RULE:
                {
                    if(*(Rule *)itemList[i] == *(Rule *)&item)
                        position = i;
                }
                break;
            }
        }
    }

    return position;
}

// NetFilter::Table

NetFilter::Table::Table(const std::string &f, const std::string &n, int tg, Validity v) : NetFilter::Item(NetFilter::Item::Type::TABLE, v)
{
    dormant = false;

    setFamily(f);
    setName(n);
    setTarget(tg);
}

NetFilter::Table::~Table()
{
}

void NetFilter::Table::setDormant(bool d)
{
    dormant = d;
}

bool NetFilter::Table::isDormant()
{
    return dormant;
}

bool operator==(const NetFilter::Table &lval, const NetFilter::Table &rval)
{
    return (lval.getFamily() == rval.getFamily() && lval.getName() == rval.getName());
}

bool operator!=(const NetFilter::Table &lval, const NetFilter::Table &rval)
{
    return !(lval == rval);
}

// NetFilter::Chain

NetFilter::Chain::Chain(const std::string &f, const std::string &t, const std::string &n, int tg, Validity v) : NetFilter::Item(NetFilter::Item::Type::CHAIN, v)
{
    setFamily(f);

    table = t;
    setName(n);
    setTarget(tg);

    init();
}

NetFilter::Chain::Chain(NetFilter::Table &t, const std::string &n, int tg, Validity v) : NetFilter::Item(NetFilter::Item::Type::CHAIN, v)
{
    setFamily(t.getFamily());

    table = t.getName();
    setName(n);
    setTarget(tg);

    init();
}

NetFilter::Chain::Chain(NetFilter::Table *t, const std::string &n, int tg, Validity v) : NetFilter::Item(NetFilter::Item::Type::CHAIN, v)
{
    setFamily(t->getFamily());

    table = t->getName();
    setName(n);
    setTarget(tg);

    init();
}

NetFilter::Chain::~Chain()
{
}

void NetFilter::Chain::init()
{
    type = TYPE_FILTER;
    hook = Hook::INPUT;
    device = "";
    priority = 0;
    policy = Policy::DROP;
}

std::string NetFilter::Chain::getTable() const
{
    return table;
}

void NetFilter::Chain::setType(const std::string &t)
{
    if(t != TYPE_FILTER && t != TYPE_NAT && t != TYPE_ROUTE)
        throw NetFilterException("Invalid chain type \"" + t + "\"");
    
    type = t;
}

std::string NetFilter::Chain::getType()
{
    return type;
}

void NetFilter::Chain::setHook(NetFilter::Hook h)
{
    hook = h;
}

NetFilter::Hook NetFilter::Chain::getHook()
{
    return hook;
}

void NetFilter::Chain::setDevice(const std::string &d)
{
    device = d;
}

std::string NetFilter::Chain::getDevice()
{
    return device;
}

void NetFilter::Chain::setPriority(int p)
{
    priority = p;
}

int NetFilter::Chain::getPriority()
{
    return priority;
}

void NetFilter::Chain::setPolicy(NetFilter::Policy p)
{
    policy = p;
}

NetFilter::Policy NetFilter::Chain::getPolicy()
{
    return policy;
}

bool operator==(const NetFilter::Chain &lval, const NetFilter::Chain &rval)
{
    return (lval.getFamily() == rval.getFamily() && lval.getTable() == rval.getTable() && lval.getName() == rval.getName());
}

bool operator!=(const NetFilter::Chain &lval, const NetFilter::Chain &rval)
{
    return !(lval == rval);
}

// NetFilter::Rule

NetFilter::Rule::Rule(const std::string &f, const std::string &t, const std::string &c, int tg, Validity v, const std::string &n) : NetFilter::Item(NetFilter::Item::Type::RULE, v)
{
    setFamily(f);

    table = t;
    chain = c;
    setTarget(tg);
    setName(n);

    init();
}

NetFilter::Rule::Rule(NetFilter::Chain &c, int tg, Validity v, const std::string &n) : NetFilter::Item(NetFilter::Item::Type::RULE, v)
{
    setFamily(c.getFamily());

    table = c.getTable();
    chain = c.getName();
    
    setTarget(tg);
    setName(n);

    init();
}

NetFilter::Rule::Rule(NetFilter::Chain *c, int tg, Validity v, const std::string &n) : NetFilter::Item(NetFilter::Item::Type::RULE, v)
{
    setFamily(c->getFamily());

    table = c->getTable();
    chain = c->getName();

    setTarget(tg);
    setName(n);

    init();
}

NetFilter::Rule::~Rule()
{
}

void NetFilter::Rule::init()
{
    protocol = Protocol::ANY;
    sourceInterface = "";
    sourcePort = 0;
    sourceAddress = {"", IPFamily::IPv4, 0};
    destinationInterface = "";
    destinationPort = 0;
    destinationAddress = {"", IPFamily::IPv4, 0};
    policy = Policy::DROP;
    statement = "";
    counter = true;
}

std::string NetFilter::Rule::getTable() const
{
    return table;
}

std::string NetFilter::Rule::getChain() const
{
    return chain;
}

void NetFilter::Rule::setProtocol(NetFilter::Protocol p)
{
    protocol = p;
}

NetFilter::Protocol NetFilter::Rule::getProtocol() const
{
    return protocol;
}

void NetFilter::Rule::setSourceInterface(const std::string &i)
{
    sourceInterface = i;
}

std::string NetFilter::Rule::getSourceInterface() const
{
    return sourceInterface;
}

void NetFilter::Rule::setSourceAddress(IPAddress ip)
{
    sourceAddress = ip;
}

IPAddress NetFilter::Rule::getSourceAddress() const
{
    return sourceAddress;
}

void NetFilter::Rule::setSourcePort(int p)
{
    sourcePort = p;
}

int NetFilter::Rule::getSourcePort() const
{
    return sourcePort;
}

void NetFilter::Rule::setDestinationInterface(const std::string &i)
{
    destinationInterface = i;
}

std::string NetFilter::Rule::getDestinationInterface() const
{
    return destinationInterface;
}

void NetFilter::Rule::setDestinationAddress(IPAddress ip)
{
    destinationAddress = ip;
}

IPAddress NetFilter::Rule::getDestinationAddress() const
{
    return destinationAddress;
}

void NetFilter::Rule::setDestinationPort(int p)
{
    destinationPort = p;
}

int NetFilter::Rule::getDestinationPort() const
{
    return destinationPort;
}

void NetFilter::Rule::setPolicy(NetFilter::Policy p)
{
    policy = p;
}

NetFilter::Policy NetFilter::Rule::getPolicy() const
{
    return policy;
}

void NetFilter::Rule::setCounter(bool c)
{
    counter = c;
}

bool NetFilter::Rule::isCounterSet() const
{
    return counter;
}

void NetFilter::Rule::setStatement(const std::string &s)
{
    statement = s;
}

std::string NetFilter::Rule::getStatement() const
{
    return statement;
}

bool operator==(const NetFilter::Rule &lval, const NetFilter::Rule &rval)
{
    return (lval.getFamily() == rval.getFamily() &&
            lval.getTable() == rval.getTable() &&
            lval.getChain() == rval.getChain() &&
            lval.getName() == rval.getName() &&
            lval.getProtocol() == rval.getProtocol() &&
            lval.getSourceInterface() == rval.getSourceInterface() &&
            lval.getSourceAddress() == rval.getSourceAddress() &&
            lval.getSourcePort() == rval.getSourcePort() &&
            lval.getDestinationInterface() == rval.getDestinationInterface() &&
            lval.getDestinationAddress() == rval.getDestinationAddress() &&
            lval.getDestinationPort() == rval.getDestinationPort() &&
            lval.getPolicy() == rval.getPolicy() &&
            lval.isCounterSet() == rval.isCounterSet() && 
            lval.getStatement() == rval.getStatement()
            );
}

bool operator!=(const NetFilter::Rule &lval, const NetFilter::Rule &rval)
{
    return !(lval == rval);
}

