/*
 * localnetwork.cpp
 *
 * This file is part of the AirVPN Suite for Linux and macOS.
 * Copyright (C) 2019-2022 AirVPN (support@airvpn.org) / https://airvpn.org
 *
 * Developed by ProMIND
 *
 * This is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with the AirVPN Suite. If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <cstring>
#include <net/if.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#if defined(__linux__) && !defined(__ANDROID__)

#include <linux/netlink.h>
#include <linux/rtnetlink.h>

#endif

#include <ifaddrs.h>
#include <unistd.h>

#include "include/localnetwork.hpp"
#include "include/airvpntools.hpp"

bool operator==(const IPAddress &lval, const IPAddress &rval)
{
    return lval.address == rval.address && lval.family == rval.family && lval.prefixLength == rval.prefixLength;
}

bool operator!=(const IPAddress &lval, const IPAddress &rval)
{
    return !(lval == rval);
}

LocalNetwork::LocalNetwork()
{
    localIPaddress.clear();
    localInterface.clear();
    loopbackInterface = "";

    scanIpAddresses();

    scanInterfaces();
}

LocalNetwork::~LocalNetwork()
{
}

void LocalNetwork::scanIpAddresses()
{
    struct ifaddrs *ifAddress = NULL;
    struct ifaddrs *ifa = NULL;
    void *address = NULL;
    char addressBuffer[INET6_ADDRSTRLEN];
    IPAddress ipEntry;

    getifaddrs(&ifAddress);

    for(ifa = ifAddress; ifa != NULL; ifa = ifa->ifa_next)
    {
        if(ifa->ifa_addr)
        {
            if(ifa->ifa_addr->sa_family == AF_INET)
            {
                address = &((struct sockaddr_in *)ifa->ifa_addr)->sin_addr;

                inet_ntop(AF_INET, address, addressBuffer, INET_ADDRSTRLEN);

                if(strcmp(addressBuffer, "127.0.0.1") != 0)
                {
                    ipEntry.address = addressBuffer;
                    ipEntry.family = IPFamily::IPv4;
                    ipEntry.prefixLength = 32;

                    localIPaddress.push_back(ipEntry);
                }
            }
            else if(ifa->ifa_addr->sa_family == AF_INET6)
            {
                address = &((struct sockaddr_in6 *)ifa->ifa_addr)->sin6_addr;

                inet_ntop(AF_INET6, address, addressBuffer, INET6_ADDRSTRLEN);

                if(strcmp(addressBuffer, "::1") != 0)
                {
                    ipEntry.address = addressBuffer;
                    ipEntry.family = IPFamily::IPv6;
                    ipEntry.prefixLength = 128;

                    localIPaddress.push_back(ipEntry);
                }
            }
        }
    }

    if(ifAddress != NULL)
        freeifaddrs(ifAddress);
}

void LocalNetwork::scanInterfaces()
{
    struct if_nameindex *ifndx, *iface;

    localInterface.clear();

    loopbackInterface = "";

    ifndx = if_nameindex();

    if(ifndx != NULL )
    {
        for(iface = ifndx; iface->if_index != 0 || iface->if_name != NULL; iface++)
        {
            if(strcmp(iface->if_name, "lo") != 0 && strcmp(iface->if_name, "lo0") != 0)
                localInterface.push_back(iface->if_name);
            else
                loopbackInterface = iface->if_name;
        }

        if_freenameindex(ifndx);
    }
}

#if defined(__linux__) && !defined(__ANDROID__)

LocalNetwork::Gateway LocalNetwork::getDefaultGatewayInterface()
{
    int route_attribute_len = 0, sock = -1, message_sequence = 1, bytes = 0, buffer_size = 0;
    char address[INET_ADDRSTRLEN], interface[IF_NAMESIZE];
    char *socket_buffer = nullptr, *message_buffer = nullptr;
    pid_t pid;
    bool done = false;
    struct nlmsghdr *nlmhptr, *netlink_message;
    struct rtmsg *route_entry;
    struct rtattr *route_attribute;
    struct timeval socket_timeout;
    Gateway gateway;

    buffer_size = 1024;

    gateway.address = "";
    gateway.interface = "";

    sock = socket(AF_NETLINK, SOCK_RAW, NETLINK_ROUTE);
    
    if(sock == -1)
        return gateway;

    pid = getpid();

    strcpy(address, "");
    strcpy(interface, "");

    message_buffer = (char *)malloc(buffer_size);
    memset(message_buffer, 0, buffer_size);

    netlink_message = (struct nlmsghdr *)message_buffer;

    netlink_message->nlmsg_len = NLMSG_LENGTH(sizeof(struct rtmsg));
    netlink_message->nlmsg_type = RTM_GETROUTE;
    netlink_message->nlmsg_flags = NLM_F_DUMP | NLM_F_REQUEST;
    netlink_message->nlmsg_seq = message_sequence;
    netlink_message->nlmsg_pid = pid;

    socket_timeout.tv_sec = 1;
    
    setsockopt(sock, SOL_SOCKET, SO_RCVTIMEO, &socket_timeout, sizeof(socket_timeout));
    
    if(send(sock, netlink_message, netlink_message->nlmsg_len, 0) == -1)
    {
        free(message_buffer);
        close(sock);

        return gateway;
    }

    socket_buffer = (char *)malloc(buffer_size);
    memset(socket_buffer, 0, buffer_size);

    done = false;

    do
    {
        bytes = recv(sock, socket_buffer, buffer_size, 0);

        if(bytes > 0)
        {
            nlmhptr = (struct nlmsghdr *)socket_buffer;

            if(NLMSG_OK(netlink_message, bytes) == 1 && netlink_message->nlmsg_type != NLMSG_ERROR)
            {
                if(netlink_message->nlmsg_type != NLMSG_NOOP)
                {
                    if(nlmhptr->nlmsg_type != NLMSG_DONE)
                    {
                        route_entry = (struct rtmsg *)NLMSG_DATA(nlmhptr);

                        if(route_entry->rtm_table == RT_TABLE_MAIN)
                        {
                            route_attribute = (struct rtattr *)RTM_RTA(route_entry);
                            route_attribute_len = RTM_PAYLOAD(nlmhptr);

                            for( ; RTA_OK(route_attribute, route_attribute_len); route_attribute = RTA_NEXT(route_attribute, route_attribute_len))
                            {
                                switch(route_attribute->rta_type)
                                {
                                    case RTA_OIF:
                                    {
                                        if_indextoname(*(int *)RTA_DATA(route_attribute), interface);
                                    }
                                    break;

                                    case RTA_GATEWAY:
                                    {
                                        inet_ntop(AF_INET, RTA_DATA(route_attribute), address, sizeof(address));
                                    }
                                    break;
                                    
                                    default: break;
                                }
                            }

                            if(strlen(address) > 0 && strlen(interface) > 0)
                            {
                                gateway.address = address;
                                gateway.interface = interface;

                                done = true;
                            }
                        }
                    }
                    else
                        done = true;

                    if((netlink_message->nlmsg_flags & NLM_F_MULTI) == 0)
                        done = true;
                }
            }
            else
                done = true;
        }
        else
            done = true;
    }
    while(((netlink_message->nlmsg_seq != message_sequence) || (netlink_message->nlmsg_pid != pid)) && done == false);

    free(message_buffer);
    free(socket_buffer);

    close(sock);

    return gateway;
}

#endif

bool LocalNetwork::isIPv6Enabled()
{
    bool enabled = false;

    for(IPAddress entry : localIPaddress)
        enabled |= (entry.family == IPFamily::IPv6);

    return enabled;
}

std::string LocalNetwork::getLoopbackInterface()
{
    return loopbackInterface;
}

std::vector<IPAddress> LocalNetwork::getLocalIPaddresses()
{
    return localIPaddress;
}

std::vector<std::string> LocalNetwork::getLocalInterfaces()
{
    return localInterface;
}

IPAddress LocalNetwork::parseIpSpecification(std::string ipSpec)
{
    std::string address = "";
    int prefixLength = 0;
    std::size_t sep = 0;
    IPAddress ipAddress;
    IPFamily ipFamily = IPFamily::IPv4;

    if(ipSpec.empty())
        return ipAddress;

    sep = ipSpec.find_last_of("/");

    if(sep == std::string::npos)
        address = ipSpec;
    else
        address = ipSpec.substr(0, sep);

    if(address.find(":") != std::string::npos)
    {
        // Cleanup IPv6 address

        address = AirVPNTools::replaceAll(address, "[", " ");
        address = AirVPNTools::replaceAll(address, "]", " ");

        ipFamily = IPFamily::IPv6;
    }
    else
        ipFamily = IPFamily::IPv4;

    if(sep != std::string::npos)
        prefixLength = std::stoi(ipSpec.substr(sep + 1));
    else
        prefixLength = 0;

    if(prefixLength == 0)
    {
        if(ipFamily == IPFamily::IPv4)
            prefixLength = 32;
        else
            prefixLength = 128;
    }

    ipAddress.address = address;
    ipAddress.family = ipFamily;
    ipAddress.prefixLength = prefixLength;

    return ipAddress;
}
