/*
 * rcparser.cpp
 *
 * This file is part of the AirVPN Suite for Linux and macOS.
 * Copyright (C) 2019-2022 AirVPN (support@airvpn.org) / https://airvpn.org
 *
 * Developed by ProMIND
 *
 * This is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with the AirVPN Suite. If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <fstream>
#include <algorithm>
#include "include/rcparser.hpp"
#include "include/airvpntools.hpp"

RCParser::RCParser(const std::string &rcfile)
{
    setup(rcfile);
}

RCParser::RCParser(const char *rcfile)
{
    setup(rcfile);
}

RCParser::~RCParser()
{
    int i;

    for(i = 0; i < rcDirective.size(); i++)
        delete rcDirective[i];

    for(i = 0; i < configDirective.size(); i++)
        delete configDirective[i];
}

void RCParser::setup(const std::string &rcfile)
{
    runControlFilePath = rcfile;

    errorDescription = "";

    configDirective.clear();
}

bool RCParser::addConfigDirective(const std::string &name, Type type)
{
    ConfigDirective *config;

    if(name.empty())
        return false;

    config = new ConfigDirective;
    
    if(config == nullptr)
        return false;

    config->name = name;
    config->type = type;

    configDirective.push_back(config);

    return true;
}

bool RCParser::isValueAllowed(const std::string &name, const std::string &value)
{
    Directive *directive;
    bool isAllowed = false;

    directive = findDirective(name);

    if(directive == nullptr)
        return false;

    if(directive->type == Type::BOOL)
    {
        if(isBoolEnabled(value) == true && isDirectiveEnabled(directive) == true)
            isAllowed = true;
        else if(isBoolEnabled(value) == false && isDirectiveEnabled(directive) == false)
            isAllowed = true;
    }
    else if(std::find(directive->value.begin(), directive->value.end(), value) != directive->value.end())
        isAllowed = true;
    else
        isAllowed = false;

    return isAllowed;
}

bool RCParser::isDirectiveEnabled(Directive *directive)
{
    bool isEnabled = false;

    if(directive == nullptr)
        return false;

    if(directive->type != Type::BOOL)
        return false;

    if(isBoolEnabled(directive->value[0]) == true)
        isEnabled = true;

    return isEnabled;
}

bool RCParser::isValueInList(Directive *directive, const std::string &value)
{
    bool found = false;

    if(directive == nullptr)
        return false;

    for(size_t i = 0; i < directive->value.size() && found == false; i++)
    {
        if(AirVPNTools::iequals(value, directive->value[i]) == true)
            found = true;
    }
    
    return found;
}

RCParser::Error RCParser::parseRCFile()
{
    std::ifstream rcfile;
    std::vector<std::string> item;
    std::locale locale;
    bool isValid = false;
    bool isNew = false;
    Directive *rcValue, *directive;
    Type directiveType;

    errorDescription = "";

    if(configDirective.empty())
    {
        errorDescription = "Configuration directives have not been set up";

        return Error::NO_DIRECTIVES_PROVIDED;
    }

    rcfile.open(runControlFilePath);

    if(rcfile.good() == false)
    {
        errorDescription = "File ";
        errorDescription += runControlFilePath;
        errorDescription += " not found";

        return Error::CANNOT_OPEN_RC_FILE;
    }

    rcDirective.clear();

    for(std::string line; getline(rcfile, line);)
    {
        line = AirVPNTools::trim(line);

        if(line.empty() == false)
        {
            item = getDirectiveAndValue(line);

            if(item[0].at(0) != '#')
            {
                if(directiveIsValid(item[0]))
                {
                    isValid = true;

                    directiveType = getConfigDirectiveType(item[0]);
                }
                else
                {
                    isValid = false;

                    directiveType = Type::UNDEFINED;
                }

                directive = findDirective(item[0]);

                if(directive != nullptr)
                {
                    rcValue = directive;

                    if(directiveType != Type::LIST)
                    {
                        rcValue = new Directive;

                        rcValue->name = item[0];
                        rcValue->isValid = false;
                        rcValue->type = directiveType;
                        rcValue->value.clear();
                        rcValue->error = "Multiple use of directive is not allowed. Ignored.";

                        isNew = true;
                        isValid = false;
                    }
                    else
                        isNew = false;
                }
                else
                {
                    rcValue = new Directive;

                    rcValue->name = item[0];
                    rcValue->isValid = isValid;
                    rcValue->type = directiveType;
                    rcValue->value.clear();

                    if(isValid == true)
                        rcValue->error = "";
                    else
                        rcValue->error = "Unknown directive.";

                    isNew = true;
                }

                if(item.size() > 1 && isValid == true)
                {
                    switch(rcValue->type)
                    {
                        case Type::UNDEFINED:
                        {
                            rcValue->isValid = false;
                            rcValue->value.push_back(item[1]);
                            rcValue->error = "Undefined type";
                        }
                        break;

                        case Type::INTEGER:
                        {
                            rcValue->value.push_back(item[1]);

                            if(item[1].find_first_not_of("+-0123456789") != std::string::npos)
                            {
                                rcValue->isValid = false;
                                rcValue->error = "Value is not an integer number";
                            }
                        }
                        break;

                        case Type::NUMBER:
                        {
                            rcValue->value.push_back(item[1]);

                            if(item[1].find_first_not_of("+-.0123456789") != std::string::npos)
                            {
                                rcValue->isValid = false;
                                rcValue->error = "Value is not a number";
                            }
                        }
                        break;

                        case Type::STRING:
                        {
                            rcValue->value.push_back(item[1]);
                        }
                        break;

                        case Type::LIST:
                        {
                            if(addListValues(rcValue->value, item[1]) != Error::OK)
                            {
                                rcfile.close();

                                return Error::PARSE_ERROR;
                            }
                        }
                        break;

                        case Type::BOOL:
                        {
                            item[1] = AirVPNTools::toLower(item[1]);

                            rcValue->value.push_back(item[1]);

                            if(isValidBool(item[1]) == false)
                            {
                                rcValue->isValid = false;

                                rcValue->error = allowedBoolValueMessage("Value");
                            }
                        }
                        break;
                    }
                }
                else
                {
                    rcfile.close();

                    if(isValid == false)
                    {
                        errorDescription = "Unknown directive ";
                        errorDescription += item[0];

                        return Error::UNKNOWN_DIRECTIVE;
                    }
                    else
                    {
                        errorDescription = "Error while parsing file ";
                        errorDescription += runControlFilePath;

                        return Error::PARSE_ERROR;
                    }
                }

                if(isNew == true)
                    rcDirective.push_back(rcValue);
            }
        }
    }

    rcfile.close();

    return Error::OK;
}

RCParser::Directives RCParser::getDirectives()
{
    return directiveVector(true);
}

RCParser::Directives RCParser::getInvalidDirectives()
{
    return directiveVector(false);
}

RCParser::Directive *RCParser::getDirective(const std::string &name)
{
    Directive *directive = nullptr;

    for(int i = 0; i < rcDirective.size() && directive == nullptr; i++)
    {
        if(rcDirective[i]->name == name && rcDirective[i]->isValid == true)
            directive = rcDirective[i];
    }

    return directive;
}

bool RCParser::directiveIsValid(const std::string &s)
{
    bool isValid = false;

    for(int i = 0; i < configDirective.size() && isValid == false; i++)
    {
        if(configDirective[i]->name == s)
            isValid = true;
    }

    return isValid;
}

RCParser::Type RCParser::getConfigDirectiveType(const std::string &s)
{
    Type type = Type::UNDEFINED;

    for(int i = 0; i < configDirective.size() && type == Type::UNDEFINED; i++)
    {
        if(configDirective[i]->name == s)
            type = configDirective[i]->type;
    }

    return type;
}

std::vector<std::string> RCParser::getDirectiveAndValue(const std::string &s)
{
    std::vector<std::string> item, result;
    std::string value;

    item = AirVPNTools::split(s, WHITE_SPACE);

    result.clear();

    if(item.size() > 0)
    {
        result.push_back(AirVPNTools::unquote(AirVPNTools::trim(item[0])));

        if(item.size() > 1)
        {
            value = "";

            for(int i = 1; i < item.size(); i++)
            {
                value += " ";

                value += item[i];
            }

            result.push_back(AirVPNTools::unquote(AirVPNTools::trim(value)));
        }
    }

    return result;
}

RCParser::Error RCParser::addListValues(std::vector<std::string> &v, std::string l, const std::string &delimiter)
{
    std::string item;
    size_t pos = 0;

    if(l.empty())
        return Error::EMPTY_LIST;

    if(l.find(delimiter) == std::string::npos)
        v.push_back(AirVPNTools::unquote(AirVPNTools::trim(l)));
    else
    {
        while((pos = l.find(delimiter)) != std::string::npos)
        {
            item = l.substr(0, pos);

            v.push_back(AirVPNTools::unquote(AirVPNTools::trim(item)));

            l.erase(0, pos + delimiter.length());
        }

        item = l.substr(0, l.length());

        v.push_back(AirVPNTools::unquote(AirVPNTools::trim(item)));
    }

    return Error::OK;
}

RCParser::Directive *RCParser::findDirective(const std::string &name)
{
    Directive *directive = nullptr;

    for(int i = 0; i < rcDirective.size() && directive == nullptr; i++)
    {
        if(rcDirective[i]->name == name)
            directive = rcDirective[i];
    }

    return directive;
}

RCParser::Directives RCParser::directiveVector(bool valid)
{
    Directives item;

    item.clear();

    for(int i = 0; i < rcDirective.size(); i++)
    {
        if(rcDirective[i]->isValid == valid)
            item.push_back(rcDirective[i]);
    }

    return item;
}

std::string RCParser::getErrorDescription()
{
    return errorDescription;
}

bool RCParser::isValidBool(const std::string &value)
{
    bool isValid = false;
    
    if(value == "yes" || value == "no" || value == "on" || value == "off" || value == "true" || value == "false" || value == "1" || value == "0")
        isValid = true;

    return isValid;
}

bool RCParser::isBoolEnabled(const std::string &value)
{
    bool isEnabled = false;

    if(value == "yes" || value == "on" || value == "true" || value == "1")
        isEnabled = true;
    
    return isEnabled;
}

std::string RCParser::allowedBoolValueMessage(std::string option)
{
    return option + " can be yes, on, true or 1 to enable it; no, off, false or 0 to disable it";
}
