/*
 * countrycontinent.cpp
 *
 * This file is part of the AirVPN Suite for Linux and macOS.
 * Copyright (C) 2019-2022 AirVPN (support@airvpn.org) / https://airvpn.org
 *
 * Developed by ProMIND
 *
 * This is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with the AirVPN Suite. If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <fstream>
#include <algorithm>
#include "include/countrycontinent.hpp"
#include "include/airvpntools.hpp"

extern void global_log(const std::string &s);

std::map<std::string, std::string> *CountryContinent::countryContinent = nullptr;
std::map<std::string, std::string> *CountryContinent::countryNames = nullptr;
std::map<std::string, std::string> *CountryContinent::continentNames = nullptr;
int CountryContinent::instanceCounter = 0;

CountryContinent::CountryContinent(const std::string &resourceDirectory)
{
    std::string line, fileName;
    std::vector<std::string> item;

    if(instanceCounter == 0)
    {
        if(countryContinent != nullptr)
            delete countryContinent;

        countryContinent = new std::map<std::string, std::string>;

        countryContinent->clear();

        fileName = resourceDirectory;
        fileName += "/";
        fileName += COUNTRY_CONTINENT_FILE_NAME;

        std::ifstream countryContinentFile(fileName);

        if(countryContinentFile.is_open())
        {
            while(std::getline(countryContinentFile, line))
            {
                item = AirVPNTools::split(line, SPLIT_DELIMITER);

                if(item.size() == 2)
                    countryContinent->insert(std::make_pair(AirVPNTools::toUpper(item[0]), AirVPNTools::toUpper(item[1])));
            }

            countryContinentFile.close();
        }
        else
            global_log("CountryContinent::CountryContinent() - Cannot open " + fileName);

        if(countryNames != nullptr)
            delete countryNames;

        countryNames = new std::map<std::string, std::string>;

        countryNames->clear();

        fileName = resourceDirectory;
        fileName += "/";
        fileName += COUNTRY_NAMES_FILE_NAME;

        std::ifstream countryNamesFile(fileName);

        if(countryNamesFile.is_open())
        {
            while(std::getline(countryNamesFile, line))
            {
                item = AirVPNTools::split(line, SPLIT_DELIMITER);

                if(item.size() == 2)
                    countryNames->insert(std::make_pair(AirVPNTools::toUpper(item[0]), item[1]));
            }

            countryNamesFile.close();
        }
        else
            global_log("CountryContinent::CountryContinent() - Cannot open " + fileName);

        if(continentNames != nullptr)
            delete continentNames;

        continentNames = new std::map<std::string, std::string>;

        continentNames->clear();

        fileName = resourceDirectory;
        fileName += "/";
        fileName += CONTINENT_NAMES_FILE_NAME;

        std::ifstream continentNamesFile(fileName);

        if(continentNamesFile.is_open())
        {
            while(std::getline(continentNamesFile, line))
            {
                item = AirVPNTools::split(line, SPLIT_DELIMITER);

                if(item.size() == 2)
                    continentNames->insert(std::make_pair(AirVPNTools::toUpper(item[0]), item[1]));
            }

            continentNamesFile.close();
        }
        else
            global_log("CountryContinent::CountryContinent() - Cannot open " + fileName);
    }

    instanceCounter++;
}

CountryContinent::~CountryContinent()
{
    instanceCounter--;

    if(instanceCounter == 0)
    {
        if(countryContinent != nullptr)
        {
            delete countryContinent;

            countryContinent = nullptr;
        }

        if(countryNames != nullptr)
        {
            delete countryNames;

            countryNames = nullptr;
        }
    }
}

std::string CountryContinent::getCountryContinent(std::string countryCode)
{
    if(countryContinent->empty())
        return "";

    countryCode = AirVPNTools::toUpper(countryCode);

    std::string continent = "";
    std::map<std::string, std::string>::iterator it = countryContinent->find(countryCode);

    if(it != countryContinent->end())
        continent = it->second;

    return continent;
}

std::string CountryContinent::getCountryName(std::string countryCode)
{
    if(countryNames->empty())
        return "";

    std::string name = "";

    countryCode = AirVPNTools::toUpper(countryCode);

    std::map<std::string, std::string>::iterator it = countryNames->find(countryCode);

    if(it != countryNames->end())
        name = it->second;

    return name;
}

std::string CountryContinent::getCountryCode(std::string countryName)
{
    std::string code = "";
    bool found = false;

    if(countryNames->empty())
        return "";

    countryName = AirVPNTools::toUpper(countryName);

    for(std::map<std::string, std::string>::iterator it = countryNames->begin(); it != countryNames->end() && found == false; it++)
    {
        if(AirVPNTools::toUpper(it->second) == countryName)
        {
            code = AirVPNTools::toUpper(it->first);

            found = true;
        }
    }

    return code;
}

std::vector<std::string> CountryContinent::searchCountry(std::string pattern)
{
    std::vector<std::string> result;

    if(countryNames->empty())
        return result;

    pattern = AirVPNTools::toUpper(pattern);

    for(std::map<std::string, std::string>::iterator it = countryNames->begin(); it != countryNames->end(); it++)
    {
        if(AirVPNTools::toUpper(it->first).find(pattern) != std::string::npos || AirVPNTools::toUpper(it->second).find(pattern) != std::string::npos || pattern == "ALL")
            result.push_back(it->first);
    }

    return result;
}

int CountryContinent::getCountries()
{
    return countryNames->size();
}

std::string CountryContinent::getContinentName(std::string continentCode)
{
    if(continentNames->empty())
        return "";

    std::string name = "";

    continentCode = AirVPNTools::toUpper(continentCode);

    std::map<std::string, std::string>::iterator it = continentNames->find(continentCode);

    if(it != continentNames->end())
        name = it->second;

    return name;
}

std::string CountryContinent::getContinentCode(std::string continentName)
{
    std::string code = "";
    bool found = false;

    if(continentNames->empty())
        return "";

    continentName = AirVPNTools::toUpper(continentName);

    for(std::map<std::string, std::string>::iterator it = continentNames->begin(); it != continentNames->end() && found == false; it++)
    {
        if(AirVPNTools::toUpper(it->second) == continentName)
        {
            code = AirVPNTools::toUpper(it->first);

            found = true;
        }
    }

    return code;
}

int CountryContinent::getContinents()
{
    return countryContinent->size();
}

bool CountryContinent::isContinent(std::string continent)
{
    bool name = false, code = false;
    
    name = (AirVPNTools::iequals(continent, "earth") ||
            AirVPNTools::iequals(continent, "europe") ||
            AirVPNTools::iequals(continent, "africa") ||
            AirVPNTools::iequals(continent, "asia") ||
            AirVPNTools::iequals(continent, "america") ||
            AirVPNTools::iequals(continent, "south america") ||
            AirVPNTools::iequals(continent, "north america") ||
            AirVPNTools::iequals(continent, "oceania"));

    code = (AirVPNTools::iequals(continent, "eur") ||
            AirVPNTools::iequals(continent, "afr") ||
            AirVPNTools::iequals(continent, "asi") ||
            AirVPNTools::iequals(continent, "ame") ||
            AirVPNTools::iequals(continent, "sam") ||
            AirVPNTools::iequals(continent, "nam") ||
            AirVPNTools::iequals(continent, "oce"));
    
    return (name | code);
}

