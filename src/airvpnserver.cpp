/*
 * airvpnserver.cpp
 *
 * This file is part of the AirVPN Suite for Linux and macOS.
 * Copyright (C) 2019-2022 AirVPN (support@airvpn.org) / https://airvpn.org
 *
 * Developed by ProMIND
 *
 * This is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with the AirVPN Suite. If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <algorithm>
#include "include/airvpnserver.hpp"
#include "include/airvpnmanifest.hpp"
#include "include/airvpntools.hpp"
#include "include/cipherdatabase.hpp"

AirVPNServer::AirVPNServer(const std::string &resourceDirectory)
{
    name = "";
    continent = "";
    countryCode = "";
    region = "";
    location = "";
    bandWidth = 0;
    maxBandWidth = 0;
    users = 0;
    IPv4Available = false;
    IPv6Available = false;
    entryIPv4.clear();
    entryIPv6.clear();
    exitIPv4 = "";
    exitIPv6 = "";
    warningOpen = "";
    warningClosed = "";
    scoreBase = 0;
    score = WORST_SCORE;
    supportCheck = false;
    openVPNAvailable = false;
    wireGuardAvailable = false;
    perfectForwardSecrecyAvailable = false;
    group = -1;
    openVPNTlsCiphers.clear();
    openVPNTlsSuiteCiphers.clear();
    openVPNDataCiphers.clear();

    countryContinent = new CountryContinent(resourceDirectory);
}

AirVPNServer::~AirVPNServer()
{
}

std::string AirVPNServer::getName()
{
    return name;
}

void AirVPNServer::setName(const std::string &n)
{
    name = n;
}

std::string AirVPNServer::getContinent()
{
    return continent;
}

void AirVPNServer::setContinent(const std::string &c)
{
    continent = c;
}

std::string AirVPNServer::getCountryCode()
{
    return countryCode;
}

std::string AirVPNServer::getCountryName()
{
    return countryContinent->getCountryName(countryCode);
}

void AirVPNServer::setCountryCode(const std::string &c)
{
    countryCode = AirVPNTools::toUpper(c);

    continent = countryContinent->getCountryContinent(c);
}

std::string AirVPNServer::getRegion()
{
    return region;
}

void AirVPNServer::setRegion(const std::string &r)
{
    region = r;
}

std::string AirVPNServer::getLocation()
{
    return location;
}

void AirVPNServer::setLocation(const std::string &l)
{
    location = l;
}

long long AirVPNServer::getBandWidth()
{
    return bandWidth;
}

void AirVPNServer::setBandWidth(long long b)
{
    bandWidth = b;
}

long long AirVPNServer::getMaxBandWidth()
{
    return maxBandWidth;
}

void AirVPNServer::setMaxBandWidth(long long b)
{
    maxBandWidth = b;
}

long long AirVPNServer::getEffectiveBandWidth()
{
    return (2 * (bandWidth * 8));
}

int AirVPNServer::getUsers()
{
    return users;
}

void AirVPNServer::setUsers(int u)
{
    users = u;
}

int AirVPNServer::getMaxUsers()
{
    return maxUsers;
}

void AirVPNServer::setMaxUsers(int u)
{
    maxUsers = u;
}

double AirVPNServer::getUserLoad()
{
    return ((double)users * 100.0) / (double)maxUsers;
}

bool AirVPNServer::isIPv4Available()
{
    return IPv4Available;
}

void AirVPNServer::setIPv4Available(bool s)
{
    IPv4Available = s;
}

bool AirVPNServer::isIPv6Available()
{
    return IPv6Available;
}

void AirVPNServer::setIPv6Available(bool s)
{
    IPv6Available = s;
}

std::map<int, std::string> AirVPNServer::getEntryIPv4()
{
    return entryIPv4;
}

void AirVPNServer::setEntryIPv4(const std::map<int, std::string> &l)
{
    entryIPv4 = l;
}

void AirVPNServer::setEntryIPv4(int entry, const std::string &IPv4)
{
    entryIPv4.insert(std::make_pair(entry, AirVPNTools::trim(IPv4)));
}

std::map<int, std::string> AirVPNServer::getEntryIPv6()
{
    return entryIPv6;
}

void AirVPNServer::setEntryIPv6(const std::map<int, std::string> &l)
{
    entryIPv6 = l;
}

void AirVPNServer::setEntryIPv6(int entry, const std::string &IPv6)
{
    entryIPv6.insert(std::make_pair(entry, AirVPNTools::trim(IPv6)));
}

std::string AirVPNServer::getExitIPv4()
{
    return exitIPv4;
}

void AirVPNServer::setExitIPv4(const std::string &e)
{
    exitIPv4 = AirVPNTools::trim(e);
}

std::string AirVPNServer::getExitIPv6()
{
    return exitIPv6;
}

void AirVPNServer::setExitIPv6(const std::string &e)
{
    exitIPv6 = AirVPNTools::trim(e);
}

std::string AirVPNServer::getWarningOpen()
{
    return warningOpen;
}

void AirVPNServer::setWarningOpen(const std::string &w)
{
    warningOpen = w;
}

std::string AirVPNServer::getWarningClosed()
{
    return warningClosed;
}

void AirVPNServer::setWarningClosed(const std::string &w)
{
    warningClosed = w;
}

int AirVPNServer::getScoreBase()
{
    return scoreBase;
}

void AirVPNServer::setScoreBase(int s)
{
    scoreBase = s;
}

bool AirVPNServer::getSupportCheck()
{
    return supportCheck;
}

void AirVPNServer::setSupportCheck(bool s)
{
    supportCheck = s;
}

int AirVPNServer::getLoad()
{
    return AirVPNTools::getLoad(bandWidth, maxBandWidth);
}

bool AirVPNServer::isOpenVPNAvailable()
{
    return openVPNAvailable;
}

void AirVPNServer::setOpenVPNAvailable(bool b)
{
    openVPNAvailable = b;
}

std::vector<int> AirVPNServer::getOpenVPNTlsCiphers()
{
    return openVPNTlsCiphers;
}

std::string AirVPNServer::getOpenVPNTlsCipherNames()
{
    return CipherDatabase::getCipherNameList(CipherDatabase::CipherType::TLS, openVPNTlsCiphers);
}

void AirVPNServer::setOpenVPNTlsCiphers(const std::vector<int> &list)
{
    openVPNTlsCiphers = list;
}

bool AirVPNServer::hasOpenVPNTlsCipher(int c)
{
    if(openVPNTlsCiphers.empty())
        return false;

    return (std::find(openVPNTlsCiphers.begin(), openVPNTlsCiphers.end(), c) != openVPNTlsCiphers.end());
}

bool AirVPNServer::hasOpenVPNTlsCipher(const std::vector<int> &cipherList)
{
    bool serverHasCipher = false;

    for(int c : cipherList)
    {
        if(hasOpenVPNTlsCipher(c))
            serverHasCipher = true;
    }

    return serverHasCipher;
}

std::vector<int> AirVPNServer::getOpenVPNTlsSuiteCiphers()
{
    return openVPNTlsSuiteCiphers;
}

std::string AirVPNServer::getOpenVPNTlsSuiteCipherNames()
{
    return CipherDatabase::getCipherNameList(CipherDatabase::CipherType::TLS_SUITE, openVPNTlsSuiteCiphers);
}

void AirVPNServer::setOpenVPNTlsSuiteCiphers(const std::vector<int> &list)
{
    openVPNTlsSuiteCiphers = list;
}

bool AirVPNServer::hasOpenVPNTlsSuiteCipher(int c)
{
    if(openVPNTlsSuiteCiphers.empty())
        return false;

    return (std::find(openVPNTlsSuiteCiphers.begin(), openVPNTlsSuiteCiphers.end(), c) != openVPNTlsSuiteCiphers.end());
}

bool AirVPNServer::hasOpenVPNTlsSuiteCipher(const std::vector<int> &cipherList)
{
    bool serverHasCipher = false;

    for(int c : cipherList)
    {
        if(hasOpenVPNTlsSuiteCipher(c))
            serverHasCipher = true;
    }

    return serverHasCipher;
}

std::vector<int> AirVPNServer::getOpenVPNDataCiphers()
{
    return openVPNDataCiphers;
}

std::string AirVPNServer::getOpenVPNDataCipherNames()
{
    return CipherDatabase::getCipherNameList(CipherDatabase::CipherType::DATA, openVPNDataCiphers);
}

void AirVPNServer::setOpenVPNDataCiphers(std::vector<int> list)
{
    openVPNDataCiphers = list;
}

bool AirVPNServer::hasOpenVPNDataCipher(int c)
{
    if(openVPNDataCiphers.empty())
        return false;

    return (std::find(openVPNDataCiphers.begin(), openVPNDataCiphers.end(), c) != openVPNDataCiphers.end());
}

bool AirVPNServer::hasOpenVPNDataCipher(const std::vector<int> &cipherList)
{
    if(cipherList.empty())
        return true;

    bool serverHasCipher = false;

    for(int c : cipherList)
    {
        if(hasOpenVPNDataCipher(c))
            serverHasCipher = true;
    }

    return serverHasCipher;
}

bool AirVPNServer::isWireGuardAvailable()
{
    return wireGuardAvailable;
}

void AirVPNServer::setWireGuardAvailable(bool b)
{
    wireGuardAvailable = b;
}

std::string AirVPNServer::getWireGuardCipherNames()
{
    return "ChaCha20-Poly1305, Post-quantum resistance ready";
}

void AirVPNServer::computeServerScore()
{
    double s = 0;

    if(maxBandWidth == 0 || !warningOpen.empty() || !warningClosed.empty())
    {
        score = WORST_SCORE;

        return;
    }

    s = (double)scoreBase / AirVPNManifest::getSpeedFactor();
    s += (double)getLoad() * AirVPNManifest::getLoadFactor();
    s += (double)users * getUserLoad();

    score = (int)s;
}

int AirVPNServer::getScore()
{
    return score;
}

void AirVPNServer::setScore(int s)
{
    score = s;
}

std::string AirVPNServer::getOpenVpnDirectives()
{
    return openVpnDirectives;
}

void AirVPNServer::setOpenVpnDirectives(const std::string &o)
{
    openVpnDirectives = o;
}

bool AirVPNServer::isAvailable()
{
    return warningClosed.empty();
}

bool AirVPNServer::isAvailable(const std::vector<int> &cipherList)
{
    return warningClosed.empty() & hasOpenVPNDataCipher(cipherList);
}

bool AirVPNServer::isPerfectForwardSecrecyAvailable()
{
    return perfectForwardSecrecyAvailable;
}

void AirVPNServer::setPerfectForwardSecrecyAvailable(bool b)
{
    perfectForwardSecrecyAvailable = b;
}
