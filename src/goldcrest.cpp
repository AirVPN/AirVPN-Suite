/*
 * goldcrest.cpp
 *
 * This file is part of the AirVPN Suite for Linux and macOS.
 * Copyright (C) 2019-2022 AirVPN (support@airvpn.org) / https://airvpn.org
 *
 * Developed by ProMIND
 *
 * This is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with the AirVPN Suite. If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <string>
#include <vector>
#include <iostream>
#include <fstream>
#include <sstream>
#include <thread>
#include <future>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <stdbool.h>
#include <ctype.h>
#include <signal.h>
#include <time.h>
#include <pwd.h>
#include <termios.h>
#include <sys/stat.h>
#include "include/btcommon.h"
#include "include/goldcrest.hpp"
#include "include/dbusconnector.hpp"
#include "include/airvpnserver.hpp"
#include "include/airvpntools.hpp"

DBusConnection *dbusConnection = nullptr;
DBusConnector *dbusConnector = nullptr;

std::string airVpnUsername, airVpnPassword;
std::ostringstream airVpnKey;
std::vector<std::string> extraOptions;
std::vector<DNSEntry> pushedDns;

std::thread *connectionStatsThread = nullptr;

std::promise<void> connectionStatsSignalExit;
std::future<void> connectionStatsFutureObject;

bool airvpn_mode = false;
bool airvpn_connect = false;
bool airvpn_key_load = false;
int conn_stat_interval = DEFAULT_CONN_STAT_SECONDS;

void signal_handler(int sig)
{
    std::ostringstream msg;
    std::string signalDescription;
    DBusResponse *dbusResponse = nullptr;
    DBusResponse::Item item;
    DBusResponse::ItemIterator it;
    static bool termination_in_progress = false;

    msg.str("");

    switch(sig)
    {
        case SIGTERM:
        case SIGINT:
        case SIGPIPE:
        case SIGHUP:
        {
            if(sig == SIGTERM)
                signalDescription = "SIGTERM";
            else if(sig == SIGINT)
                signalDescription = "SIGINT";
            else if(sig == SIGPIPE)
                signalDescription = "SIGPIPE";
            else
                signalDescription = "SIGHUP";

            msg << "Caught " << signalDescription << " signal. Terminating.";
            
            if(termination_in_progress == false)
                termination_in_progress = true;
            else
                return;

            log(msg.str().c_str());

            if(get_server_status() == BLUETIT_STATUS_CONNECTED)
            {
                msg.str("");

                msg << "Requesting VPN connection termination to " << BLUETIT_SHORT_NAME;

                log(msg.str().c_str());

                dbusResponse = send_method_to_server(BT_METHOD_STOP_CONNECTION);

                dbusConnector->unreferenceResponse(dbusResponse);

                dbus_message_loop_until(BT_EVENT_DISCONNECTED);
            }

            cleanup_and_exit(NULL, EXIT_SUCCESS);
        }
        break;

        case SIGUSR1:
        {
            int status;

            msg << "Caught SIGUSR1 signal. ";

            status = get_server_status();

            switch(status)
            {
                case BLUETIT_STATUS_CONNECTED:
                {
                    msg << "Requesting " << BLUETIT_SHORT_NAME << " to pause VPN connection.";

                    dbusResponse = send_method_to_server(BT_METHOD_PAUSE_CONNECTION);

                    dbusConnector->unreferenceResponse(dbusResponse);
                }
                break;

                case BLUETIT_STATUS_PAUSED:
                {
                    msg << "Requesting " << BLUETIT_SHORT_NAME << " to resume VPN connection.";

                    dbusResponse = send_method_to_server(BT_METHOD_RESUME_CONNECTION);

                    dbusConnector->unreferenceResponse(dbusResponse);
                }
                break;

                default:
                {
                    msg << BLUETIT_SHORT_NAME << " is not connected or paused. Signal ignored.";

                    log(msg.str().c_str());
                }
                break;
            }

            log(msg.str().c_str());
        }
        break;

        case SIGUSR2:
        {
            msg << "Caught SIGUSR2 signal. Reconnecting VPN.";

            dbusResponse = send_method_to_server(BT_METHOD_RECONNECT_CONNECTION);

            dbusConnector->unreferenceResponse(dbusResponse);

            log(msg.str().c_str());
        }
        break;

        default:
        {
            msg << "Caught unhandled signal " << sig << "  Continuing service.";

            log(msg.str().c_str());
        }
        break;
    }
}

void cleanup_and_exit(const char *error_msg, int exit_value)
{
    if(connectionStatsThread != nullptr)
    {
        if(connectionStatsThread->joinable())
        {
            try
            {
                connectionStatsSignalExit.set_value();
            }
            catch(std::future_error& e)
            {
                std::ostringstream osstring;

                osstring.str("");
                
                osstring << "ERROR: Connection Stats Thread (" << e.code() << "): " << e.what()
;
                log(osstring.str().c_str());
            }

            if(connectionStatsThread->joinable())
                connectionStatsThread->join();

            delete connectionStatsThread;

            connectionStatsThread = nullptr;
        }
    }

    if(error_msg != NULL && strlen(error_msg) > 0)
        log(error_msg);

    if(dbusConnector)
        delete dbusConnector;

    exit(exit_value);
}

int get_server_status()
{
    std::string msg;
    DBusMessage *dbusReply = nullptr;
    std::vector<std::string> dbusItems;
    int server_status = BLUETIT_STATUS_UNKNOWN;

    dbusItems.clear();

    try
    {
        dbusReply = dbusConnector->callMethodWithReply(BT_SERVER_BUS_NAME, BT_SERVER_OBJECT_PATH_NAME, BT_METHOD_BLUETIT_STATUS, dbusItems);
    }
    catch(DBusConnectorException &e)
    {
        return server_status;
    }

    if(dbusReply == nullptr)
    {
        msg = "ERROR: get_server_status() - no reply from ";
        msg += BLUETIT_SHORT_NAME;

        cleanup_and_exit(msg.c_str(), EXIT_FAILURE);
    }

    server_status = dbusConnector->getInt(dbusReply);

    switch(server_status)
    {
        case BLUETIT_STATUS_READY:
        {
            msg = BLUETIT_SHORT_NAME;
            msg += " is ready";
        }
        break;

        case BLUETIT_STATUS_CONNECTED:
        {
            msg = BLUETIT_SHORT_NAME;
            msg += " is connected to VPN";
        }
        break;

        case BLUETIT_STATUS_PAUSED:
        {
            msg = BLUETIT_SHORT_NAME;
            msg += ": VPN connection is paused.";
        }
        break;

        case BLUETIT_STATUS_DIRTY_EXIT:
        {
            msg = "It seems ";
            msg += BLUETIT_SHORT_NAME;
            msg += " did not exit gracefully or has been killed.\n";
            msg += "Your system may not be working properly and your network connection may not work\n";
            msg += "as expected. To recover your network settings, run this program again and use\n";
            msg += "the \"--recover-network\" option.";
        }
        break;

        case BLUETIT_STATUS_RESOURCE_DIRECTORY_ERROR:
        case BLUETIT_STATUS_INIT_ERROR:
        case BLUETIT_STATUS_LOCK_ERROR:
        {
            msg = BLUETIT_SHORT_NAME;
            msg += " error. Please see system log for details.";
        }
        break;

        case BLUETIT_STATUS_UNKNOWN:
        {
            msg = "Unknown ";
            msg += BLUETIT_SHORT_NAME;
            msg += " status. Please see system log for details.";
        }
        break;

        default:
        {
            msg = "ERROR: D-Bus service ";
            msg += BT_SERVER_BUS_NAME;
            msg += " is not available";

            dbusConnector->unreferenceMessage(dbusReply);

            cleanup_and_exit(msg.c_str(), EXIT_FAILURE);
        }
        break;
    }

    log(msg.c_str());

    dbusConnector->unreferenceMessage(dbusReply);

    return server_status;
}

void log(const char *msg)
{
    time_t timer;
    char buffer[26];
    struct tm* tm_info;

    timer = time(NULL);
    tm_info = localtime(&timer);

    strftime(buffer, 26, "%Y-%m-%d %H:%M:%S", tm_info);

    printf("%s %s", buffer, msg);

    if(msg[strlen(msg)-1] != '\n')
        putchar('\n');
}

void usage(const char *exec_name)
{
    std::cout << "usage: " << exec_name << " <options> [config-file]" << std::endl << std::endl;
    std::cout << formatOption(BT_CLIENT_OPTION_HELP, BT_CLIENT_OPTION_HELP_SHORT, BT_CLIENT_OPTION_HELP_DESC) << std::endl;
    std::cout << formatOption(BT_CLIENT_OPTION_VERSION, BT_CLIENT_OPTION_VERSION_SHORT, BT_CLIENT_OPTION_VERSION_DESC) << std::endl;
    std::cout << formatOption(BT_CLIENT_OPTION_BLUETIT_STATUS, BT_CLIENT_OPTION_BLUETIT_STATUS_SHORT, BT_CLIENT_OPTION_BLUETIT_STATUS_DESC) << std::endl;
    std::cout << formatOption(BT_CLIENT_OPTION_BLUETIT_STATS, BT_CLIENT_OPTION_BLUETIT_STATS_SHORT, BT_CLIENT_OPTION_BLUETIT_STATS_DESC) << std::endl;
    std::cout << formatOption(BT_CLIENT_OPTION_AIR_CONNECT, BT_CLIENT_OPTION_AIR_CONNECT_SHORT, BT_CLIENT_OPTION_AIR_CONNECT_DESC) << std::endl;
    std::cout << formatOption(BT_CLIENT_OPTION_AIR_SERVER, BT_CLIENT_OPTION_AIR_SERVER_SHORT, BT_CLIENT_OPTION_AIR_SERVER_DESC) << std::endl;
    std::cout << formatOption(BT_CLIENT_OPTION_AIR_COUNTRY, BT_CLIENT_OPTION_AIR_COUNTRY_SHORT, BT_CLIENT_OPTION_AIR_COUNTRY_DESC) << std::endl;
    std::cout << formatOption(BT_CLIENT_OPTION_AIR_VPN_TYPE, BT_CLIENT_OPTION_AIR_VPN_TYPE_SHORT, BT_CLIENT_OPTION_AIR_VPN_TYPE_DESC) << std::endl;
    std::cout << formatOption(BT_CLIENT_OPTION_AIR_TLS_MODE, BT_CLIENT_OPTION_AIR_TLS_MODE_SHORT, BT_CLIENT_OPTION_AIR_TLS_MODE_DESC) << std::endl;
    std::cout << formatOption(BT_CLIENT_OPTION_AIR_IPV6, BT_CLIENT_OPTION_AIR_IPV6_SHORT, BT_CLIENT_OPTION_AIR_IPV6_DESC) << std::endl;
    std::cout << formatOption(BT_CLIENT_OPTION_AIR_6TO4, BT_CLIENT_OPTION_AIR_6TO4_SHORT, BT_CLIENT_OPTION_AIR_6TO4_DESC) << std::endl;
    std::cout << formatOption(BT_CLIENT_OPTION_AIR_USER, BT_CLIENT_OPTION_AIR_USER_SHORT, BT_CLIENT_OPTION_AIR_USER_DESC) << std::endl;
    std::cout << formatOption(BT_CLIENT_OPTION_AIR_PASSWORD, BT_CLIENT_OPTION_AIR_PASSWORD_SHORT, BT_CLIENT_OPTION_AIR_PASSWORD_DESC) << std::endl;
    std::cout << formatOption(BT_CLIENT_OPTION_AIR_KEY, BT_CLIENT_OPTION_AIR_KEY_SHORT, BT_CLIENT_OPTION_AIR_KEY_DESC) << std::endl;
    std::cout << formatOption(BT_CLIENT_OPTION_AIR_KEY_LIST, BT_CLIENT_OPTION_AIR_KEY_LIST_SHORT, BT_CLIENT_OPTION_AIR_KEY_LIST_DESC) << std::endl;
    std::cout << formatOption(BT_CLIENT_OPTION_AIR_KEY_LOAD, BT_CLIENT_OPTION_AIR_KEY_LOAD_SHORT, BT_CLIENT_OPTION_AIR_KEY_LOAD_DESC) << std::endl;
    std::cout << formatOption(BT_CLIENT_OPTION_AIR_SAVE, BT_CLIENT_OPTION_AIR_SAVE_SHORT, BT_CLIENT_OPTION_AIR_SAVE_DESC) << std::endl;
    std::cout << formatOption(BT_CLIENT_OPTION_AIR_INFO, BT_CLIENT_OPTION_AIR_INFO_SHORT, BT_CLIENT_OPTION_AIR_INFO_DESC) << std::endl;
    std::cout << formatOption(BT_CLIENT_OPTION_AIR_LIST, BT_CLIENT_OPTION_AIR_LIST_SHORT, BT_CLIENT_OPTION_AIR_LIST_DESC) << std::endl;
    std::cout << formatOption(BT_CLIENT_OPTION_AIR_WHITE_SERVER_LIST, BT_CLIENT_OPTION_AIR_WHITE_SERVER_LIST_SHORT, BT_CLIENT_OPTION_AIR_WHITE_SERVER_LIST_DESC) << std::endl;
    std::cout << formatOption(BT_CLIENT_OPTION_AIR_BLACK_SERVER_LIST, BT_CLIENT_OPTION_AIR_BLACK_SERVER_LIST_SHORT, BT_CLIENT_OPTION_AIR_BLACK_SERVER_LIST_DESC) << std::endl;
    std::cout << formatOption(BT_CLIENT_OPTION_AIR_WHITE_COUNTRY_LIST, BT_CLIENT_OPTION_AIR_WHITE_COUNTRY_LIST_SHORT, BT_CLIENT_OPTION_AIR_WHITE_COUNTRY_LIST_DESC) << std::endl;
    std::cout << formatOption(BT_CLIENT_OPTION_AIR_BLACK_COUNTRY_LIST, BT_CLIENT_OPTION_AIR_BLACK_COUNTRY_LIST_SHORT, BT_CLIENT_OPTION_AIR_BLACK_COUNTRY_LIST_DESC) << std::endl;
    std::cout << formatOption(BT_CLIENT_OPTION_RESPONSE, BT_CLIENT_OPTION_RESPONSE_SHORT, BT_CLIENT_OPTION_RESPONSE_DESC) << std::endl;
    std::cout << formatOption(BT_CLIENT_OPTION_DC, BT_CLIENT_OPTION_DC_SHORT, BT_CLIENT_OPTION_DC_DESC) << std::endl;
    std::cout << formatOption(BT_CLIENT_OPTION_CIPHER, BT_CLIENT_OPTION_CIPHER_SHORT, BT_CLIENT_OPTION_CIPHER_DESC) << std::endl;
    std::cout << formatOption(BT_CLIENT_OPTION_LIST_DATA_CIPHERS, BT_CLIENT_OPTION_LIST_DATA_CIPHERS_SHORT, BT_CLIENT_OPTION_LIST_DATA_CIPHERS_DESC) << std::endl;
    std::cout << formatOption(BT_CLIENT_OPTION_SERVER, BT_CLIENT_OPTION_SERVER_SHORT, BT_CLIENT_OPTION_SERVER_DESC) << std::endl;
    std::cout << formatOption(BT_CLIENT_OPTION_PROTO, BT_CLIENT_OPTION_PROTO_SHORT, BT_CLIENT_OPTION_PROTO_DESC) << std::endl;
    std::cout << formatOption(BT_CLIENT_OPTION_PORT, BT_CLIENT_OPTION_PORT_SHORT, BT_CLIENT_OPTION_PORT_DESC) << std::endl;
    std::cout << formatOption(BT_CLIENT_OPTION_TCP_QUEUE_LIMIT, BT_CLIENT_OPTION_TCP_QUEUE_LIMIT_SHORT, BT_CLIENT_OPTION_TCP_QUEUE_LIMIT_DESC) << std::endl;
    std::cout << formatOption(BT_CLIENT_OPTION_ALLOWUAF, BT_CLIENT_OPTION_ALLOWUAF_SHORT, BT_CLIENT_OPTION_ALLOWUAF_DESC) << std::endl;
    std::cout << formatOption(BT_CLIENT_OPTION_NCP_DISABLE, BT_CLIENT_OPTION_NCP_DISABLE_SHORT, BT_CLIENT_OPTION_NCP_DISABLE_DESC) << std::endl;
    std::cout << formatOption(BT_CLIENT_OPTION_NETWORK_LOCK, BT_CLIENT_OPTION_NETWORK_LOCK_SHORT, BT_CLIENT_OPTION_NETWORK_LOCK_DESC) << std::endl;
    std::cout << formatOption(BT_CLIENT_OPTION_IGNORE_DNS_PUSH, BT_CLIENT_OPTION_IGNORE_DNS_PUSH_SHORT, BT_CLIENT_OPTION_IGNORE_DNS_PUSH_DESC) << std::endl;
    std::cout << formatOption(BT_CLIENT_OPTION_TIMEOUT, BT_CLIENT_OPTION_TIMEOUT_SHORT, BT_CLIENT_OPTION_TIMEOUT_DESC) << std::endl;
    std::cout << formatOption(BT_CLIENT_OPTION_COMPRESS, BT_CLIENT_OPTION_COMPRESS_SHORT, BT_CLIENT_OPTION_COMPRESS_DESC) << std::endl;
    std::cout << formatOption(BT_CLIENT_OPTION_PK_PASSWORD, BT_CLIENT_OPTION_PK_PASSWORD_SHORT, BT_CLIENT_OPTION_PK_PASSWORD_DESC) << std::endl;
    std::cout << formatOption(BT_CLIENT_OPTION_TVM_OVERRIDE, BT_CLIENT_OPTION_TVM_OVERRIDE_SHORT, BT_CLIENT_OPTION_TVM_OVERRIDE_DESC) << std::endl;
    std::cout << formatOption(BT_CLIENT_OPTION_TCPROF_OVERRIDE, BT_CLIENT_OPTION_TCPROF_OVERRIDE_SHORT, BT_CLIENT_OPTION_TCPROF_OVERRIDE_DESC) << std::endl;
    std::cout << formatOption(BT_CLIENT_OPTION_PROXY_HOST, BT_CLIENT_OPTION_PROXY_HOST_SHORT, BT_CLIENT_OPTION_PROXY_HOST_DESC) << std::endl;
    std::cout << formatOption(BT_CLIENT_OPTION_PROXY_PORT, BT_CLIENT_OPTION_PROXY_PORT_SHORT, BT_CLIENT_OPTION_PROXY_PORT_DESC) << std::endl;
    std::cout << formatOption(BT_CLIENT_OPTION_PROXY_USERNAME, BT_CLIENT_OPTION_PROXY_USERNAME_SHORT, BT_CLIENT_OPTION_PROXY_USERNAME_DESC) << std::endl;
    std::cout << formatOption(BT_CLIENT_OPTION_PROXY_PASSWORD, BT_CLIENT_OPTION_PROXY_PASSWORD_SHORT, BT_CLIENT_OPTION_PROXY_PASSWORD_DESC) << std::endl;
    std::cout << formatOption(BT_CLIENT_OPTION_PROXY_BASIC, BT_CLIENT_OPTION_PROXY_BASIC_SHORT, BT_CLIENT_OPTION_PROXY_BASIC_DESC) << std::endl;
    std::cout << formatOption(BT_CLIENT_OPTION_ALT_PROXY, BT_CLIENT_OPTION_ALT_PROXY_SHORT, BT_CLIENT_OPTION_ALT_PROXY_DESC) << std::endl;

#ifdef ENABLE_OVPNDCO

    std::cout << formatOption(BT_CLIENT_OPTION_DCO_SHORT, BT_CLIENT_OPTION_DCO, BT_CLIENT_OPTION_DCO_DESC) << std::endl;

#endif

    std::cout << formatOption(BT_CLIENT_OPTION_CACHE_PASSWORD, BT_CLIENT_OPTION_CACHE_PASSWORD_SHORT, BT_CLIENT_OPTION_CACHE_PASSWORD_DESC) << std::endl;
    std::cout << formatOption(BT_CLIENT_OPTION_NO_CERT, BT_CLIENT_OPTION_NO_CERT_SHORT, BT_CLIENT_OPTION_NO_CERT_DESC) << std::endl;
    std::cout << formatOption(BT_CLIENT_OPTION_DEF_KEYDIR, BT_CLIENT_OPTION_DEF_KEYDIR_SHORT, BT_CLIENT_OPTION_DEF_KEYDIR_DESC) << std::endl;
    std::cout << formatOption(BT_CLIENT_OPTION_SSL_DEBUG, BT_CLIENT_OPTION_SSL_DEBUG_SHORT, BT_CLIENT_OPTION_SSL_DEBUG_DESC) << std::endl;
    std::cout << formatOption(BT_CLIENT_OPTION_AUTO_SESS, BT_CLIENT_OPTION_AUTO_SESS_SHORT, BT_CLIENT_OPTION_AUTO_SESS_DESC) << std::endl;
    std::cout << formatOption(BT_CLIENT_OPTION_AUTH_RETRY, BT_CLIENT_OPTION_AUTH_RETRY_SHORT, BT_CLIENT_OPTION_AUTH_RETRY_DESC) << std::endl;
    std::cout << formatOption(BT_CLIENT_OPTION_PERSIST_TUN, BT_CLIENT_OPTION_PERSIST_TUN_SHORT, BT_CLIENT_OPTION_PERSIST_TUN_DESC) << std::endl;
    std::cout << formatOption(BT_CLIENT_OPTION_PEER_INFO, BT_CLIENT_OPTION_PEER_INFO_SHORT, BT_CLIENT_OPTION_PEER_INFO_DESC) << std::endl;
    std::cout << formatOption(BT_CLIENT_OPTION_GREMLIN, BT_CLIENT_OPTION_GREMLIN_SHORT, BT_CLIENT_OPTION_GREMLIN_DESC) << std::endl;
    std::cout << formatOption(BT_CLIENT_OPTION_EPKI_CA, BT_CLIENT_OPTION_EPKI_CA_SHORT, BT_CLIENT_OPTION_EPKI_CA_DESC) << std::endl;
    std::cout << formatOption(BT_CLIENT_OPTION_EPKI_CERT, BT_CLIENT_OPTION_EPKI_CERT_SHORT, BT_CLIENT_OPTION_EPKI_CERT_DESC) << std::endl;
    std::cout << formatOption(BT_CLIENT_OPTION_EPKI_KEY, BT_CLIENT_OPTION_EPKI_KEY_SHORT, BT_CLIENT_OPTION_EPKI_KEY_DESC) << std::endl;

#ifdef OPENVPN_REMOTE_OVERRIDE

    std::cout << formatOption(BT_CLIENT_OPTION_REMOTE_OVERRIDE_SHORT, BT_CLIENT_OPTION_REMOTE_OVERRIDE, BT_CLIENT_OPTION_REMOTE_OVERRIDE_DESC) << std::endl;

#endif

    std::cout << formatOption(BT_CLIENT_OPTION_GUI_VERSION, BT_CLIENT_OPTION_GUI_VERSION_SHORT, BT_CLIENT_OPTION_GUI_VERSION_DESC) << std::endl;
    std::cout << formatOption(BT_CLIENT_OPTION_RECOVER_NETWORK, BT_CLIENT_OPTION_RECOVER_NETWORK_SHORT, BT_CLIENT_OPTION_RECOVER_NETWORK_DESC) << std::endl;
    std::cout << formatOption(BT_CLIENT_OPTION_DISCONNECT, BT_CLIENT_OPTION_DISCONNECT_SHORT, BT_CLIENT_OPTION_DISCONNECT_DESC) << std::endl;
    std::cout << formatOption(BT_CLIENT_OPTION_PAUSE, BT_CLIENT_OPTION_PAUSE_SHORT, BT_CLIENT_OPTION_PAUSE_DESC) << std::endl;
    std::cout << formatOption(BT_CLIENT_OPTION_RESUME, BT_CLIENT_OPTION_RESUME_SHORT, BT_CLIENT_OPTION_RESUME_DESC) << std::endl;
    std::cout << formatOption(BT_CLIENT_OPTION_RECONNECT, BT_CLIENT_OPTION_RECONNECT_SHORT, BT_CLIENT_OPTION_RECONNECT_DESC) << std::endl << std::endl;

    aboutDevelopmentCredits();
}

std::string formatOption(std::string longOption, std::string shortOption, std::string description)
{
    std::string paddedOption = "";
    
    if(longOption.empty() == false)
        paddedOption += "--" + longOption;
    
    if(shortOption.empty() == false)
        paddedOption += ", -" + shortOption;

    if(description.empty() == false)
        paddedOption = AirVPNTools::pad(paddedOption, PAD_OPTION_SIZE) + ": " + description;
    
    return paddedOption;
}

void aboutDevelopmentCredits()
{
    std::cout << "Open Source Project by AirVPN (https://airvpn.org)" << std::endl << std::endl;
    std::cout << "Linux and macOS design, development and coding by ProMIND" << std::endl << std::endl;

    std::cout << "Special thanks to the AirVPN community for the valuable help," << std::endl;
    std::cout << "support, suggestions and testing." << std::endl << std::endl;
}

DBusResponse *send_method_to_server(const char *method)
{
    std::ostringstream os;
    std::vector<std::string> dbusItems;
    DBusMessage *dbusReply = nullptr;
    DBusResponse *dbusResponse = nullptr;

    dbusItems.clear();

    try
    {
        dbusReply = dbusConnector->callMethodWithReply(BT_SERVER_BUS_NAME, BT_SERVER_OBJECT_PATH_NAME, method, dbusItems);

        if(dbusReply == nullptr)
            cleanup_and_exit("Error in dbus_message_new_method_call", EXIT_FAILURE);

        dbusResponse = dbusConnector->getResponse(dbusReply);

        if(dbusResponse->getResponse() == "")
        {
            os.str("");
            os << "ERROR: D-Bus service " << BT_SERVER_BUS_NAME << " is not available";
            cleanup_and_exit(os.str().c_str(), EXIT_FAILURE);
        }
        else if(dbusResponse->getResponse() != BT_OK)
            log(dbusResponse->getResponse().c_str());
    }
    catch(DBusConnectorException &e)
    {
        os.str("");

        os << "ERROR: D-Bus service " << BT_SERVER_BUS_NAME << " is not available. (" << e.what() << ")";

        cleanup_and_exit(os.str().c_str(), EXIT_FAILURE);
    }

    dbusConnector->unreferenceMessage(dbusReply);

    return dbusResponse;
}

DBusResponse *send_method_with_args_to_server(const char *method, const std::vector<std::string> &args)
{
    std::ostringstream os;
    DBusMessage *dbusReply = nullptr;
    DBusResponse *dbusResponse = nullptr;

    try
    {
        dbusReply = dbusConnector->callMethodWithReply(BT_SERVER_BUS_NAME, BT_SERVER_OBJECT_PATH_NAME, method, args);

        if(dbusReply == nullptr)
            cleanup_and_exit("Error in dbus_message_new_method_call", EXIT_FAILURE);

        dbusResponse = dbusConnector->getResponse(dbusReply);

        if(dbusResponse->getResponse() == "")
        {
            os.str("");
            os << "ERROR: D-Bus service " << BT_SERVER_BUS_NAME << " is not available";
            cleanup_and_exit(os.str().c_str(), EXIT_FAILURE);
        }
        else if(dbusResponse->getResponse() != BT_OK)
            log(dbusResponse->getResponse().c_str());
    }
    catch(DBusConnectorException &e)
    {
        os.str("");

        os << "ERROR: D-Bus service " << BT_SERVER_BUS_NAME << " is not available. (" << e.what() << ")";

        cleanup_and_exit(os.str().c_str(), EXIT_FAILURE);
    }

    dbusConnector->unreferenceMessage(dbusReply);

    return dbusResponse;
}

bool check_airvpn_mode(int argc, char **argv)
{
    bool check = false;

    for(int i = 0; i < argc && check == false; i++)
    {
        if(strcmp(argv[i], "--" BT_CLIENT_OPTION_AIR_CONNECT) == 0 || strcmp(argv[i], "-" BT_CLIENT_OPTION_AIR_CONNECT_SHORT) == 0 ||
           strcmp(argv[i], "--" BT_CLIENT_OPTION_AIR_SERVER) == 0 || strcmp(argv[i], "-" BT_CLIENT_OPTION_AIR_SERVER_SHORT) == 0 ||
           strcmp(argv[i], "--" BT_CLIENT_OPTION_AIR_COUNTRY) == 0 || strcmp(argv[i], "-" BT_CLIENT_OPTION_AIR_COUNTRY_SHORT) == 0 ||
           strcmp(argv[i], "--" BT_CLIENT_OPTION_AIR_VPN_TYPE) == 0 || strcmp(argv[i], "-" BT_CLIENT_OPTION_AIR_VPN_TYPE_SHORT) == 0 ||
           strcmp(argv[i], "--" BT_CLIENT_OPTION_AIR_TLS_MODE) == 0 || strcmp(argv[i], "-" BT_CLIENT_OPTION_AIR_TLS_MODE_SHORT) == 0 ||
           strcmp(argv[i], "--" BT_CLIENT_OPTION_AIR_IPV6) == 0  || strcmp(argv[i], "-" BT_CLIENT_OPTION_AIR_IPV6_SHORT) == 0 ||
           strcmp(argv[i], "--" BT_CLIENT_OPTION_AIR_6TO4) == 0  || strcmp(argv[i], "-" BT_CLIENT_OPTION_AIR_6TO4_SHORT) == 0 ||
           strcmp(argv[i], "--" BT_CLIENT_OPTION_AIR_USER) == 0 || strcmp(argv[i], "-" BT_CLIENT_OPTION_AIR_USER_SHORT) == 0 ||
           strcmp(argv[i], "--" BT_CLIENT_OPTION_AIR_PASSWORD) == 0 || strcmp(argv[i], "-" BT_CLIENT_OPTION_AIR_PASSWORD_SHORT) == 0 ||
           strcmp(argv[i], "--" BT_CLIENT_OPTION_AIR_KEY) == 0 || strcmp(argv[i], "-" BT_CLIENT_OPTION_AIR_KEY_SHORT) == 0 ||
           strcmp(argv[i], "--" BT_CLIENT_OPTION_AIR_KEY_LIST) == 0 || strcmp(argv[i], "-" BT_CLIENT_OPTION_AIR_KEY_LIST_SHORT) == 0 ||
           strcmp(argv[i], "--" BT_CLIENT_OPTION_AIR_SAVE) == 0 || strcmp(argv[i], "-" BT_CLIENT_OPTION_AIR_SAVE_SHORT) == 0 ||
           strcmp(argv[i], "--" BT_CLIENT_OPTION_AIR_KEY_LOAD) == 0 || strcmp(argv[i], "-" BT_CLIENT_OPTION_AIR_KEY_LOAD_SHORT) == 0 ||
           strcmp(argv[i], "--" BT_CLIENT_OPTION_AIR_INFO) == 0 || strcmp(argv[i], "-" BT_CLIENT_OPTION_AIR_INFO_SHORT) == 0 ||
           strcmp(argv[i], "--" BT_CLIENT_OPTION_AIR_LIST) == 0 || strcmp(argv[i], "-" BT_CLIENT_OPTION_AIR_LIST_SHORT) == 0 ||
           strcmp(argv[i], "--" BT_CLIENT_OPTION_AIR_WHITE_SERVER_LIST) == 0 || strcmp(argv[i], "-" BT_CLIENT_OPTION_AIR_WHITE_SERVER_LIST_SHORT) == 0 ||
           strcmp(argv[i], "--" BT_CLIENT_OPTION_AIR_BLACK_SERVER_LIST) == 0 || strcmp(argv[i], "-" BT_CLIENT_OPTION_AIR_BLACK_SERVER_LIST_SHORT) == 0 ||
           strcmp(argv[i], "--" BT_CLIENT_OPTION_AIR_WHITE_COUNTRY_LIST) == 0 || strcmp(argv[i], "-" BT_CLIENT_OPTION_AIR_WHITE_COUNTRY_LIST_SHORT) == 0 ||
           strcmp(argv[i], "--" BT_CLIENT_OPTION_AIR_BLACK_COUNTRY_LIST) == 0 || strcmp(argv[i], "-" BT_CLIENT_OPTION_AIR_BLACK_COUNTRY_LIST_SHORT) == 0 )
                check = true;
    }

    return check;
}

bool check_airvpn_connect(int argc, char **argv)
{
    bool check = false;

    for(int i = 0; i < argc && check == false; i++)
    {
        if(strcmp(argv[i], "--" BT_CLIENT_OPTION_AIR_CONNECT) == 0 || strcmp(argv[i], "-" BT_CLIENT_OPTION_AIR_CONNECT_SHORT) == 0)
            check = true;
    }

    return check;
}

bool check_airvpn_key_load(int argc, char **argv)
{
    std::ostringstream os;
    bool check = false;
    std::ifstream keyFile;

    for(int i = 0; i < argc && check == false; i++)
    {
        if(strcmp(argv[i], "--" BT_CLIENT_OPTION_AIR_KEY_LOAD) == 0 || strcmp(argv[i], "-" BT_CLIENT_OPTION_AIR_KEY_LOAD_SHORT) == 0)
        {
            check = true;

            if(i + 1 < argc)
            {
                keyFile.open(argv[i + 1]);

                if(keyFile.good())
                {
                    airVpnKey << keyFile.rdbuf();

                    keyFile.close();
                }
                else
                {
                    os.str("");

                    os << "--" << BT_CLIENT_OPTION_AIR_KEY_LOAD << ": file " << argv[1] << " not found";

                    cleanup_and_exit(os.str().c_str(), EXIT_FAILURE);
                }
            }
            else
                cleanup_and_exit("--" BT_CLIENT_OPTION_AIR_KEY_LOAD ": no key file name provided", EXIT_FAILURE);
        }
    }

    return check;
}

void check_airvpn_credentials(int argc, char **argv)
{
    struct termios tty;
    bool required = false;
    bool username_option = false;
    bool password_option = false;

    for(int i = 0; i < argc; i++)
    {
        if(strcmp(argv[i], "--" BT_CLIENT_OPTION_AIR_CONNECT) == 0 || strcmp(argv[i], "-" BT_CLIENT_OPTION_AIR_CONNECT_SHORT) == 0 ||
           strcmp(argv[i], "--" BT_CLIENT_OPTION_AIR_KEY) == 0 || strcmp(argv[i], "-" BT_CLIENT_OPTION_AIR_KEY_SHORT) == 0 ||
           strcmp(argv[i], "--" BT_CLIENT_OPTION_AIR_KEY_LIST) == 0 || strcmp(argv[i], "-" BT_CLIENT_OPTION_AIR_KEY_LIST_SHORT) == 0 ||
           strcmp(argv[i], "--" BT_CLIENT_OPTION_AIR_SAVE) == 0 || strcmp(argv[i], "-" BT_CLIENT_OPTION_AIR_SAVE_SHORT) == 0 ||
           strcmp(argv[i], "--" BT_CLIENT_OPTION_AIR_KEY_LOAD) == 0 || strcmp(argv[i], "-" BT_CLIENT_OPTION_AIR_KEY_LOAD_SHORT) == 0)
                required = true;

        if(strcmp(argv[i], "--" BT_CLIENT_OPTION_AIR_USER) == 0 || strcmp(argv[i], "-" BT_CLIENT_OPTION_AIR_USER_SHORT) == 0)
            username_option = true;

        if(strcmp(argv[i], "--" BT_CLIENT_OPTION_AIR_PASSWORD) == 0 || strcmp(argv[i], "-" BT_CLIENT_OPTION_AIR_PASSWORD_SHORT) == 0)
            password_option = true;
    }

    if(required)
    {
        if(username_option == false)
        {
            if(airVpnUsername.empty())
            {
                while(airVpnUsername.empty())
                {
                    std::cout << "AirVPN Username: ";

                    std::getline(std::cin, airVpnUsername);
                }
            }

            airVpnUsername = AirVPNTools::trim(airVpnUsername);

            extraOptions.push_back("--" BT_CLIENT_OPTION_AIR_USER);
            extraOptions.push_back(airVpnUsername);
        }

        if(password_option == false)
        {
            if(airVpnPassword.empty())
            {
                tcgetattr(STDIN_FILENO, &tty);

                tty.c_lflag &= ~ECHO;

                tcsetattr(STDIN_FILENO, TCSANOW, &tty);

                while(airVpnPassword.empty())
                {
                    std::cout << "AirVPN Password for user " << airVpnUsername << ": ";

                    std::getline(std::cin, airVpnPassword);

                    std::cout << std::endl;
                }

                tty.c_lflag |= ECHO;

                tcsetattr(STDIN_FILENO, TCSANOW, &tty);
            }

            airVpnPassword = AirVPNTools::trim(airVpnPassword);

            extraOptions.push_back("--" BT_CLIENT_OPTION_AIR_PASSWORD);
            extraOptions.push_back(airVpnPassword);
        }
    }
}

void airvpn_server_info(DBusResponse *dbusResponse)
{
    std::ostringstream os;
    DBusResponse::Item item;
    int server_score;
    long timestamp;

    if(dbusResponse->rows() == 0)
    {
        log("ERROR: AirVPN server not found");

        return;
    }

    item = dbusResponse->getItem(0);

    log("** AirVPN Server Information **");

    os.str("");
    os << "Name: " << dbusResponse->getItemValue(item, "name");
    log(os.str().c_str());

    os.str("");
    os << "Country ISO code: " << dbusResponse->getItemValue(item, "country_code");
    log(os.str().c_str());

    os.str("");
    os << "Country: " << dbusResponse->getItemValue(item, "country");
    log(os.str().c_str());

    os.str("");
    os << "Location: " << dbusResponse->getItemValue(item, "location");
    log(os.str().c_str());

    os.str("");
    os << "Current traffic rate: ";
    
    try
    {
        os << AirVPNTools::formatTransferRate(std::stoll(dbusResponse->getItemValue(item, "effective_bandwidth")));
    }
    catch(std::exception &e)
    {
        os << "--";
    }

    log(os.str().c_str());

    os.str("");
    os << "Maximum bandwidth: ";

    try
    {
        os << AirVPNTools::formatTransferRate(std::stoll(dbusResponse->getItemValue(item, "max_bandwidth")) * AirVPNTools::ONE_DECIMAL_MEGA);
    }
    catch(std::exception &e)
    {
        os << "--";
    }
    
    log(os.str().c_str());

    os.str("");
    os << "Users: " << dbusResponse->getItemValue(item, "users");
    log(os.str().c_str());

    os.str("");
    os << "Load: " << dbusResponse->getItemValue(item, "load") << "%";
    log(os.str().c_str());

    os.str("");
    os << "IPv4 available: " << dbusResponse->getItemValue(item, "ipv4_available");
    log(os.str().c_str());

    os.str("");
    os << "IPv6 available: " << dbusResponse->getItemValue(item, "ipv6_available");
    log(os.str().c_str());

    os.str("");
    os << "OpenVPN available: " << dbusResponse->getItemValue(item, "openvpn_available");
    log(os.str().c_str());

    if(dbusResponse->getItemValue(item, "openvpn_tls_ciphers").empty() == false)
    {
        os.str("");
        os << "OpenVPN TLS ciphers: " << dbusResponse->getItemValue(item, "openvpn_tls_ciphers");
        log(os.str().c_str());
    }

    if(dbusResponse->getItemValue(item, "openvpn_tls_suite_ciphers").empty() == false)
    {
        os.str("");
        os << "OpenVPN TLS suite ciphers: " << dbusResponse->getItemValue(item, "openvpn_tls_suite_ciphers");
        log(os.str().c_str());
    }

    if(dbusResponse->getItemValue(item, "openvpn_data_ciphers").empty() == false)
    {
        os.str("");
        os << "OpenVPN Data ciphers: " << dbusResponse->getItemValue(item, "openvpn_data_ciphers");
        log(os.str().c_str());
    }

    os.str("");
    os << "WireGuard available: " << dbusResponse->getItemValue(item, "wireguard_available");
    log(os.str().c_str());

    os.str("");
    os << "WireGuard ciphers: " << dbusResponse->getItemValue(item, "wireguard_ciphers");
    log(os.str().c_str());

    os.str("");
    os << "Perfect Forward Secrecy (PFS) available: " << dbusResponse->getItemValue(item, "pfs_available");
    log(os.str().c_str());

    try
    {
        server_score = std::stoi(dbusResponse->getItemValue(item, "score"));
    }
    catch(std::exception &e)
    {
        server_score = -1;
    }
    
    if(server_score != AirVPNServer::WORST_SCORE)
    {
        os.str("");
        os << "Score: " << dbusResponse->getItemValue(item, "score");
        log(os.str().c_str());
    }

    os.str("");
    os << "Available: " << dbusResponse->getItemValue(item, "available");
    log(os.str().c_str());

    if(dbusResponse->getItemValue(item, "open_status").empty() == false)
    {
        os.str("");
        os << "Open status: " << dbusResponse->getItemValue(item, "open_status");
        log(os.str().c_str());
    }

    if(dbusResponse->getItemValue(item, "close_status").empty() == false)
    {
        os.str("");
        os << "Close status: " << dbusResponse->getItemValue(item, "close_status");
        log(os.str().c_str());
    }

    try
    {
        timestamp = std::stoi(dbusResponse->getItemValue(item, "timestamp"));
    }
    catch(std::exception &e)
    {
        timestamp = -1;
    }
    
    if(timestamp > 0)
    {
        os.str("");
        os << "Date/time: " << AirVPNTools::formatTimestamp(timestamp);
        log(os.str().c_str());
    }

    try
    {
        timestamp = std::stoi(dbusResponse->getItemValue(item, "next_update_timestamp"));
    }
    catch(std::exception &e)
    {
        timestamp = -1;
    }
    
    if(timestamp > 0)
    {
        os.str("");
        os << "Next update: " << AirVPNTools::formatTimestamp(timestamp);
        log(os.str().c_str());
    }
}

void airvpn_server_list(DBusResponse *dbusResponse)
{
    std::ostringstream os;
    DBusResponse::Item item;

    if(dbusResponse->rows() == 0)
    {
        log("ERROR: No AirVPN server found");

        return;
    }

    os.str("");
    os << "** AirVPN Server List " << "(" << dbusResponse->rows() << ") **";
    log(os.str().c_str());

    os.str("");
    os << AirVPNTools::pad("Name", 20) << " ";
    os << AirVPNTools::pad("Country", 30) << " ";
    os << AirVPNTools::pad("Location", 20);
    log(os.str().c_str());

    os.str("");
    os << AirVPNTools::pad("", 20, '-') << " ";
    os << AirVPNTools::pad("", 30, '-') << " ";
    os << AirVPNTools::pad("", 20, '-');
    log(os.str().c_str());

    for(int i = 0; i < dbusResponse->rows(); i++)
    {
        item = dbusResponse->getItem(i);

        os.str("");
        os << AirVPNTools::pad(dbusResponse->getItemValue(item, "name"), 21);
        os << AirVPNTools::pad(dbusResponse->getItemValue(item, "country"), 31);
        os << AirVPNTools::pad(dbusResponse->getItemValue(item, "location"), 20);

        log(os.str().c_str());
    }
}

void airvpn_country_info(DBusResponse *dbusResponse)
{
    std::ostringstream os;
    DBusResponse::Item item;
    long timestamp;

    if(dbusResponse->rows() == 0)
    {
        log("ERROR: AirVPN country not found");

        return;
    }

    item = dbusResponse->getItem(0);

    log("** AirVPN Country Information **");

    os.str("");
    os << "Country ISO Code: " << dbusResponse->getItemValue(item, "country_iso_code");
    log(os.str().c_str());

    os.str("");
    os << "Country: " << dbusResponse->getItemValue(item, "country_name");
    log(os.str().c_str());

    os.str("");
    os << "Servers: " << dbusResponse->getItemValue(item, "servers");
    log(os.str().c_str());

    os.str("");
    os << "Users: " << dbusResponse->getItemValue(item, "users");
    log(os.str().c_str());

    os.str("");
    os << "Bandwidth: ";
    
    try
    {
        os << AirVPNTools::formatTransferRate(std::stoll(dbusResponse->getItemValue(item, "bandwidth")));
    }
    catch(std::exception &e)
    {
        os << "--";
    }

    log(os.str().c_str());

    os.str("");
    
    os << "Maximum bandwidth: ";

    try
    {
        os << AirVPNTools::formatTransferRate(std::stoll(dbusResponse->getItemValue(item, "max_bandwidth")) * AirVPNTools::ONE_DECIMAL_MEGA);
    }
    catch(std::exception &e)
    {
        os << "--";
    }

    log(os.str().c_str());

    try
    {
        timestamp = std::stoi(dbusResponse->getItemValue(item, "timestamp"));
    }
    catch(std::exception &e)
    {
        timestamp = -1;
    }
    
    if(timestamp > 0)
    {
        os.str("");
        os << "Date/time: " << AirVPNTools::formatTimestamp(timestamp);
        log(os.str().c_str());
    }

    try
    {
        timestamp = std::stoi(dbusResponse->getItemValue(item, "next_update_timestamp"));
    }
    catch(std::exception &e)
    {
        timestamp = -1;
    }
    
    if(timestamp > 0)
    {
        os.str("");
        os << "Next update: " << AirVPNTools::formatTimestamp(timestamp);
        log(os.str().c_str());
    }
}

void airvpn_country_list(DBusResponse *dbusResponse)
{
    std::ostringstream os;
    DBusResponse::Item item;

    if(dbusResponse->rows() == 0)
    {
        log("ERROR: No AirVPN country found");

        return;
    }

    os.str("");
    os << "** AirVPN Country List " << "(" << dbusResponse->rows() << ") **";
    log(os.str().c_str());

    os.str("");
    os << AirVPNTools::pad("ISO Code", 8) << " ";
    os << AirVPNTools::pad("Name", 30) << " ";
    os << AirVPNTools::pad("Servers", 7) << " ";
    os << AirVPNTools::pad("Users", 5) << " ";
    os << AirVPNTools::pad("Bandwidth", 12) << " ";
    os << AirVPNTools::pad("Max BW", 10);
    log(os.str().c_str());

    os.str("");
    os << AirVPNTools::pad("", 8, '-') << " ";
    os << AirVPNTools::pad("", 30, '-') << " ";
    os << AirVPNTools::pad("", 7, '-') << " ";
    os << AirVPNTools::pad("", 5, '-') << " ";
    os << AirVPNTools::pad("", 12, '-') << " ";
    os << AirVPNTools::pad("", 10, '-');
    log(os.str().c_str());

    for(int i = 0; i < dbusResponse->rows(); i++)
    {
        item = dbusResponse->getItem(i);

        os.str("");
        os << AirVPNTools::pad(dbusResponse->getItemValue(item, "country_iso_code"), 9);
        os << AirVPNTools::pad(dbusResponse->getItemValue(item, "country_name"), 31);
        os << AirVPNTools::padRight(dbusResponse->getItemValue(item, "servers"), 7);
        os << " ";
        os << AirVPNTools::padRight(dbusResponse->getItemValue(item, "users"), 5);
        os << " ";
        
        try
        {
            os << AirVPNTools::padRight(AirVPNTools::formatTransferRate(std::stoll(dbusResponse->getItemValue(item, "bandwidth"))), 12);
        }
        catch(std::exception &e)
        {
            os << "--";
        }

        os << " ";
        
        try
        {
            os << AirVPNTools::padRight(AirVPNTools::formatTransferRate(std::stoll(dbusResponse->getItemValue(item, "max_bandwidth")) * AirVPNTools::ONE_DECIMAL_MEGA), 10);
        }
        catch(std::exception &e)
        {
            os << "--";
        }

        log(os.str().c_str());
    }
}

void airvpn_key_list(DBusResponse *dbusResponse)
{
    std::ostringstream os;
    DBusResponse::Item item;

    if(dbusResponse->rows() == 0)
    {
        log("ERROR: No AirVPN key found");

        return;
    }

    os.str("");
    os << "** AirVPN keys for user " << airVpnUsername << " (" << dbusResponse->rows() << ") **";
    log(os.str().c_str());

    os.str("");
    os << AirVPNTools::pad("Name", 48);
    log(os.str().c_str());

    os.str("");
    os << AirVPNTools::pad("", 48, '-');
    log(os.str().c_str());

    for(int i = 0; i < dbusResponse->rows(); i++)
    {
        item = dbusResponse->getItem(i);

        os.str("");
        os << AirVPNTools::pad(dbusResponse->getItemValue(item, "key"), 48);
        log(os.str().c_str());
    }
}

void airvpn_save(DBusResponse *dbusResponse)
{
    std::ostringstream os;
    DBusResponse::Item item;
    std::ofstream profile;

    if(dbusResponse->rows() == 0)
    {
        log("ERROR: Server returned an empty dataset");

        return;
    }

    item = dbusResponse->getItem(0);

    os.str("");
    os << "Saving " << dbusResponse->getItemValue(item, "type") << " ";
    os << dbusResponse->getItemValue(item, "name") << " for user ";
    os << dbusResponse->getItemValue(item, "user") << " to " << dbusResponse->getItemValue(item, "file_name");
    log(os.str().c_str());

    profile.open(dbusResponse->getItemValue(item, "file_name"));
    profile << dbusResponse->getItemValue(item, "content");
    profile.close();
}

bool read_configuration()
{
    std::string configFile = "", homeDirectory = "", path = "";
    std::string xdgConfigFile = "", rcFile = "";
    std::ostringstream os;
    RCParser *rcParser = nullptr;
    RCParser::Error rcError;
    RCParser::Directive *rcDirective = nullptr;
    struct stat xdg_config_dir;

    homeDirectory = getpwuid(getuid())->pw_dir;

    xdgConfigFile = homeDirectory + GOLDCREST_XDG_CONFIG_FILE;
    rcFile = homeDirectory + GOLDCREST_RC_FILE;

    if(access(xdgConfigFile.c_str(), F_OK) == 0)
        configFile = xdgConfigFile;
    else if(access(rcFile.c_str(), F_OK) == 0)
        configFile = rcFile;
    else
    {
        path = homeDirectory + XDG_CONFIG_DIR;

        if(stat(path.c_str(), &xdg_config_dir) == 0 && S_ISDIR(xdg_config_dir.st_mode) != 0)
            configFile = xdgConfigFile;
        else
            configFile = rcFile;
    }

    if(access(configFile.c_str(), F_OK) != 0)
    {
        std::ofstream rcTemplate(configFile);

        rcTemplate << GOLDCREST_RC_TEMPLATE;

        rcTemplate.close();
    }

    os.str("");
    os << "Reading run control directives from file " << configFile;
    log(os.str().c_str());

    rcParser = new RCParser(configFile);

    rcParser->addConfigDirective(RC_DIRECTIVE_AIRVPN_SERVER, RCParser::Type::STRING);
    rcParser->addConfigDirective(RC_DIRECTIVE_AIRVPN_VPN_TYPE, RCParser::Type::STRING);
    rcParser->addConfigDirective(RC_DIRECTIVE_AIRVPN_TLS_MODE, RCParser::Type::STRING);
    rcParser->addConfigDirective(RC_DIRECTIVE_AIRVPN_IPV6, RCParser::Type::BOOL);
    rcParser->addConfigDirective(RC_DIRECTIVE_AIRVPN_6TO4, RCParser::Type::BOOL);
    rcParser->addConfigDirective(RC_DIRECTIVE_AIRVPN_USERNAME, RCParser::Type::STRING);
    rcParser->addConfigDirective(RC_DIRECTIVE_AIRVPN_PASSWORD, RCParser::Type::STRING);
    rcParser->addConfigDirective(RC_DIRECTIVE_AIRVPN_KEY, RCParser::Type::STRING);
    rcParser->addConfigDirective(RC_DIRECTIVE_AIRWHITESERVERLIST, RCParser::Type::LIST);
    rcParser->addConfigDirective(RC_DIRECTIVE_AIRBLACKSERVERLIST, RCParser::Type::LIST);
    rcParser->addConfigDirective(RC_DIRECTIVE_AIRWHITECOUNTRYLIST, RCParser::Type::LIST);
    rcParser->addConfigDirective(RC_DIRECTIVE_AIRBLACKCOUNTRYLIST, RCParser::Type::LIST);
    rcParser->addConfigDirective(RC_DIRECTIVE_GC_CIPHER, RCParser::Type::STRING);
    rcParser->addConfigDirective(RC_DIRECTIVE_GC_PROTO, RCParser::Type::STRING);
    rcParser->addConfigDirective(RC_DIRECTIVE_GC_SERVER, RCParser::Type::STRING);
    rcParser->addConfigDirective(RC_DIRECTIVE_GC_PORT, RCParser::Type::INTEGER);
    rcParser->addConfigDirective(RC_DIRECTIVE_GC_TCP_QUEUE_LIMIT, RCParser::Type::INTEGER);
    rcParser->addConfigDirective(RC_DIRECTIVE_GC_NCP_DISABLE, RCParser::Type::BOOL);
    rcParser->addConfigDirective(RC_DIRECTIVE_GC_NETWORK_LOCK, RCParser::Type::STRING);
    rcParser->addConfigDirective(RC_DIRECTIVE_GC_IGNORE_DNS_PUSH, RCParser::Type::BOOL);
    rcParser->addConfigDirective(RC_DIRECTIVE_GC_ALLOWUAF, RCParser::Type::STRING);
    rcParser->addConfigDirective(RC_DIRECTIVE_GC_TIMEOUT, RCParser::Type::INTEGER);
    rcParser->addConfigDirective(RC_DIRECTIVE_GC_COMPRESS, RCParser::Type::STRING);
    rcParser->addConfigDirective(RC_DIRECTIVE_GC_PROXY_HOST, RCParser::Type::STRING);
    rcParser->addConfigDirective(RC_DIRECTIVE_GC_PROXY_PORT, RCParser::Type::INTEGER);
    rcParser->addConfigDirective(RC_DIRECTIVE_GC_PROXY_USERNAME, RCParser::Type::STRING);
    rcParser->addConfigDirective(RC_DIRECTIVE_GC_PROXY_PASSWORD, RCParser::Type::STRING);
    rcParser->addConfigDirective(RC_DIRECTIVE_GC_PROXY_BASIC, RCParser::Type::BOOL);
    rcParser->addConfigDirective(RC_DIRECTIVE_GC_ALT_PROXY, RCParser::Type::BOOL);
    rcParser->addConfigDirective(RC_DIRECTIVE_GC_PERSIST_TUN, RCParser::Type::BOOL);
    rcParser->addConfigDirective(RC_DIRECTIVE_GC_CONN_STAT_SECS, RCParser::Type::INTEGER);

    rcError = rcParser->parseRCFile();

    if(rcError != RCParser::Error::OK)
    {
        switch(rcError)
        {
            case RCParser::Error::NO_DIRECTIVES_PROVIDED:
            {
                cleanup_and_exit("ERROR: No rc directives provided to RCParser. Exiting.", EXIT_FAILURE);
            }
            break;

            case RCParser::Error::CANNOT_OPEN_RC_FILE:
            {
                log("Cannot open run control file. Using default values.");
            }
            break;

            case RCParser::Error::UNKNOWN_DIRECTIVE:
            {
                os.str("");

                os << "Error while parsing " << configFile << " file. ";

                os << rcParser->getErrorDescription() << ". Exiting.";

                cleanup_and_exit(os.str().c_str(), EXIT_FAILURE);
            }
            break;

            case RCParser::Error::PARSE_ERROR:
            {
                os.str("");

                os << "Error while parsing " << configFile << " file. Exiting.";

                cleanup_and_exit(os.str().c_str(), EXIT_FAILURE);
            }
            break;

            default:
            {
                os.str("");
                os << "Unexpected error while parsing " << configFile << " file. Exiting.";

                cleanup_and_exit(os.str().c_str(), EXIT_FAILURE);
            }
            break;
        }
    }

    RCParser::Directives rcInvalidDirective = rcParser->getInvalidDirectives();

    for(int i = 0; i < rcInvalidDirective.size(); i++)
    {
        os.str("");

        os << "ERROR: invalid RC directive '" << rcInvalidDirective[i]->name << "' - " << rcInvalidDirective[i]->error;

        log(os.str().c_str());
    }

    if(airvpn_connect == true)
    {
        rcDirective = rcParser->getDirective(RC_DIRECTIVE_AIRVPN_SERVER);

        if(rcDirective != nullptr)
        {
            extraOptions.push_back("--" RC_DIRECTIVE_AIRVPN_SERVER);
            extraOptions.push_back(rcDirective->value[0]);
        }
    }

    rcDirective = rcParser->getDirective(RC_DIRECTIVE_AIRVPN_VPN_TYPE);

    if(rcDirective != nullptr)
    {
        extraOptions.push_back("--" RC_DIRECTIVE_AIRVPN_VPN_TYPE);
        extraOptions.push_back(rcDirective->value[0]);
    }

    rcDirective = rcParser->getDirective(RC_DIRECTIVE_AIRVPN_TLS_MODE);

    if(rcDirective != nullptr)
    {
        extraOptions.push_back("--" RC_DIRECTIVE_AIRVPN_TLS_MODE);
        extraOptions.push_back(rcDirective->value[0]);
    }

    rcDirective = rcParser->getDirective(RC_DIRECTIVE_AIRVPN_IPV6);

    if(rcDirective != nullptr)
    {
        extraOptions.push_back("--" RC_DIRECTIVE_AIRVPN_IPV6);
        extraOptions.push_back(rcDirective->value[0]);
    }

    rcDirective = rcParser->getDirective(RC_DIRECTIVE_AIRVPN_6TO4);

    if(rcDirective != nullptr)
    {
        extraOptions.push_back("--" RC_DIRECTIVE_AIRVPN_6TO4);
        extraOptions.push_back(rcDirective->value[0]);
    }

    rcDirective = rcParser->getDirective(RC_DIRECTIVE_AIRVPN_USERNAME);

    if(rcDirective != nullptr)
        airVpnUsername = rcDirective->value[0];

    rcDirective = rcParser->getDirective(RC_DIRECTIVE_AIRVPN_PASSWORD);

    if(rcDirective != nullptr)
        airVpnPassword = rcDirective->value[0];

    rcDirective = rcParser->getDirective(RC_DIRECTIVE_AIRVPN_KEY);

    if(rcDirective != nullptr)
    {
        extraOptions.push_back("--" RC_DIRECTIVE_AIRVPN_KEY);
        extraOptions.push_back(rcDirective->value[0]);
    }

    rcDirective = rcParser->getDirective(RC_DIRECTIVE_AIRWHITESERVERLIST);

    if(rcDirective != nullptr)
    {
        extraOptions.push_back("--" RC_DIRECTIVE_AIRWHITESERVERLIST);
        extraOptions.push_back(rcDirective->value[0]);
    }

    rcDirective = rcParser->getDirective(RC_DIRECTIVE_AIRBLACKSERVERLIST);

    if(rcDirective != nullptr)
    {
        extraOptions.push_back("--" RC_DIRECTIVE_AIRBLACKSERVERLIST);
        extraOptions.push_back(rcDirective->value[0]);
    }

    rcDirective = rcParser->getDirective(RC_DIRECTIVE_AIRWHITECOUNTRYLIST);

    if(rcDirective != nullptr)
    {
        extraOptions.push_back("--" RC_DIRECTIVE_AIRWHITECOUNTRYLIST);
        extraOptions.push_back(rcDirective->value[0]);
    }

    rcDirective = rcParser->getDirective(RC_DIRECTIVE_AIRBLACKCOUNTRYLIST);

    if(rcDirective != nullptr)
    {
        extraOptions.push_back("--" RC_DIRECTIVE_AIRBLACKCOUNTRYLIST);
        extraOptions.push_back(rcDirective->value[0]);
    }

    rcDirective = rcParser->getDirective(RC_DIRECTIVE_GC_CIPHER);

    if(rcDirective != nullptr)
    {
        extraOptions.push_back("--" RC_DIRECTIVE_GC_CIPHER);
        extraOptions.push_back(rcDirective->value[0]);
    }

    rcDirective = rcParser->getDirective(RC_DIRECTIVE_GC_PROTO);

    if(rcDirective != nullptr)
    {
        extraOptions.push_back("--" RC_DIRECTIVE_GC_PROTO);
        extraOptions.push_back(rcDirective->value[0]);
    }

    rcDirective = rcParser->getDirective(RC_DIRECTIVE_GC_SERVER);

    if(rcDirective != nullptr)
    {
        extraOptions.push_back("--" RC_DIRECTIVE_GC_SERVER);
        extraOptions.push_back(rcDirective->value[0]);
    }

    rcDirective = rcParser->getDirective(RC_DIRECTIVE_GC_PORT);

    if(rcDirective != nullptr)
    {
        extraOptions.push_back("--" RC_DIRECTIVE_GC_PORT);
        extraOptions.push_back(rcDirective->value[0]);
    }

    rcDirective = rcParser->getDirective(RC_DIRECTIVE_GC_TCP_QUEUE_LIMIT);

    if(rcDirective != nullptr)
    {
        extraOptions.push_back("--" RC_DIRECTIVE_GC_TCP_QUEUE_LIMIT);
        extraOptions.push_back(rcDirective->value[0]);
    }

    rcDirective = rcParser->getDirective(RC_DIRECTIVE_GC_NCP_DISABLE);

    if(rcDirective != nullptr && rcParser->isDirectiveEnabled(rcDirective) == true)
        extraOptions.push_back("--" RC_DIRECTIVE_GC_NCP_DISABLE);

    rcDirective = rcParser->getDirective(RC_DIRECTIVE_GC_NETWORK_LOCK);

    if(rcDirective != nullptr)
    {
        extraOptions.push_back("--" RC_DIRECTIVE_GC_NETWORK_LOCK);
        extraOptions.push_back(AirVPNTools::normalizeBoolValue(rcDirective->value[0], "on", "off"));
    }

    rcDirective = rcParser->getDirective(RC_DIRECTIVE_GC_IGNORE_DNS_PUSH);

    if(rcDirective != nullptr && rcParser->isDirectiveEnabled(rcDirective) == true)
        extraOptions.push_back("--" RC_DIRECTIVE_GC_IGNORE_DNS_PUSH);

    rcDirective = rcParser->getDirective(RC_DIRECTIVE_GC_ALLOWUAF);

    if(rcDirective != nullptr)
    {
        extraOptions.push_back("--" RC_DIRECTIVE_GC_ALLOWUAF);
        extraOptions.push_back(AirVPNTools::normalizeBoolValue(rcDirective->value[0], "yes", "no"));
    }

    rcDirective = rcParser->getDirective(RC_DIRECTIVE_GC_TIMEOUT);

    if(rcDirective != nullptr)
    {
        extraOptions.push_back("--" RC_DIRECTIVE_GC_TIMEOUT);
        extraOptions.push_back(rcDirective->value[0]);
    }

    rcDirective = rcParser->getDirective(RC_DIRECTIVE_GC_COMPRESS);

    if(rcDirective != nullptr)
    {
        extraOptions.push_back("--" RC_DIRECTIVE_GC_COMPRESS);
        extraOptions.push_back(AirVPNTools::normalizeBoolValue(rcDirective->value[0], "yes", "no"));
    }

    rcDirective = rcParser->getDirective(RC_DIRECTIVE_GC_PROXY_HOST);

    if(rcDirective != nullptr)
    {
        extraOptions.push_back("--" RC_DIRECTIVE_GC_PROXY_HOST);
        extraOptions.push_back(rcDirective->value[0]);
    }

    rcDirective = rcParser->getDirective(RC_DIRECTIVE_GC_PROXY_PORT);

    if(rcDirective != nullptr)
    {
        extraOptions.push_back("--" RC_DIRECTIVE_GC_PROXY_PORT);
        extraOptions.push_back(rcDirective->value[0]);
    }

    rcDirective = rcParser->getDirective(RC_DIRECTIVE_GC_PROXY_USERNAME);

    if(rcDirective != nullptr)
    {
        extraOptions.push_back("--" RC_DIRECTIVE_GC_PROXY_USERNAME);
        extraOptions.push_back(rcDirective->value[0]);
    }

    rcDirective = rcParser->getDirective(RC_DIRECTIVE_GC_PROXY_PASSWORD);

    if(rcDirective != nullptr)
    {
        extraOptions.push_back("--" RC_DIRECTIVE_GC_PROXY_PASSWORD);
        extraOptions.push_back(rcDirective->value[0]);
    }

    rcDirective = rcParser->getDirective(RC_DIRECTIVE_GC_PROXY_BASIC);

    if(rcDirective != nullptr && rcParser->isDirectiveEnabled(rcDirective) == true)
        extraOptions.push_back("--" RC_DIRECTIVE_GC_PROXY_BASIC);

    rcDirective = rcParser->getDirective(RC_DIRECTIVE_GC_ALT_PROXY);

    if(rcDirective != nullptr && rcParser->isDirectiveEnabled(rcDirective) == true)
        extraOptions.push_back("--" RC_DIRECTIVE_GC_ALT_PROXY);

    rcDirective = rcParser->getDirective(RC_DIRECTIVE_GC_PERSIST_TUN);

    if(rcDirective != nullptr && rcParser->isDirectiveEnabled(rcDirective) == true)
        extraOptions.push_back("--" RC_DIRECTIVE_GC_PERSIST_TUN);

    rcDirective = rcParser->getDirective(RC_DIRECTIVE_GC_CONN_STAT_SECS);

    if(rcDirective != nullptr)
    {
        try
        {
            conn_stat_interval = std::stoi(rcDirective->value[0]);
        }
        catch(std::exception &e)
        {
            conn_stat_interval = 0;
        }

        if(conn_stat_interval < 0)
        {
            os.str("");
            
            os << "ERROR: illegal value '" << rcDirective->value[0] << "' for directive '" << RC_DIRECTIVE_GC_CONN_STAT_SECS << "' in " << configFile << " file";

            cleanup_and_exit(os.str().c_str() ,EXIT_FAILURE);
        }
    }

    delete rcParser;

    return true;
}

void connection_stats(int interval_seconds, const std::future<void> &future)
{
    pushedDns.clear();

    while(future.wait_for(std::chrono::seconds(interval_seconds)) == std::future_status::timeout)
    {
        show_connection_stats();
    }
}

void dbus_message_loop_until(const std::string &exit_event)
{
    bool done = false;
    char *s;
    std::string event, message;
    DBusMessage *dbusMessage = nullptr;
    std::vector<std::string> dbusItems;
    std::ostringstream os;

    if(dbusConnector == nullptr)
        return;

    try
    {
        while(dbusConnector->readWriteDispatch() && done == false)
        {
            while((dbusMessage = dbusConnector->popMessage()) != NULL && done == false)
            {
                if(dbusConnector->isMethod(dbusMessage, BT_METHOD_EVENT))
                {
                    dbusItems = dbusConnector->getVector(dbusMessage);

                    if(dbusItems.empty() == false)
                    {
                        event = dbusItems[0];
                        message = dbusItems[1];

                        if(message.empty() == false)
                            log(message.c_str());

                        if(event == BT_EVENT_CONNECTED)
                        {
                        }
                        else if(event == BT_EVENT_DISCONNECTED)
                        {
                            if(dbusItems.size() == 7)
                            {
                                os.str("");

                                os << "Connection time: " << AirVPNTools::formatTime(std::stol(dbusItems[2]));

                                log(os.str().c_str());

                                os.str("");

                                os << "Total transferred Input data: " << AirVPNTools::formatDataVolume(std::stoll(dbusItems[3]), true);

                                log(os.str().c_str());

                                os.str("");

                                os << "Total transferred Output data: " << AirVPNTools::formatDataVolume(std::stoll(dbusItems[4]), true);

                                log(os.str().c_str());

                                os.str("");

                                os << "Maximum Input rate: " << AirVPNTools::formatTransferRate(std::stoll(dbusItems[5]), true);

                                log(os.str().c_str());

                                os.str("");

                                os << "Maximum Output rate: " << AirVPNTools::formatTransferRate(std::stoll(dbusItems[6]), true);

                                log(os.str().c_str());
                            }
                            else
                            {
                                os.str("");

                                os << "ERROR: wrong item count for event '" << event << "'";

                                log(os.str().c_str());
                            }
                        }
                        else if(event == BT_EVENT_PAUSE)
                        {
                        }
                        else if(event == BT_EVENT_RESUME)
                        {
                        }
                        else if(event == BT_EVENT_END_OF_SESSION || event == exit_event)
                        {
                            os.str("");

                            os << BLUETIT_SHORT_NAME << " session terminated";

                            log(os.str().c_str());

                            done = true;
                        }
                        else
                        {
                            os.str("");

                            os << "ERROR: Received unhandled dbus event '" << event << "'";

                            log(os.str().c_str());
                        }
                    }
                    else
                        log("ERROR: Cannot retrieve dbus event message");
                }
                else if(dbusConnector->isMethod(dbusMessage, BT_METHOD_LOG))
                {
                    if(dbusConnector->getArgs(dbusMessage, DBUS_TYPE_STRING, &s, DBUS_TYPE_INVALID))
                        log(dbusConnector->stringToLocale(s).c_str());
                    else
                        log("ERROR: Cannot retrieve dbus log message");
                }

                dbusConnector->unreferenceMessage(dbusMessage);
            }
        }
    }
    catch(DBusConnectorException &e)
    {
        os.str("");

        os << "ERROR: D-Bus service " << BT_SERVER_BUS_NAME << " is not available. (" << e.what() << ")";

        cleanup_and_exit(os.str().c_str(), EXIT_FAILURE);
    }
}

void show_connection_stats()
{
    std::ostringstream os;
    DBusMessage *dbusReply = nullptr;
    DBusResponse *dbusResponse = nullptr;
    std::vector<std::string> dbusItems;
    DBusResponse::Item item;
    DBusResponse::ItemIterator it;

    if(pushedDns.size() == 0)
        get_pushed_dns();

    try
    {
        dbusReply = dbusConnector->callMethodWithReply(BT_SERVER_BUS_NAME, BT_SERVER_OBJECT_PATH_NAME, BT_METHOD_CONNECTION_STATS, dbusItems);

        if(dbusReply == nullptr)
        {
            os.str("");

            os << "ERROR: D-Bus service " << BT_SERVER_BUS_NAME << " is not available";

            cleanup_and_exit(os.str().c_str(), EXIT_FAILURE);
        }

        dbusResponse = dbusConnector->getResponse(dbusReply);

        std::string response = dbusResponse->getResponse();

        if(response == BT_OK)
        {
            if(dbusResponse->rows() > 0)
            {
                item = dbusResponse->getItem(0);

                it = item.find("status");
                
                if(it != item.end())
                {
                    if(it->second == "CONNECTED" || it->second == "PAUSED")
                    {
                        log("----------------------");

                        it = item.find("airvpn_server_name");
                        
                        if(it != item.end() && it->second != "")
                        {
                            os.str("");

                            os << "Connected to AirVPN server " << it->second << " (";
                            
                            it = item.find("airvpn_server_location");
                            
                            if(it != item.end())
                                os << it->second << ", ";

                            it = item.find("airvpn_server_country");
                            
                            if(it != item.end())
                                os << it->second;

                            os << ")";

                            log(os.str().c_str());
                            
                            os.str("");

                            it = item.find("airvpn_server_users");

                            if(it != item.end())
                                os << "Users " << it->second;

                            it = item.find("airvpn_server_load");

                            if(it != item.end())
                                os << " - Load " << it->second << "%";

                            it = item.find("airvpn_server_bandwidth");

                            if(it != item.end())
                            {
                                os << " - Bandwidth ";
                                
                                try
                                {
                                    os << AirVPNTools::formatTransferRate(std::stoll(it->second), true);
                                }
                                catch(std::exception &e)
                                {
                                    os << "--";
                                }
                            }

                            it = item.find("airvpn_server_max_bandwidth");

                            if(it != item.end())
                            {
                                try
                                {
                                    os << " - Max " << AirVPNTools::formatTransferRate(std::stoll(it->second) * AirVPNTools::ONE_DECIMAL_MEGA);
                                }
                                catch(std::exception &e)
                                {
                                    os << "--";
                                }
                            }

                            log(os.str().c_str());
                        }

                        os.str("");

                        it = item.find("server_ip");
                    
                        if(it != item.end())
                            os << "Server IP Address " << it->second;

                        it = item.find("vpn_type");
                    
                        if(it != item.end())
                            os << " - VPN Type " << vpn_type_description(it->second);

                        it = item.find("server_port");
                    
                        if(it != item.end())
                            os << " - Port " << it->second;

                        it = item.find("server_proto");
                    
                        if(it != item.end())
                            os << " - Protocol " << it->second;

                        it = item.find("cipher");
                    
                        if(it != item.end())
                            os << " - Cipher " << it->second;

                        log(os.str().c_str());

                        os.str("");

                        it = item.find("topology");
                    
                        if(it != item.end())
                            os << "Network topology: " << it->second;

                        it = item.find("ping");
                    
                        if(it != item.end())
                            os << " - Server ping " << it->second << " s";

                        it = item.find("ping_restart");
                    
                        if(it != item.end())
                            os << " - Ping restart " << it->second << " s";

                        log(os.str().c_str());

                        if(pushedDns.size() > 0)
                        {
                            os.str("");

                            os << "Pushed DNS:";

                            for(DNSEntry dns : pushedDns)
                                os << " " << dns.address << " (" << dns.type << ")";

                            log(os.str().c_str());
                        }

                        it = item.find("connection_time");
                        
                        if(it != item.end())
                        {
                            os.str("");

                            os << "Connection time: ";

                            try
                            {
                                os << AirVPNTools::formatTime(std::stol(it->second));
                            }
                            catch(std::exception &e)
                            {
                                os << "--";
                            }
                        }

                        log(os.str().c_str());

                        it = item.find("status");
                        
                        if(it != item.end())
                        {
                            if(it->second == "PAUSED")
                                log("Connection is paused");
                        }

                        os.str("");

                        os << "Transferred data: In ";

                        it = item.find("bytes_in");
                        
                        if(it != item.end())
                        {
                            try
                            {
                                os << AirVPNTools::formatDataVolume(std::stoll(it->second), true);
                            }
                            catch(std::exception &e)
                            {
                                os << "--";
                            }
                        }

                        os << ", Out ";

                        it = item.find("bytes_out");
                        
                        if(it != item.end())
                        {
                            try
                            {
                                os << AirVPNTools::formatDataVolume(std::stoll(it->second), true);
                            }
                            catch(std::exception &e)
                            {
                                os << "--";
                            }
                        }

                        log(os.str().c_str());

                        os.str("");

                        os << "Current rate: In ";

                        it = item.find("rate_in");
                        
                        if(it != item.end())
                        {
                            try
                            {
                                os << AirVPNTools::formatTransferRate(std::stoll(it->second), true);
                            }
                            catch(std::exception &e)
                            {
                                os << "--";
                            }
                        }

                        os << ", Out ";

                        it = item.find("rate_out");
                        
                        if(it != item.end())
                        {
                            try
                            {
                                os << AirVPNTools::formatTransferRate(std::stoll(it->second), true);
                            }
                            catch(std::exception &e)
                            {
                                os << "--";
                            }
                        }

                        log(os.str().c_str());

                        os.str("");

                        os << "Maximum rate: In ";

                        it = item.find("max_rate_in");
                        
                        if(it != item.end())
                        {
                            try
                            {
                                os << AirVPNTools::formatTransferRate(std::stoll(it->second), true);
                            }
                            catch(std::exception &e)
                            {
                                os << "--";
                            }
                        }

                        os << ", Out ";

                        it = item.find("max_rate_out");
                        
                        if(it != item.end())
                        {
                            try
                            {
                                os << AirVPNTools::formatTransferRate(std::stoll(it->second), true);
                            }
                            catch(std::exception &e)
                            {
                                os << "--";
                            }
                        }

                        log(os.str().c_str());
                    }
                    else
                    {
                        log("Bluetit is not connected to a VPN");

                        os.str("");

                        os << "Bluetit status: " << it->second;

                        log(os.str().c_str());
                    }
                }
            }
        }
        else
            log(response.c_str());

        dbusConnector->unreferenceResponse(dbusResponse);

        dbusConnector->unreferenceMessage(dbusReply);
    }
    catch(DBusConnectorException &e)
    {
        os.str("");

        os << "ERROR: D-Bus service " << BT_SERVER_BUS_NAME << " is not available. (" << e.what() << ")";

        dbusConnector->unreferenceResponse(dbusResponse);

        dbusConnector->unreferenceMessage(dbusReply);

        cleanup_and_exit(os.str().c_str(), EXIT_FAILURE);
    }
}

void get_pushed_dns()
{
    DBusMessage *dbusReply = nullptr;
    DBusResponse *dbusResponse = nullptr;
    std::vector<std::string> dbusItems;
    DBusResponse::Item item;
    std::ostringstream os;

    if(pushedDns.size() > 0)
        return;
        
    dbusItems.clear();

    try
    {
        dbusReply = dbusConnector->callMethodWithReply(BT_SERVER_BUS_NAME, BT_SERVER_OBJECT_PATH_NAME, BT_METHOD_LIST_PUSHED_DNS, dbusItems);

        if(dbusReply == nullptr)
        {
            os.str("");

            os << "ERROR: D-Bus service " << BT_SERVER_BUS_NAME << " is not available";

            cleanup_and_exit(os.str().c_str() ,EXIT_FAILURE);
        }

        dbusResponse = dbusConnector->getResponse(dbusReply);

        std::string response = dbusResponse->getResponse();

        if(response == BT_OK && dbusResponse->rows() > 0)
        {
            for(int i = 0; i < dbusResponse->rows(); i++)
            {
                DNSEntry dns;

                item = dbusResponse->getItem(i);

                dns.address = dbusResponse->getItemValue(item, "address");
                dns.type = dbusResponse->getItemValue(item, "type");

                pushedDns.push_back(dns);
            }
        }

        dbusConnector->unreferenceResponse(dbusResponse);

        dbusConnector->unreferenceMessage(dbusReply);
    }
    catch(DBusConnectorException &e)
    {
        os.str("");

        os << "ERROR: D-Bus service " << BT_SERVER_BUS_NAME << " is not available. (" << e.what() << ")";

        cleanup_and_exit(os.str().c_str(), EXIT_FAILURE);
    }
}

void list_data_ciphers()
{
    std::ostringstream os;
    DBusMessage *dbusReply = nullptr;
    DBusResponse *dbusResponse = nullptr;
    std::vector<std::string> dbusItems;
    DBusResponse::Item item;
    DBusResponse::ItemIterator it;

    dbusItems.clear();

    try
    {
        dbusReply = dbusConnector->callMethodWithReply(BT_SERVER_BUS_NAME, BT_SERVER_OBJECT_PATH_NAME, BT_METHOD_LIST_DATA_CIPHERS, dbusItems);

        if(dbusReply == nullptr)
        {
            os.str("");

            os << "ERROR: D-Bus service " << BT_SERVER_BUS_NAME << " is not available";

            cleanup_and_exit(os.str().c_str() ,EXIT_FAILURE);
        }

        dbusResponse = dbusConnector->getResponse(dbusReply);

        std::string response = dbusResponse->getResponse();

        if(response == BT_OK)
        {
            if(dbusResponse->rows() > 0)
            {
                log("** Supported data ciphers **");

                for(int i = 0; i < dbusResponse->rows(); i++)
                {
                    item = dbusResponse->getItem(i);
                
                    log(dbusResponse->getItemValue(item, "cipher").c_str());
                }
            }
            else
                log("No data cipher is supported");
        }
        else
            log(response.c_str());

        dbusConnector->unreferenceResponse(dbusResponse);

        dbusConnector->unreferenceMessage(dbusReply);
    }
    catch(DBusConnectorException &e)
    {
        os.str("");

        os << "ERROR: D-Bus service " << BT_SERVER_BUS_NAME << " is not available. (" << e.what() << ")";

        dbusConnector->unreferenceResponse(dbusResponse);

        dbusConnector->unreferenceMessage(dbusReply);

        cleanup_and_exit(os.str().c_str(), EXIT_FAILURE);
    }
}

std::string vpn_type_description(const std::string &vpn_type)
{
    std::string vpn_type_description = "";

    if(vpn_type == VPN_TYPE_OPENVPN)
        vpn_type_description = VPN_TYPE_OPENVPN_NAME;
    else if(vpn_type == VPN_TYPE_WIREGUARD)
        vpn_type_description = VPN_TYPE_WIREGUARD_NAME;
    else
        vpn_type_description = "Unknown VPN type";

    return vpn_type_description;
}

int main(int argc, char **argv)
{
    std::ostringstream os, profile_content;
    std::ifstream openvpn_profile;
    std::string value;
    DBusMessage *dbusReply = nullptr;
    DBusResponse *dbusResponse = nullptr;
    std::vector<std::string> dbusItems;
    char *profile_name;
    int n, bluetit_status;
    bool do_network_recovery = false, show_server_status_only = false, show_server_stats_only = false;
    bool do_disconnect = false, do_pause = false, do_resume = false, do_reconnect = false, do_list_data_ciphers = false;

    airVpnUsername = "";
    airVpnPassword = "";
    airVpnKey.str("");

    extraOptions.clear();

    airvpn_mode = check_airvpn_mode(argc, argv);

    airvpn_connect = check_airvpn_connect(argc, argv);

    airvpn_key_load = check_airvpn_key_load(argc, argv);

    std::cout << GOLDCREST_FULL_NAME << " - " << GOLDCREST_RELEASE_DATE << std::endl << std::endl;

    if(argc < 2)
    {
        usage(argv[0]);

        exit(EXIT_FAILURE);
    }

    if(read_configuration() == false)
        cleanup_and_exit("ERROR: Cannot create or read configuration file", EXIT_FAILURE);

    check_airvpn_credentials(argc, argv);

    if(argc == 2)
    {
        if(strcmp(argv[1], "-" BT_CLIENT_OPTION_HELP_SHORT) == 0 || strcmp(argv[1], "--" BT_CLIENT_OPTION_HELP) == 0)
        {
            usage(argv[0]);

            exit(EXIT_SUCCESS);
        }
        else if(strcmp(argv[1], "-" BT_CLIENT_OPTION_VERSION_SHORT) == 0 || strcmp(argv[1], "--" BT_CLIENT_OPTION_VERSION) == 0)
        {
            aboutDevelopmentCredits();

            exit(EXIT_SUCCESS);
        }
        else if(strcmp(argv[1], "--" BT_CLIENT_OPTION_RECOVER_NETWORK) == 0)
            do_network_recovery = true;
        else if(strcmp(argv[1], "--" BT_CLIENT_OPTION_BLUETIT_STATUS) == 0)
            show_server_status_only = true;
        else if(strcmp(argv[1], "--" BT_CLIENT_OPTION_BLUETIT_STATS) == 0)
            show_server_stats_only = true;
        else if(strcmp(argv[1], "--" BT_CLIENT_OPTION_DISCONNECT) == 0)
            do_disconnect = true;
        else if(strcmp(argv[1], "--" BT_CLIENT_OPTION_PAUSE) == 0)
            do_pause = true;
        else if(strcmp(argv[1], "--" BT_CLIENT_OPTION_RECONNECT) == 0)
            do_reconnect = true;
        else if(strcmp(argv[1], "--" BT_CLIENT_OPTION_RESUME) == 0)
            do_resume = true;
        else if(strcmp(argv[1], "--" BT_CLIENT_OPTION_LIST_DATA_CIPHERS) == 0)
            do_list_data_ciphers = true;
    }

    if(do_network_recovery == false && do_disconnect == false && do_pause == false && do_resume == false && do_reconnect == false && do_list_data_ciphers == false && show_server_status_only == false && show_server_stats_only == false && airvpn_mode == false)
    {
        profile_name = argv[argc - 1];

        if(profile_name[0] == '-') // Last argument is an option. No openvpn profile provided
        {
            usage(argv[0]);

            exit(EXIT_FAILURE);
        }

        if(access(profile_name, F_OK) != 0)
        {
            std::cout << "ERROR: profile " << profile_name << " not found" << std::endl;

            cleanup_and_exit(NULL, EXIT_FAILURE);
        }
    }

    signal(SIGCHLD, SIG_IGN);
    signal(SIGTSTP, SIG_IGN);
    signal(SIGTTOU, SIG_IGN);
    signal(SIGTTIN, SIG_IGN);

    signal(SIGHUP, signal_handler);
    signal(SIGTERM, signal_handler);
    signal(SIGINT, signal_handler);
    signal(SIGPIPE, signal_handler);
    signal(SIGUSR1, signal_handler);
    signal(SIGUSR2, signal_handler);

    try
    {
        dbusConnector = new DBusConnector(BT_DBUS_INTERFACE_NAME, BT_CLIENT_BUS_NAME);

        if(!dbusConnector)
            cleanup_and_exit("Failed to create DBus object", EXIT_FAILURE);
    }
    catch(DBusConnectorException &e)
    {
        os.str("");

        os << "DBusConnectorException: " << e.what();

        cleanup_and_exit(os.str().c_str(), EXIT_FAILURE);
    }

    dbusResponse = send_method_to_server(BT_METHOD_VERSION);

    dbusConnector->unreferenceResponse(dbusResponse);

    dbusResponse = send_method_to_server(BT_METHOD_OPENVPN_INFO);

    dbusConnector->unreferenceResponse(dbusResponse);

    dbusResponse = send_method_to_server(BT_METHOD_OPENVPN_COPYRIGHT);

    dbusConnector->unreferenceResponse(dbusResponse);

    dbusResponse = send_method_to_server(BT_METHOD_SSL_LIBRARY_VERSION);

    dbusConnector->unreferenceResponse(dbusResponse);

    if(do_network_recovery == true)
    {
        dbusResponse = send_method_to_server(BT_METHOD_RECOVER_NETWORK);

        dbusConnector->unreferenceResponse(dbusResponse);

        dbus_message_loop_until(BT_EVENT_END_OF_SESSION);

        cleanup_and_exit(NULL, EXIT_SUCCESS);
    }

    if(do_disconnect == true)
    {
        dbusResponse = send_method_to_server(BT_METHOD_STOP_CONNECTION);

        dbusConnector->unreferenceResponse(dbusResponse);

        dbus_message_loop_until(BT_EVENT_END_OF_SESSION);

        cleanup_and_exit(NULL, EXIT_SUCCESS);
    }

    if(do_pause == true)
    {
        dbusResponse = send_method_to_server(BT_METHOD_SESSION_PAUSE);

        dbusConnector->unreferenceResponse(dbusResponse);

        dbus_message_loop_until(BT_EVENT_END_OF_SESSION);

        cleanup_and_exit(NULL, EXIT_SUCCESS);
    }

    if(do_resume == true)
    {
        dbusResponse = send_method_to_server(BT_METHOD_SESSION_RESUME);

        dbusConnector->unreferenceResponse(dbusResponse);

        dbus_message_loop_until(BT_EVENT_END_OF_SESSION);

        cleanup_and_exit(NULL, EXIT_SUCCESS);
    }

    if(do_reconnect == true)
    {
        dbusResponse = send_method_to_server(BT_METHOD_SESSION_RECONNECT);

        dbusConnector->unreferenceResponse(dbusResponse);

        dbus_message_loop_until(BT_EVENT_END_OF_SESSION);

        cleanup_and_exit(NULL, EXIT_SUCCESS);
    }

    if(do_list_data_ciphers == true)
    {
        list_data_ciphers();

        cleanup_and_exit(NULL, EXIT_SUCCESS);
    }

    bluetit_status = get_server_status();

    if(show_server_status_only == true)
    {
        dbusResponse = send_method_to_server(BT_METHOD_NETWORK_LOCK_STATUS);

        dbusConnector->unreferenceResponse(dbusResponse);

        show_connection_stats();

        cleanup_and_exit(NULL, EXIT_SUCCESS);
    }

    if(show_server_stats_only == true)
    {
        show_connection_stats();

        cleanup_and_exit(NULL, EXIT_SUCCESS);
    }

    if(bluetit_status != BLUETIT_STATUS_READY)
        cleanup_and_exit(NULL, EXIT_FAILURE);

    // set options

    dbusResponse = send_method_to_server(BT_METHOD_RESET_BLUETIT_OPTIONS);

    dbusConnector->unreferenceResponse(dbusResponse);

    if(argc > 2 || airvpn_mode == true)
    {
        int limit = argc;

        if(airvpn_mode == false)
            limit--;

        if(airvpn_mode == true && airvpn_key_load == true)
        {
            dbusItems.clear();

            dbusItems.push_back(airVpnKey.str());

            dbusResponse = send_method_with_args_to_server(BT_METHOD_AIRVPN_SET_KEY, dbusItems);

            value = dbusResponse->getResponse();

            dbusConnector->unreferenceResponse(dbusResponse);

            if(value != BT_OK)
                cleanup_and_exit(value.c_str(), EXIT_FAILURE);
        }

        dbusItems.clear();

        if(extraOptions.empty() == false)
        {
            for(std::string option : extraOptions)
                dbusItems.push_back(option);
        }

        for(n = 1; n < limit; n++)
            dbusItems.push_back(argv[n]);

        try
        {
            dbusReply = dbusConnector->callMethodWithReply(BT_SERVER_BUS_NAME, BT_SERVER_OBJECT_PATH_NAME, BT_METHOD_SET_OPTIONS, dbusItems);

            if(dbusReply == nullptr)
            {
                os.str("");

                os << "ERROR: D-Bus service " << BT_SERVER_BUS_NAME << " is not available";

                cleanup_and_exit(os.str().c_str() ,EXIT_FAILURE);
            }

            dbusResponse = dbusConnector->getResponse(dbusReply);

            std::string response = dbusResponse->getResponse();

            if(response == BT_OK)
            {
                os.str("");

                os << BLUETIT_SHORT_NAME << " successfully set to command line options";

                log(os.str().c_str());
            }
            else if(response == BT_DATASET_AIRVPN_SERVER_INFO)
            {
                airvpn_server_info(dbusResponse);
            }
            else if(response == BT_DATASET_AIRVPN_SERVER_LIST)
            {
                airvpn_server_list(dbusResponse);
            }
            else if(response == BT_DATASET_AIRVPN_COUNTRY_INFO)
            {
                airvpn_country_info(dbusResponse);
            }
            else if(response == BT_DATASET_AIRVPN_COUNTRY_LIST)
            {
                airvpn_country_list(dbusResponse);
            }
            else if(response == BT_DATASET_AIRVPN_KEY_LIST)
            {
                airvpn_key_list(dbusResponse);
            }
            else if(response == BT_DATASET_AIRVPN_SAVE)
            {
                airvpn_save(dbusResponse);
            }
            else
            {
                std::string message = dbusResponse->getResponse();

                dbusConnector->unreferenceResponse(dbusResponse);

                dbusConnector->unreferenceMessage(dbusReply);

                cleanup_and_exit(message.c_str(), EXIT_FAILURE);
            }

            dbusConnector->unreferenceResponse(dbusResponse);

            dbusConnector->unreferenceMessage(dbusReply);
        }
        catch(DBusConnectorException &e)
        {
            os.str("");

            os << "ERROR: D-Bus service " << BT_SERVER_BUS_NAME << " is not available. (" << e.what() << ")";

            dbusConnector->unreferenceResponse(dbusResponse);

            dbusConnector->unreferenceMessage(dbusReply);

            cleanup_and_exit(os.str().c_str(), EXIT_FAILURE);
        }
    }

    if(airvpn_mode == true && airvpn_connect == false)
        cleanup_and_exit("", EXIT_SUCCESS);

    if(airvpn_mode == false)
    {
        openvpn_profile.open(profile_name);

        if(openvpn_profile.good())
        {
            profile_content << openvpn_profile.rdbuf();

            openvpn_profile.close();
        }
        else
        {
            os.str("");

            os << "Error while reading " << profile_name;

            cleanup_and_exit(os.str().c_str(), EXIT_FAILURE);
        }

        dbusItems.clear();

        dbusItems.push_back(profile_content.str());

        dbusResponse = send_method_with_args_to_server(BT_METHOD_SET_OPENVPN_PROFILE, dbusItems);

        value = dbusResponse->getResponse();

        dbusConnector->unreferenceResponse(dbusResponse);

        if(value != BT_OK)
            cleanup_and_exit(NULL, EXIT_FAILURE);

        dbusResponse = send_method_to_server(BT_METHOD_START_CONNECTION);

        value = dbusResponse->getResponse();

        dbusConnector->unreferenceResponse(dbusResponse);

        if(value == BT_OK)
        {
            os.str("");

            os << "Requesting VPN connection to " << BLUETIT_SHORT_NAME;

            log(os.str().c_str());
        }
        else
            cleanup_and_exit(NULL, EXIT_FAILURE);
    }
    else
    {
        if(airvpn_connect == true)
        {
            dbusResponse = send_method_to_server(BT_METHOD_AIRVPN_START_CONNECTION);

            value = dbusResponse->getResponse();

            dbusConnector->unreferenceResponse(dbusResponse);

            if(value == BT_OK)
            {
                os.str("");

                os << "Requesting AirVPN connection to " << BLUETIT_SHORT_NAME;

                log(os.str().c_str());
            }
            else
                cleanup_and_exit(NULL, EXIT_FAILURE);
        }
    }

    if(conn_stat_interval > 0)
    {
        connectionStatsFutureObject = connectionStatsSignalExit.get_future();

        connectionStatsThread = new std::thread(connection_stats, conn_stat_interval, std::move(connectionStatsFutureObject));
    }

    dbus_message_loop_until(BT_EVENT_END_OF_SESSION);

    cleanup_and_exit("", EXIT_SUCCESS);
}
