/*
 * airvpnserverprovider.cpp
 *
 * This file is part of the AirVPN Suite for Linux and macOS.
 * Copyright (C) 2019-2022 AirVPN (support@airvpn.org) / https://airvpn.org
 *
 * Developed by ProMIND
 *
 * This is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with the AirVPN Suite. If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <fstream>
#include <algorithm>
#include "include/airvpnserverprovider.hpp"
#include "include/airvpntools.hpp"

extern void global_log(const std::string &s);

#if defined(MAKING_BLUETIT)

extern void global_syslog(const std::string &s);

#endif

AirVPNServerProvider::AirVPNServerProvider(AirVPNUser *user, const std::string &resourceDirectory)
{
    airVPNManifest = new AirVPNManifest(resourceDirectory);

    loadConnectionPriorities(resourceDirectory);

    airVPNUser = user;

    userIP = "";
    userCountry = "";
    tlsMode = TLSMode::NOT_SET;
    IPv4Available = true;
    IPv6Available = true;

    serverWhitelist.clear();
    serverBlacklist.clear();
    countryWhitelist.clear();
    countryBlacklist.clear();

    countryContinent = new CountryContinent(resourceDirectory);
}

AirVPNServerProvider::~AirVPNServerProvider()
{
    if(airVPNManifest != nullptr)
        delete airVPNManifest;

    if(countryContinent != nullptr)
        delete countryContinent;
}

bool AirVPNServerProvider::compareServerScore(AirVPNServer s1, AirVPNServer s2)
{
    return (s1.getScore() < s2.getScore());
}

void AirVPNServerProvider::loadConnectionPriorities(const std::string &resourceDirectory)
{
    std::string line, fileName;
    std::vector<std::string> item;

    fileName = resourceDirectory;
    fileName += "/";
    fileName += AIRVPN_CONNECTION_PRIORITY_FILE_NAME;

    std::ifstream connectionPriorityFile(fileName);

    if(connectionPriorityFile.is_open())
    {
        while(std::getline(connectionPriorityFile, line))
        {
            item = AirVPNTools::split(line, "->");

            if(item.size() == 2)
                connectionPriority.insert(std::make_pair(AirVPNTools::toUpper(AirVPNTools::trim(item[0])), AirVPNTools::toUpper(AirVPNTools::trim(item[1]))));
        }

        connectionPriorityFile.close();
    }
    else
    {
#if defined(MAKING_BLUETIT)
        
        global_syslog("ERROR: AirVPNServerProvider::loadConnectionPriorities() - cannot open " + fileName);
        
#endif
    }
}

std::string AirVPNServerProvider::getUserIP()
{
    return userIP;
}

void AirVPNServerProvider::setUserIP(const std::string &u)
{
    userIP = u;
}

std::string AirVPNServerProvider::getUserCountry()
{
    return userCountry;
}

void AirVPNServerProvider::setUserCountry(const std::string &c)
{
    userCountry = AirVPNTools::toUpper(c);

    userContinent = countryContinent->getCountryContinent(userCountry);
}

AirVPNServerProvider::TLSMode AirVPNServerProvider::getTlsMode()
{
    return tlsMode;
}

void AirVPNServerProvider::setTlsMode(AirVPNServerProvider::TLSMode m)
{
    tlsMode = m;
}

bool AirVPNServerProvider::isIPv4Available()
{
    return IPv4Available;
}

void AirVPNServerProvider::setIPv4Available(bool s)
{
    IPv4Available = s;
}

bool AirVPNServerProvider::isIPv6Available()
{
    return IPv6Available;
}

void AirVPNServerProvider::setIPv6Available(bool s)
{
    IPv6Available = s;
}

std::vector<std::string> AirVPNServerProvider::getServerWhitelist()
{
    return serverWhitelist;
}

void AirVPNServerProvider::setServerWhitelist(const std::vector<std::string> &list)
{
    serverWhitelist = list;
}

std::vector<std::string> AirVPNServerProvider::getServerBlacklist()
{
    return serverBlacklist;
}

void AirVPNServerProvider::setServerBlacklist(const std::vector<std::string> &list)
{
    serverBlacklist = list;
}

std::vector<std::string> AirVPNServerProvider::getCountryWhitelist()
{
    return countryWhitelist;
}

void AirVPNServerProvider::setCountryWhitelist(const std::vector<std::string> &list)
{
    countryWhitelist = list;
}

std::vector<std::string> AirVPNServerProvider::getCountryBlacklist()
{
    return countryBlacklist;
}

void AirVPNServerProvider::setCountryBlacklist(const std::vector<std::string> &list)
{
    countryBlacklist = list;
}

std::vector<AirVPNServer> AirVPNServerProvider::getFilteredServerList(bool forbidLocalServerConnection)
{
    bool include = false, ipEntryFound = false;
    std::vector<AirVPNServer> serverList, priorityServerList, filteredServerList;
    std::vector<std::string> userConnectionPriority;
    std::vector<int> cipherList;
    std::map<int, std::string> ipEntry;
    int validItems = 0, validItemsForInclusion = 1;

    if(airVPNManifest == nullptr)
    {
        global_log("AirVPNServerProdiver::getFilteredServerList(): airVPNManifest is null");

        return filteredServerList;
    }

    cipherList = CipherDatabase::getMatchingCiphersArrayList("CIPHER", CipherDatabase::CipherType::DATA);

    if(serverWhitelist.size() == 0 && countryWhitelist.size() == 0)
        serverList = airVPNManifest->getAirVpnServerList();
    else
    {
        serverList.clear();

        if(serverWhitelist.size() > 0)
        {
            for(std::string name : serverWhitelist)
            {
                AirVPNServer airVPNServer = airVPNManifest->getServerByName(name);

                if(airVPNServer.getName() != "" && airVPNServer.isAvailable(cipherList))
                    serverList.push_back(airVPNServer);
            }
        }

        if(countryWhitelist.size() > 0)
        {
            for(AirVPNServer server : airVPNManifest->getAirVpnServerList())
            {
                if(std::find(countryWhitelist.begin(), countryWhitelist.end(), AirVPNTools::toUpper(server.getCountryCode())) != countryWhitelist.end() &&
                    server.isAvailable(cipherList) &&
                    std::find(serverWhitelist.begin(), serverWhitelist.end(), AirVPNTools::toLower(server.getName())) == serverWhitelist.end())
                    serverList.push_back(server);
            }
        }
    }

    userConnectionPriority = getUserConnectionPriority();

    if(forbidLocalServerConnection == false)
        userConnectionPriority.insert(userConnectionPriority.begin(), AirVPNTools::toUpper(userCountry));

    priorityServerList.clear();

    if(serverList.size() > 0)
    {
        for(int srv = 0; srv < serverList.size(); srv++)
        {
            std::string area = "";
            int penalty = 0, i = 0;

            include = false;

            AirVPNServer airVPNServer = serverList[srv];

            if(userConnectionPriority.size() > 0 && serverWhitelist.size() == 0 && countryWhitelist.size() == 0)
            {
                for(i = 0; i < userConnectionPriority.size() && include == false; i++)
                {
                    area = userConnectionPriority[i];

                    if(area.length() == 2)
                    {
                        if(AirVPNTools::iequals(area, airVPNServer.getCountryCode()))
                        {
                            include = true;

                            penalty = i;
                        }
                    }
                    else if(area.length() == 3)
                    {
                        if(AirVPNTools::iequals(area, airVPNServer.getContinent()))
                        {
                            include = true;

                            penalty = i;
                        }
                    }
                    else
                    {
                        for(std::string place : AirVPNTools::split(airVPNServer.getLocation(), ","))
                        {
                            if(AirVPNTools::iequals(area, place))
                            {
                                include = true;

                                penalty = i;
                            }
                        }
                    }
                }

                if(include == false)
                {
                    include = true;

                    penalty = i + 1;
                }
            }
            else
            {
                include = true;
                
                penalty = 0;
            }

            if(AirVPNTools::iequals(userCountry, airVPNServer.getCountryCode()) && forbidLocalServerConnection)
                include = false;

            if(!airVPNServer.isAvailable(cipherList))
                include = false;

            if(std::find(serverBlacklist.begin(), serverBlacklist.end(), AirVPNTools::toLower(airVPNServer.getName())) != serverBlacklist.end())
                include = false;

            if(std::find(countryBlacklist.begin(), countryBlacklist.end(), AirVPNTools::toUpper(airVPNServer.getCountryCode())) != countryBlacklist.end())
                include = false;

            if(include)
            {
                airVPNServer.setScore(airVPNServer.getScore() + (10000 * penalty));

                priorityServerList.push_back(airVPNServer);
            }
        }
    }

    if(priorityServerList.size() == 0)
        return priorityServerList;

    filteredServerList.clear();

    if(IPv4Available)
        validItemsForInclusion++;

    if(IPv6Available)
        validItemsForInclusion++;

    for(AirVPNServer server : priorityServerList)
    {
        validItems = 0;

        if(IPv4Available && server.isIPv4Available() == true)
            validItems++;

        if(IPv6Available && server.isIPv6Available() == true)
            validItems++;

        ipEntry = server.getEntryIPv4();
        ipEntryFound = false;

        switch(tlsMode)
        {
            case TLS_AUTH:
            {
                if(!ipEntry[1].empty())
                    ipEntryFound = true;

                if(!ipEntry[2].empty())
                    ipEntryFound = true;

                if(ipEntryFound)
                    validItems++;
            }
            break;

            case TLS_CRYPT:
            {
                if(!ipEntry[3].empty())
                    ipEntryFound = true;

                if(!ipEntry[4].empty())
                    ipEntryFound = true;

                if (ipEntryFound)
                    validItems++;
            }
            break;
            
            default:    break;
        }

        if(validItems == validItemsForInclusion)
            filteredServerList.push_back(server);
    }

    std::sort(filteredServerList.begin(), filteredServerList.end(), compareServerScore);

    return filteredServerList;
}

std::vector<std::string> AirVPNServerProvider::getUserConnectionPriority()
{
    std::vector<std::string> areaList;
    std::string areaPriority;
    std::map<std::string, std::string>::iterator it;
    int i;

    areaList.clear();

    if(AirVPNTools::toUpper(userCountry) != "RU")
    {
        it = connectionPriority.find(AirVPNTools::toUpper(userCountry));

        if(it != connectionPriority.end())
            areaPriority = it->second;
        else
            areaPriority = "";
    }
    else
    {
        std::string ruArea = "";

        if(airVPNUser->getUserLongitude() < 103.851959)
            ruArea = "R1";
        else
            ruArea = "R2";

        it = connectionPriority.find(ruArea);

        if(it != connectionPriority.end())
            areaPriority = it->second;
        else
            areaPriority = "";
    }

    if(!areaPriority.empty())
    {
        std::vector<std::string> priority = AirVPNTools::split(areaPriority, ",");

        if(priority.size() > 0)
        {
            for(i = 0; i < priority.size(); i++)
                areaList.push_back(priority[i]);
        }
    }

    it = connectionPriority.find(userContinent);
    
    if(it != connectionPriority.end())
        areaPriority = it->second;
    else
        areaPriority = "";

    if(!areaPriority.empty())
    {
        std::vector<std::string> priority = AirVPNTools::split(areaPriority, ",");

        if(priority.size() > 0)
        {
            for(i = 0; i < priority.size(); i++)
                areaList.push_back(priority[i]);
        }
    }

    if(areaList.empty())
    {
        it = connectionPriority.find("DEFAULT");
        
        if(it != connectionPriority.end())
            areaPriority = it->second;
        else
            areaPriority = "";

        if(!areaPriority.empty())
        {
            std::vector<std::string> priority = AirVPNTools::split(areaPriority, ",");

            if(priority.size() > 0)
            {
                for(i = 0; i < priority.size(); i++)
                    areaList.push_back(priority[i]);
            }
        }
    }

    return areaList;
}
