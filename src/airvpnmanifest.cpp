/*
 * airvpnmanifest.cpp
 *
 * This file is part of the AirVPN Suite for Linux and macOS.
 * Copyright (C) 2019-2022 AirVPN (support@airvpn.org) / https://airvpn.org
 *
 * Developed by ProMIND
 *
 * This is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with the AirVPN Suite. If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <fstream>
#include <unistd.h>
#include "include/airvpnmanifest.hpp"

std::string AirVPNManifest::rsaPublicKeyModulus = "";
std::string AirVPNManifest::rsaPublicKeyExponent = "";
AirVPNManifest::Type AirVPNManifest::manifestDocumentType = AirVPNManifest::Type::NOT_SET;
double AirVPNManifest::speedFactor = 1.0;
double AirVPNManifest::loadFactor = 1.0;
double AirVPNManifest::userFactor = 1.0;
std::string AirVPNManifest::checkDnsQuery = "";
std::string AirVPNManifest::openVpnDirectives = "";
std::vector<std::string> AirVPNManifest::bootStrapServerUrl = {};
std::map<std::string, AirVPNManifest::CountryStats> AirVPNManifest::countryStats = {};
std::vector<AirVPNServer> AirVPNManifest::airVpnServer = {};

AirVPNManifest::AirVPNManifest(const std::string &resDir)
{
    resrouceDirectory = resDir;

    airVpnTools = new AirVPNTools();

#if defined(__linux__)
    systemDescription = "linux";
#elif __APPLE__
    systemDescription = "osx";
#endif
}

AirVPNManifest::~AirVPNManifest()
{
    if(airVpnTools != nullptr)
        delete airVpnTools;

    xmlCleanupParser();
}

void AirVPNManifest::resetBootServers()
{
    bootStrapServerUrl.clear();

    airVpnTools->resetBootServers();
}

void AirVPNManifest::addBootServer(const std::string &server)
{
    bootStrapServerUrl.push_back(server);

    airVpnTools->addBootServer(server);
}

void AirVPNManifest::setRsaPublicKeyModulus(const std::string &modulus)
{
    rsaPublicKeyModulus = modulus;

    airVpnTools->setRSAModulus(modulus);
}

std::string AirVPNManifest::getRsaPublicKeyModulus()
{
    return rsaPublicKeyModulus;
}

void AirVPNManifest::setRsaPublicKeyExponent(const std::string &exponent)
{
    rsaPublicKeyExponent = exponent;

    airVpnTools->setRSAExponent(exponent);
}

std::string AirVPNManifest::getRsaPublicKeyExponent()
{
    return rsaPublicKeyExponent;
}

std::string AirVPNManifest::getManifest()
{
    return manifestDocument;
}

AirVPNManifest::Type AirVPNManifest::getManifestType()
{
    return manifestDocumentType;
}

std::string AirVPNManifest::getErrorDescription()
{
    return errorDescription;
}

void AirVPNManifest::initializeManifestData()
{
    processTimeTS = std::time(nullptr);
    manifestTimeTS = 0;
    manifestNextUpdateTS = 0;
    nextUpdateIntervalMinutes = 0;
    dnsCheckHost = "";
    dnsCheckRes1 = "";
    dnsCheckRes2 = "";
    speedFactor = 1.0;
    loadFactor = 1.0;
    userFactor = 1.0;
    latencyFactor = 0;
    penalityFactor = 0;
    pingerDelay = 0;
    pingerRetry = 0;
    checkDomain = "";
    checkDnsQuery = "";
    openVpnDirectives = "";
    modeProtocol = "";
    modePort = 0;
    modeAlt = 0;
    airVpnServer.clear();
    rsaPublicKeyModulus = "";
    rsaPublicKeyExponent = "";
}

std::vector<AirVPNManifest::Message> AirVPNManifest::getMessages()
{
    return manifestMessage;
}

void AirVPNManifest::setMessageShown(int m)
{
    if(m >= manifestMessage.size())
        return;

    manifestMessage[m].shown = true;
}

long AirVPNManifest::getManifestTimeTS()
{
    return manifestTimeTS;
}

void AirVPNManifest::setManifestTimeTS(long t)
{
    manifestTimeTS = t;
}

long AirVPNManifest::getManifestNextUpdateTS()
{
    return manifestNextUpdateTS;
}

void AirVPNManifest::setManifestNextUpdateTS(long t)
{
    manifestNextUpdateTS = t;
}

int AirVPNManifest::getNextUpdateIntervalMinutes()
{
    return nextUpdateIntervalMinutes;
}

void AirVPNManifest::setNextUpdateIntervalMinutes(int n)
{
    nextUpdateIntervalMinutes = n;
}

std::string AirVPNManifest::getDnsCheckHost()
{
    return dnsCheckHost;
}

void AirVPNManifest::setDnsCheckHost(const std::string &d)
{
    dnsCheckHost = d;
}

std::string AirVPNManifest::getDnsCheckRes1()
{
    return dnsCheckRes1;
}

void AirVPNManifest::setDnsCheckRes1(const std::string &d)
{
    dnsCheckRes1 = d;
}

std::string AirVPNManifest::getDnsCheckRes2()
{
    return dnsCheckRes2;
}

void AirVPNManifest::setDnsCheckRes2(const std::string &d)
{
    dnsCheckRes2 = d;
}

double AirVPNManifest::getSpeedFactor()
{
    return speedFactor;
}

void AirVPNManifest::setSpeedFactor(double s)
{
    speedFactor = s;
}

double AirVPNManifest::getLoadFactor()
{
    return loadFactor;
}

void AirVPNManifest::setLoadFactor(double l)
{
    loadFactor = l;
}

double AirVPNManifest::getUserFactor()
{
    return userFactor;
}

void AirVPNManifest::setUserFactor(double u)
{
    userFactor = u;
}

int AirVPNManifest::getLatencyFactor()
{
    return latencyFactor;
}

void AirVPNManifest::setLatencyFactor(int l)
{
    latencyFactor = l;
}

int AirVPNManifest::getPenalityFactor()
{
    return penalityFactor;
}

void AirVPNManifest::setPenalityFactor(int p)
{
    penalityFactor = p;
}

int AirVPNManifest::getPingerDelay()
{
    return pingerDelay;
}

void AirVPNManifest::setPingerDelay(int p)
{
    pingerDelay = p;
}

int AirVPNManifest::getPingerRetry()
{
    return pingerRetry;
}

void AirVPNManifest::setPingerRetry(int p)
{
    pingerRetry = p;
}

std::string AirVPNManifest::getCheckDomain()
{
    return checkDomain;
}

void AirVPNManifest::setCheckDomain(const std::string &c)
{
    checkDomain = c;
}

std::string AirVPNManifest::getCheckDnsQuery()
{
    return checkDnsQuery;
}

void AirVPNManifest::setCheckDnsQuery(const std::string &c)
{
    checkDnsQuery = c;
}

std::string AirVPNManifest::getOpenVpnDirectives()
{
    return openVpnDirectives;
}

void AirVPNManifest::setOpenVpnDirectives(const std::string &o)
{
    openVpnDirectives = o;
}

std::string AirVPNManifest::getServerOpenVpnDirectives(const std::string &serverIP)
{
    std::string ovpnDirective = "";
    std::vector<std::string> excludedDirective = { "cipher", "comp-lzo" }, directiveItem;
    bool excludeItem = false;

    AirVPNServer server = getServerByIP(serverIP);

    ovpnDirective = openVpnDirectives;
    
    if(server.getName().empty() == false)
        ovpnDirective = server.getOpenVpnDirectives();

    directiveItem = AirVPNTools::split(ovpnDirective, "\n");

    ovpnDirective = "";

    for(std::string item : directiveItem)
    {
        excludeItem = false;

        for(std::string exclude : excludedDirective)
        {
            if(AirVPNTools::toLower(item).find(exclude) !=std::string::npos)
                excludeItem = true;
        }
        
        if(excludeItem == false)
            ovpnDirective += item + "\n";
    }

    return ovpnDirective;
}

std::string AirVPNManifest::getModeProtocol()
{
    return modeProtocol;
}

void AirVPNManifest::setModeProtocol(const std::string &m)
{
    modeProtocol = m;
}

int AirVPNManifest::getModePort()
{
    return modePort;
}

void AirVPNManifest::setModePort(int m)
{
    modePort = m;
}

int AirVPNManifest::getModeAlt()
{
    return modeAlt;
}

void AirVPNManifest::setModeAlt(int m)
{
    modeAlt = m;
}

std::vector<std::string> AirVPNManifest::getBootStrapServerUrlList()
{
    return bootStrapServerUrl;
}

void AirVPNManifest::setBootStrapServerUrlList(const std::vector<std::string> &b)
{
    bootStrapServerUrl = b;
}

void AirVPNManifest::addBootStrapServerUrl(const std::string &url)
{
    bootStrapServerUrl.push_back(url);
}

std::vector<AirVPNServer> AirVPNManifest::getAirVpnServerList()
{
    return airVpnServer;
}

void AirVPNManifest::addAirVpnServer(AirVPNServer server)
{
    airVpnServer.push_back(server);
}

AirVPNManifest::CountryStats AirVPNManifest::getCountryStats(const std::string &code)
{
    CountryStats c;

    std::map<std::string, CountryStats>::iterator it = countryStats.find(AirVPNTools::toUpper(code));

    if(it != countryStats.end())
        c = it->second;
    else
    {
        c.countryISOCode = "";
        c.servers = 0;
        c.users = 0;
        c.bandWidth = 0;
        c.maxBandWidth = 0;
    }

    return c;
}

std::vector<AirVPNServer> AirVPNManifest::getServerListByCountry(std::string code, bool availableOnly)
{
    bool includeContinent = false;

    std::vector<AirVPNServer> serverList;

    if(CountryContinent::isContinent(code) == false)
    {
        if(code.length() > 2)
            code = CountryContinent::getCountryCode(code);

        for(int i = 0; i < airVpnServer.size(); i++)
        {
            AirVPNServer server = airVpnServer.at(i);

            if(AirVPNTools::iequals(server.getCountryCode(), code))
            {
                if(availableOnly == false || (availableOnly == true && server.isAvailable() == true))
                    serverList.push_back(server);
            }
        }
    }
    else
    {
        if(code.length() > 3)
            code = CountryContinent::getContinentCode(code);
        else
            code = AirVPNTools::toUpper(code);

        for(int i = 0; i < airVpnServer.size(); i++)
        {
            includeContinent = false;

            AirVPNServer server = airVpnServer.at(i);

            if(code == "EARTH")
                includeContinent = true;
            else if(code == "AME" && (AirVPNTools::iequals(server.getContinent(), "SAM") || AirVPNTools::iequals(server.getContinent(), "NAM")))
                includeContinent = true;
            else if(AirVPNTools::iequals(server.getContinent(), code))
                includeContinent = true;
            
            if(includeContinent == true && (availableOnly == false || (availableOnly == true && server.isAvailable() == true)))
                serverList.push_back(server);
        }
    }

    return serverList;
}

AirVPNServer AirVPNManifest::getServerByName(std::string name)
{
    if(name == "")
        return AirVPNServer("");

    name = AirVPNTools::toLower(name);

    for(int i = 0; i < airVpnServer.size(); i++)
    {
        if(AirVPNTools::toLower(airVpnServer[i].getName()) == name)
            return airVpnServer[i];
    }

    return AirVPNServer("");
}

AirVPNServer AirVPNManifest::getServerByIP(std::string ipAddress)
{
    if(ipAddress == "")
        return AirVPNServer("");

    ipAddress = AirVPNTools::toLower(ipAddress);

    for(int i = 0; i < airVpnServer.size(); i++)
    {
        for(std::pair<int, std::string> entry : airVpnServer[i].getEntryIPv4())
        {
            if(ipAddress == entry.second)
                return airVpnServer[i];
        }

        for(std::pair<int, std::string> entry : airVpnServer[i].getEntryIPv6())
        {
            if(ipAddress == entry.second)
                return airVpnServer[i];
        }
    }

    return AirVPNServer("");
}

std::string AirVPNManifest::getTlsCipherDescription(int code)
{
    return cipherDatabase.getCipher(code, CipherDatabase::CipherType::TLS);
}

std::string AirVPNManifest::getTlsSuiteCipherDescription(int code)
{
    return cipherDatabase.getCipher(code, CipherDatabase::CipherType::TLS_SUITE);
}

std::string AirVPNManifest::getDataCipherDescription(int code)
{
    return cipherDatabase.getCipher(code, CipherDatabase::CipherType::DATA);
}

std::string AirVPNManifest::getCipherDescription(int code, CipherDatabase::CipherType type)
{
    return cipherDatabase.getCipher(code, type);
}

std::vector<std::string> AirVPNManifest::searchServer(std::string pattern)
{
    std::vector<std::string> item;
    bool found = false;

    pattern = AirVPNTools::toUpper(pattern);

    item.clear();

    for(AirVPNServer server : airVpnServer)
    {
        found = false;

        if(pattern != "ALL")
        {
            if(AirVPNTools::toUpper(server.getName()).find(pattern) != std::string::npos)
                found = true;

            if(AirVPNTools::toUpper(server.getCountryCode()).find(pattern) != std::string::npos)
                found = true;

            if(AirVPNTools::toUpper(server.getCountryName()).find(pattern) != std::string::npos)
                found = true;
        }
        else
            found = true;

        if(found == true)
            item.push_back(server.getName());
    }

    return item;
}

AirVPNManifest::Error AirVPNManifest::loadManifest(bool forceLocal)
{
    std::string fileName, line;
    std::ofstream manifestFile;
    std::ifstream localManifestFile;
    AirVPNTools::AirVPNServerError res;

    fileName = resrouceDirectory;
    fileName += "/";
    fileName += manifestFileName;

    if(forceLocal == false)
    {
        airVpnTools->resetAirVPNDocumentRequest();
        airVpnTools->setAirVPNDocumentParameter("system", systemDescription);
        airVpnTools->setAirVPNDocumentParameter("act", "manifest");

        res = airVpnTools->requestAirVPNDocument();
    }
    else
        res = AirVPNTools::AirVPNServerError::EMPTY_DOCUMENT;

    if(res == AirVPNTools::AirVPNServerError::OK)
    {
        manifestDocument = airVpnTools->getRequestedDocument();
        
        manifestDocumentType = FROM_SERVER;
    }
    else
    {
        if(access(fileName.c_str(), F_OK) == 0)
        {
            localManifestFile.open(fileName);

            if(localManifestFile.is_open())
            {
                manifestDocument = "";

                while(std::getline(localManifestFile, line))
                {
                    manifestDocument += line;
                    manifestDocument += "\n";
                }

                localManifestFile.close();

                manifestDocumentType = STORED;

                manifestError = OK;
                errorDescription = "OK";
            }
            else
            {
                manifestError = CANNOT_RETRIEVE_MANIFEST;
                errorDescription = "ERROR: AirVPNManifest->loadManifest() failed: Cannot open local copy of manifest";

                manifestDocument = "";
                manifestDocumentType = NOT_SET;
            }
        }
        else
        {
            manifestError = CANNOT_RETRIEVE_MANIFEST;
            errorDescription = "ERROR: AirVPNManifest->loadManifest() failed: " + airVpnTools->getDocumentRequestErrorDescription();

            manifestDocument = "";
            manifestDocumentType = NOT_SET;
        }
    }
    
    AirVPNManifest::Error error = processXmlManifest();

    if(error == OK)
    {
        manifestError = OK;
        errorDescription = "OK";

        manifestFile.open(fileName);

        if(manifestFile.is_open())
        {
            manifestFile << manifestDocument;

            manifestFile.close();
        }
    }
    else
    {
        manifestError = PARSE_ERROR;
        errorDescription = "ERROR: AirVPNManifest->loadManifest() ";

        if(error == CANNOT_RETRIEVE_MANIFEST)
            errorDescription += "Cannot retrieve manifest from server.";
        else if(error == EMPTY_MANIFEST)
            errorDescription += "Server returned an empty manifest.";
        else if(error == NULL_XML_DOCUMENT)
            errorDescription += "Server returned an unknown manifest.";
        else if(error == PARSE_ERROR)
            errorDescription += "Failed to parse. Malformed manifest.";
        else
            errorDescription += "Failed to parse. Unknown error.";

        manifestDocument = "";
        manifestDocumentType = NOT_SET;
    }

    return manifestError;
}

AirVPNManifest::Error AirVPNManifest::processXmlManifest()
{
    std::string value;
    std::vector<std::string> row;
    int i, j, group;
    xmlNode *node = nullptr;
    std::map<std::string, CountryStats>::iterator it;

    if(manifestDocument.empty())
        return EMPTY_MANIFEST;

    cleanupXmlParser();

    airVpnManifestDocument = xmlReadMemory(manifestDocument.c_str(), manifestDocument.size(), "noname.xml", NULL, 0);

    if(airVpnManifestDocument == nullptr)
        return NULL_XML_DOCUMENT;

    rootNode = xmlDocGetRootElement(airVpnManifestDocument);

    initializeManifestData();

    processTimeTS = std::time(nullptr);

    countryStats.clear();

    // manifest attributes

    node = AirVPNTools::findXmlNode((xmlChar *)"manifest", rootNode);

    if(node != NULL)
    {
        try
        {
            manifestTimeTS = std::stol(AirVPNTools::getXmlAttribute(node, "time"));
        }
        catch(std::exception &e)
        {
            manifestTimeTS = 0;
        }

        try
        {
            manifestNextUpdateTS = std::stol(AirVPNTools::getXmlAttribute(node, "next"));
        }
        catch(std::exception &e)
        {
            manifestNextUpdateTS = 0;
        }

        try
        {
            nextUpdateIntervalMinutes = std::stol(AirVPNTools::getXmlAttribute(node, "next_update"));
        }
        catch(std::exception &e)
        {
            nextUpdateIntervalMinutes = 0;
        }

        dnsCheckHost = AirVPNTools::getXmlAttribute(node, "dnscheck_host");
        dnsCheckRes1 = AirVPNTools::getXmlAttribute(node, "dnscheck_res1");
        dnsCheckRes2 = AirVPNTools::getXmlAttribute(node, "dnscheck_res2");

        try
        {
            speedFactor = std::stod(AirVPNTools::getXmlAttribute(node, "speed_factor", "1.0"));
        }
        catch(std::exception &e)
        {
            speedFactor = 1.0;
        }

        try
        {
            loadFactor = std::stod(AirVPNTools::getXmlAttribute(node, "load_factor", "1.0"));
        }
        catch(std::exception &e)
        {
            loadFactor = 1.0;
        }

        try
        {
            userFactor = std::stod(AirVPNTools::getXmlAttribute(node, "users_factor", "1.0"));
        }
        catch(std::exception &e)
        {
            userFactor = 1.0;
        }

        try
        {
            latencyFactor = std::stoi(AirVPNTools::getXmlAttribute(node, "latency_factor", "0"));
        }
        catch(std::exception &e)
        {
            latencyFactor = 0;
        }

        try
        {
            penalityFactor = std::stoi(AirVPNTools::getXmlAttribute(node, "penality_factor", "0"));
        }
        catch(std::exception &e)
        {
            penalityFactor = 0;
        }

        try
        {
            pingerDelay = std::stoi(AirVPNTools::getXmlAttribute(node, "pinger_delay", "0"));
        }
        catch(std::exception &e)
        {
            pingerDelay = 0;
        }

        try
        {
            pingerRetry = std::stoi(AirVPNTools::getXmlAttribute(node, "pinger_retry", "0"));
        }
        catch(std::exception &e)
        {
            pingerRetry = 0;
        }

        checkDomain = AirVPNTools::getXmlAttribute(node, "check_domain");
        checkDnsQuery = AirVPNTools::getXmlAttribute(node, "check_dns_query");

        openVpnDirectives = AirVPNTools::convertXmlEntities(AirVPNTools::getXmlAttribute(node, "openvpn_directives"));
        modeProtocol = AirVPNTools::getXmlAttribute(node, "mode_protocol");

        try
        {
            modePort = std::stoi(AirVPNTools::getXmlAttribute(node, "mode_port", "0"));
        }
        catch(std::exception &e)
        {
            modePort = 0;
        }

        try
        {
            modeAlt = std::stoi(AirVPNTools::getXmlAttribute(node, "mode_alt", "0"));
        }
        catch(std::exception &e)
        {
            modeAlt = 0;
        }
    }

    node = rootNode;
    manifestMessage.clear();

    while(node != NULL)
    {
        node = AirVPNTools::findXmlNode((xmlChar *)"message", node);

        if(node != NULL)
        {
            Message message;

            message.text = AirVPNTools::getXmlAttribute(node, "text");
            message.url = AirVPNTools::getXmlAttribute(node, "url");
            message.link = AirVPNTools::getXmlAttribute(node, "link");
            message.html = AirVPNTools::getXmlAttribute(node, "html");
            
            try
            {
                message.fromUTCTimeTS = std::stol(AirVPNTools::getXmlAttribute(node, "from_time", "0"));
            }
            catch(std::exception &e)
            {
                message.fromUTCTimeTS = 0;
            }

            try
            {
                message.toUTCTimeTS = std::stol(AirVPNTools::getXmlAttribute(node, "to_time", "0"));
            }
            catch(std::exception &e)
            {
                message.toUTCTimeTS = 0;
            }

            message.shown = false;

            manifestMessage.push_back(message);

            node = node->next;
        }
    }

    if(AirVPNTools::findXmlNode((xmlChar *)"urls", rootNode) != NULL)
    {
        node = rootNode;

        while(node != NULL)
        {
            node = AirVPNTools::findXmlNode((xmlChar *)"url", node);

            if(node != NULL)
            {
                std::string url;

                url = AirVPNTools::getXmlAttribute(node, "address");

                if(url.empty() == false)
                {
                    if(std::find(bootStrapServerUrl.begin(), bootStrapServerUrl.end(), url) == bootStrapServerUrl.end())
                        addBootStrapServerUrl(url);
                }

                node = node->next;
            }
        }
    }

    node = AirVPNTools::findXmlNode((xmlChar *)"Exponent", rootNode);

    if(node != NULL)
        setRsaPublicKeyExponent((char *)xmlNodeGetContent(node));

    node = AirVPNTools::findXmlNode((xmlChar *)"Modulus", rootNode);

    if(node != NULL)
        setRsaPublicKeyModulus((char *)xmlNodeGetContent(node));

    airVPNServerGroup.reset();
    cipherDatabase.reset();

    if(AirVPNTools::findXmlNode((xmlChar *)"servers_groups", rootNode) != NULL)
    {
        node = rootNode;

        while(node != NULL)
        {
            node = AirVPNTools::findXmlNode((xmlChar *)"servers_group", node);

            if(node != NULL)
            {
                AirVPNServerGroup::ServerGroup serverGroup;

                try
                {
                    serverGroup.group = std::stoi(AirVPNTools::getXmlAttribute(node, "group", "-1"));
                }
                catch(std::exception &e)
                {
                    serverGroup.group = -1;
                }

                if(group != -1)
                {
                    serverGroup.ipv4Support = (AirVPNTools::getXmlAttribute(node, "support_ipv4") == "true") ? true : false;
                    serverGroup.ipv6Support = (AirVPNTools::getXmlAttribute(node, "support_ipv6") == "true") ? true : false;
                    serverGroup.checkSupport = (AirVPNTools::getXmlAttribute(node, "support_check") == "true") ? true : false;

                    serverGroup.tlsCiphers = cipherDatabase.getCodeArrayList(AirVPNTools::getXmlAttribute(node, "ciphers_tls"), CipherDatabase::CipherType::TLS);

                    serverGroup.tlsSuiteCiphers = cipherDatabase.getCodeArrayList(AirVPNTools::getXmlAttribute(node, "ciphers_tlssuites"), CipherDatabase::CipherType::TLS_SUITE);

                    serverGroup.dataCiphers = cipherDatabase.getCodeArrayList(AirVPNTools::getXmlAttribute(node, "ciphers_data"), CipherDatabase::CipherType::DATA);

                    airVPNServerGroup.add(serverGroup);
                }

                node = node->next;
            }
        }
    }

    if(AirVPNTools::findXmlNode((xmlChar *)"servers", rootNode) != NULL)
    {
        node = rootNode;
        airVpnServer.clear();

        while(node != NULL)
        {
            node = AirVPNTools::findXmlNode((xmlChar *)"server", node);

            if(node != NULL)
            {
                AirVPNServer localAirVPNServer(resrouceDirectory);

                value = AirVPNTools::getXmlAttribute(node, "name");

                if(!value.empty())
                {
                    localAirVPNServer.setName(value);
                    localAirVPNServer.setCountryCode(AirVPNTools::toUpper(AirVPNTools::getXmlAttribute(node, "country_code")));
                    localAirVPNServer.setLocation(AirVPNTools::getXmlAttribute(node, "location"));
                    
                    try
                    {
                        i = std::stoi(AirVPNTools::getXmlAttribute(node, "bw", "0"));
                    }
                    catch(std::exception &e)
                    {
                        i = 0;
                    }

                    localAirVPNServer.setBandWidth(i);

                    try
                    {
                        i = std::stoi(AirVPNTools::getXmlAttribute(node, "bw_max", "0"));
                    }
                    catch(std::exception &e)
                    {
                        i = 0;
                    }

                    localAirVPNServer.setMaxBandWidth(i);

                    try
                    {
                        i = std::stoi(AirVPNTools::getXmlAttribute(node, "users", "0"));
                    }
                    catch(std::exception &e)
                    {
                        i = 0;
                    }

                    localAirVPNServer.setUsers(i);

                    try
                    {
                        i = std::stoi(AirVPNTools::getXmlAttribute(node, "users_max", "0"));
                    }
                    catch(std::exception &e)
                    {
                        i = 0;
                    }

                    localAirVPNServer.setMaxUsers(i);

                    group = std::stoi(AirVPNTools::getXmlAttribute(node, "group", "-1"));

                    if(group != -1)
                    {
                        localAirVPNServer.setIPv4Available(airVPNServerGroup.getGroupIPv4Support(group));
                        localAirVPNServer.setIPv6Available(airVPNServerGroup.getGroupIPv6Support(group));
                        localAirVPNServer.setSupportCheck(airVPNServerGroup.getGroupSupportCheck(group));
                        localAirVPNServer.setOpenVPNTlsCiphers(airVPNServerGroup.getGroupTlsCiphers(group));
                        localAirVPNServer.setOpenVPNTlsSuiteCiphers(airVPNServerGroup.getGroupTlsSuiteCiphers(group));
                        localAirVPNServer.setOpenVPNDataCiphers(airVPNServerGroup.getGroupDataCiphers(group));
                    }

                    localAirVPNServer.setOpenVPNAvailable(true);
                    localAirVPNServer.setWireGuardAvailable(true);

                    localAirVPNServer.setWarningOpen(AirVPNTools::getXmlAttribute(node, "warning_open"));
                    localAirVPNServer.setWarningClosed(AirVPNTools::getXmlAttribute(node, "warning_closed"));

                    localAirVPNServer.setOpenVpnDirectives(AirVPNTools::getXmlAttribute(node, "openvpn_directives"));

                    try
                    {
                        i = std::stoi(AirVPNTools::getXmlAttribute(node, "scorebase", "0"));
                    }
                    catch(std::exception &e)
                    {
                        i = 0;
                    }

                    localAirVPNServer.setScoreBase(i);

                    value = AirVPNTools::getXmlAttribute(node, "ips_entry");

                    if(!value.empty())
                    {
                        row = AirVPNTools::split(value, ",");

                        if(localAirVPNServer.isIPv6Available())
                        {
                            j = row.size() / 2;

                            for(i = 0; i < j; i++)
                            {
                                localAirVPNServer.setEntryIPv4(i, row[i]);
                                localAirVPNServer.setEntryIPv6(i, row[i + j]);
                            }
                        }
                        else
                        {
                            for(i = 0; i < row.size(); i++)
                                localAirVPNServer.setEntryIPv4(i, row[i]);
                        }
                    }

                    value = AirVPNTools::getXmlAttribute(node, "ips_exit");

                    if(!value.empty())
                    {
                        if(localAirVPNServer.isIPv6Available())
                        {
                            row = AirVPNTools::split(value, ",");

                            localAirVPNServer.setExitIPv4(row[0]);
                            localAirVPNServer.setExitIPv6(row[1]);
                        }
                        else
                            localAirVPNServer.setExitIPv4(AirVPNTools::trim(value));
                    }

                    localAirVPNServer.computeServerScore();

                    localAirVPNServer.setPerfectForwardSecrecyAvailable(true);

                    airVpnServer.push_back(localAirVPNServer);

                    CountryStats cStats;

                    it = countryStats.find(localAirVPNServer.getCountryCode());

                    if(it != countryStats.end())
                    {
                        cStats = it->second;

                        cStats.servers++;
                        cStats.users += localAirVPNServer.getUsers();
                        cStats.bandWidth += localAirVPNServer.getEffectiveBandWidth();
                        cStats.maxBandWidth += localAirVPNServer.getMaxBandWidth();

                        it->second = cStats;
                    }
                    else
                    {
                        cStats.countryISOCode = localAirVPNServer.getCountryCode();
                        cStats.servers = 1;
                        cStats.users = localAirVPNServer.getUsers();
                        cStats.bandWidth = localAirVPNServer.getEffectiveBandWidth();
                        cStats.maxBandWidth = localAirVPNServer.getMaxBandWidth();

                        countryStats.insert(std::make_pair(localAirVPNServer.getCountryCode(), cStats));
                    }

                    it = countryStats.find(localAirVPNServer.getContinent());

                    if(it != countryStats.end())
                    {
                        cStats = it->second;

                        cStats.servers++;
                        cStats.users += localAirVPNServer.getUsers();
                        cStats.bandWidth += localAirVPNServer.getEffectiveBandWidth();
                        cStats.maxBandWidth += localAirVPNServer.getMaxBandWidth();

                        it->second = cStats;
                    }
                    else
                    {
                        cStats.countryISOCode = localAirVPNServer.getContinent();
                        cStats.servers = 1;
                        cStats.users = localAirVPNServer.getUsers();
                        cStats.bandWidth = localAirVPNServer.getEffectiveBandWidth();
                        cStats.maxBandWidth = localAirVPNServer.getMaxBandWidth();

                        countryStats.insert(std::make_pair(localAirVPNServer.getContinent(), cStats));
                    }

                    if(localAirVPNServer.getContinent() == "NAM" || localAirVPNServer.getContinent() == "SAM")
                    {
                        it = countryStats.find("AME");

                        if(it != countryStats.end())
                        {
                            cStats = it->second;

                            cStats.servers++;
                            cStats.users += localAirVPNServer.getUsers();
                            cStats.bandWidth += localAirVPNServer.getEffectiveBandWidth();
                            cStats.maxBandWidth += localAirVPNServer.getMaxBandWidth();

                            it->second = cStats;
                        }
                        else
                        {
                            cStats.countryISOCode = "AME";
                            cStats.servers = 1;
                            cStats.users = localAirVPNServer.getUsers();
                            cStats.bandWidth = localAirVPNServer.getEffectiveBandWidth();
                            cStats.maxBandWidth = localAirVPNServer.getMaxBandWidth();

                            countryStats.insert(std::make_pair("AME", cStats));
                        }
                    }

                    it = countryStats.find("EARTH");

                    if(it != countryStats.end())
                    {
                        cStats = it->second;

                        cStats.servers++;
                        cStats.users += localAirVPNServer.getUsers();
                        cStats.bandWidth += localAirVPNServer.getEffectiveBandWidth();
                        cStats.maxBandWidth += localAirVPNServer.getMaxBandWidth();

                        it->second = cStats;
                    }
                    else
                    {
                        cStats.countryISOCode = "EARTH";
                        cStats.servers = 1;
                        cStats.users = localAirVPNServer.getUsers();
                        cStats.bandWidth = localAirVPNServer.getEffectiveBandWidth();
                        cStats.maxBandWidth = localAirVPNServer.getMaxBandWidth();

                        countryStats.insert(std::make_pair("EARTH", cStats));
                    }
                }

                node = node->next;
            }
        }
    }

    return OK;
}

void AirVPNManifest::cleanupXmlParser()
{
    if(airVpnManifestDocument == nullptr)
        return;

    xmlFreeDoc(airVpnManifestDocument);
}
