/*
 * countrycontinent.hpp
 *
 * This file is part of AirVPN's hummingbird Linux/macOS OpenVPN Client software.
 * Copyright (C) 2019-2022 AirVPN (support@airvpn.org) / https://airvpn.org
 *
 * Developed by ProMIND
 *
 * This is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Hummingbird. If not, see <http://www.gnu.org/licenses/>.
 *
 */

#pragma once

#include <string>
#include <vector>
#include <map>

class CountryContinent
{
    public:

    CountryContinent(const std::string &resourceDirectory);
    ~CountryContinent();

    static std::string getCountryContinent(std::string countryCode);
    static std::string getCountryName(std::string countryCode);
    static std::string getCountryCode(std::string countryName);
    static std::vector<std::string> searchCountry(std::string pattern);
    static int getCountries();
    static std::string getContinentName(std::string continentCode);
    static std::string getContinentCode(std::string continentName);
    static int getContinents();
    static bool isContinent(std::string continent);

    private:

    const std::string COUNTRY_CONTINENT_FILE_NAME = "country_continent.csv";
    const std::string COUNTRY_NAMES_FILE_NAME = "country_names.csv";
    const std::string CONTINENT_NAMES_FILE_NAME = "continent_names.csv";
    const std::string SPLIT_DELIMITER = ",";

    static std::map<std::string, std::string> *countryContinent;
    static std::map<std::string, std::string> *countryNames;
    static std::map<std::string, std::string> *continentNames;
    static int instanceCounter;
};
