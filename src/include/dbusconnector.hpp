/*
 * dbusconnector.hpp
 *
 * This file is part of AirVPN's hummingbird Linux/macOS OpenVPN Client software.
 * Copyright (C) 2019-2022 AirVPN (support@airvpn.org) / https://airvpn.org
 *
 * Developed by ProMIND
 *
 * This is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Hummingbird. If not, see <http://www.gnu.org/licenses/>.
 *
 */

#pragma once

#include <string>
#include <vector>
#include <map>
#include <mutex>
#include <unistd.h>
#include <iconv.h>
#include <dbus/dbus.h>

class DBusResponse;     // Forward declaration. Class is defined below

class DBusConnector
{
    public:

    DBusConnector(const std::string &interface, const std::string &bus);
    ~DBusConnector();

    bool readWriteDispatch(int timeout_millis = DEFAULT_READ_WRITE_TIMEOUT_MILLISECONDS);
    DBusMessage *popMessage();
    bool isMethod(DBusMessage *dbusMessage, const std::string &method);
    bool callMethod(const std::string &bus, const std::string &path, const std::string &method, const std::vector<std::string> &item);
    DBusMessage *callMethodWithReply(const std::string &bus, const std::string &path, const std::string &method, const std::vector<std::string> &item);
    bool replyToMessage(DBusMessage *dbusMessage, DBusResponse response);
    bool replyToMessage(DBusMessage *dbusMessage, const std::vector<std::string> &item);
    bool replyToMessage(DBusMessage *dbusMessage, int value);
    bool getArgs(DBusMessage *dbusMessage, int firstArgType, ...);
    std::vector<std::string> getVector(DBusMessage *dbusMessage);
    int getInt(DBusMessage *dbusMessage);
    DBusResponse *getResponse(DBusMessage *dbusMessage);
    void unreferenceResponse(DBusResponse *response);
    void unreferenceMessage(DBusMessage *dbusMessage);
    std::string stringToUTF8(const std::string &str);
    std::string stringToLocale(const std::string &str);

    private:

    DBusConnection *dbusConnection = nullptr;
    DBusError dbusError;
    std::string busName;
    std::string interfaceName;
    std::timed_mutex mutex;
    std::string localeEncoding;
    bool isEncodingNeeded = false;
    iconv_t iconvToUTF8, iconvToLocale;
    char *iconvIn;
    char *iconvOut;

    static const int ICONV_OUT_LEN = 512;
    static const int DEFAULT_READ_WRITE_TIMEOUT_MILLISECONDS = 50;
    static const int MUTEX_LOCK_TIMEOUT_SECONDS = 5;

    bool connect();
    void disconnect();
    bool isDBusConnectionAvailable();
    std::string iconvString(iconv_t iconverter, std::string str);
    void lock();
    void unlock();
};

class DBusResponse
{
    public:

    DBusResponse();
    ~DBusResponse();

    typedef std::map<std::string, std::string> Item;
    typedef std::vector<Item>::iterator Iterator;
    typedef Item::iterator ItemIterator;

    void clear();
    void setResponse(const std::string &value);
    std::string getResponse();
    void add(Item item);
    void addToItem(Item &item, const std::string &key, const std::string &value);
    Item getItem(int row);
    int rows();
    int items(int row);
    Iterator begin();
    Iterator end();
    ItemIterator begin(Item item);
    ItemIterator end(Item item);
    std::string getItemValue(Item item, const std::string &key);
    std::string itemKey(ItemIterator it);
    std::string itemValue(ItemIterator it);
    bool fromString(const std::string &str);
    std::string toString();

    private:

    const std::string RESPONSE_KEY = "[#>RESPONSE<#]";
    const std::string ROW_SEPARATOR = "[#>ROW<#]";
    const std::string ITEM_SEPARATOR = "[#>ITEM<#]";
    const std::string VALUE_SEPARATOR = "[#>VALUE<#]";

    std::string response;
    std::vector<Item> data;

    std::vector<std::string> split(const std::string &s, const std::string &delimiter);
};

class DBusConnectorException : public std::exception
{
    public:

    explicit DBusConnectorException(const char* errorMessage)
    {
        exceptionMessage = errorMessage;
    }

    explicit DBusConnectorException(const std::string& errorMessage)
    {
        exceptionMessage = errorMessage;
    }

    virtual ~DBusConnectorException() throw()
    {
    }

    virtual const char *what() const throw()
    {
       return exceptionMessage.c_str();
    }

    protected:

    std::string exceptionMessage;
};
