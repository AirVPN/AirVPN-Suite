/*
 * airvpnserverprovider.hpp
 *
 * This file is part of AirVPN's hummingbird Linux/macOS OpenVPN Client software.
 * Copyright (C) 2019-2022 AirVPN (support@airvpn.org) / https://airvpn.org
 *
 * Developed by ProMIND
 *
 * This is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Hummingbird. If not, see <http://www.gnu.org/licenses/>.
 *
 */

#pragma once

#include <string>
#include <map>
#include <vector>

#include "airvpnmanifest.hpp"
#include "airvpnserver.hpp"
#include "airvpnuser.hpp"
#include "countrycontinent.hpp"

class AirVPNServerProvider
{
    public:

    enum TLSMode
    {
        NOT_SET,
        TLS_AUTH,
        TLS_CRYPT
    };

    AirVPNServerProvider(AirVPNUser *user, const std::string &resourceDirectory);
    ~AirVPNServerProvider();
    
    std::string getUserIP();
    void setUserIP(const std::string &u);
    std::string getUserCountry();
    void setUserCountry(const std::string &c);
    TLSMode getTlsMode();
    void setTlsMode(TLSMode m);
    bool isIPv4Available();
    void setIPv4Available(bool s);
    bool isIPv6Available();
    void setIPv6Available(bool s);
    std::vector<std::string> getServerWhitelist();
    void setServerWhitelist(const std::vector<std::string> &list);
    std::vector<std::string> getServerBlacklist();
    void setServerBlacklist(const std::vector<std::string> &list);
    std::vector<std::string> getCountryWhitelist();
    void setCountryWhitelist(const std::vector<std::string> &list);
    std::vector<std::string> getCountryBlacklist();
    void setCountryBlacklist(const std::vector<std::string> &list);

    std::vector<AirVPNServer> getFilteredServerList(bool forbidLocalServerConnection);

    private:

    std::string AIRVPN_CONNECTION_PRIORITY_FILE_NAME = "connection_priority.txt";

    AirVPNManifest *airVPNManifest = nullptr;
    AirVPNUser *airVPNUser = nullptr;

    std::string userIP = "";
    std::string userCountry = "";
    std::string userContinent = "";
    TLSMode tlsMode;
    bool IPv4Available = false;
    bool IPv6Available = false;

    std::vector<std::string> serverWhitelist, serverBlacklist;
    std::vector<std::string> countryWhitelist, countryBlacklist;

    CountryContinent *countryContinent = nullptr;

    std::map<std::string, std::string> connectionPriority;

    static bool compareServerScore(AirVPNServer s1, AirVPNServer s2);

    void loadConnectionPriorities(const std::string &resourceDirectory);

    std::vector<std::string> getUserConnectionPriority();
};
