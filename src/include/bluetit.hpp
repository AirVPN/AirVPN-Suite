/*
 * bluetit.hpp
 *
 * This file is part of AirVPN's Linux/macOS OpenVPN Client software.
 * Copyright (C) 2019-2022 AirVPN (support@airvpn.org) / https://airvpn.org
 *
 * Developed by ProMIND
 *
 * This is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with AirVPN-Suite. If not, see <http://www.gnu.org/licenses/>.
 *
 */

#pragma once

#include "btcommon.h"
#include "wireguardclient.hpp"
#include "dbusconnector.hpp"
#include "rcparser.hpp"
#include "optionparser.hpp"

#include <future>

#define BLUETIT_FULL_NAME           BLUETIT_NAME " " BLUETIT_VERSION
#define BLUETIT_RESOURCE_DIRECTORY  "/etc/airvpn"
#define BLUETIT_LOCK_FILE           BLUETIT_RESOURCE_DIRECTORY "/bluetit.lock"
#define BLUETIT_RC_FILE             BLUETIT_RESOURCE_DIRECTORY "/bluetit.rc"
#define HUMMINGBIRD_LOCK_FILE       BLUETIT_RESOURCE_DIRECTORY "/hummingbird.lock"
#define RESOLVDOTCONF_BACKUP        BLUETIT_RESOURCE_DIRECTORY "/resolv.conf.airvpnbackup"
#define SYSTEM_DNS_BACKUP_FILE      BLUETIT_RESOURCE_DIRECTORY "/systemdns.airvpnbackup"
#define CONNECTION_SEQUENCE_FILE    BLUETIT_RESOURCE_DIRECTORY "/connection_sequence.csv"
#define BLUETIT_LOG_NAME            "bluetit"

#define TLS_MODE_CRYPT                      "tls-crypt"
#define TLS_MODE_AUTH                       "tls-auth"

#define DEFAULT_OPENVPN_PROTO               "udp"
#define DEFAULT_OPENVPN_PORT                "443"
#define DEFAULT_WIREGUARD_PORT              "1637"
#define DEFAULT_VPN_TYPE                    VPN_TYPE_OPENVPN
#define DEFAULT_TLS_MODE                    TLS_MODE_CRYPT
#define DEFAULT_IPV6_MODE                   false
#define DEFAULT_6TO4_MODE                   false

#define DEFAULT_MANIFEST_UPDATE_INTERVAL    15
#define MANIFEST_THREAD_SLEEP_SECONDS       5
#define DEFAULT_MAX_CONNECTION_RETRIES      10

#define CONNECTION_STATS_INTERVAL           2

#define WAIT_AIRVPN_MANIFEST_TIMEOUT        20

#define WAIT_NETWORK_INTERVAL                2
#define WAIT_NETWORK_MESSAGE_INTERVAL        60

// Common return messages

#define MESSAGE_AIRVPN_NOT_AVAILABLE            "ERROR: AirVPN services and connection are disabled"
#define MESSAGE_AIRVPN_SERVER_NOT_FOUND         "ERROR: AirVPN server not found"
#define MESSAGE_AIRVPN_COUNTRY_NOT_FOUND        "ERROR: AirVPN country not found"
#define MESSAGE_AIRVPN_USER_PROFILE_NOT_FOUND   "ERROR: AirVPN user profile not found"

// Run Control directives

#define RC_DIRECTIVE_BOOTSERVER                 "bootserver"
#define RC_DIRECTIVE_RSAEXPONENT                "rsaexponent"
#define RC_DIRECTIVE_RSAMODULUS                 "rsamodulus"
#define RC_DIRECTIVE_MANIFESTUPDATEINTERVAL     "manifestupdateinterval"
#define RC_DIRECTIVE_AIRCONNECTATBOOT           "airconnectatboot"
#define RC_DIRECTIVE_NETWORKLOCKPERSIST         "networklockpersist"
#define RC_DIRECTIVE_AIRUSERNAME                "airusername"
#define RC_DIRECTIVE_AIRPASSWORD                "airpassword"
#define RC_DIRECTIVE_AIRKEY                     "airkey"
#define RC_DIRECTIVE_AIRVPNTYPE                 "airvpntype"
#define RC_DIRECTIVE_AIRSERVER                  "airserver"
#define RC_DIRECTIVE_AIRCOUNTRY                 "aircountry"
#define RC_DIRECTIVE_AIRPORT                    "airport"
#define RC_DIRECTIVE_AIRPROTO                   "airproto"
#define RC_DIRECTIVE_AIRCIPHER                  "aircipher"
#define RC_DIRECTIVE_AIRIPV6                    "airipv6"
#define RC_DIRECTIVE_AIR6TO4                    "air6to4"
#define RC_DIRECTIVE_AIRWHITESERVERLIST         "airwhiteserverlist"
#define RC_DIRECTIVE_AIRBLACKSERVERLIST         "airblackserverlist"
#define RC_DIRECTIVE_AIRWHITECOUNTRYLIST        "airwhitecountrylist"
#define RC_DIRECTIVE_AIRBLACKCOUNTRYLIST        "airblackcountrylist"
#define RC_DIRECTIVE_FORBIDQUICKHOMECOUNTRY     "forbidquickhomecountry"
#define RC_DIRECTIVE_ALLOWUSERVPNPROFILES       "allowuservpnprofiles"
#define RC_DIRECTIVE_COUNTRY                    "country"
#define RC_DIRECTIVE_REMOTE                     "remote"
#define RC_DIRECTIVE_PROTO                      "proto"
#define RC_DIRECTIVE_PORT                       "port"
#define RC_DIRECTIVE_TUNPERSIST                 "tunpersist"
#define RC_DIRECTIVE_CIPHER                     "cipher"
#define RC_DIRECTIVE_MAXCONNECTIONRETRIES       "maxconnretries"
#define RC_DIRECTIVE_TCPQUEUELIMIT              "tcpqueuelimit"
#define RC_DIRECTIVE_NCPDISABLE                 "ncpdisable"
#define RC_DIRECTIVE_NETWORKLOCK                "networklock"
#define RC_DIRECTIVE_IGNOREDNSPUSH              "ignorednspush"
#define RC_DIRECTIVE_TIMEOUT                    "timeout"
#define RC_DIRECTIVE_COMPRESS                   "compress"
#define RC_DIRECTIVE_TLSVERSIONMIN              "tlsversionmin"
#define RC_DIRECTIVE_PROXYHOST                  "proxyhost"
#define RC_DIRECTIVE_PROXYPORT                  "proxyport"
#define RC_DIRECTIVE_PROXYUSERNAME              "proxyusername"
#define RC_DIRECTIVE_PROXYPASSWORD              "proxypassword"
#define RC_DIRECTIVE_PROXYBASIC                 "proxybasic"

#define BLUETIT_RC_TEMPLATE       "#\n" \
                                  "# bluetit runcontrol file\n" \
                                  "#\n" \
                                  "\n" \
                                  "# " RC_DIRECTIVE_BOOTSERVER "              <ip|url>\n" \
                                  "# " RC_DIRECTIVE_RSAEXPONENT "             <value>\n" \
                                  "# " RC_DIRECTIVE_RSAMODULUS "              <value>\n" \
                                  "# " RC_DIRECTIVE_AIRCONNECTATBOOT "        <off|quick|server|country>\n" \
                                  "# " RC_DIRECTIVE_NETWORKLOCKPERSIST "      <on|nftables|iptables|pf|off>\n" \
                                  "# " RC_DIRECTIVE_AIRUSERNAME "             <airvpn_username>\n" \
                                  "# " RC_DIRECTIVE_AIRPASSWORD "             <aivpn_password>\n" \
                                  "# " RC_DIRECTIVE_AIRKEY "                  <airvpn_user_key>\n" \
                                  "# " RC_DIRECTIVE_AIRVPNTYPE "              <" VPN_TYPE_OPENVPN "|" VPN_TYPE_WIREGUARD ">\n" \
                                  "# " RC_DIRECTIVE_AIRSERVER "               <airvpn_server_name>\n" \
                                  "# " RC_DIRECTIVE_AIRCOUNTRY "              <airvpn_country_name>\n" \
                                  "# " RC_DIRECTIVE_AIRPROTO "                <udp|tcp>\n" \
                                  "# " RC_DIRECTIVE_AIRPORT "                 <port>\n" \
                                  "# " RC_DIRECTIVE_AIRCIPHER "               <cipher_name>\n" \
                                  "# " RC_DIRECTIVE_AIRIPV6 "                 <yes|no>\n" \
                                  "# " RC_DIRECTIVE_AIR6TO4 "                 <yes|no>\n" \
                                  "# " RC_DIRECTIVE_MANIFESTUPDATEINTERVAL "  <minutes>\n" \
                                  "# " RC_DIRECTIVE_AIRWHITESERVERLIST "      <server list>\n" \
                                  "# " RC_DIRECTIVE_AIRBLACKSERVERLIST "      <server list>\n" \
                                  "# " RC_DIRECTIVE_AIRWHITECOUNTRYLIST "      <country list>\n" \
                                  "# " RC_DIRECTIVE_AIRBLACKCOUNTRYLIST "     <country list>\n" \
                                  "# " RC_DIRECTIVE_FORBIDQUICKHOMECOUNTRY "  <yes|no>\n" \
                                  "# " RC_DIRECTIVE_ALLOWUSERVPNPROFILES "    <yes|no>\n" \
                                  "# " RC_DIRECTIVE_COUNTRY "                 <ISO code>\n" \
                                  "# " RC_DIRECTIVE_REMOTE "                  <ip|url list>\n" \
                                  "# " RC_DIRECTIVE_PROTO "                   <udp|tcp>\n" \
                                  "# " RC_DIRECTIVE_PORT "                    <port>\n" \
                                  "# " RC_DIRECTIVE_TUNPERSIST "              <yes|no>\n" \
                                  "# " RC_DIRECTIVE_CIPHER "                  <cipher_names>\n" \
                                  "# " RC_DIRECTIVE_MAXCONNECTIONRETRIES "          <number>\n" \
                                  "# " RC_DIRECTIVE_TCPQUEUELIMIT "           <value>\n" \
                                  "# " RC_DIRECTIVE_NCPDISABLE "              <yes|no>\n" \
                                  "# " RC_DIRECTIVE_NETWORKLOCK "             <on|nftables|iptables|pf|off>\n" \
                                  "# " RC_DIRECTIVE_IGNOREDNSPUSH "           <yes|no>\n" \
                                  "# " RC_DIRECTIVE_TIMEOUT "                 <seconds>\n" \
                                  "# " RC_DIRECTIVE_COMPRESS "                <yes|no|asym>\n" \
                                  "# " RC_DIRECTIVE_TLSVERSIONMIN "           <disabled|default|tls_1_x>\n" \
                                  "# " RC_DIRECTIVE_PROXYHOST "               <ip|url>\n" \
                                  "# " RC_DIRECTIVE_PROXYPORT "               <port>\n" \
                                  "# " RC_DIRECTIVE_PROXYUSERNAME "           <username>\n" \
                                  "# " RC_DIRECTIVE_PROXYPASSWORD "           <password>\n" \
                                  "# " RC_DIRECTIVE_PROXYBASIC "              <yes|no>\n"


#define NETLOCKMODE_PERSISTENT  1
#define NETLOCKMODE_SESSION     2
                                  
struct ConnectionScheme
{
    std::string protocol;
    int port;
    int entry;
};

void signal_handler(int signum);
void create_daemon();
bool check_if_root();
void global_log(const std::string &s);
void global_syslog(const std::string &s);
int bluetit_status(void);
std::string bluetit_status_description(void);
void cleanup_and_exit(int exit_code);
std::string recover_network();
bool setup_rc_directives();
bool setup_options();
bool areWhiteBlackListsDefined();
void reset_settings();
std::string default_port(std::string vpntype = "");
std::string vpn_type_description();
bool enable_network_lock(int mode);
bool disable_network_lock();
bool add_airvpn_bootstrap_to_network_lock();
bool remove_airvpn_bootstrap_from_network_lock();
std::string check_rc_value(const std::string &description, const std::string &option, const std::string &value);
std::string denied_rc_value(const std::string &description, const std::string &option, const std::string &value);
std::string set_openvpn_client_config();
std::string set_openvpn_profile(std::string profile);
std::string set_bluetit_options(const std::vector<std::string> &option);
std::string bluetit_option_allowed(const OptionParser::Option &option, int allowed_status);
void start_boot_airvpn_connection();
void start_airvpn_connection();
void start_airvpn_quick_connection(const std::future<void> &future);
bool establish_openvpn_connection();
void start_openvpn_connection(int max_connection_retries);
std::string stop_openvpn_connection();
void stop_connection_thread();
void stop_connection_stats_thread();
std::string reconnect_openvpn();
std::string pause_openvpn_connection();
std::string resume_openvpn_connection();
void connection_stats_updater(const std::future<void> &future);
void connection_stats(DBusResponse &response);
void airvpn_server_info(const std::string &name, DBusResponse &response);
void airvpn_server_list(const std::string &pattern, DBusResponse &response);
void airvpn_country_info(const std::string &name, DBusResponse &response);
void airvpn_country_list(const std::string &pattern, DBusResponse &response);
void airvpn_key_list(const std::string &username, const std::string &password, DBusResponse &response);
void airvpn_key_save(const std::string &username, const std::string &password, DBusResponse &response);
void airvpn_server_save(const std::string &username, const std::string &password, bool create_country, DBusResponse &response);
std::string airvpn_create_profile(std::string server, bool create_country_profile, int ipEntryOffset = 0);
void airvpn_manifest_updater(int interval_minutes, const std::future<void> &future);
bool airvpn_user_login();
void airvpn_user_logout();
void terminate_client_session();
void send_event(const std::string &event, const std::string &message, const std::vector<std::string> &payloadItems = {});
OpenVpnClient::EventCallback openvpn_client_connected_event_callback(OpenVpnClient::EventData eventData);
