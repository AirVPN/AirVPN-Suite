/*
 * airvpnmanifest.hpp
 *
 * This file is part of AirVPN's hummingbird Linux/macOS OpenVPN Client software.
 * Copyright (C) 2019-2022 AirVPN (support@airvpn.org) / https://airvpn.org
 *
 * Developed by ProMIND
 *
 * This is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Hummingbird. If not, see <http://www.gnu.org/licenses/>.
 *
 */

#pragma once

#include "airvpntools.hpp"
#include "airvpnserver.hpp"
#include "airvpnservergroup.hpp"
#include "cipherdatabase.hpp"

#include <libxml/parser.h>
#include <libxml/tree.h>

#include <string>
#include <vector>
#include <map>
#include <ctime>

class AirVPNManifest
{
    public:

    enum Error
    {
        OK,
        CANNOT_RETRIEVE_MANIFEST,
        EMPTY_MANIFEST,
        NULL_XML_DOCUMENT,
        PARSE_ERROR
    };

    enum Type
    {
        NOT_SET,
        PROCESSING,
        STORED,
        FROM_SERVER
    };

    typedef struct Message
    {
        std::string text;
        std::string url;
        std::string link;
        std::string html;
        long fromUTCTimeTS;
        long toUTCTimeTS;
        bool shown;
    } Message;

    typedef struct CountryStats
    {
        std::string countryISOCode;
        int servers;
        int users;
        long long bandWidth;
        long long maxBandWidth;
    } CountryStats;

    AirVPNManifest(const std::string &resDir);
    ~AirVPNManifest();

    void resetBootServers();
    void addBootServer(const std::string &server);

    void setRsaPublicKeyModulus(const std::string &modulus);
    static std::string getRsaPublicKeyModulus();
    void setRsaPublicKeyExponent(const std::string &exponent);
    static std::string getRsaPublicKeyExponent();

    Error loadManifest(bool forceLocal = false);

    std::string getManifest();
    static Type getManifestType();
    std::string getErrorDescription();

    std::vector<Message> getMessages();
    void setMessageShown(int m);
    long getManifestTimeTS();
    void setManifestTimeTS(long t);
    long getManifestNextUpdateTS();
    void setManifestNextUpdateTS(long t);
    int getNextUpdateIntervalMinutes();
    void setNextUpdateIntervalMinutes(int n);
    std::string getDnsCheckHost();
    void setDnsCheckHost(const std::string &d);
    std::string getDnsCheckRes1();
    void setDnsCheckRes1(const std::string &d);
    std::string getDnsCheckRes2();
    void setDnsCheckRes2(const std::string &d);
    static double getSpeedFactor();
    void setSpeedFactor(double s);
    static double getLoadFactor();
    void setLoadFactor(double l);
    static double getUserFactor();
    void setUserFactor(double u);
    int getLatencyFactor();
    void setLatencyFactor(int l);
    int getPenalityFactor();
    void setPenalityFactor(int p);
    int getPingerDelay();
    void setPingerDelay(int p);
    int getPingerRetry();
    void setPingerRetry(int p);
    std::string getCheckDomain();
    void setCheckDomain(const std::string &c);
    static std::string getCheckDnsQuery();
    void setCheckDnsQuery(const std::string &c);
    std::string getOpenVpnDirectives();
    void setOpenVpnDirectives(const std::string &o);
    static std::string getServerOpenVpnDirectives(const std::string &serverIP);
    std::string getModeProtocol();
    void setModeProtocol(const std::string &m);
    int getModePort();
    void setModePort(int m);
    int getModeAlt();
    void setModeAlt(int m);
    static std::vector<std::string> getBootStrapServerUrlList();
    void setBootStrapServerUrlList(const std::vector<std::string> &b);
    void addBootStrapServerUrl(const std::string &url);
    std::vector<AirVPNServer> getAirVpnServerList();
    void addAirVpnServer(AirVPNServer server);
    static CountryStats getCountryStats(const std::string &code);
    static std::vector<AirVPNServer> getServerListByCountry(std::string code, bool availableOnly = false);
    static AirVPNServer getServerByName(std::string name);
    static AirVPNServer getServerByIP(std::string name);
    std::string getTlsCipherDescription(int code);
    std::string getTlsSuiteCipherDescription(int code);
    std::string getDataCipherDescription(int code);

    std::vector<std::string> searchServer(std::string pattern);

    private:

    std::string resrouceDirectory;
    static std::vector<std::string> bootStrapServerUrl;
    static std::string rsaPublicKeyModulus, rsaPublicKeyExponent;

    std::time_t processTimeTS;
    long manifestTimeTS;
    long manifestNextUpdateTS;
    int nextUpdateIntervalMinutes;
    std::vector<Message> manifestMessage;
    std::string dnsCheckHost;
    std::string dnsCheckRes1;
    std::string dnsCheckRes2;
    static double speedFactor;
    static double loadFactor;
    static double userFactor;
    int latencyFactor;
    int penalityFactor;
    int pingerDelay;
    int pingerRetry;
    std::string checkDomain;
    static std::string checkDnsQuery;
    static std::string openVpnDirectives;
    std::string modeProtocol;
    int modePort;
    int modeAlt;
    static std::vector<AirVPNServer> airVpnServer;
    static std::map<std::string, CountryStats> countryStats;
    AirVPNServerGroup airVPNServerGroup;
    CipherDatabase cipherDatabase;
    bool refreshPending;

    AirVPNTools *airVpnTools = nullptr;

    xmlDoc *airVpnManifestDocument = nullptr;

    xmlNode *rootNode = nullptr;

    Error manifestError;

    std::string errorDescription;
    std::string systemDescription;
    std::string manifestDocument;

    static Type manifestDocumentType;

    const std::string manifestFileName = "airvpn-manifest.xml";

    void initializeManifestData();
    Error processXmlManifest();
    void cleanupXmlParser();
    std::string getCipherDescription(int code, CipherDatabase::CipherType type);
};
