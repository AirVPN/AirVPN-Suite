/*
 * airvpnservergroup.hpp
 *
 * This file is part of AirVPN's hummingbird Linux/macOS OpenVPN Client software.
 * Copyright (C) 2019-2022 AirVPN (support@airvpn.org) / https://airvpn.org
 *
 * Developed by ProMIND
 *
 * This is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Hummingbird. If not, see <http://www.gnu.org/licenses/>.
 *
 */

#pragma once

#include <vector>
#include <map>

class AirVPNServerGroup
{
    public:

    AirVPNServerGroup();
    ~AirVPNServerGroup();

    typedef struct ServerGroup
    {
        int group;
        bool ipv4Support;
        bool ipv6Support;
        bool checkSupport;
        std::vector<int> tlsCiphers;
        std::vector<int> tlsSuiteCiphers;
        std::vector<int> dataCiphers;
    } ServerGroup;

    bool add(ServerGroup s);
    bool getGroupIPv4Support(int group);
    bool getGroupIPv6Support(int group);
    bool getGroupSupportCheck(int group);
    std::vector<int> getGroupTlsCiphers(int group);
    std::vector<int> getGroupTlsSuiteCiphers(int group);
    std::vector<int> getGroupDataCiphers(int group);
    void reset();

    private:

    std::map<int, ServerGroup> serverGroupDatabase;
};

