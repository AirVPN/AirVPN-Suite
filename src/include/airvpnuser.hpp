/*
 * airvpnuser.hpp
 *
 * This file is part of AirVPN's hummingbird Linux/macOS OpenVPN Client software.
 * Copyright (C) 2019-2022 AirVPN (support@airvpn.org) / https://airvpn.org
 *
 * Developed by ProMIND
 *
 * This is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Hummingbird. If not, see <http://www.gnu.org/licenses/>.
 *
 */

#pragma once

#include "airvpntools.hpp"
#include "airvpnmanifest.hpp"
#include <libxml/parser.h>
#include <libxml/tree.h>
#include <curl/curl.h>

#include <string>
#include <map>

class AirVPNUser
{
    public:

    enum UserProfileType
    {
        NOT_SET,
        FROM_SERVER
    };

    enum UserLocationStatus
    {
        SET,
        UNKNOWN
    };

    enum Error
    {
        OK,
        INVALID_USER,
        CANNOT_RETRIEVE_PROFILE,
        EMPTY_PROFILE,
        NULL_XML_DOCUMENT,
        PARSE_ERROR,
        AIRVPN_PARAMETERS_ERROR
    };

    typedef struct UserKey
    {
        std::string name = "";
        std::string certificate = "";
        std::string privateKey = "";
        std::string wireguardPrivateKey = "";
        std::string wireguardPresharedKey = "";
        std::string wireguardIPv4 = "";
        std::string wireguardIPv6 = "";
        std::string wireguardDnsIPv4 = "";
        std::string wireguardDnsIPv6 = "";
    } UserKey;

    AirVPNUser(const std::string &userName, const std::string &password);
    ~AirVPNUser();

    std::string getUserIP();
    void setUserIP(const std::string &ip);
    std::string getUserCountry();
    void setUserCountry(const std::string &country);
    float getUserLatitude();
    void setUserLatitude(float l);
    float getUserLongitude();
    void setUserLongitude(float l);
    bool isUserValid();
    std::string getExpirationDate();
    void setExpirationDate(const std::string &e);
    int getDaysToExpiration();
    void setDaysToExpiration(int d);
    std::string getCertificateAuthorityCertificate();
    void setCertificateAuthorityCertificate(const std::string &c);
    std::string getTlsAuthKey();
    void setTlsAuthKey(const std::string &t);
    std::string getTlsCryptKey();
    void setTlsCryptKey(const std::string &t);
    std::string getSshKey();
    void setSshKey(const std::string &s);
    std::string getSshPpk();
    void setSshPpk(const std::string &s);
    std::string getSslCertificate();
    void setSslCertificate(const std::string &s);
    std::string getWireGuardPublicKey();
    void setWireGuardPublicKey(const std::string &s);
    std::map<std::string, UserKey> getUserKeys();
    void setUserKeys(const std::map<std::string, UserKey> &u);
    UserKey getUserKey(const std::string &name);
    void setUserKey(const std::string &name, const UserKey &u);
    void addUserKey(const std::string &name, const UserKey &key);
    std::vector<std::string> getUserKeyNames();
    UserProfileType getUserProfileType();
    std::string getUserName();
    void setUserName(const std::string &u);
    std::string getUserPassword();
    void setUserPassword(const std::string &p);
    std::string getCurrentProfile();
    std::string getFirstProfileName();
    void setCurrentProfile(const std::string &p);
    Error getUserProfileError();
    std::string getUserProfileErrorDescription();
    
    UserLocationStatus detectUserLocation();
    std::string getUserLocationStatusError();

    std::string getOpenVPNProfile(const std::string &userKeyName, std::string server, int port, const std::string &proto, const std::string &tlsMode, const std::string &cipher, bool connectIPv6, bool mode6to4, bool createCountryProfile, const std::string &countryCode, const std::string &customProfile);
    std::string getWireGuardProfile(const std::string &userKeyName, std::string server, int port, int listenPort, int fwMark, const std::string &allowedIPs, int persistentKeepalive, bool connectIPv6, bool mode6to4, bool createCountryProfile, const  std::string &countryCode);

    private:

    const long SERVER_CONNECTION_TIMEOUT = 15L;
    const long SERVER_TIMEOUT = 30L;

    const char *AIRVPN_USER_DATA_FILE_NAME = "AirVPNUser.dat";
    const char *AIRVPN_USER_PROFILE_FILE_NAME = "AirVPNUserProfile.dat";
    const char *AIRVPN_USER_DATA = "AirVPNUser";
    const char *AIRVPN_USER_NAME_ITEM = "UserName";
    const char *AIRVPN_USER_PASSWORD_ITEM = "Password";
    const char *AIRVPN_USER_CURRENT_PROFILE = "CurrentProfile";

    AirVPNTools *airVpnTools = nullptr;

    xmlDoc *airVpnUserProfileDocument = nullptr;
    
    Error userProfileError;

    std::string userProfileErrorDescription;

    xmlNode *rootNode = nullptr;

    std::string airVPNUserName;
    std::string airVPNUserPassword;
    std::string airVPNUserCurrentProfile;
    std::string userProfileDocument;
    
    UserProfileType userProfileType;

    bool userIsValid;
    bool userLoginFailed;
    std::string expirationDate;
    std::string airVPNSubscriptionExpirationDate;
    int daysToExpiration;
    std::string certificateAuthorityCertificate;
    std::string tlsAuthKey;
    std::string tlsCryptKey;
    std::string sshKey;
    std::string sshPpk;
    std::string sslCertificate;
    std::string wireguardPublicKey;

    std::map<std::string, UserKey> userKey;

    std::string userIP;
    std::string userCountry;
    float userLatitude;
    float userLongitude;

    std::string curlReadBuffer;
    std::string userLocationStatusError;

    void initializeUserData();
    void initializeUserProfileData();
    void loadUserProfile();
    Error processXmlUserProfile();
    void cleanupXmlParser();

    static size_t curlWriteCallback(void *data, size_t size, size_t nmemb, void *userp);
};
