/*
 * rcparser.hpp
 *
 * This file is part of AirVPN's Linux/macOS OpenVPN Client software.
 * Copyright (C) 2019-2022 AirVPN (support@airvpn.org) / https://airvpn.org
 *
 * Developed by ProMIND
 *
 * This is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Hummingbird. If not, see <http://www.gnu.org/licenses/>.
 *
 */

#pragma once

#include <vector>
#include <string>
#include <algorithm>

class RCParser
{
    public:

    enum Error
    {
        OK,
        CANNOT_OPEN_RC_FILE,
        NO_DIRECTIVES_PROVIDED,
        PARSE_ERROR,
        UNKNOWN_DIRECTIVE,
        EMPTY_LIST
    };

    enum Type
    {
        UNDEFINED,
        INTEGER,
        NUMBER,
        STRING,
        LIST,
        BOOL
    };

    typedef struct ConfigDirective
    {
        std::string name;
        Type type;
    } ConfigDirective;

    typedef struct Directive
    {
        std::string name;
        bool isValid;
        Type type;
        std::vector<std::string> value;
        std::string error;
    } Directive;

    typedef std::vector<Directive *> Directives;

    RCParser(const std::string &rcfile);
    RCParser(const char *rcfile);
    ~RCParser();

    Error parseRCFile();
    Directives getDirectives();
    Directives getInvalidDirectives();
    Directive *getDirective(const std::string &name);
    bool addConfigDirective(const std::string &name, Type type);
    bool isValueAllowed(const std::string &name, const std::string &value);
    bool isDirectiveEnabled(Directive *directive);
    bool isValueInList(Directive *directive, const std::string &value);
    std::string getErrorDescription();

    static bool isValidBool(const std::string &value);
    static bool isBoolEnabled(const std::string &value);
    static std::string allowedBoolValueMessage(std::string option);

    private:

    const std::string WHITE_SPACE = " \t";

    std::string runControlFilePath;
    std::vector<ConfigDirective *> configDirective;
    Directives rcDirective;
    std::string errorDescription;

    void setup(const std::string &rcfile);
    bool directiveIsValid(const std::string &s);
    std::vector<std::string> getDirectiveAndValue(const std::string &s);
    Type getConfigDirectiveType(const std::string &s);
    Error addListValues(std::vector<std::string> &v, std::string l, const std::string &delimiter = ",");
    Directive *findDirective(const std::string &name);
    Directives directiveVector(bool valid);
};

