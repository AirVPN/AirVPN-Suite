/*
 * btcommon.hpp
 *
 * This file is part of AirVPN's Linux/macOS OpenVPN Client software.
 * Copyright (C) 2019-2022 AirVPN (support@airvpn.org) / https://airvpn.org
 *
 * Developed by ProMIND
 *
 * This is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with AirVPN-Suite. If not, see <http://www.gnu.org/licenses/>.
 *
 */

#pragma once

#define EXPAND_MACRO(m) #m
#define TO_STRING(m) EXPAND_MACRO(m)

// Bluetit common descriptions

#define BLUETIT_NAME            "Bluetit - AirVPN OpenVPN3 Service"
#define BLUETIT_SHORT_NAME      "Bluetit"
#define BLUETIT_VERSION         "1.3.0"
#define BLUETIT_RELEASE_DATE    "1 June 2023"

// Supported VPN types

#define VPN_TYPE_WIREGUARD                  "wireguard"
#define VPN_TYPE_OPENVPN                    "openvpn"

#define VPN_TYPE_WIREGUARD_NAME             "WireGuard"
#define VPN_TYPE_OPENVPN_NAME               "OpenVPN"

// Status

#define BLUETIT_STATUS_READY                     1
#define BLUETIT_STATUS_CONNECTED                 2
#define BLUETIT_STATUS_PAUSED                    3
#define BLUETIT_STATUS_DIRTY_EXIT                4
#define BLUETIT_STATUS_RESOURCE_DIRECTORY_ERROR  5
#define BLUETIT_STATUS_INIT_ERROR                6
#define BLUETIT_STATUS_LOCK_ERROR                7
#define BLUETIT_STATUS_UNKNOWN                   99

// D-Bus

#define BT_DBUS_INTERFACE_NAME      "org.airvpn.dbus"
#define BT_SERVER_BUS_NAME          "org.airvpn.server"
#define BT_CLIENT_BUS_NAME          "org.airvpn.client"
#define BT_SERVER_OBJECT_PATH_NAME  "/org/airvpn/server"
#define BT_CLIENT_OBJECT_PATH_NAME  "/org/airvpn/client"

// Commands / Methods

#define BT_METHOD_VERSION                   "version"
#define BT_METHOD_BLUETIT_STATUS            "bluetit_status"
#define BT_METHOD_OPENVPN_INFO              "openvpn_info"
#define BT_METHOD_OPENVPN_COPYRIGHT         "openvpn_copyright"
#define BT_METHOD_SSL_LIBRARY_VERSION       "ssl_library_version"
#define BT_METHOD_RESET_BLUETIT_OPTIONS     "reset_bluetit_options"
#define BT_METHOD_SET_OPTIONS               "set_options"
#define BT_METHOD_SET_OPENVPN_PROFILE       "set_openvpn_profile"
#define BT_METHOD_START_CONNECTION          "start_connection"
#define BT_METHOD_STOP_CONNECTION           "stop_connection"
#define BT_METHOD_PAUSE_CONNECTION          "pause_connection"
#define BT_METHOD_RESUME_CONNECTION         "resume_connection"
#define BT_METHOD_RECONNECT_CONNECTION      "reconnect_connection"
#define BT_METHOD_SESSION_PAUSE             "session_pause"
#define BT_METHOD_SESSION_RESUME            "session_resume"
#define BT_METHOD_SESSION_RECONNECT         "session_reconnect"
#define BT_METHOD_CONNECTION_STATS          "connection_stats"
#define BT_METHOD_ENABLE_NETWORK_LOCK       "enable_network_lock"
#define BT_METHOD_DISABLE_NETWORK_LOCK      "disable_network_lock"
#define BT_METHOD_NETWORK_LOCK_STATUS       "network_lock_status"
#define BT_METHOD_RECOVER_NETWORK           "recover_network"
#define BT_METHOD_LIST_DATA_CIPHERS         "list_data_ciphers"
#define BT_METHOD_LIST_PUSHED_DNS           "list_pushed_dns"
#define BT_METHOD_AIRVPN_SET_KEY            "airvpn_set_key"
#define BT_METHOD_AIRVPN_START_CONNECTION   "airvpn_start_connection"
#define BT_METHOD_EVENT                     "event"
#define BT_METHOD_LOG                       "log"

// Events

#define BT_EVENT_END_OF_SESSION             "event_end_of_session"
#define BT_EVENT_CONNECTED                  "event_connected"
#define BT_EVENT_DISCONNECTED               "event_disconnected"
#define BT_EVENT_PAUSE                      "event_pause"
#define BT_EVENT_RESUME                     "event_resume"
#define BT_EVENT_ERROR                      "event_error"

// Response datasets

#define BT_DATASET_AIRVPN_SERVER_INFO        "airvpn_server_info"
#define BT_DATASET_AIRVPN_SERVER_LIST        "airvpn_server_list"
#define BT_DATASET_AIRVPN_COUNTRY_INFO       "airvpn_country_info"
#define BT_DATASET_AIRVPN_COUNTRY_LIST       "airvpn_country_list"
#define BT_DATASET_AIRVPN_KEY_LIST           "airvpn_key_list"
#define BT_DATASET_AIRVPN_SAVE               "airvpn_save"

// Return codes

#define BT_OK                           "OK"
#define BT_ERROR                        "ERROR"

#define TCP_QUEUE_LIMIT_DEFAULT         8192

// Client options

#define BT_CLIENT_OPTION_AIR_CONNECT_SHORT              "O"
#define BT_CLIENT_OPTION_AIR_CONNECT                    "air-connect"
#define BT_CLIENT_OPTION_AIR_CONNECT_DESC               "Start AirVPN connection"
#define BT_CLIENT_OPTION_AIR_SERVER_SHORT               "S"
#define BT_CLIENT_OPTION_AIR_SERVER                     "air-server"
#define BT_CLIENT_OPTION_AIR_SERVER_DESC                "AirVPN server name <pattern>"
#define BT_CLIENT_OPTION_AIR_COUNTRY_SHORT              "Z"
#define BT_CLIENT_OPTION_AIR_COUNTRY                    "air-country"
#define BT_CLIENT_OPTION_AIR_COUNTRY_DESC               "AirVPN country or continent <pattern|country ISO code|continent code>"
#define BT_CLIENT_OPTION_AIR_VPN_TYPE_SHORT             "f"
#define BT_CLIENT_OPTION_AIR_VPN_TYPE                   "air-vpn-type"
#define BT_CLIENT_OPTION_AIR_VPN_TYPE_DESC              "VPN type for AirVPN connection <wireguard|openvpn>"
#define BT_CLIENT_OPTION_AIR_TLS_MODE_SHORT             "T"
#define BT_CLIENT_OPTION_AIR_TLS_MODE                   "air-tls-mode"
#define BT_CLIENT_OPTION_AIR_TLS_MODE_DESC              "TLS mode for AirVPN connection <auto|auth|crypt>"
#define BT_CLIENT_OPTION_AIR_IPV6_SHORT                 "V"
#define BT_CLIENT_OPTION_AIR_IPV6                       "air-ipv6"
#define BT_CLIENT_OPTION_AIR_IPV6_DESC                  "IPv6 mode for AirVPN connection <on|off>"
#define BT_CLIENT_OPTION_AIR_USER_SHORT                 "U"
#define BT_CLIENT_OPTION_AIR_USER                       "air-user"
#define BT_CLIENT_OPTION_AIR_USER_DESC                  "AirVPN user name"
#define BT_CLIENT_OPTION_AIR_PASSWORD_SHORT             "P"
#define BT_CLIENT_OPTION_AIR_PASSWORD                   "air-password"
#define BT_CLIENT_OPTION_AIR_PASSWORD_DESC              "AirVPN user password"
#define BT_CLIENT_OPTION_AIR_KEY_SHORT                  "F"
#define BT_CLIENT_OPTION_AIR_KEY                        "air-key"
#define BT_CLIENT_OPTION_AIR_KEY_DESC                   "AirVPN user key <name>"
#define BT_CLIENT_OPTION_AIR_KEY_LIST_SHORT             "K"
#define BT_CLIENT_OPTION_AIR_KEY_LIST                   "air-key-list"
#define BT_CLIENT_OPTION_AIR_KEY_LIST_DESC              "AirVPN user key list"
#define BT_CLIENT_OPTION_AIR_SAVE_SHORT                 "W"
#define BT_CLIENT_OPTION_AIR_SAVE                       "air-save"
#define BT_CLIENT_OPTION_AIR_SAVE_DESC                  "Save an AirVPN server, country profile or user key to local file <file_name>"
#define BT_CLIENT_OPTION_AIR_KEY_LOAD_SHORT             "Q"
#define BT_CLIENT_OPTION_AIR_KEY_LOAD                   "air-key-load"
#define BT_CLIENT_OPTION_AIR_KEY_LOAD_DESC              "Load (use) AirVPN user key from local file <file_name>"
#define BT_CLIENT_OPTION_AIR_INFO_SHORT                 "I"
#define BT_CLIENT_OPTION_AIR_INFO                       "air-info"
#define BT_CLIENT_OPTION_AIR_INFO_DESC                  "AirVPN info (requires --air-server or --air-country)"
#define BT_CLIENT_OPTION_AIR_LIST_SHORT                 "L"
#define BT_CLIENT_OPTION_AIR_LIST                       "air-list"
#define BT_CLIENT_OPTION_AIR_LIST_DESC                  "AirVPN server or country list <pattern>"
#define BT_CLIENT_OPTION_AIR_WHITE_SERVER_LIST_SHORT    "G"
#define BT_CLIENT_OPTION_AIR_WHITE_SERVER_LIST          "air-white-server-list"
#define BT_CLIENT_OPTION_AIR_WHITE_SERVER_LIST_DESC     "AirVPN white server list <list>"
#define BT_CLIENT_OPTION_AIR_BLACK_SERVER_LIST_SHORT    "M"
#define BT_CLIENT_OPTION_AIR_BLACK_SERVER_LIST          "air-black-server-list"
#define BT_CLIENT_OPTION_AIR_BLACK_SERVER_LIST_DESC     "AirVPN black server list <list>"
#define BT_CLIENT_OPTION_AIR_WHITE_COUNTRY_LIST_SHORT   "J"
#define BT_CLIENT_OPTION_AIR_WHITE_COUNTRY_LIST         "air-white-country-list"
#define BT_CLIENT_OPTION_AIR_WHITE_COUNTRY_LIST_DESC    "AirVPN white country list <list>"
#define BT_CLIENT_OPTION_AIR_BLACK_COUNTRY_LIST_SHORT   "X"
#define BT_CLIENT_OPTION_AIR_BLACK_COUNTRY_LIST         "air-black-country-list"
#define BT_CLIENT_OPTION_AIR_BLACK_COUNTRY_LIST_DESC    "AirVPN black country list <list>"
#define BT_CLIENT_OPTION_AIR_6TO4_SHORT                 "B"
#define BT_CLIENT_OPTION_AIR_6TO4                       "air-6to4"
#define BT_CLIENT_OPTION_AIR_6TO4_DESC                  "Enable IPv6 over IPv4 for AirVPN connection <on|off>"
#define BT_CLIENT_OPTION_RESPONSE_SHORT                 "r"
#define BT_CLIENT_OPTION_RESPONSE                       "response"
#define BT_CLIENT_OPTION_RESPONSE_DESC                  "Static response"
#define BT_CLIENT_OPTION_DC_SHORT                       "D"
#define BT_CLIENT_OPTION_DC                             "dc"
#define BT_CLIENT_OPTION_DC_DESC                        "Dynamic challenge/response cookie"
#define BT_CLIENT_OPTION_PROTO_SHORT                    "p"
#define BT_CLIENT_OPTION_PROTO                          "proto"
#define BT_CLIENT_OPTION_PROTO_DESC                     "Protocol override <udp|tcp>"
#define BT_CLIENT_OPTION_ALLOWUAF_SHORT                 "6"
#define BT_CLIENT_OPTION_ALLOWUAF                       "allowuaf"
#define BT_CLIENT_OPTION_ALLOWUAF_DESC                  "Allow unused address families <yes|no|default>"
#define BT_CLIENT_OPTION_SERVER_SHORT                   "s"
#define BT_CLIENT_OPTION_SERVER                         "server"
#define BT_CLIENT_OPTION_SERVER_DESC                    "Server override"
#define BT_CLIENT_OPTION_PORT_SHORT                     "R"
#define BT_CLIENT_OPTION_PORT                           "port"
#define BT_CLIENT_OPTION_PORT_DESC                      "Port override"
#define BT_CLIENT_OPTION_CIPHER_SHORT                   "C"
#define BT_CLIENT_OPTION_CIPHER                         "cipher"
#define BT_CLIENT_OPTION_CIPHER_DESC                    "Encrypt packets with specific cipher algorithm <alg>"
#define BT_CLIENT_OPTION_LIST_DATA_CIPHERS_SHORT        ""
#define BT_CLIENT_OPTION_LIST_DATA_CIPHERS              "list-data-ciphers"
#define BT_CLIENT_OPTION_LIST_DATA_CIPHERS_DESC         "List supported data ciphers"
#define BT_CLIENT_OPTION_TCP_QUEUE_LIMIT_SHORT          "l"
#define BT_CLIENT_OPTION_TCP_QUEUE_LIMIT                "tcp-queue-limit"
#define BT_CLIENT_OPTION_TCP_QUEUE_LIMIT_DESC           "Size of TCP packet queue <1-65535> default " TO_STRING(TCP_QUEUE_LIMIT_DEFAULT)
#define BT_CLIENT_OPTION_NCP_DISABLE_SHORT              "n"
#define BT_CLIENT_OPTION_NCP_DISABLE                    "ncp-disable"
#define BT_CLIENT_OPTION_NCP_DISABLE_DESC               "Disable negotiable crypto parameters"
#define BT_CLIENT_OPTION_IGNORE_DNS_PUSH_SHORT          "i"
#define BT_CLIENT_OPTION_IGNORE_DNS_PUSH                "ignore-dns-push"
#define BT_CLIENT_OPTION_IGNORE_DNS_PUSH_DESC           "Ignore DNS push request and use system DNS settings"
#define BT_CLIENT_OPTION_NETWORK_LOCK_SHORT             "N"
#define BT_CLIENT_OPTION_NETWORK_LOCK                   "network-lock"
#define BT_CLIENT_OPTION_NETWORK_LOCK_DESC              "Network filter and lock mode <on|iptables|nftables|pf|off> default on"
#define BT_CLIENT_OPTION_GUI_VERSION_SHORT              "E"
#define BT_CLIENT_OPTION_GUI_VERSION                    "gui-version"
#define BT_CLIENT_OPTION_GUI_VERSION_DESC               "Set custom gui version <text>"
#define BT_CLIENT_OPTION_TIMEOUT_SHORT                  "t"
#define BT_CLIENT_OPTION_TIMEOUT                        "timeout"
#define BT_CLIENT_OPTION_TIMEOUT_DESC                   "Connection timeout <seconds>"
#define BT_CLIENT_OPTION_COMPRESS_SHORT                 "c"
#define BT_CLIENT_OPTION_COMPRESS                       "compress"
#define BT_CLIENT_OPTION_COMPRESS_DESC                  "Compression mode <yes|no|asym>"
#define BT_CLIENT_OPTION_PK_PASSWORD_SHORT              "z"
#define BT_CLIENT_OPTION_PK_PASSWORD                    "pk-password"
#define BT_CLIENT_OPTION_PK_PASSWORD_DESC               "Private key password"
#define BT_CLIENT_OPTION_TVM_OVERRIDE_SHORT             "m"
#define BT_CLIENT_OPTION_TVM_OVERRIDE                   "tvm-override"
#define BT_CLIENT_OPTION_TVM_OVERRIDE_DESC              "tls-version-min override <disabled|default|tls_1_x>"
#define BT_CLIENT_OPTION_PROXY_HOST_SHORT               "y"
#define BT_CLIENT_OPTION_PROXY_HOST                     "proxy-host"
#define BT_CLIENT_OPTION_PROXY_HOST_DESC                "HTTP proxy hostname/IP"
#define BT_CLIENT_OPTION_PROXY_PORT_SHORT               "q"
#define BT_CLIENT_OPTION_PROXY_PORT                     "proxy-port"
#define BT_CLIENT_OPTION_PROXY_PORT_DESC                "HTTP proxy port"
#define BT_CLIENT_OPTION_PROXY_USERNAME_SHORT           "u"
#define BT_CLIENT_OPTION_PROXY_USERNAME                 "proxy-username"
#define BT_CLIENT_OPTION_PROXY_USERNAME_DESC            "HTTP proxy username"
#define BT_CLIENT_OPTION_PROXY_PASSWORD_SHORT           "w"
#define BT_CLIENT_OPTION_PROXY_PASSWORD                 "proxy-password"
#define BT_CLIENT_OPTION_PROXY_PASSWORD_DESC            "HTTP proxy password"
#define BT_CLIENT_OPTION_PEER_INFO_SHORT                "e"
#define BT_CLIENT_OPTION_PEER_INFO                      "peer-info"
#define BT_CLIENT_OPTION_PEER_INFO_DESC                 "Peer information key/value list in the form K1=V1,K2=V2,..."
#define BT_CLIENT_OPTION_GREMLIN_SHORT                  "g"
#define BT_CLIENT_OPTION_GREMLIN                        "gremlin"
#define BT_CLIENT_OPTION_GREMLIN_DESC                   "Gremlin information <send_delay_ms|recv_delay_ms|send_drop_prob|recv_drop_prob>"
#define BT_CLIENT_OPTION_PROXY_BASIC_SHORT              "b"
#define BT_CLIENT_OPTION_PROXY_BASIC                    "proxy-basic"
#define BT_CLIENT_OPTION_PROXY_BASIC_DESC               "Allow HTTP basic auth"
#define BT_CLIENT_OPTION_ALT_PROXY_SHORT                "A"
#define BT_CLIENT_OPTION_ALT_PROXY                      "alt-proxy"
#define BT_CLIENT_OPTION_ALT_PROXY_DESC                 "Enable alternative proxy module"
#define BT_CLIENT_OPTION_DCO_SHORT                      "d"
#define BT_CLIENT_OPTION_DCO                            "dco"
#define BT_CLIENT_OPTION_DCO_DESC                       "Enable data channel offload"
#define BT_CLIENT_OPTION_CACHE_PASSWORD_SHORT           "H"
#define BT_CLIENT_OPTION_CACHE_PASSWORD                 "cache-password"
#define BT_CLIENT_OPTION_CACHE_PASSWORD_DESC            "Cache password"
#define BT_CLIENT_OPTION_NO_CERT_SHORT                  "x"
#define BT_CLIENT_OPTION_NO_CERT                        "no-cert"
#define BT_CLIENT_OPTION_NO_CERT_DESC                   "Disable client certificate"
#define BT_CLIENT_OPTION_PERSIST_TUN_SHORT              "j"
#define BT_CLIENT_OPTION_PERSIST_TUN                    "persist-tun"
#define BT_CLIENT_OPTION_PERSIST_TUN_DESC               "Keep TUN interface open across reconnections"
#define BT_CLIENT_OPTION_DEF_KEYDIR_SHORT               "k"
#define BT_CLIENT_OPTION_DEF_KEYDIR                     "def-keydir"
#define BT_CLIENT_OPTION_DEF_KEYDIR_DESC                "Default key direction <bi|0|1>"
#define BT_CLIENT_OPTION_HELP_SHORT                     "h"
#define BT_CLIENT_OPTION_HELP                           "help"
#define BT_CLIENT_OPTION_HELP_DESC                      "Show the help page"
#define BT_CLIENT_OPTION_VERSION_SHORT                  "v"
#define BT_CLIENT_OPTION_VERSION                        "version"
#define BT_CLIENT_OPTION_VERSION_DESC                   "Show version info"
#define BT_CLIENT_OPTION_AUTO_SESS_SHORT                "a"
#define BT_CLIENT_OPTION_AUTO_SESS                      "auto-sess"
#define BT_CLIENT_OPTION_AUTO_SESS_DESC                 "Request autologin session"
#define BT_CLIENT_OPTION_AUTH_RETRY_SHORT               "Y"
#define BT_CLIENT_OPTION_AUTH_RETRY                     "auth-retry"
#define BT_CLIENT_OPTION_AUTH_RETRY_DESC                "Retry connection on auth failure"
#define BT_CLIENT_OPTION_TCPROF_OVERRIDE_SHORT          "o"
#define BT_CLIENT_OPTION_TCPROF_OVERRIDE                "tcprof-override"
#define BT_CLIENT_OPTION_TCPROF_OVERRIDE_DESC           "tls-cert-profile override <legacy|preferred|...>"
#define BT_CLIENT_OPTION_SSL_DEBUG_SHORT                ""
#define BT_CLIENT_OPTION_SSL_DEBUG                      "ssl-debug"
#define BT_CLIENT_OPTION_SSL_DEBUG_DESC                 "SSL debug level"
#define BT_CLIENT_OPTION_EPKI_CERT_SHORT                ""
#define BT_CLIENT_OPTION_EPKI_CERT                      "epki-cert"
#define BT_CLIENT_OPTION_EPKI_CERT_DESC                 "Simulate external PKI certificate"
#define BT_CLIENT_OPTION_EPKI_CA_SHORT                  ""
#define BT_CLIENT_OPTION_EPKI_CA                        "epki-ca"
#define BT_CLIENT_OPTION_EPKI_CA_DESC                   "Simulate external PKI certificate supporting intermediate/root certificates"
#define BT_CLIENT_OPTION_EPKI_KEY_SHORT                 ""
#define BT_CLIENT_OPTION_EPKI_KEY                       "epki-key"
#define BT_CLIENT_OPTION_EPKI_KEY_DESC                  "Simulate external PKI private key"
#define BT_CLIENT_OPTION_REMOTE_OVERRIDE_SHORT          ""
#define BT_CLIENT_OPTION_REMOTE_OVERRIDE                "remote-override"
#define BT_CLIENT_OPTION_REMOTE_OVERRIDE_DESC           "Command to run to generate next remote <returning host|ip|port|proto>"
#define BT_CLIENT_OPTION_RECOVER_NETWORK_SHORT          ""
#define BT_CLIENT_OPTION_RECOVER_NETWORK                "recover-network"
#define BT_CLIENT_OPTION_RECOVER_NETWORK_DESC           "Recover network settings after a crash or unexpected exit"
#define BT_CLIENT_OPTION_DISCONNECT_SHORT               ""
#define BT_CLIENT_OPTION_DISCONNECT                     "disconnect"
#define BT_CLIENT_OPTION_DISCONNECT_DESC                "Disconnect the VPN session, including Bluetit boot connection"
#define BT_CLIENT_OPTION_PAUSE_SHORT                    ""
#define BT_CLIENT_OPTION_PAUSE                          "pause"
#define BT_CLIENT_OPTION_PAUSE_DESC                     "Pause the VPN connection, including Bluetit boot connection"
#define BT_CLIENT_OPTION_RESUME_SHORT                   ""
#define BT_CLIENT_OPTION_RESUME                         "resume"
#define BT_CLIENT_OPTION_RESUME_DESC                    "Resume a paused VPN connection, including Bluetit boot connection"
#define BT_CLIENT_OPTION_RECONNECT_SHORT                ""
#define BT_CLIENT_OPTION_RECONNECT                      "reconnect"
#define BT_CLIENT_OPTION_RECONNECT_DESC                 "Reconnect the VPN connection, including Bluetit boot connection"
#define BT_CLIENT_OPTION_BLUETIT_STATUS_SHORT           ""
#define BT_CLIENT_OPTION_BLUETIT_STATUS                 "bluetit-status"
#define BT_CLIENT_OPTION_BLUETIT_STATUS_DESC            "Show Bluetit status and exit"
#define BT_CLIENT_OPTION_BLUETIT_STATS_SHORT            ""
#define BT_CLIENT_OPTION_BLUETIT_STATS                  "bluetit-stats"
#define BT_CLIENT_OPTION_BLUETIT_STATS_DESC             "Show Bluetit connection statistics and exit"
