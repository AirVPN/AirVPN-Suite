/*
 * wireguardclient.hpp
 *
 * This file is part of the AirVPN Suite for Linux and macOS.
 * Copyright (C) 2019-2023 AirVPN (support@airvpn.org) / https://airvpn.org
 *
 * Developed by ProMIND
 *
 * This is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with the AirVPN Suite. If not, see <http://www.gnu.org/licenses/>.
 *
 */

#pragma once

#include <string>
#include <sstream>
#include <vector>
#include "wireguard.h"
#include "localnetwork.hpp"

class WireGuardClient
{
    public:

    WireGuardClient();
    ~WireGuardClient();
    
    bool setConfiguration(std::string configuration);
    
    private:

    enum ConfigSection
    {
        SECTION_UNDEFINED,
        SECTION_INTERFACE,
        SECTION_PEER
    };

    std::string privateKey, publicKey, presharedKey;
    int endPointPort, persistentKeepalive, customMTU;
    std::vector<IPAddress> dnsAddress;
    std::vector<IPAddress> ipAddress;
    std::vector<IPAddress> allowedIpAddress;
    IPAddress endPoint;
    std::string wireGuardProfile;
    
    void log(std::string s);
    void log(std::ostringstream os);
    void syslog(std::string s);
    void syslog(std::ostringstream os);
};
