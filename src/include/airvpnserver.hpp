/*
 * airvpnserver.hpp
 *
 * This file is part of AirVPN's hummingbird Linux/macOS OpenVPN Client software.
 * Copyright (C) 2019-2022 AirVPN (support@airvpn.org) / https://airvpn.org
 *
 * Developed by ProMIND
 *
 * This is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Hummingbird. If not, see <http://www.gnu.org/licenses/>.
 *
 */

#pragma once

#include "countrycontinent.hpp"

#include <string>
#include <vector>
#include <map>

class AirVPNServer
{
    public:

    const static int WORST_SCORE = 99999999;

    AirVPNServer(const std::string &resourceDirectory);
    ~AirVPNServer();

    std::string getName();
    void setName(const std::string &n);
    std::string getContinent();
    void setContinent(const std::string &c);
    std::string getCountryCode();
    std::string getCountryName();
    void setCountryCode(const std::string &c);
    std::string getRegion();
    void setRegion(const std::string &r);
    std::string getLocation();
    void setLocation(const std::string &l);
    long long getBandWidth();
    void setBandWidth(long long b);
    long long getMaxBandWidth();
    void setMaxBandWidth(long long b);
    long long getEffectiveBandWidth();
    int getUsers();
    void setUsers(int u);
    int getMaxUsers();
    void setMaxUsers(int u);
    double getUserLoad();
    bool isIPv4Available();
    void setIPv4Available(bool s);
    bool isIPv6Available();
    void setIPv6Available(bool s);
    std::map<int, std::string> getEntryIPv4();
    void setEntryIPv4(const std::map<int, std::string> &l);
    void setEntryIPv4(int entry, const std::string &IPv4);
    std::map<int, std::string> getEntryIPv6();
    void setEntryIPv6(const std::map<int, std::string> &l);
    void setEntryIPv6(int entry, const std::string &IPv6);
    std::string getExitIPv4();
    void setExitIPv4(const std::string &e);
    std::string getExitIPv6();
    void setExitIPv6(const std::string &e);
    std::string getWarningOpen();
    void setWarningOpen(const std::string &w);
    std::string getWarningClosed();
    void setWarningClosed(const std::string &w);
    int getScoreBase();
    void setScoreBase(int s);
    bool getSupportCheck();
    void setSupportCheck(bool s);
    int getLoad();
    bool isOpenVPNAvailable();
    void setOpenVPNAvailable(bool b);
    std::vector<int> getOpenVPNTlsCiphers();
    std::string getOpenVPNTlsCipherNames();
    void setOpenVPNTlsCiphers(const std::vector<int> &list);
    bool hasOpenVPNTlsCipher(int c);
    bool hasOpenVPNTlsCipher(const std::vector<int> &cipherList);
    std::vector<int> getOpenVPNTlsSuiteCiphers();
    std::string getOpenVPNTlsSuiteCipherNames();
    void setOpenVPNTlsSuiteCiphers(const std::vector<int> &list);
    bool hasOpenVPNTlsSuiteCipher(int c);
    bool hasOpenVPNTlsSuiteCipher(const std::vector<int> &cipherList);
    std::vector<int> getOpenVPNDataCiphers();
    std::string getOpenVPNDataCipherNames();
    void setOpenVPNDataCiphers(std::vector<int> list);
    bool hasOpenVPNDataCipher(int c);
    bool hasOpenVPNDataCipher(const std::vector<int> &cipherList);
    bool isWireGuardAvailable();
    void setWireGuardAvailable(bool b);
    std::string getWireGuardCipherNames();
    void computeServerScore();
    int getScore();
    void setScore(int s);
    std::string getOpenVpnDirectives();
    void setOpenVpnDirectives(const std::string &o);
    bool isAvailable();
    bool isAvailable(const std::vector<int> &cipherList);
    bool isPerfectForwardSecrecyAvailable();
    void setPerfectForwardSecrecyAvailable(bool b);

    private:

    CountryContinent *countryContinent;

    std::string name;
    std::string continent;
    std::string countryCode;
    std::string region;
    std::string location;
    long long bandWidth;
    long long maxBandWidth;
    int users;
    int maxUsers;
    bool IPv4Available;
    bool IPv6Available;
    std::map<int, std::string> entryIPv4;
    std::map<int, std::string> entryIPv6;
    std::string exitIPv4;
    std::string exitIPv6;
    std::string warningOpen;
    std::string warningClosed;
    int scoreBase;
    int score;
    std::string openVpnDirectives;
    bool supportCheck;
    int group;
    bool openVPNAvailable;
    bool wireGuardAvailable;
    std::vector<int> openVPNTlsCiphers;
    std::vector<int> openVPNTlsSuiteCiphers;
    std::vector<int> openVPNDataCiphers;
    bool perfectForwardSecrecyAvailable;

    void init();
};
