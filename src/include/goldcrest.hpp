/*
 * goldcrest.hpp
 *
 * This file is part of AirVPN's Linux/macOS OpenVPN Client software.
 * Copyright (C) 2019-2022 AirVPN (support@airvpn.org) / https://airvpn.org
 *
 * Developed by ProMIND
 *
 * This is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Hummingbird. If not, see <http://www.gnu.org/licenses/>.
 *
 */

#pragma once

#include "btcommon.h"
#include "dbusconnector.hpp"
#include "rcparser.hpp"

#define GOLDCREST_NAME              "Goldcrest - AirVPN Bluetit Client"
#define GOLDCREST_VERSION           "1.3.0"
#define GOLDCREST_RELEASE_DATE      "1 June 2023"
#define GOLDCREST_FULL_NAME         GOLDCREST_NAME " " GOLDCREST_VERSION
#define XDG_CONFIG_DIR              "/.config"
#define GOLDCREST_XDG_CONFIG_FILE   XDG_CONFIG_DIR "/goldcrest.rc"
#define GOLDCREST_RC_FILE           "/.goldcrest.rc"

#define RC_DIRECTIVE_AIRVPN_SERVER          "air-server"
#define RC_DIRECTIVE_AIRVPN_VPN_TYPE        "air-vpn-type"
#define RC_DIRECTIVE_AIRVPN_TLS_MODE        "air-tls-mode"
#define RC_DIRECTIVE_AIRVPN_IPV6            "air-ipv6"
#define RC_DIRECTIVE_AIRVPN_6TO4            "air-6to4"
#define RC_DIRECTIVE_AIRVPN_USERNAME        "air-user"
#define RC_DIRECTIVE_AIRVPN_PASSWORD        "air-password"
#define RC_DIRECTIVE_AIRVPN_KEY             "air-key"
#define RC_DIRECTIVE_AIRWHITESERVERLIST     "air-white-server-list"
#define RC_DIRECTIVE_AIRBLACKSERVERLIST     "air-black-server-list"
#define RC_DIRECTIVE_AIRWHITECOUNTRYLIST    "air-white-country-list"
#define RC_DIRECTIVE_AIRBLACKCOUNTRYLIST    "air-black-country-list"
#define RC_DIRECTIVE_GC_CIPHER              "cipher"
#define RC_DIRECTIVE_GC_PROTO               "proto"
#define RC_DIRECTIVE_GC_SERVER              "server"
#define RC_DIRECTIVE_GC_PORT                "port"
#define RC_DIRECTIVE_GC_TCP_QUEUE_LIMIT     "tcp-queue-limit"
#define RC_DIRECTIVE_GC_NCP_DISABLE         "ncp-disable"
#define RC_DIRECTIVE_GC_NETWORK_LOCK        "network-lock"
#define RC_DIRECTIVE_GC_IGNORE_DNS_PUSH     "ignore-dns-push"
#define RC_DIRECTIVE_GC_ALLOWUAF            "allowuaf"
#define RC_DIRECTIVE_GC_TIMEOUT             "timeout"
#define RC_DIRECTIVE_GC_COMPRESS            "compress"
#define RC_DIRECTIVE_GC_PROXY_HOST          "proxy-host"
#define RC_DIRECTIVE_GC_PROXY_PORT          "proxy-port"
#define RC_DIRECTIVE_GC_PROXY_USERNAME      "proxy-username"
#define RC_DIRECTIVE_GC_PROXY_PASSWORD      "proxy-password"
#define RC_DIRECTIVE_GC_PROXY_BASIC         "proxy-basic"
#define RC_DIRECTIVE_GC_ALT_PROXY           "alt-proxy"
#define RC_DIRECTIVE_GC_PERSIST_TUN         "persist-tun"
#define RC_DIRECTIVE_GC_CONN_STAT_SECS      "conn-stat-interval"

#define GOLDCREST_RC_TEMPLATE       "#\n" \
                                    "# goldcrest runcontrol file\n" \
                                    "#\n" \
                                    "\n" \
                                    "# " RC_DIRECTIVE_AIRVPN_SERVER "            <server_name>\n" \
                                    "# " RC_DIRECTIVE_AIRVPN_VPN_TYPE "          <openvpn|wireguard>\n" \
                                    "# " RC_DIRECTIVE_AIRVPN_TLS_MODE "          <auto|auth|crypt>\n" \
                                    "# " RC_DIRECTIVE_AIRVPN_IPV6 "              <on|off>\n" \
                                    "# " RC_DIRECTIVE_AIRVPN_6TO4 "              <on|off>\n" \
                                    "# " RC_DIRECTIVE_AIRVPN_USERNAME "              <username>\n" \
                                    "# " RC_DIRECTIVE_AIRVPN_PASSWORD "          <password>\n" \
                                    "# " RC_DIRECTIVE_AIRVPN_KEY "               <name>\n" \
                                    "# " RC_DIRECTIVE_GC_CIPHER "                <cipher_name>\n" \
                                    "# " RC_DIRECTIVE_GC_PROTO "                 <udp|tcp>\n" \
                                    "# " RC_DIRECTIVE_GC_SERVER "                <server_ip|server_url>\n" \
                                    "# " RC_DIRECTIVE_GC_PORT "                  <port>\n" \
                                    "# " RC_DIRECTIVE_GC_TCP_QUEUE_LIMIT "       <n>\n" \
                                    "# " RC_DIRECTIVE_GC_NCP_DISABLE "           <yes|no>\n" \
                                    "# " RC_DIRECTIVE_GC_NETWORK_LOCK "          <on|iptables|nftables|pf|off>\n" \
                                    "# " RC_DIRECTIVE_GC_IGNORE_DNS_PUSH "       <yes|no>\n" \
                                    "# " RC_DIRECTIVE_GC_ALLOWUAF "              <yes|no|default>\n" \
                                    "# " RC_DIRECTIVE_GC_TIMEOUT "               <seconds>\n" \
                                    "# " RC_DIRECTIVE_GC_COMPRESS "              <yes|no|asym>\n" \
                                    "# " RC_DIRECTIVE_GC_PROXY_HOST "            <host_ip|host_url>\n" \
                                    "# " RC_DIRECTIVE_GC_PROXY_PORT "            <port>\n" \
                                    "# " RC_DIRECTIVE_GC_PROXY_USERNAME "        <proxy_username>\n" \
                                    "# " RC_DIRECTIVE_GC_PROXY_PASSWORD "        <proxy_password>\n" \
                                    "# " RC_DIRECTIVE_GC_PROXY_BASIC "           <yes|no>\n" \
                                    "# " RC_DIRECTIVE_GC_ALT_PROXY "             <yes|no>\n" \
                                    "# " RC_DIRECTIVE_GC_PERSIST_TUN "           <on|off>\n" \
                                    "# " RC_DIRECTIVE_GC_CONN_STAT_SECS "    <seconds>\n"

#define DEFAULT_CONN_STAT_SECONDS   60
#define PAD_OPTION_SIZE             30

struct DNSEntry
{
    std::string address;
    std::string type;
};

void signal_handler(int sig);
void cleanup_and_exit(const char *msg, int exit_value);
int get_server_status();
DBusResponse *send_method_to_server(const char *method);
DBusResponse *send_method_with_args_to_server(const char *method, const std::vector<std::string> &args);
void dbus_message_loop_until(const std::string &exit_event);
bool check_airvpn_mode(int argc, char **argv);
bool check_airvpn_connect(int argc, char **argv);
bool check_airvpn_key_load(int argc, char **argv);
void check_airvpn_credentials(int argc, char **argv);
bool read_configuration();
void airvpn_server_info(DBusResponse *response);
void airvpn_server_list(DBusResponse *response);
void airvpn_country_info(DBusResponse *response);
void airvpn_country_list(DBusResponse *response);
void airvpn_key_list(DBusResponse *response);
void airvpn_save(DBusResponse *response);
void log(const char *msg);
void usage();
std::string formatOption(std::string longOption, std::string shortOption = "", std::string description = "");
void aboutDevelopmentCredits();
void connection_stats(int interval_seconds, const std::future<void> &future);
void show_connection_stats();
void get_pushed_dns();
void list_data_ciphers();
std::string vpn_type_description(const std::string &vpn_type);
