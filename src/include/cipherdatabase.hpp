/*
 * cipherdatabase.hpp
 *
 * This file is part of AirVPN's hummingbird Linux/macOS OpenVPN Client software.
 * Copyright (C) 2019-2022 AirVPN (support@airvpn.org) / https://airvpn.org
 *
 * Developed by ProMIND
 *
 * This is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Hummingbird. If not, see <http://www.gnu.org/licenses/>.
 *
 */

#pragma once

#include <string>
#include <vector>
#include <map>

class CipherDatabase
{
    public:

    const int CIPHER_NOT_FOUND = -1;
    const int INVALID_CIPHER = -2;

    enum CipherType
    {
        TLS,
        TLS_SUITE,
        DATA
    };

    CipherDatabase();
    ~CipherDatabase();

    void reset();
    void addCipherString(const std::string &ciphers, CipherType type);
    int addCipher(const std::string &cipher, CipherType type);
    int getCode(const std::string &cipher, CipherType type);
    std::vector<int> getCodeArrayList(const std::string &ciphers, CipherType type);
    std::string getMatchingCiphers(const std::string &pattern, CipherType type);
    static std::vector<int> getMatchingCiphersArrayList(const std::string &pattern, CipherType type);
    static std::string getCipherNameList(CipherType type, const std::vector<int> &cipher);
    static std::string getCipher(int code, CipherType type);
    static std::map<std::string, int> *getCipherDB(CipherType type);

    private:

    static std::map<std::string, int> tlsCipherDB;
    static std::map<std::string, int> tlsSuiteCipherDB;
    static std::map<std::string, int> dataCipherDB;
    int lastCode;
};
