/*
 * openvpnclient.hpp
 *
 * This file is part of AirVPN's Linux/macOS OpenVPN Client software.
 * Copyright (C) 2019-2022 AirVPN (support@airvpn.org) / https://airvpn.org
 *
 * Developed by ProMIND
 *
 * This is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Hummingbird. If not, see <http://www.gnu.org/licenses/>.
 *
 */

#pragma once

#include <stdlib.h>
#include <string>
#include <iostream>
#include <sstream>
#include <algorithm>
#include <thread>
#include <memory>
#include <mutex>
#include <vector>
#include <map>
#include <dirent.h>
#include <errno.h>
#include <resolv.h>
#include <arpa/inet.h>
#include <openvpn/common/platform.hpp>

#define USE_TUN_BUILDER

#if defined(OPENVPN_PLATFORM_LINUX)

    #include "dnsmanager.hpp"
    #include "loadmod.h"

#endif

#ifdef OPENVPN_PLATFORM_MAC

    #include <CoreFoundation/CFBundle.h>
    #include <ApplicationServices/ApplicationServices.h>
    #include <SystemConfiguration/SystemConfiguration.h>

#endif

// don't export core symbols
#define OPENVPN_CORE_API_VISIBILITY_HIDDEN

#define OPENVPN_DEBUG_VERBOSE_ERRORS
// #define OPENVPN_DEBUG_CLIPROTO

#include "execproc.h"
#include "localnetwork.hpp"
#include "netfilter.hpp"

// If enabled, don't direct ovpn3 core logging to
// ClientAPI::OpenVPNClient::log() virtual method.
// Instead, logging will go to LogBaseSimple::log().
// In this case, make sure to define:
//   LogBaseSimple log;
// at the top of your main() function to receive
// log messages from all threads.
// Also, note that the OPENVPN_LOG_GLOBAL setting
// MUST be consistent across all compilation units.

#ifdef OPENVPN_USE_LOG_BASE_SIMPLE

    #define OPENVPN_LOG_GLOBAL // use global rather than thread-local  object pointer

    #include <openvpn/log/logbasesimple.hpp>

#endif

#include <client/ovpncli.cpp>

#if defined(OPENVPN_PLATFORM_LINUX)

    // use SITNL by default
    #ifndef OPENVPN_USE_IPROUTE2

        #define OPENVPN_USE_SITNL

    #endif

    #include <openvpn/tun/linux/client/tuncli.hpp>

    // we use a static polymorphism and define a
    // platform-specific TunSetup class, responsible
    // for setting up tun device

    #define TUN_CLASS_SETUP TunLinuxSetup::Setup<TUN_LINUX>

#elif defined(OPENVPN_PLATFORM_MAC)

    #include <openvpn/tun/mac/client/tuncli.hpp>

    #define TUN_CLASS_SETUP TunMac::Setup

#endif

// should be included before other openvpn includes,
// with the exception of openvpn/log includes

#include <openvpn/common/options.hpp>
#include <openvpn/common/exception.hpp>
#include <openvpn/common/string.hpp>
#include <openvpn/common/signal.hpp>
#include <openvpn/common/file.hpp>
#include <openvpn/common/getopt.hpp>
#include <openvpn/common/getpw.hpp>
#include <openvpn/common/cleanup.hpp>
#include <openvpn/time/timestr.hpp>
#include <openvpn/ssl/peerinfo.hpp>
#include <openvpn/ssl/sslchoose.hpp>

#ifdef OPENVPN_REMOTE_OVERRIDE

    #include <openvpn/common/process.hpp>

#endif

#if defined(USE_MBEDTLS)

    #include <openvpn/mbedtls/util/pkcs1.hpp>

#endif

#define DEFAULT_TUN_MTU     1500

using namespace openvpn;

class OpenVpnClientBase : public ClientAPI::OpenVPNClient, public LocalNetwork
{
    protected:

    NetFilter *netFilter = nullptr;

#if defined(OPENVPN_PLATFORM_LINUX)
    DNSManager *dnsManager = nullptr;
#endif

    std::vector<IPAddress> dnsTable;
    std::vector<IPAddress> systemDnsTable;
    std::vector<IPAddress> remoteServerIpList;
    TUN_CLASS_SETUP::Config tunnelConfig;
    ClientAPI::Config config;
    ClientAPI::EvalConfig evalConfig;
    OptionList optionList;
    bool dnsHasBeenPushed = false;
    bool gatewayHasBeenRerouted = false;
    bool IPv4GatewayHasBeenRerouted = false;
    bool IPv6GatewayHasBeenRerouted = false;
    NetFilter::Mode networkLockMode = NetFilter::Mode::UNKNOWN;
    bool dnsPushIgnored = false;
    bool netFilterIsPrivate = true;
    bool systemDNSallowedInNetFilter = false;

    std::string resourceDirectory;
    std::string dnsBackupFile;
    std::string resolvDotConfFile;

    bool saveSystemDNS();

    public:

    OpenVpnClientBase(NetFilter::Mode netFilterMode, const std::string &resDir, const std::string &dnsBkpFile, const std::string &resolvConfFile);
    OpenVpnClientBase(NetFilter *externalNetFilter, const std::string &resDir, const std::string &dnsBkpFile, const std::string &resolvConfFile);
    ~OpenVpnClientBase();

    void setup(const std::string &resDir, const std::string &dnsBkpFile, const std::string &resolvConfFile);

    bool allowSystemDNS();
    bool rejectSystemDNS();

    bool allowProxy();
    bool rejectProxy();

    bool tun_builder_new() override;
    bool tun_builder_set_layer(int layer) override;
    bool tun_builder_set_remote_address(const std::string &address, bool ipv6) override;
    bool tun_builder_add_address(const std::string &address, int prefix_length, const std::string &gateway, bool ipv6, bool net30) override;
    bool tun_builder_set_route_metric_default(int metric) override;
    bool tun_builder_reroute_gw(bool ipv4, bool ipv6, unsigned int flags) override;
    bool tun_builder_add_route(const std::string &address, int prefix_length, int metric, bool ipv6) override;
    bool tun_builder_exclude_route(const std::string& address, int prefix_length, int metric, bool ipv6) override;
    bool tun_builder_add_dns_server(const std::string &address, bool ipv6) override;
    bool tun_builder_add_search_domain(const std::string& domain) override;
    bool tun_builder_set_mtu(int mtu) override;
    bool tun_builder_set_session_name(const std::string &name) override;
    bool tun_builder_add_proxy_bypass(const std::string& bypass_host) override;
    bool tun_builder_set_proxy_auto_config_url(const std::string& url) override;
    bool tun_builder_set_proxy_http(const std::string& host, int port) override;
    bool tun_builder_set_proxy_https(const std::string& host, int port) override;
    bool tun_builder_add_wins_server(const std::string& address) override;
    bool tun_builder_set_allow_family(int af, bool allow) override;
    bool tun_builder_set_adapter_domain_suffix(const std::string& name) override;
    int tun_builder_establish() override;
    bool tun_builder_persist() override;
    const std::vector<std::string> tun_builder_get_local_networks(bool ipv6) override;
    void tun_builder_establish_lite() override;
    void tun_builder_teardown(bool disconnect) override;
    bool ignore_dns_push() override;

#ifdef ENABLE_OVPNDCO

    bool tun_builder_dco_available() override;
    int tun_builder_dco_enable(const std::string& dev_name) override;
    void tun_builder_dco_new_peer(uint32_t peer_id, uint32_t transport_fd, struct sockaddr *sa, socklen_t salen, IPv4::Addr &vpn4, IPv6::Addr &vpn6) override;
    void tun_builder_dco_set_peer(uint32_t peer_id, int keepalive_interval, int keepalive_timeout) override;
    void tun_builder_dco_del_peer(uint32_t peer_id) override;
    void tun_builder_dco_get_peer(uint32_t peer_id, bool sync) override;
    void tun_builder_dco_new_key(unsigned int key_slot, const KoRekey::KeyConfig* kc) override;
    void tun_builder_dco_swap_keys(uint32_t peer_id) override;
    void tun_builder_dco_del_key(uint32_t peer_id, unsigned int key_slot) override;
    void tun_builder_dco_establish() override;

#endif

    bool socket_protect(int socket, std::string remote, bool ipv6) override;

    void setConfig(ClientAPI::Config c);
    ClientAPI::Config getConfig();
    void setEvalConfig(ClientAPI::EvalConfig e);
    ClientAPI::EvalConfig getEvalConfig();
    void setNetworkLockMode(NetFilter::Mode mode);
    NetFilter::Mode getNetworkLockMode();
    void ignoreDnsPush(bool ignore);
    bool isDnsPushIgnored();
    std::vector<IPAddress> getPushedDns();

    static std::string openVPNInfo();
    static std::string openVPNCopyright();
    static std::string sslLibraryVersion();

    private:

    TUN_CLASS_SETUP::Ptr tunnelSetup = new TUN_CLASS_SETUP();

    TunBuilderCapture tunnelBuilderCapture;
};

class OpenVpnClient : public OpenVpnClientBase
{
    public:

    enum ClockTickAction
    {
        CT_UNDEF,
        CT_STOP,
        CT_RECONNECT,
        CT_PAUSE,
        CT_RESUME,
        CT_STATS,
    };

    enum Status
    {
        DISCONNECTED,
        CONNECTED,
        CONNECTING,
        RECONNECTING,
        PAUSED,
        RESUMING,
        CLIENT_RESTART,
        CONNECTION_TIMEOUT,
        INACTIVE_TIMEOUT
    };

    enum RestoreNetworkMode
    {
        FULL,
        DNS_ONLY,
        FIREWALL_ONLY
    };

    struct EventData
    {
        ClientAPI::Event vpnEvent;
        ClientAPI::ConnectionInfo connectionInfo;
        Status status;
        std::string message;
    };

    typedef void (*EventCallbackFunction)(EventData data);
    typedef void EventCallback;

    OpenVpnClient(NetFilter::Mode netFilterMode, const std::string &resDir, const std::string &dnsBkpFile, const std::string &resolvConfFile);
    OpenVpnClient(NetFilter *externalNetFilter, const std::string &resDir, const std::string &dnsBkpFile, const std::string &resolvConfFile);
    ~OpenVpnClient();

    Status getStatus();
    bool eventError();
    bool eventFatalError();

    bool is_dynamic_challenge() const;
    std::string dynamic_challenge_cookie();

    std::string epki_ca;
    std::string epki_cert;

#if defined(USE_MBEDTLS)

    MbedTLSPKI::PKContext epki_ctx; // external PKI context

#endif

    void set_clock_tick_action(const ClockTickAction action);
    void print_stats();
    std::map<std::string, std::string> get_connection_stats();

    bool restoreNetworkSettings(RestoreNetworkMode mode = RestoreNetworkMode::FULL, bool stdOutput = false);

#ifdef OPENVPN_REMOTE_OVERRIDE

    void set_remote_override_cmd(const std::string &cmd);

#endif
    
    void setConnectionInformation(const std::string &s);

    bool subscribeEvent(ClientEvent::Type event, EventCallbackFunction callbackFunction);
    bool unsubscribeEvent(ClientEvent::Type event, EventCallbackFunction callbackFunction);

    static bool isDataCipherSupported(std::string cipher);
    static std::vector<std::string> getSupportedDataCiphers();
    static std::string resolveProfile(std::string profile, bool ipv6 = false);
    static bool profileNeedsResolution(std::string profile);

    private:

    std::mutex log_mutex;
    std::string dc_cookie;
    std::string connectionInformation;

    std::map<ClientEvent::Type, EventCallbackFunction> eventSubscription;
    std::map<ClientEvent::Type, EventCallbackFunction>::iterator eventSubscriptionIterator;

    static std::vector<std::string> supportedDataCipher;

    RandomAPI::Ptr rng;      // random data source for epki
    volatile ClockTickAction clock_tick_action = CT_UNDEF;
    Status status;
    bool event_error;
    bool event_fatal_error;

#ifdef OPENVPN_REMOTE_OVERRIDE

    std::string remote_override_cmd;

#endif

    virtual void event(const ClientAPI::Event &ev) override;
    virtual void log(const ClientAPI::LogInfo &log) override;
    virtual void clock_tick() override;
    virtual void external_pki_cert_request(ClientAPI::ExternalPKICertRequest &certreq) override;
    virtual void external_pki_sign_request(ClientAPI::ExternalPKISignRequest &signreq) override;

    static int rng_callback(void *arg, unsigned char *data, size_t len);

    virtual bool pause_on_connection_timeout() override;

#ifdef OPENVPN_REMOTE_OVERRIDE

    virtual bool remote_override_enabled() override;
    virtual void remote_override(ClientAPI::RemoteOverride &ro);

#endif

    void init();

    bool addServer(IPFamily ipFamily, const std::string &serverIP);

    void onDisconnectedEvent(const ClientAPI::Event &ev, const ClientAPI::ConnectionInfo &connectionInfo);
    void onConnectedEvent(const ClientAPI::Event &ev, const ClientAPI::ConnectionInfo &connectionInfo);
    void onReconnectingEvent(const ClientAPI::Event &ev, const ClientAPI::ConnectionInfo &connectionInfo);
    void onAuthPendingEvent(const ClientAPI::Event &ev, const ClientAPI::ConnectionInfo &connectionInfo);
    void onResolveEvent(const ClientAPI::Event &ev, const ClientAPI::ConnectionInfo &connectionInfo);
    void onWaitEvent(const ClientAPI::Event &ev, const ClientAPI::ConnectionInfo &connectionInfo);
    void onWaitProxyEvent(const ClientAPI::Event &ev, const ClientAPI::ConnectionInfo &connectionInfo);
    void onConnectingEvent(const ClientAPI::Event &ev, const ClientAPI::ConnectionInfo &connectionInfo);
    void onGetConfigEvent(const ClientAPI::Event &ev, const ClientAPI::ConnectionInfo &connectionInfo);
    void onAssignIpEvent(const ClientAPI::Event &ev, const ClientAPI::ConnectionInfo &connectionInfo);
    void onAddRoutesEvent(const ClientAPI::Event &ev, const ClientAPI::ConnectionInfo &connectionInfo);
    void onEchoOptEvent(const ClientAPI::Event &ev, const ClientAPI::ConnectionInfo &connectionInfo);
    void onInfoEvent(const ClientAPI::Event &ev, const ClientAPI::ConnectionInfo &connectionInfo);
    void onWarnEvent(const ClientAPI::Event &ev, const ClientAPI::ConnectionInfo &connectionInfo);
    void onPauseEvent(const ClientAPI::Event &ev, const ClientAPI::ConnectionInfo &connectionInfo);
    void onResumeEvent(const ClientAPI::Event &ev, const ClientAPI::ConnectionInfo &connectionInfo);
    void onRelayEvent(const ClientAPI::Event &ev, const ClientAPI::ConnectionInfo &connectionInfo);
    void onCompressionEnabledEvent(const ClientAPI::Event &ev, const ClientAPI::ConnectionInfo &connectionInfo);
    void onUnsupportedFeatureEvent(const ClientAPI::Event &ev, const ClientAPI::ConnectionInfo &connectionInfo);
    void onTransportErrorEvent(const ClientAPI::Event &ev, const ClientAPI::ConnectionInfo &connectionInfo);
    void onTunErrorEvent(const ClientAPI::Event &ev, const ClientAPI::ConnectionInfo &connectionInfo);
    void onClientRestartEvent(const ClientAPI::Event &ev, const ClientAPI::ConnectionInfo &connectionInfo);
    void onAuthFailedEvent(const ClientAPI::Event &ev, const ClientAPI::ConnectionInfo &connectionInfo);
    void onCertVerifyFailEvent(const ClientAPI::Event &ev, const ClientAPI::ConnectionInfo &connectionInfo);
    void onTlsVersionMinEvent(const ClientAPI::Event &ev, const ClientAPI::ConnectionInfo &connectionInfo);
    void onClientHaltEvent(const ClientAPI::Event &ev, const ClientAPI::ConnectionInfo &connectionInfo);
    void onClientSetupEvent(const ClientAPI::Event &ev, const ClientAPI::ConnectionInfo &connectionInfo);
    void onTunHaltEvent(const ClientAPI::Event &ev, const ClientAPI::ConnectionInfo &connectionInfo);
    void onConnectionTimeoutEvent(const ClientAPI::Event &ev, const ClientAPI::ConnectionInfo &connectionInfo);
    void onInactiveTimeoutEvent(const ClientAPI::Event &ev, const ClientAPI::ConnectionInfo &connectionInfo);
    void onDynamicChallengeEvent(const ClientAPI::Event &ev, const ClientAPI::ConnectionInfo &connectionInfo);
    void onProxyNeedCredsEvent(const ClientAPI::Event &ev, const ClientAPI::ConnectionInfo &connectionInfo);
    void onProxyErrorEvent(const ClientAPI::Event &ev, const ClientAPI::ConnectionInfo &connectionInfo);
    void onTunSetupFailedEvent(const ClientAPI::Event &ev, const ClientAPI::ConnectionInfo &connectionInfo);
    void onTunIFaceCreateEvent(const ClientAPI::Event &ev, const ClientAPI::ConnectionInfo &connectionInfo);
    void onTunIFaceDisabledEvent(const ClientAPI::Event &ev, const ClientAPI::ConnectionInfo &connectionInfo);
    void onEpkiErrorEvent(const ClientAPI::Event &ev, const ClientAPI::ConnectionInfo &connectionInfo);
    void onEpkiInvalidAliasEvent(const ClientAPI::Event &ev, const ClientAPI::ConnectionInfo &connectionInfo);
    void onRelayErrorEvent(const ClientAPI::Event &ev, const ClientAPI::ConnectionInfo &connectionInfo);

    void raiseEvent(ClientEvent::Type event, const EventData &data);
};

using namespace openvpn;

// OpenVpnClientBase class

OpenVpnClientBase::OpenVpnClientBase(NetFilter::Mode netFilterMode, const std::string &resDir, const std::string &dnsBkpFile, const std::string &resolvConfFile)
{
    networkLockMode = netFilterMode;

    if(networkLockMode != NetFilter::Mode::OFF && networkLockMode != NetFilter::Mode::UNKNOWN)
    {
        netFilter = new NetFilter(resDir, networkLockMode);

        netFilterIsPrivate = true;

        if(netFilter != nullptr && netFilter->init())
            OPENVPN_LOG("Network filter successfully initialized");
        else
        {
            OPENVPN_LOG("ERROR: Cannot initialize network filter");
        
            netFilterIsPrivate = false;

            networkLockMode = NetFilter::Mode::OFF;
        }
    }
    else
    {
        netFilter = nullptr;

        netFilterIsPrivate = false;
    }

    setup(resDir, dnsBkpFile, resolvConfFile);
}

OpenVpnClientBase::OpenVpnClientBase(NetFilter *externalNetFilter, const std::string &resDir, const std::string &dnsBkpFile, const std::string &resolvConfFile)
{
    netFilter = externalNetFilter;

    netFilterIsPrivate = false;

    if(netFilter != nullptr)
    {
        networkLockMode = netFilter->getMode();
    }
    else
    {
        netFilterIsPrivate = false;

        networkLockMode = NetFilter::Mode::OFF;

        OPENVPN_LOG("WARNING: provided a null NetFilter object. Network filter and lock are disabled.");
    }

    setup(resDir, dnsBkpFile, resolvConfFile);
}

OpenVpnClientBase::~OpenVpnClientBase()
{
    if(netFilter != nullptr && netFilterIsPrivate == true)
        delete netFilter;

#if defined(OPENVPN_PLATFORM_LINUX)

    if(dnsManager != nullptr)
        delete dnsManager;

#endif
}

void OpenVpnClientBase::setup(const std::string &resDir, const std::string &dnsBkpFile, const std::string &resolvConfFile)
{
    resourceDirectory = resDir;
    dnsBackupFile = dnsBkpFile;
    resolvDotConfFile = resolvConfFile;

#if defined(OPENVPN_PLATFORM_LINUX)

    dnsManager = new DNSManager(resolvDotConfFile);

#endif

    dnsTable.clear();
    systemDnsTable.clear();
    remoteServerIpList.clear();

    dnsHasBeenPushed = false;
    dnsPushIgnored = false;
    gatewayHasBeenRerouted = false;
    IPv4GatewayHasBeenRerouted = false;
    IPv6GatewayHasBeenRerouted = false;
    systemDNSallowedInNetFilter = false;
}

bool OpenVpnClientBase::tun_builder_new()
{
    tunnelBuilderCapture.tun_builder_set_mtu(DEFAULT_TUN_MTU);

    return true;
}

bool OpenVpnClientBase::tun_builder_set_layer(int layer)
{
    return true;
}

bool OpenVpnClientBase::tun_builder_set_remote_address(const std::string &address, bool ipv6)
{
    std::ostringstream os;

    if(isIPv6Enabled() == false && ipv6 == true)
    {
        os.str("");

        os << "ERROR: Cannot set remote address " << address << " pushed by the server is ignored. IPv6 is not available in this system";
        
        OPENVPN_LOG(os.str());

        return false;
    }
    
    return tunnelBuilderCapture.tun_builder_set_remote_address(address, ipv6);
}

bool OpenVpnClientBase::tun_builder_add_address(const std::string &address, int prefix_length, const std::string &gateway, bool ipv6, bool net30)
{
    std::ostringstream os;

    if(isIPv6Enabled() == false && ipv6 == true)
    {
        os.str("");

        os << "ERROR: Cannot add address " << address << " pushed by the server. IPv6 is not available in this system";
        
        OPENVPN_LOG(os.str());

        return false;
    }
    
    return tunnelBuilderCapture.tun_builder_add_address(address, prefix_length, gateway, ipv6, net30);
}

bool OpenVpnClientBase::tun_builder_set_route_metric_default(int metric)
{
    return true;
}

bool OpenVpnClientBase::tun_builder_reroute_gw(bool ipv4, bool ipv6, unsigned int flags)
{
    std::ostringstream os;

    if(isIPv6Enabled() == false && ipv6 == true)
    {
        os.str("");

        os << "ERROR: Cannot reroute gateway to IPv6 addresses. IPv6 is not available in this system";
        
        OPENVPN_LOG(os.str());

        return false;
    }

    gatewayHasBeenRerouted = true;

    if(ipv4 == true)
        IPv4GatewayHasBeenRerouted = true;
    
    if(ipv6 == true)
        IPv6GatewayHasBeenRerouted = true;

    return tunnelBuilderCapture.tun_builder_reroute_gw(ipv4, ipv6, flags);
}

bool OpenVpnClientBase::tun_builder_add_route(const std::string &address, int prefix_length, int metric, bool ipv6)
{
    std::ostringstream os;

    if(isIPv6Enabled() == false && ipv6 == true)
    {
        os.str("");

        os << "ERROR: Cannot add route to address " << address << " pushed by the server. IPv6 is not available in this system";
        
        OPENVPN_LOG(os.str());

        return false;
    }
    
    return tunnelBuilderCapture.tun_builder_add_route(address, prefix_length, metric, ipv6);
}

bool OpenVpnClientBase::tun_builder_exclude_route(const std::string& address, int prefix_length, int metric, bool ipv6)
{
    return false;
}

bool OpenVpnClientBase::tun_builder_add_dns_server(const std::string &address, bool ipv6)
{
    std::ostringstream os;
    IPAddress dnsEntry;

    if(isIPv6Enabled() == false && ipv6 == true)
    {
        os.str("");

        os << "ERROR: Cannot add DNS " << address << " pushed by the server. IPv6 is not available in this system";
        
        OPENVPN_LOG(os.str());

        return false;
    }
    
    if(dnsPushIgnored == false)
    {
        os.str("");

        os << "VPN Server has pushed ";

        if(ipv6)
            os << "IPv6";
        else
            os << "IPv4";

        os << " DNS server " << address;

        OPENVPN_LOG(os.str());

        dnsEntry.address = address;
        dnsEntry.family = (ipv6 ? IPFamily::IPv6 : IPFamily::IPv4);
        dnsEntry.prefixLength = (ipv6 ? 128 : 32);

        dnsTable.push_back(dnsEntry);

        dnsHasBeenPushed = true;

        if(netFilter != nullptr && netFilter->isNetworkLockAvailable())
            netFilter->commitAllowRule(dnsEntry.family, NetFilter::Hook::OUTPUT, "", NetFilter::Protocol::ANY, "", 0, dnsEntry.address + "/" + std::to_string(dnsEntry.prefixLength), 0);
    }

#if defined(OPENVPN_PLATFORM_LINUX)

    DNSManager::Error retval;
    bool setDefault;

    if(dnsPushIgnored == false)
    {
        os.str("");

        retval = dnsManager->addAddressToResolvDotConf(address, (ipv6 ? IPFamily::IPv6 : IPFamily::IPv4));

        if(retval == DNSManager::Error::OK)
        {
            os << "Setting pushed ";

            if(ipv6)
                os << "IPv6";
            else
                os << "IPv4";

            os << " DNS server " << address << " in resolv.conf";
        }
        else if(retval == DNSManager::Error::RESOLV_DOT_CONF_OPEN_ERROR)
        {
            os << "ERROR: Cannot open resolv.conf";
        }
        else if(retval == DNSManager::Error::RESOLV_DOT_CONF_RENAME_ERROR)
        {
            os << "ERROR: Cannot create a backup copy of resolv.conf";
        }
        else if(retval == DNSManager::Error::RESOLV_DOT_CONF_WRITE_ERROR)
        {
            os << "ERROR: Cannot write in resolv.conf";
        }
        else
        {
            os << "ERROR: resolv.conf generic error";
        }

        OPENVPN_LOG(os.str());

        if(dnsManager->systemHasSystemdResolved() && dnsManager->systemHasResolvectl())
        {
            scanInterfaces();

            for(std::string interface : localInterface)
            {
                os.str("");

                if(interface.substr(0, 3) == "tun")
                    setDefault = true;
                else
                    setDefault = false;
                    
                retval = dnsManager->addAddressToResolved(interface, address.c_str(), (ipv6 ? IPFamily::IPv6 : IPFamily::IPv4), setDefault);

                if(retval == DNSManager::Error::OK)
                {
                    os << "Setting pushed ";

                    if(ipv6)
                        os << "IPv6";
                    else
                        os << "IPv4";

                    os << " DNS server " << address << " for interface " << interface << " via systemd-resolved";
                    
                    if(setDefault == true)
                        os << " (default route)";
                }
                else if(retval == DNSManager::Error::RESOLVED_IS_NOT_AVAILABLE)
                {
                    os << "ERROR systemd-resolved is not available on this system";
                }
                else if(retval == DNSManager::Error::RESOLVED_ADD_DNS_ERROR)
                {
                    os << "ERROR systemd-resolved: Failed to add DNS server " << address << " for interface " << interface;
                }
                else if(retval == DNSManager::Error::RESOLVED_SET_DEFAULT_ROUTE_ERROR)
                {
                    os << "ERROR systemd-resolved: Failed to set default route for interface " << interface;
                }
                else if(retval == DNSManager::Error::NO_RESOLVED_COMMAND)
                {
                    os << "ERROR systemd-resolved: resolvectl or systemd-resolve command not found";
                }
                else
                {
                    os << "ERROR systemd-resolved: Unknown error while adding DNS server " << address << " for interface " << interface;
                }

                OPENVPN_LOG(os.str());
            }
        }
    }
    else
    {
        os.str("");

        os << "WARNING: ignoring server DNS push request for address " << address;

        OPENVPN_LOG(os.str());
    }

#endif

    return tunnelBuilderCapture.tun_builder_add_dns_server(address, ipv6);
}

bool OpenVpnClientBase::tun_builder_add_search_domain(const std::string& domain)
{
    return false;
}

bool OpenVpnClientBase::tun_builder_set_mtu(int mtu)
{
    std::ostringstream os;

    bool result = tunnelBuilderCapture.tun_builder_set_mtu(mtu);
    
    os.str("");

    if(result == true)
        os << "Successfully";
    else
    {
        os << "ERROR: Failed to";
        
        result = false;
    }

    os << " set tunnel MTU to " << mtu;

    OPENVPN_LOG(os.str());

    return result;
}

bool OpenVpnClientBase::tun_builder_set_session_name(const std::string &name)
{
    return tunnelBuilderCapture.tun_builder_set_session_name(name);
}

bool OpenVpnClientBase::tun_builder_add_proxy_bypass(const std::string& bypass_host)
{
    return false;
}

bool OpenVpnClientBase::tun_builder_set_proxy_auto_config_url(const std::string& url)
{
    return false;
}

bool OpenVpnClientBase::tun_builder_set_proxy_http(const std::string& host, int port)
{
    return false;
}

bool OpenVpnClientBase::tun_builder_set_proxy_https(const std::string& host, int port)
{
    return false;
}

bool OpenVpnClientBase::tun_builder_add_wins_server(const std::string& address)
{
    return false;
}

bool OpenVpnClientBase::tun_builder_set_allow_family(int af, bool allow)
{
    return true;
}

bool OpenVpnClientBase::tun_builder_set_adapter_domain_suffix(const std::string& name)
{
    return true;
}
    
int OpenVpnClientBase::tun_builder_establish()
{
    if(!tunnelSetup)
        tunnelSetup.reset(new TUN_CLASS_SETUP());

    tunnelConfig.layer = Layer(Layer::Type::OSI_LAYER_3);

    tunnelConfig.add_bypass_routes_on_establish = true;

    return tunnelSetup->establish(tunnelBuilderCapture, &tunnelConfig, nullptr, std::cout);
}

bool OpenVpnClientBase::tun_builder_persist()
{
    return true;
}
    
const std::vector<std::string> OpenVpnClientBase::tun_builder_get_local_networks(bool ipv6)
{
    return {};
}

void OpenVpnClientBase::tun_builder_establish_lite()
{
}

void OpenVpnClientBase::tun_builder_teardown(bool disconnect)
{
    std::ostringstream os;

    auto os_print = Cleanup([&os](){ OPENVPN_LOG_STRING(os.str()); });

    tunnelSetup->destroy(os);
}

bool OpenVpnClientBase::ignore_dns_push()
{
    return dnsPushIgnored;
}

#ifdef ENABLE_OVPNDCO

bool OpenVpnClientBase::tun_builder_dco_available()
{
    return false;
}

int OpenVpnClientBase::tun_builder_dco_enable(const std::string& dev_name)
{
    return -1;
}

void OpenVpnClientBase::tun_builder_dco_new_peer(uint32_t peer_id, uint32_t transport_fd, struct sockaddr *sa, socklen_t salen, IPv4::Addr &vpn4, IPv6::Addr &vpn6)
{
}

void OpenVpnClientBase::tun_builder_dco_set_peer(uint32_t peer_id, int keepalive_interval, int keepalive_timeout)
{
}

void OpenVpnClientBase::tun_builder_dco_del_peer(uint32_t peer_id)
{
}

void OpenVpnClientBase::tun_builder_dco_get_peer(uint32_t peer_id, bool sync)
{
}

void OpenVpnClientBase::tun_builder_dco_new_key(unsigned int key_slot, const KoRekey::KeyConfig* kc)
{
}

void OpenVpnClientBase::tun_builder_dco_swap_keys(uint32_t peer_id)
{
}

void OpenVpnClientBase::tun_builder_dco_del_key(uint32_t peer_id, unsigned int key_slot)
{
}

void OpenVpnClientBase::tun_builder_dco_establish()
{
}

#endif

bool OpenVpnClientBase::socket_protect(int socket, std::string remote, bool ipv6)
{
    (void)socket;
    std::ostringstream os;

    if(isIPv6Enabled() == false && ipv6 == true)
    {
        os.str("");

        os << "ERROR: Cannot do socket protection for " << remote << " and pushed by the server. IPv6 is not available in this system";

        OPENVPN_LOG(os.str());

        return false;
    }

    return true; // tunnelSetup->add_bypass_route(server.address, server.ipv6, os);
}

void OpenVpnClientBase::setConfig(const ClientAPI::Config c)
{
    OptionList::KeyValueList kvl;

    config = c;

    optionList.clear();

    kvl.reserve(config.contentList.size());

    for(size_t i = 0; i < config.contentList.size(); ++i)
        kvl.push_back(new OptionList::KeyValue(config.contentList[i].key, config.contentList[i].value));

    ParseClientConfig::parse(config.content, &kvl, optionList);
}

ClientAPI::Config OpenVpnClientBase::getConfig()
{
    return config;
}

void OpenVpnClientBase::setEvalConfig(const ClientAPI::EvalConfig e)
{
    evalConfig = e;
}

ClientAPI::EvalConfig OpenVpnClientBase::getEvalConfig()
{
    return evalConfig;
}

void OpenVpnClientBase::setNetworkLockMode(NetFilter::Mode mode)
{
    if(netFilter != nullptr)
    {
        networkLockMode = mode;

        netFilter->setMode(mode);
    }
    else
    {
        networkLockMode = NetFilter::Mode::OFF;

        OPENVPN_LOG("WARNING: Network filter and lock are disabled. Mode setting is ignored");
    }
}

NetFilter::Mode OpenVpnClientBase::getNetworkLockMode()
{
    return networkLockMode;
}

void OpenVpnClientBase::ignoreDnsPush(bool ignore)
{
    dnsPushIgnored = ignore;
}

bool OpenVpnClientBase::isDnsPushIgnored()
{
    return dnsPushIgnored;
}

std::vector<IPAddress> OpenVpnClientBase::getPushedDns()
{
    std::vector<IPAddress> pushedDns(dnsTable);
    
    return pushedDns;
}

std::string OpenVpnClientBase::openVPNInfo()
{
    return ClientAPI::OpenVPNClientHelper::platform();
}

std::string OpenVpnClientBase::openVPNCopyright()
{
    return ClientAPI::OpenVPNClientHelper::copyright();
}

std::string OpenVpnClientBase::sslLibraryVersion()
{
    return ClientAPI::OpenVPNClientHelper::ssl_library_version();
}

bool OpenVpnClientBase::saveSystemDNS()
{
    IPAddress ipEntry;
    std::ofstream systemDNSDumpFile;
    bool res = true;

    res_ninit(&_res);

    dnsTable.clear();
    systemDnsTable.clear();

    try
    {
        systemDNSDumpFile.open(dnsBackupFile);

        for(int i=0; i < MAXNS; i++)
        {
            if(_res.nsaddr_list[i].sin_addr.s_addr > 0)
            {
                ipEntry.address = inet_ntoa(_res.nsaddr_list[i].sin_addr);

                if(_res.nsaddr_list[i].sin_family == AF_INET)
                {
                    ipEntry.family = IPFamily::IPv4;
                    ipEntry.prefixLength = 32;
                }
                else
                {
                    ipEntry.family = IPFamily::IPv6;
                    ipEntry.prefixLength = 128;
                }

                systemDnsTable.push_back(ipEntry);

                systemDNSDumpFile << ipEntry.address << std::endl;
            }
        }

        systemDNSDumpFile.close();
    }
    catch(Exception &e)
    {
        res = false;
    }

    return res;
}

bool OpenVpnClientBase::allowSystemDNS()
{
    std::ostringstream os;
    bool res = true;

    if(netFilter == nullptr)
        return true;

    if(systemDNSallowedInNetFilter == true || netFilter->isNetworkLockAvailable() == false)
        return true;

    if(systemDnsTable.empty())
        saveSystemDNS();

    for(IPAddress dns : systemDnsTable)
    {
        netFilter->addAllowRule(dns.family, NetFilter::Hook::OUTPUT, "", NetFilter::Protocol::ANY, "", 0, dns.address, 0);

        os.str("");

        os << "Allowing system DNS " << dns.address << " to pass through the network filter";

        OPENVPN_LOG(os.str());
    }

    if(netFilter->commitRules() == false)
    {
        os.str("");

        os << "ERROR: Cannot allow system DNS to pass through network filter";

        OPENVPN_LOG(os.str());
        
        res = false;
    }

    systemDNSallowedInNetFilter = true;

    return res;
}

bool OpenVpnClientBase::rejectSystemDNS()
{
    std::ostringstream os;

    if(netFilter == nullptr)
        return true;

    if(systemDNSallowedInNetFilter == false || netFilter->isNetworkLockAvailable() == false)
        return true;
    
    for(IPAddress dns : systemDnsTable)
    {
        netFilter->commitRemoveAllowRule(dns.family, NetFilter::Hook::OUTPUT, "", NetFilter::Protocol::ANY, "", 0, dns.address, 0);

        os.str("");

        os << "System DNS " << dns.address << " is now rejected by the network filter";

        OPENVPN_LOG(os.str());
    }

    systemDNSallowedInNetFilter = false;

    return true;
}

bool OpenVpnClientBase::allowProxy()
{
    std::ostringstream os;
    bool res = true;

    if(netFilter == nullptr)
        return true;

    if(netFilter->isNetworkLockAvailable() == false)
        return true;

    if(config.proxyHost != "" && config.proxyPort != "")
    {
        IPAddress proxy = LocalNetwork::parseIpSpecification(config.proxyHost);
        
        res = netFilter->commitAllowRule(proxy.family, NetFilter::Hook::OUTPUT, "", NetFilter::Protocol::TCP, "", 0, proxy.address + "/" + std::to_string(proxy.prefixLength), std::stoi(config.proxyPort));

        if(res == true)
            OPENVPN_LOG("Allowing proxy " + config.proxyHost + ":" + config.proxyPort + " to pass through the network filter");
        else
            OPENVPN_LOG("ERROR: Cannot allow proxy " + config.proxyHost + ":" + config.proxyPort + " to pass through the network filter");
    }

    return res;
}

bool OpenVpnClientBase::rejectProxy()
{
    std::ostringstream os;
    bool res = false;

    if(netFilter == nullptr)
        return true;

    if(netFilter->isNetworkLockAvailable() == false)
        return true;

    if(config.proxyHost != "" && config.proxyPort != "")
    {
        IPAddress proxy = LocalNetwork::parseIpSpecification(config.proxyHost);
        
        res = netFilter->commitRemoveAllowRule(proxy.family, NetFilter::Hook::OUTPUT, "", NetFilter::Protocol::TCP, "", 0, proxy.address + "/" + std::to_string(proxy.prefixLength), std::stoi(config.proxyPort));

        if(res == true)
            OPENVPN_LOG("Proxy " + config.proxyHost + "/" + config.proxyPort + " is now rejected by the network filter");
        else
            OPENVPN_LOG("ERROR: Cannot reject proxy " + config.proxyHost + "/" + config.proxyPort + " from the network filter");
    }

    return res;
}

// OpenVpnClient class

OpenVpnClient::OpenVpnClient(NetFilter::Mode netFilterMode, const std::string &resDir, const std::string &dnsBkpFile, const std::string &resolvConfFile) : OpenVpnClientBase(netFilterMode, resDir, dnsBkpFile, resolvConfFile)
{
    init();
}

OpenVpnClient::OpenVpnClient(NetFilter *externalNetFilter, const std::string &resDir, const std::string &dnsBkpFile, const std::string &resolvConfFile) : OpenVpnClientBase(externalNetFilter, resDir, dnsBkpFile, resolvConfFile)
{
    init();
}


OpenVpnClient::~OpenVpnClient()
{
}

void OpenVpnClient::init()
{
    status = Status::DISCONNECTED;

    connectionInformation = "";
    
    event_error = false;
    event_fatal_error = false;
}

OpenVpnClient::Status OpenVpnClient::getStatus()
{
    return status;
}

bool OpenVpnClient::eventError()
{
    return event_error;
}

bool OpenVpnClient::eventFatalError()
{
    return event_fatal_error;
}

void OpenVpnClient::setConnectionInformation(const std::string &s)
{
    connectionInformation = s;
}

bool OpenVpnClient::is_dynamic_challenge() const
{
    return !dc_cookie.empty();
}

std::string OpenVpnClient::dynamic_challenge_cookie()
{
    return dc_cookie;
}

void OpenVpnClient::set_clock_tick_action(const ClockTickAction action)
{
    clock_tick_action = action;
}

void OpenVpnClient::print_stats()
{
    std::ostringstream os;
    bool print_header = true;
    const int n = stats_n();
    std::vector<long long> stats = stats_bundle();
    std::string stat_name;

    for(int i = 0; i < n; ++i)
    {
        const long long value = stats[i];
        stat_name = stats_name(i);

        if(value)
        {
            if(print_header == true)
            {
                OPENVPN_LOG("*** Connection Statistics ***");
                
                print_header = false;
            }

            os.str("");

            if(stat_name == "BYTES_IN")
                os << "Total traffic in: " << AirVPNTools::formatDataVolume(value, true);
            else if(stat_name == "BYTES_OUT")
                os << "Total traffic out: " << AirVPNTools::formatDataVolume(value, true);
            else if(stat_name == "PACKETS_IN")
                os << "Total packets in: " << value;
            else if(stat_name == "PACKETS_OUT")
                os << "Total packets out: " << value;
            if(stat_name == "TUN_BYTES_IN")
                os << "Total tunnel traffic in: " << AirVPNTools::formatDataVolume(value, true);
            else if(stat_name == "TUN_BYTES_OUT")
                os << "Total tunnel traffic out: " << AirVPNTools::formatDataVolume(value, true);
            else if(stat_name == "TUN_PACKETS_IN")
                os << "Total tunnel packets in: " << value;
            else if(stat_name == "TUN_PACKETS_OUT")
                os << "Total tunnel packets out: " << value;

            if(os.str().empty() == false)
                OPENVPN_LOG(os.str());
        }
    }
}

std::map<std::string, std::string> OpenVpnClient::get_connection_stats()
{
    int n;
    std::vector<long long> stats;
    std::map<std::string, std::string> conn_stats;
    openvpn::ClientAPI::ConnectionInfo connectionInfo;

    connectionInfo = connection_info();

    conn_stats.insert(std::make_pair("user", connectionInfo.user));
    conn_stats.insert(std::make_pair("server_host", connectionInfo.serverHost));
    conn_stats.insert(std::make_pair("server_port", connectionInfo.serverPort));
    conn_stats.insert(std::make_pair("server_proto", connectionInfo.serverProto));
    conn_stats.insert(std::make_pair("server_ip", connectionInfo.serverIp));
    conn_stats.insert(std::make_pair("vpn_ipv4", connectionInfo.vpnIp4));
    conn_stats.insert(std::make_pair("vpn_ipv6", connectionInfo.vpnIp6));
    conn_stats.insert(std::make_pair("gateway_ipv4", connectionInfo.gw4));
    conn_stats.insert(std::make_pair("gateway_ipv6", connectionInfo.gw6));
    conn_stats.insert(std::make_pair("client_ip", connectionInfo.clientIp));
    conn_stats.insert(std::make_pair("tun_name", connectionInfo.tunName));
    conn_stats.insert(std::make_pair("topology", connectionInfo.topology));
    conn_stats.insert(std::make_pair("cipher", connectionInfo.cipher));
    conn_stats.insert(std::make_pair("ping", std::to_string(connectionInfo.ping)));
    conn_stats.insert(std::make_pair("ping_restart", std::to_string(connectionInfo.ping_restart)));

    n = stats_n();
    stats = stats_bundle();

    for(int i = 0; i < n; ++i)
    {
        const long long value = stats[i];

        if(value)
            conn_stats.insert(std::make_pair(AirVPNTools::toLower(stats_name(i)), std::to_string(value)));
    }

    return conn_stats;
}

#ifdef OPENVPN_REMOTE_OVERRIDE

    void OpenVpnClient::set_remote_override_cmd(const std::string &cmd)
    {
        remote_override_cmd = cmd;
    }

#endif

void OpenVpnClient::event(const ClientAPI::Event &ev)
{
    std::ostringstream os;
    ClientAPI::ConnectionInfo connectionInfo = connection_info();

    os.str("");

    os << "EVENT: " << ev.name;

    if(ev.fatal)
    {
        os << " [FATAL ERROR]";
        
        event_fatal_error = true;
    }
    else if(ev.error)
    {
        os << " [ERROR]";
        
        event_error = true;
    }

    if(!ev.info.empty())
        os << " " << ev.info;

    OPENVPN_LOG(os.str());

    if(ev.name == "DISCONNECTED")
    {
        onDisconnectedEvent(ev, connectionInfo);
    }
    else if(ev.name == "CONNECTED")
    {
        onConnectedEvent(ev, connectionInfo);
    }
    else if(ev.name == "RECONNECTING")
    {
        onReconnectingEvent(ev, connectionInfo);
    }
    else if(ev.name == "AUTH_PENDING")
    {
        onAuthPendingEvent(ev, connectionInfo);
    }
    else if(ev.name == "RESOLVE")
    {
        onResolveEvent(ev, connectionInfo);
    }
    else if(ev.name == "WAIT")
    {
        onWaitEvent(ev, connectionInfo);
    }
    else if(ev.name == "WAIT_PROXY")
    {
        onWaitProxyEvent(ev, connectionInfo);
    }
    else if(ev.name == "CONNECTING")
    {
        onConnectingEvent(ev, connectionInfo);
    }
    else if(ev.name == "GET_CONFIG")
    {
        onGetConfigEvent(ev, connectionInfo);
    }
    else if(ev.name == "ASSIGN_IP")
    {
        onAssignIpEvent(ev, connectionInfo);
    }
    else if(ev.name == "ADD_ROUTES")
    {
        onAddRoutesEvent(ev, connectionInfo);
    }
    else if(ev.name == "ECHO")
    {
        onEchoOptEvent(ev, connectionInfo);
    }
    else if(ev.name == "INFO")
    {
        onInfoEvent(ev, connectionInfo);
    }
    else if(ev.name == "WARN")
    {
        onWarnEvent(ev, connectionInfo);
    }
    else if(ev.name == "PAUSE")
    {
        onPauseEvent(ev, connectionInfo);
    }
    else if(ev.name == "RESUME")
    {
        onResumeEvent(ev, connectionInfo);
    }
    else if(ev.name == "RELAY")
    {
        onRelayEvent(ev, connectionInfo);
    }
    else if(ev.name == "COMPRESSION_ENABLED")
    {
        onCompressionEnabledEvent(ev, connectionInfo);
    }
    else if(ev.name == "UNSUPPORTED_FEATURE")
    {
        onUnsupportedFeatureEvent(ev, connectionInfo);
    }
    else if(ev.name == "TRANSPORT_ERROR")
    {
        onTransportErrorEvent(ev, connectionInfo);
    }
    else if(ev.name == "TUN_ERROR")
    {
        onTunErrorEvent(ev, connectionInfo);
    }
    else if(ev.name == "CLIENT_RESTART")
    {
        onClientRestartEvent(ev, connectionInfo);
    }
    else if(ev.name == "AUTH_FAILED")
    {
        onAuthFailedEvent(ev, connectionInfo);
    }
    else if(ev.name == "CERT_VERIFY_FAIL")
    {
        onCertVerifyFailEvent(ev, connectionInfo);
    }
    else if(ev.name == "TLS_VERSION_MIN")
    {
        onTlsVersionMinEvent(ev, connectionInfo);
    }
    else if(ev.name == "CLIENT_HALT")
    {
        onClientHaltEvent(ev, connectionInfo);
    }
    else if(ev.name == "CLIENT_SETUP")
    {
        onClientSetupEvent(ev, connectionInfo);
    }
    else if(ev.name == "TUN_HALT")
    {
        onTunHaltEvent(ev, connectionInfo);
    }
    else if(ev.name == "CONNECTION_TIMEOUT")
    {
        onConnectionTimeoutEvent(ev, connectionInfo);
    }
    else if(ev.name == "INACTIVE_TIMEOUT")
    {
        onInactiveTimeoutEvent(ev, connectionInfo);
    }
    else if(ev.name == "DYNAMIC_CHALLENGE")
    {
        onDynamicChallengeEvent(ev, connectionInfo);
    }
    else if(ev.name == "PROXY_NEED_CREDS")
    {
        onProxyNeedCredsEvent(ev, connectionInfo);
    }
    else if(ev.name == "PROXY_ERROR")
    {
        onProxyErrorEvent(ev, connectionInfo);
    }
    else if(ev.name == "TUN_SETUP_FAILED")
    {
        onTunSetupFailedEvent(ev, connectionInfo);
    }
    else if(ev.name == "TUN_IFACE_CREATE")
    {
        onTunIFaceCreateEvent(ev, connectionInfo);
    }
    else if(ev.name == "TUN_IFACE_DISABLED")
    {
        onTunIFaceDisabledEvent(ev, connectionInfo);
    }
    else if(ev.name == "EPKI_ERROR")
    {
        onEpkiErrorEvent(ev, connectionInfo);
    }
    else if(ev.name == "EPKI_INVALID_ALIAS")
    {
        onEpkiInvalidAliasEvent(ev, connectionInfo);
    }
    else if(ev.name == "RELAY_ERROR")
    {
        onRelayErrorEvent(ev, connectionInfo);
    }
}

bool OpenVpnClient::addServer(IPFamily ipFamily, const std::string &serverIP)
{
    std::string filterServerIP;
    IPAddress ipEntry;
    bool res = false;

    if(ipFamily == IPFamily::ANY || serverIP == "" || serverIP == "0.0.0.0" || serverIP == "::/0")
        return false;

    ipEntry.address = serverIP;

    if(ipFamily == IPFamily::IPv4)
    {
        ipEntry.family = IPFamily::IPv4;

        ipEntry.prefixLength = 32;
    }
    else
    {
        ipEntry.family = IPFamily::IPv6;

        ipEntry.prefixLength = 128;
    }

    if(std::find(remoteServerIpList.begin(), remoteServerIpList.end(), ipEntry) == remoteServerIpList.end())
    {
        if(netFilter != nullptr && netFilter->isNetworkLockAvailable())
            netFilter->addAllowRule(ipEntry.family, NetFilter::Hook::OUTPUT, "", NetFilter::Protocol::ANY, "", 0, ipEntry.address + "/" + std::to_string(ipEntry.prefixLength), 0);

        remoteServerIpList.push_back(ipEntry);

        res = true;
    }

    return res;
}

// event handlers

void OpenVpnClient::onDisconnectedEvent(const ClientAPI::Event &ev, const ClientAPI::ConnectionInfo &connectionInfo)
{
    EventData eventData;

    restoreNetworkSettings();

    status = Status::DISCONNECTED;

    eventData.status = status;
    eventData.vpnEvent = ev;
    eventData.connectionInfo = connectionInfo;
    eventData.message = "";

    raiseEvent(ClientEvent::Type::DISCONNECTED, eventData);
}

void OpenVpnClient::onConnectedEvent(const ClientAPI::Event &ev, const ClientAPI::ConnectionInfo &connectionInfo)
{
    std::ostringstream os;
    EventData eventData;
    bool ignoreRedirectGateway = false;

    if(connectionInformation != "")
        OPENVPN_LOG(connectionInformation);

    if(netFilter != nullptr && netFilter->isNetworkLockAvailable())
        netFilter->addIgnoredInterface(tunnelConfig.iface_name);

#if defined(OPENVPN_PLATFORM_LINUX)

    DNSManager::Error retval;
    bool setDefault;

    if(dnsManager->systemHasSystemdResolved() && dnsManager->systemHasResolvectl())
    {
        scanInterfaces();

        for(std::string interface : localInterface)
        {
            if(interface.substr(0, 3) == "tun")
                setDefault = true;
            else
                setDefault = false;
                
            for(IPAddress dns : dnsTable)
            {
                os.str("");
                
                retval = dnsManager->addAddressToResolved(interface, dns.address.c_str(), dns.family, setDefault);

                if(retval == DNSManager::Error::OK)
                {
                    os << "Setting pushed ";

                    if(dns.family == IPFamily::IPv6)
                        os << "IPv6";
                    else
                        os << "IPv4";

                    os << " DNS server " << dns.address << " for interface " << interface << " via systemd-resolved";
                    
                    if(setDefault == true)
                        os << " (default route)";
                }
                else if(retval == DNSManager::Error::RESOLVED_IS_NOT_AVAILABLE)
                {
                    os << "ERROR systemd-resolved is not available on this system";
                }
                else if(retval == DNSManager::Error::RESOLVED_ADD_DNS_ERROR)
                {
                    os << "ERROR systemd-resolved: Failed to add DNS server " << dns.address << " for interface " << interface;
                }
                else if(retval == DNSManager::Error::RESOLVED_SET_DEFAULT_ROUTE_ERROR)
                {
                    os << "ERROR systemd-resolved: Failed to set default route for interface " << interface;
                }
                else if(retval == DNSManager::Error::NO_RESOLVED_COMMAND)
                {
                    os << "ERROR systemd-resolved: resolvectl or systemd-resolve command not found";
                }
                else
                {
                    os << "ERROR systemd-resolved: Unknown error while adding DNS server " << dns.address << " for interface " << interface;
                }

                OPENVPN_LOG(os.str());
            }
        }
    }

#endif

    if(netFilter != nullptr && dnsHasBeenPushed == true && netFilter->isNetworkLockAvailable() && dnsPushIgnored == false)
    {
        os.str("");

        os << "Server has pushed its own DNS. Rejecting system DNS from network filter.";

        OPENVPN_LOG(os.str());

        rejectSystemDNS();
    }

    if(netFilter != nullptr && netFilter->isNetworkLockAvailable() && remoteServerIpList.size() > 1)
    {
        os.str("");

        os << "OpenVPN profile has multiple remote directives. Removing unused servers from network filter.";

        OPENVPN_LOG(os.str());

        for(IPAddress server : remoteServerIpList)
        {
            if(server.address != connectionInfo.serverIp)
            {
                if(netFilter != nullptr)
                    netFilter->commitRemoveAllowRule(server.family, NetFilter::Hook::OUTPUT, "", NetFilter::Protocol::ANY, "", 0, server.address, 0);

                os.str("");

                os << "Server IPv";

                if(server.family == IPFamily::IPv4)
                    os << "4";
                else
                    os << "6";

                os << " " << server.address << " has been removed from the network filter";

                OPENVPN_LOG(os.str());
            }
        }
    }

    ignoreRedirectGateway = false;

    if(optionList.exists("pull-filter") == true)
    {
        for(auto ndx : optionList.get_index("pull-filter"))
        {
            auto option = optionList[ndx];
            option.exact_args(3);

            if(option.get(1, -1) == "ignore" && option.get(2, -1) == "redirect-gateway")
                ignoreRedirectGateway = true;
        }
    }

    if(netFilter != nullptr && netFilter->isNetworkLockEnabled())
    {
        if(gatewayHasBeenRerouted == true && IPv4GatewayHasBeenRerouted == false && ignoreRedirectGateway == true)
            OPENVPN_LOG("WARNING: OpenVPN \"redirect-gateway\" directive has been ignored and network lock is enabled: IPv4 traffic through local gateway is not allowed.");

        if(gatewayHasBeenRerouted == true && IPv6GatewayHasBeenRerouted == false && isIPv6Enabled() == true && ignoreRedirectGateway == true)
            OPENVPN_LOG("WARNING: OpenVPN \"redirect-gateway\" directive has been ignored and network lock is enabled: IPv6 traffic through local gateway is not allowed.");
    }

    status = Status::CONNECTED;

    eventData.status = status;
    eventData.vpnEvent = ev;
    eventData.connectionInfo = connectionInfo;
    eventData.message = "";

    raiseEvent(ClientEvent::Type::CONNECTED, eventData);
}

void OpenVpnClient::onReconnectingEvent(const ClientAPI::Event &ev, const ClientAPI::ConnectionInfo &connectionInfo)
{
    EventData eventData;

    if(config.tunPersist == false)
        restoreNetworkSettings(RestoreNetworkMode::DNS_ONLY);

    status = Status::RECONNECTING;

    eventData.status = status;
    eventData.vpnEvent = ev;
    eventData.connectionInfo = connectionInfo;
    eventData.message = "";

    raiseEvent(ClientEvent::Type::RECONNECTING, eventData);
}

void OpenVpnClient::onAuthPendingEvent(const ClientAPI::Event &ev, const ClientAPI::ConnectionInfo &connectionInfo)
{
    EventData eventData;

    eventData.status = status;
    eventData.vpnEvent = ev;
    eventData.connectionInfo = connectionInfo;
    eventData.message = "";

    raiseEvent(ClientEvent::Type::AUTH_PENDING, eventData);
}

void OpenVpnClient::onResolveEvent(const ClientAPI::Event &ev, const ClientAPI::ConnectionInfo &connectionInfo)
{
    std::string serverIP, filterServerIP, protocol;
    std::ostringstream os;
    struct addrinfo ipinfo, *ipres = NULL, *current_ai;
    int aires;
    char resip[64];
    IPFamily ipFamily;
    EventData eventData;

#if defined(OPENVPN_PLATFORM_LINUX)

    if(status != Status::RECONNECTING)
    {
        if(dnsPushIgnored == false)
        {
            if(dnsManager->systemHasNetworkManager())
            {
                os.str("");

                os << "WARNING: NetworkManager is running on this system and may interfere with DNS management and cause DNS leaks";

                OPENVPN_LOG(os.str());
            }

            if(dnsManager->systemHasSystemdResolved())
            {
                os.str("");

                os << "WARNING: systemd-resolved is running on this system and may interfere with DNS management and cause DNS leaks";

                OPENVPN_LOG(os.str());
            }
        }
    }

#endif

    if(status != Status::RECONNECTING)
        saveSystemDNS();

    if(netFilter != nullptr && netFilter->isNetworkLockAvailable())
    {
        if(status != Status::RECONNECTING)
        {
            for(IPAddress ip : localIPaddress)
            {
                os.str("");

                os << "Local IPv";

                if(ip.family == IPFamily::IPv6)
                    os << "6";
                else
                    os << "4";

                os << " address " << ip.address;

                OPENVPN_LOG(os.str());
            }

            for(std::string interface : localInterface)
            {
                os.str("");

                os << "Local interface " << interface;

                OPENVPN_LOG(os.str());
            }

            if(netFilter != nullptr && netFilterIsPrivate == true)
                netFilter->setup(loopbackInterface);
        }

        os.str("");

        os << "Setting up network filter and lock";

        OPENVPN_LOG(os.str());

        // Allow system DNS to pass through network filter. It may be later denied by DNS push

        allowSystemDNS();

        // Allow Proxy to pass through network filter

        allowProxy();

        // Adding profile's remote entries to network filter

        if(evalConfig.remoteList.size() > 1)
        {
            os.str("");

            os << "OpenVPN profile has multiple remote directives. Temporarily adding remote servers to network filter.";

            OPENVPN_LOG(os.str());
        }

        remoteServerIpList.clear();

        for(ClientAPI::RemoteEntry remoteEntry : evalConfig.remoteList)
        {
            if(ipres != NULL)
            {
                freeaddrinfo(ipres);

                ipres = NULL;
            }

            memset(&ipinfo, 0, sizeof(ipinfo));

            ipinfo.ai_family = PF_UNSPEC;
            ipinfo.ai_flags = AI_NUMERICHOST;
            ipFamily = IPFamily::ANY;

            aires = getaddrinfo(remoteEntry.server.c_str(), NULL, &ipinfo, &ipres);

            if(aires || ipres == NULL)
            {
                ipFamily = IPFamily::ANY;
            }
            else
            {
                serverIP = remoteEntry.server;

                switch(ipres->ai_family)
                {
                    case AF_INET:
                    {
                        ipFamily = IPFamily::IPv4;
                    }
                    break;

                    case AF_INET6:
                    {
                        ipFamily = IPFamily::IPv6;
                    }
                    break;

                    default:
                    {
                        ipFamily = IPFamily::ANY;
                    }
                    break;
                }
            }

            if(ipFamily == IPFamily::ANY)
            {
                memset(&ipinfo, 0, sizeof(ipinfo));

                ipinfo.ai_family = PF_UNSPEC;

                os.str("");

                aires = getaddrinfo(remoteEntry.server.c_str(), NULL, &ipinfo, &ipres);

                if(aires == 0)
                {
                    for(current_ai = ipres; current_ai != NULL; current_ai = current_ai->ai_next)
                    {
                        getnameinfo(current_ai->ai_addr, current_ai->ai_addrlen, resip, sizeof(resip), NULL, 0, NI_NUMERICHOST);

                        serverIP = resip;

                        switch(current_ai->ai_family)
                        {
                            case AF_INET:
                            {
                                protocol = "4";
                                ipFamily = IPFamily::IPv4;
                            }
                            break;

                            case AF_INET6:
                            {
                                protocol = "6";
                                ipFamily = IPFamily::IPv6;
                            }
                            break;

                            default:
                            {
                                protocol = "??";
                                ipFamily = IPFamily::ANY;
                            }
                            break;
                        }

                        if(addServer(ipFamily, serverIP) == true)
                        {
                            os.str("");

                            os << "Resolved server " << remoteEntry.server << " into IPv" << protocol<< " " << serverIP;

                            OPENVPN_LOG(os.str());;

                            os.str("");

                            os << "Adding IPv" << protocol << " server " << serverIP << " to network filter";

                            OPENVPN_LOG(os.str());
                        }
                    }
                }
                else
                {
                    ipFamily = IPFamily::ANY;
                    serverIP = "";

                    os.str("");

                    os << "WARNING: Cannot resolve " << remoteEntry.server << " (" << gai_strerror(aires) << ")";

                    OPENVPN_LOG(os.str());
                }
            }
            else
            {
                if(addServer(ipFamily, serverIP) == true)
                {
                    os.str("");

                    os << "Adding IPv";

                    if(ipFamily == IPFamily::IPv4)
                        os << "4";
                    else
                        os << "6";

                    os << " server " << serverIP << " to network filter";

                    OPENVPN_LOG(os.str());
                }
            }
        }

        if(ipres != NULL)
            freeaddrinfo(ipres);

        os.str("");

        if(netFilter != nullptr && netFilter->commitRules() == true)
            os << "Network filter and lock successfully activated";
        else
            os << "ERROR: Cannot activate network filter and lock";

        OPENVPN_LOG(os.str());
    }

    eventData.status = status;
    eventData.vpnEvent = ev;
    eventData.connectionInfo = connectionInfo;
    eventData.message = "";

    raiseEvent(ClientEvent::Type::RESOLVE, eventData);
}

void OpenVpnClient::onWaitEvent(const ClientAPI::Event &ev, const ClientAPI::ConnectionInfo &connectionInfo)
{
    EventData eventData;

    eventData.status = status;
    eventData.vpnEvent = ev;
    eventData.connectionInfo = connectionInfo;
    eventData.message = "";

    raiseEvent(ClientEvent::Type::WAIT, eventData);
}

void OpenVpnClient::onWaitProxyEvent(const ClientAPI::Event &ev, const ClientAPI::ConnectionInfo &connectionInfo)
{
    EventData eventData;

    eventData.status = status;
    eventData.vpnEvent = ev;
    eventData.connectionInfo = connectionInfo;
    eventData.message = "";

    raiseEvent(ClientEvent::Type::WAIT_PROXY, eventData);
}

void OpenVpnClient::onConnectingEvent(const ClientAPI::Event &ev, const ClientAPI::ConnectionInfo &connectionInfo)
{
    EventData eventData;

    status = Status::CONNECTING;

    eventData.status = status;
    eventData.vpnEvent = ev;
    eventData.connectionInfo = connectionInfo;
    eventData.message = "";

    raiseEvent(ClientEvent::Type::CONNECTING, eventData);
}

void OpenVpnClient::onGetConfigEvent(const ClientAPI::Event &ev, const ClientAPI::ConnectionInfo &connectionInfo)
{
    EventData eventData;

    eventData.status = status;
    eventData.vpnEvent = ev;
    eventData.connectionInfo = connectionInfo;
    eventData.message = "";

    raiseEvent(ClientEvent::Type::GET_CONFIG, eventData);
}

void OpenVpnClient::onAssignIpEvent(const ClientAPI::Event &ev, const ClientAPI::ConnectionInfo &connectionInfo)
{
    EventData eventData;

    eventData.status = status;
    eventData.vpnEvent = ev;
    eventData.connectionInfo = connectionInfo;
    eventData.message = "";

    raiseEvent(ClientEvent::Type::ASSIGN_IP, eventData);
}

void OpenVpnClient::onAddRoutesEvent(const ClientAPI::Event &ev, const ClientAPI::ConnectionInfo &connectionInfo)
{
    EventData eventData;

    eventData.status = status;
    eventData.vpnEvent = ev;
    eventData.connectionInfo = connectionInfo;
    eventData.message = "";

    raiseEvent(ClientEvent::Type::ADD_ROUTES, eventData);
}

void OpenVpnClient::onEchoOptEvent(const ClientAPI::Event &ev, const ClientAPI::ConnectionInfo &connectionInfo)
{
    EventData eventData;

    eventData.status = status;
    eventData.vpnEvent = ev;
    eventData.connectionInfo = connectionInfo;
    eventData.message = "";

    raiseEvent(ClientEvent::Type::ECHO_OPT, eventData);
}

void OpenVpnClient::onInfoEvent(const ClientAPI::Event &ev, const ClientAPI::ConnectionInfo &connectionInfo)
{
    EventData eventData;

    eventData.status = status;
    eventData.vpnEvent = ev;
    eventData.connectionInfo = connectionInfo;
    eventData.message = "";

    raiseEvent(ClientEvent::Type::INFO, eventData);
}

void OpenVpnClient::onWarnEvent(const ClientAPI::Event &ev, const ClientAPI::ConnectionInfo &connectionInfo)
{
    EventData eventData;

    eventData.status = status;
    eventData.vpnEvent = ev;
    eventData.connectionInfo = connectionInfo;
    eventData.message = "";

    raiseEvent(ClientEvent::Type::WARN, eventData);
}

void OpenVpnClient::onPauseEvent(const ClientAPI::Event &ev, const ClientAPI::ConnectionInfo &connectionInfo)
{
    EventData eventData;

    status = Status::PAUSED;

    eventData.status = status;
    eventData.vpnEvent = ev;
    eventData.connectionInfo = connectionInfo;
    eventData.message = "";

    raiseEvent(ClientEvent::Type::PAUSE, eventData);
}

void OpenVpnClient::onResumeEvent(const ClientAPI::Event &ev, const ClientAPI::ConnectionInfo &connectionInfo)
{
    EventData eventData;

    status = Status::RESUMING;

    eventData.status = status;
    eventData.vpnEvent = ev;
    eventData.connectionInfo = connectionInfo;
    eventData.message = "";

    raiseEvent(ClientEvent::Type::RESUME, eventData);
}

void OpenVpnClient::onRelayEvent(const ClientAPI::Event &ev, const ClientAPI::ConnectionInfo &connectionInfo)
{
    EventData eventData;

    eventData.status = status;
    eventData.vpnEvent = ev;
    eventData.connectionInfo = connectionInfo;
    eventData.message = "";

    raiseEvent(ClientEvent::Type::RELAY, eventData);
}

void OpenVpnClient::onCompressionEnabledEvent(const ClientAPI::Event &ev, const ClientAPI::ConnectionInfo &connectionInfo)
{
    EventData eventData;

    eventData.status = status;
    eventData.vpnEvent = ev;
    eventData.connectionInfo = connectionInfo;
    eventData.message = "";

    raiseEvent(ClientEvent::Type::COMPRESSION_ENABLED, eventData);
}

void OpenVpnClient::onUnsupportedFeatureEvent(const ClientAPI::Event &ev, const ClientAPI::ConnectionInfo &connectionInfo)
{
    EventData eventData;

    eventData.status = status;
    eventData.vpnEvent = ev;
    eventData.connectionInfo = connectionInfo;
    eventData.message = "";

    raiseEvent(ClientEvent::Type::UNSUPPORTED_FEATURE, eventData);
}

void OpenVpnClient::onTransportErrorEvent(const ClientAPI::Event &ev, const ClientAPI::ConnectionInfo &connectionInfo)
{
    EventData eventData;

    eventData.status = status;
    eventData.vpnEvent = ev;
    eventData.connectionInfo = connectionInfo;
    eventData.message = "";

    raiseEvent(ClientEvent::Type::TRANSPORT_ERROR, eventData);
}

void OpenVpnClient::onTunErrorEvent(const ClientAPI::Event &ev, const ClientAPI::ConnectionInfo &connectionInfo)
{
    EventData eventData;

    eventData.status = status;
    eventData.vpnEvent = ev;
    eventData.connectionInfo = connectionInfo;
    eventData.message = "";

    raiseEvent(ClientEvent::Type::TUN_ERROR, eventData);
}

void OpenVpnClient::onClientRestartEvent(const ClientAPI::Event &ev, const ClientAPI::ConnectionInfo &connectionInfo)
{
    EventData eventData;

    status = Status::CLIENT_RESTART;

    eventData.status = status;
    eventData.vpnEvent = ev;
    eventData.connectionInfo = connectionInfo;
    eventData.message = "";

    raiseEvent(ClientEvent::Type::CLIENT_RESTART, eventData);
}

void OpenVpnClient::onAuthFailedEvent(const ClientAPI::Event &ev, const ClientAPI::ConnectionInfo &connectionInfo)
{
    EventData eventData;

    eventData.status = status;
    eventData.vpnEvent = ev;
    eventData.connectionInfo = connectionInfo;
    eventData.message = "";

    raiseEvent(ClientEvent::Type::AUTH_FAILED, eventData);
}

void OpenVpnClient::onCertVerifyFailEvent(const ClientAPI::Event &ev, const ClientAPI::ConnectionInfo &connectionInfo)
{
    EventData eventData;

    eventData.status = status;
    eventData.vpnEvent = ev;
    eventData.connectionInfo = connectionInfo;
    eventData.message = "";

    raiseEvent(ClientEvent::Type::CERT_VERIFY_FAIL, eventData);
}

void OpenVpnClient::onTlsVersionMinEvent(const ClientAPI::Event &ev, const ClientAPI::ConnectionInfo &connectionInfo)
{
    EventData eventData;

    eventData.status = status;
    eventData.vpnEvent = ev;
    eventData.connectionInfo = connectionInfo;
    eventData.message = "";

    raiseEvent(ClientEvent::Type::TLS_VERSION_MIN, eventData);
}

void OpenVpnClient::onClientHaltEvent(const ClientAPI::Event &ev, const ClientAPI::ConnectionInfo &connectionInfo)
{
    EventData eventData;

    eventData.status = status;
    eventData.vpnEvent = ev;
    eventData.connectionInfo = connectionInfo;
    eventData.message = "";

    raiseEvent(ClientEvent::Type::CLIENT_HALT, eventData);
}

void OpenVpnClient::onClientSetupEvent(const ClientAPI::Event &ev, const ClientAPI::ConnectionInfo &connectionInfo)
{
    EventData eventData;

    eventData.status = status;
    eventData.vpnEvent = ev;
    eventData.connectionInfo = connectionInfo;
    eventData.message = "";

    raiseEvent(ClientEvent::Type::CLIENT_SETUP, eventData);
}

void OpenVpnClient::onTunHaltEvent(const ClientAPI::Event &ev, const ClientAPI::ConnectionInfo &connectionInfo)
{
    EventData eventData;

    eventData.status = status;
    eventData.vpnEvent = ev;
    eventData.connectionInfo = connectionInfo;
    eventData.message = "";

    raiseEvent(ClientEvent::Type::TUN_HALT, eventData);
}

void OpenVpnClient::onConnectionTimeoutEvent(const ClientAPI::Event &ev, const ClientAPI::ConnectionInfo &connectionInfo)
{
    EventData eventData;

    status = Status::CONNECTION_TIMEOUT;

    eventData.status = status;
    eventData.vpnEvent = ev;
    eventData.connectionInfo = connectionInfo;
    eventData.message = "";

    raiseEvent(ClientEvent::Type::CONNECTION_TIMEOUT, eventData);
}

void OpenVpnClient::onInactiveTimeoutEvent(const ClientAPI::Event &ev, const ClientAPI::ConnectionInfo &connectionInfo)
{
    EventData eventData;

    status = Status::INACTIVE_TIMEOUT;

    eventData.status = status;
    eventData.vpnEvent = ev;
    eventData.connectionInfo = connectionInfo;
    eventData.message = "";

    raiseEvent(ClientEvent::Type::INACTIVE_TIMEOUT, eventData);
}

void OpenVpnClient::onDynamicChallengeEvent(const ClientAPI::Event &ev, const ClientAPI::ConnectionInfo &connectionInfo)
{
    std::ostringstream os;
    EventData eventData;

    dc_cookie = ev.info;

    ClientAPI::DynamicChallenge dc;

    if(ClientAPI::OpenVPNClientHelper::parse_dynamic_challenge(ev.info, dc))
    {
        os << "DYNAMIC CHALLENGE" << std::endl;
        os << "challenge: " << dc.challenge << std::endl;
        os << "echo: " << dc.echo << std::endl;
        os << "responseRequired: " << dc.responseRequired << std::endl;
        os << "stateID: " << dc.stateID << std::endl;

        OPENVPN_LOG(os.str());
    }

    eventData.status = status;
    eventData.vpnEvent = ev;
    eventData.connectionInfo = connectionInfo;
    eventData.message = "";

    raiseEvent(ClientEvent::Type::DYNAMIC_CHALLENGE, eventData);
}

void OpenVpnClient::onProxyNeedCredsEvent(const ClientAPI::Event &ev, const ClientAPI::ConnectionInfo &connectionInfo)
{
    EventData eventData;

    eventData.status = status;
    eventData.vpnEvent = ev;
    eventData.connectionInfo = connectionInfo;
    eventData.message = "";

    raiseEvent(ClientEvent::Type::PROXY_NEED_CREDS, eventData);
}

void OpenVpnClient::onProxyErrorEvent(const ClientAPI::Event &ev, const ClientAPI::ConnectionInfo &connectionInfo)
{
    EventData eventData;

    eventData.status = status;
    eventData.vpnEvent = ev;
    eventData.connectionInfo = connectionInfo;
    eventData.message = "";

    raiseEvent(ClientEvent::Type::PROXY_ERROR, eventData);
}

void OpenVpnClient::onTunSetupFailedEvent(const ClientAPI::Event &ev, const ClientAPI::ConnectionInfo &connectionInfo)
{
    EventData eventData;

    eventData.status = status;
    eventData.vpnEvent = ev;
    eventData.connectionInfo = connectionInfo;
    eventData.message = "";

    raiseEvent(ClientEvent::Type::TUN_SETUP_FAILED, eventData);
}

void OpenVpnClient::onTunIFaceCreateEvent(const ClientAPI::Event &ev, const ClientAPI::ConnectionInfo &connectionInfo)
{
    EventData eventData;

    eventData.status = status;
    eventData.vpnEvent = ev;
    eventData.connectionInfo = connectionInfo;
    eventData.message = "";

    raiseEvent(ClientEvent::Type::TUN_IFACE_CREATE, eventData);
}

void OpenVpnClient::onTunIFaceDisabledEvent(const ClientAPI::Event &ev, const ClientAPI::ConnectionInfo &connectionInfo)
{
    EventData eventData;

    eventData.status = status;
    eventData.vpnEvent = ev;
    eventData.connectionInfo = connectionInfo;
    eventData.message = "";

    raiseEvent(ClientEvent::Type::TUN_IFACE_DISABLED, eventData);
}

void OpenVpnClient::onEpkiErrorEvent(const ClientAPI::Event &ev, const ClientAPI::ConnectionInfo &connectionInfo)
{
    EventData eventData;

    eventData.status = status;
    eventData.vpnEvent = ev;
    eventData.connectionInfo = connectionInfo;
    eventData.message = "";

    raiseEvent(ClientEvent::Type::EPKI_ERROR, eventData);
}

void OpenVpnClient::onEpkiInvalidAliasEvent(const ClientAPI::Event &ev, const ClientAPI::ConnectionInfo &connectionInfo)
{
    EventData eventData;

    eventData.status = status;
    eventData.vpnEvent = ev;
    eventData.connectionInfo = connectionInfo;
    eventData.message = "";

    raiseEvent(ClientEvent::Type::EPKI_INVALID_ALIAS, eventData);
}

void OpenVpnClient::onRelayErrorEvent(const ClientAPI::Event &ev, const ClientAPI::ConnectionInfo &connectionInfo)
{
    EventData eventData;

    eventData.status = status;
    eventData.vpnEvent = ev;
    eventData.connectionInfo = connectionInfo;
    eventData.message = "";

    raiseEvent(ClientEvent::Type::RELAY_ERROR, eventData);
}

bool OpenVpnClient::restoreNetworkSettings(RestoreNetworkMode mode, bool stdOutput)
{
    std::ostringstream os;
    bool success = true;
    NetFilter *localNetFilter = nullptr;

#if defined(OPENVPN_PLATFORM_LINUX)

    if(dnsPushIgnored == false && (mode == RestoreNetworkMode::FULL || mode == RestoreNetworkMode::DNS_ONLY))
    {
        DNSManager::Error retval;

        // restore resolv.conf

        retval = dnsManager->restoreResolvDotConf();

        os.str("");

        if(retval == DNSManager::Error::OK)
        {
            os << "Successfully restored DNS settings";
        }
        else if(retval == DNSManager::Error::RESOLV_DOT_CONF_OPEN_ERROR)
        {
            os << "WARNING: resolv.conf not found. DNS settings do not need to be restored.";

            success = true;
        }
        else if(retval == DNSManager::Error::RESOLV_DOT_CONF_RESTORE_NOT_FOUND)
        {
            os << "WARNING: Backup copy of resolv.conf not found. DNS settings do not need to be restored.";

            success = true;
        }
        else if(retval == DNSManager::Error::RESOLV_DOT_CONF_RESTORE_ERROR)
        {
            os << "ERROR: Cannot restore DNS settings.";

            success = false;
        }
        else
        {
            os << "ERROR: Cannot restore DNS settings. Unknown error.";

            success = false;
        }

        os << std::endl;

        if(stdOutput == false)
            OPENVPN_LOG_STRING(os.str());
        else
            std::cout << os.str();

        if(dnsManager->systemHasSystemdResolved())
        {
            os.str("");

            retval = dnsManager->restoreResolved();

            if(retval == DNSManager::Error::OK)
            {
                os << "Restoring systemd-resolved DNS settings";
            }
            else if(retval == DNSManager::Error::RESOLVED_IS_NOT_AVAILABLE)
            {
                os << "ERROR systemd-resolved is not available on this system";

                success = false;
            }
            else if(retval == DNSManager::Error::RESOLVED_REVERT_DNS_ERROR)
            {
                os << "ERROR systemd-resolved: Failed to revert DNS configuration";

                success = false;
            }
            else if(retval == DNSManager::Error::RESOLVED_RESTORE_ERROR)
            {
                os << "ERROR systemd-resolved: Failed to restore DNS configuration";

                success = false;
            }
            else if(retval == DNSManager::Error::NO_RESOLVED_COMMAND)
            {
                os << "ERROR systemd-resolved: resolvectl or systemd-resolve command not found";

                success = false;
            }
            else
            {
                os << "ERROR systemd-resolved: Unknown error while reverting DNS";

                success = false;
            }

            os << std::endl;

            if(stdOutput == false)
                OPENVPN_LOG_STRING(os.str());
            else
                std::cout << os.str();
        }
    }

#endif

#if defined(OPENVPN_PLATFORM_MAC)

    if(dnsPushIgnored == false && (mode == RestoreNetworkMode::FULL || mode == RestoreNetworkMode::DNS_ONLY))
    {
        std::ifstream systemDNSDumpFile;
        std::string line;
        SCDynamicStoreRef dnsStore;
        CFMutableArrayRef dnsArray;
        CFDictionaryRef dnsDictionary;
        CFArrayRef dnsList;
        CFIndex ndx, listItems;
        bool setValueSuccess = true;

        os.str("");

        if(access(dnsBackupFile.c_str(), F_OK) == 0)
        {
            dnsArray = CFArrayCreateMutable(NULL, 0, NULL);

            systemDNSDumpFile.open(dnsBackupFile);

            while(std::getline(systemDNSDumpFile, line))
                CFArrayAppendValue(dnsArray, CFStringCreateWithFormat(NULL, NULL, CFSTR("%s"), line.c_str()));

            systemDNSDumpFile.close();

            dnsStore = SCDynamicStoreCreate(kCFAllocatorSystemDefault, CFSTR("AirVPNDNSRestore"), NULL, NULL);

            dnsDictionary = CFDictionaryCreate(NULL, (const void **)(CFStringRef []){ CFSTR("ServerAddresses") }, (const void **)&dnsArray, 1, &kCFTypeDictionaryKeyCallBacks, &kCFTypeDictionaryValueCallBacks);

            dnsList = SCDynamicStoreCopyKeyList(dnsStore, CFSTR("Setup:/Network/(Service/.+|Global)/DNS"));

            listItems = CFArrayGetCount(dnsList);

            if(listItems > 0)
            {
                ndx = 0;

                while(ndx < listItems)
                {
                    setValueSuccess &= SCDynamicStoreSetValue(dnsStore, (CFStringRef)CFArrayGetValueAtIndex(dnsList, ndx), dnsDictionary);

                    ndx++;
                }
            }

            if(setValueSuccess == true)
                os << "Successfully restored system DNS.";
            else
            {
                os << "ERROR: Error while restoring DNS settings.";

                success = false;
            }
        }
        else
        {
            os << "WARNING: Backup copy of resolv.conf not found. DNS settings do not need to be restored.";

            success = true;
        }

        os << std::endl;

        if(stdOutput == false)
            OPENVPN_LOG_STRING(os.str());
        else
            std::cout << os.str();
    }

#endif

    unlink(dnsBackupFile.c_str());

    os.str("");

    if(mode == RestoreNetworkMode::FULL || mode == RestoreNetworkMode::FIREWALL_ONLY)
    {
        if(networkLockMode == NetFilter::Mode::OFF || netFilter == nullptr)
        {
            localNetFilter = new NetFilter(resourceDirectory, NetFilter::Mode::AUTO);

            if(localNetFilter->restore())
                os << "Network filter successfully restored";
            else
                os << "WARNING: Backup copy of network filter not found. Network settings do not need to be restored.";

            success = true;
            
            if(networkLockMode == NetFilter::Mode::OFF)
                delete localNetFilter;
        }
        else if(netFilter != nullptr)
        {
            if(netFilter->rollbackSession())
                os << "Session network filter and lock rollback successful";

            success = true;
        }
        else
        {
            os << "ERROR: Cannot create or use NetFilter object.";

            success = false;
        }

        os << std::endl;

        if(stdOutput == false)
            OPENVPN_LOG_STRING(os.str());
        else
            std::cout << os.str();
    }

    return success;
}

void OpenVpnClient::log(const ClientAPI::LogInfo &log)
{
    std::lock_guard<std::mutex> lock(log_mutex);

    std::cout << date_time() << ' ' << log.text << std::flush;
}

void OpenVpnClient::clock_tick()
{
    const ClockTickAction action = clock_tick_action;
    clock_tick_action = CT_UNDEF;

    switch(action)
    {
        case CT_STOP:
        {
            std::cout << "signal: CT_STOP" << std::endl;

            stop();
        }
        break;

        case CT_RECONNECT:
        {
            std::cout << "signal: CT_RECONNECT" << std::endl;

            reconnect(0);
        }
        break;

        case CT_PAUSE:
        {
            std::cout << "signal: CT_PAUSE" << std::endl;

            pause("clock-tick pause");
        }
        break;

        case CT_RESUME:
        {
            std::cout << "signal: CT_RESUME" << std::endl;

            resume();
        }
        break;

        case CT_STATS:
        {
            std::cout << "signal: CT_STATS" << std::endl;

            print_stats();
        }
        break;

        default: break;
    }
}

void OpenVpnClient::external_pki_cert_request(ClientAPI::ExternalPKICertRequest &certreq)
{
    if(!epki_cert.empty())
    {
        certreq.cert = epki_cert;
        certreq.supportingChain = epki_ca;
    }
    else
    {
        certreq.error = true;
        certreq.errorText = "external_pki_cert_request not implemented";
    }
}

void OpenVpnClient::external_pki_sign_request(ClientAPI::ExternalPKISignRequest &signreq)
{
#if defined(USE_MBEDTLS)

    if(epki_ctx.defined())
    {
        try
        {
            // decode base64 sign request
            BufferAllocated signdata(256, BufferAllocated::GROW);
            base64->decode(signdata, signreq.data);

            // get MD alg
            const mbedtls_md_type_t md_alg = PKCS1::DigestPrefix::MbedTLSParse().alg_from_prefix(signdata);

            // log info
            OPENVPN_LOG("SIGN[" << PKCS1::DigestPrefix::MbedTLSParse::to_string(md_alg) << ',' << signdata.size() << "]: " << render_hex_generic(signdata));

            // allocate buffer for signature
            BufferAllocated sig(mbedtls_pk_get_len(epki_ctx.get()), BufferAllocated::ARRAY);

            // sign it
            size_t sig_size = 0;

            const int status = mbedtls_pk_sign(epki_ctx.get(), md_alg, signdata.c_data(), signdata.size(), sig.data(), &sig_size, rng_callback, this);

            if(status != 0)
                throw Exception("mbedtls_pk_sign failed, err=" + openvpn::to_string(status));

            if(sig.size() != sig_size)
                throw Exception("unexpected signature size");

            // encode base64 signature

            signreq.sig = base64->encode(sig);

            OPENVPN_LOG("SIGNATURE[" << sig_size << "]: " << signreq.sig);
        }
        catch(const std::exception &e)
        {
            signreq.error = true;
            signreq.errorText = std::string("external_pki_sign_request: ") + e.what();
        }
    }
    else

#endif

    {
        signreq.error = true;
        signreq.errorText = "external_pki_sign_request not implemented";
    }
}

int OpenVpnClient::rng_callback(void *arg, unsigned char *data, size_t len)
{
    OpenVpnClient *self = (OpenVpnClient *)arg;

    if(!self->rng)
    {
        self->rng.reset(new SSLLib::RandomAPI(false));
        self->rng->assert_crypto();
    }

    return self->rng->rand_bytes_noexcept(data, len) ? 0 : -1; // using -1 as a general-purpose mbed TLS error code
}

bool OpenVpnClient::pause_on_connection_timeout()
{
    return false;
}

#ifdef OPENVPN_REMOTE_OVERRIDE

bool OpenVpnClient::remote_override_enabled()
{
    return !remote_override_cmd.empty();
}

void OpenVpnClient::remote_override(ClientAPI::RemoteOverride &ro)
{
    RedirectPipe::InOut pio;
    Argv argv;
    argv.emplace_back(remote_override_cmd);
    OPENVPN_LOG(argv.to_string());

    const int status = system_cmd(remote_override_cmd, argv, nullptr, pio, RedirectPipe::IGNORE_ERR);

    if(!status)
    {
        const std::string out = string::first_line(pio.out);

        OPENVPN_LOG("REMOTE OVERRIDE: " << out);

        auto svec = string::split(out, ',');

        if(svec.size() == 4)
        {
            ro.host = svec[0];
            ro.ip = svec[1];
            ro.port = svec[2];
            ro.proto = svec[3];
        }
        else
            ro.error = "cannot parse remote-override, expecting host,ip,port,proto (at least one or both of host and ip must be defined)";
    }
    else
        ro.error = "status=" + std::to_string(status);
}

#endif

void OpenVpnClient::raiseEvent(ClientEvent::Type event, const EventData &data)
{
    for(eventSubscriptionIterator = eventSubscription.begin(); eventSubscriptionIterator != eventSubscription.end(); eventSubscriptionIterator++)
    {
        if(eventSubscriptionIterator->first == event)
            (*eventSubscriptionIterator->second)(data);
    }
}

bool OpenVpnClient::subscribeEvent(ClientEvent::Type event, EventCallbackFunction callbackFunction)
{
    bool subscriptionFound = false;

    if(callbackFunction == nullptr)
        return false;

    for(eventSubscriptionIterator = eventSubscription.begin(); eventSubscriptionIterator != eventSubscription.end(); eventSubscriptionIterator++)
    {
        if(eventSubscriptionIterator->second == callbackFunction)
            subscriptionFound = true;
    }

    if(subscriptionFound == true)
        return false;

    eventSubscription.insert(std::make_pair(event, callbackFunction));

    return true;
}

bool OpenVpnClient::unsubscribeEvent(ClientEvent::Type event, EventCallbackFunction callbackFunction)
{
    bool subscriptionFound = false;

    if(callbackFunction == nullptr)
        return false;

    for(eventSubscriptionIterator = eventSubscription.begin(); eventSubscriptionIterator != eventSubscription.end(); eventSubscriptionIterator++)
    {
        if(eventSubscriptionIterator->second == callbackFunction)
        {
            subscriptionFound = true;
            
            eventSubscription.erase(eventSubscriptionIterator);
        }
    }

    return subscriptionFound;
}

std::vector<std::string> OpenVpnClient::supportedDataCipher = { "AES-128-GCM", "AES-192-GCM", "AES-256-GCM", "CHACHA20-POLY1305" };

bool OpenVpnClient::isDataCipherSupported(std::string cipher)
{
    return (std::find(supportedDataCipher.begin(), supportedDataCipher.end(), cipher) != supportedDataCipher.end());
}

std::vector<std::string> OpenVpnClient::getSupportedDataCiphers()
{
    return std::vector<std::string>(supportedDataCipher);
}


bool OpenVpnClient::profileNeedsResolution(std::string profile)
{
    ClientAPI::OpenVPNClientHelper clientHelper;
    ClientAPI::Config config;
    ClientAPI::EvalConfig evalConfig;
    struct addrinfo ipinfo, *ipres = NULL;
    int aires;
    std::ostringstream os;
    bool needsResolving = false;

    config.guiVersion = "";

    config.serverOverride = "";
    config.portOverride = "";
    config.protoOverride = "";
    config.cipherOverrideAlgorithm = "";
    config.connTimeout = 0;
    config.compressionMode = "";
    config.allowUnusedAddrFamilies = "";
    config.tcpQueueLimit = 0;
    config.disableNCP = "";
    config.privateKeyPassword = "";
    config.tlsVersionMinOverride = "";
    config.tlsCertProfileOverride = "";
    config.disableClientCert = "";
    config.proxyHost = "";
    config.proxyPort = "";
    config.proxyUsername = "";
    config.proxyPassword = "";
    config.proxyAllowCleartextAuth = "";
    config.altProxy = "";
    config.dco = "";
    config.defaultKeyDirection = -1;
    config.sslDebugLevel = 0;
    config.googleDnsFallback = false;
    config.autologinSessions = "";
    config.retryOnAuthFailed = "";
    config.tunPersist = "";
    config.gremlinConfig = "";
    config.info = true;
    config.wintun = false;
    config.content = profile;

    evalConfig = clientHelper.eval_config(config);

    for(ClientAPI::RemoteEntry remoteEntry : evalConfig.remoteList)
    {
        if(ipres != NULL)
        {
            freeaddrinfo(ipres);

            ipres = NULL;
        }

        memset(&ipinfo, 0, sizeof(ipinfo));

        ipinfo.ai_family = PF_UNSPEC;
        ipinfo.ai_flags = AI_NUMERICHOST;

        aires = getaddrinfo(remoteEntry.server.c_str(), NULL, &ipinfo, &ipres);
        
        if(aires != 0)
            needsResolving = true;
        else
        {
            if(ipres->ai_family != AF_INET && ipres->ai_family != AF_INET6)
                needsResolving = true;
        }
    }

    if(ipres != NULL)
        freeaddrinfo(ipres);

    return needsResolving;
}

std::string OpenVpnClient::resolveProfile(std::string profile, bool ipv6)
{
    ClientAPI::OpenVPNClientHelper clientHelper;
    ClientAPI::Config config;
    ClientAPI::EvalConfig evalConfig;
    struct addrinfo ipinfo, *ipres = NULL;
    int aires;
    char resip[64];
    std::string protocol;
    std::ostringstream os;
    size_t pos;

    config.guiVersion = "";

    config.serverOverride = "";
    config.portOverride = "";
    config.protoOverride = "";
    config.cipherOverrideAlgorithm = "";
    config.connTimeout = 0;
    config.compressionMode = "";
    config.allowUnusedAddrFamilies = "";
    config.tcpQueueLimit = 0;
    config.disableNCP = "";
    config.privateKeyPassword = "";
    config.tlsVersionMinOverride = "";
    config.tlsCertProfileOverride = "";
    config.disableClientCert = "";
    config.proxyHost = "";
    config.proxyPort = "";
    config.proxyUsername = "";
    config.proxyPassword = "";
    config.proxyAllowCleartextAuth = "";
    config.altProxy = "";
    config.dco = "";
    config.defaultKeyDirection = -1;
    config.sslDebugLevel = 0;
    config.googleDnsFallback = false;
    config.autologinSessions = "";
    config.retryOnAuthFailed = "";
    config.tunPersist = "";
    config.gremlinConfig = "";
    config.info = true;
    config.wintun = false;
    config.content = profile;

    evalConfig = clientHelper.eval_config(config);

    for(ClientAPI::RemoteEntry remoteEntry : evalConfig.remoteList)
    {
        if(ipres != NULL)
        {
            freeaddrinfo(ipres);

            ipres = NULL;
        }

        memset(&ipinfo, 0, sizeof(ipinfo));

        if(ipv6 == true)
            ipinfo.ai_family = PF_INET6;
        else
            ipinfo.ai_family = PF_INET;

        ipinfo.ai_flags = AI_NUMERICHOST;

        aires = getaddrinfo(remoteEntry.server.c_str(), NULL, &ipinfo, &ipres);

        if(aires || ipres == NULL)
        {
            memset(&ipinfo, 0, sizeof(ipinfo));

            if(ipv6 == true)
                ipinfo.ai_family = PF_INET6;
            else
                ipinfo.ai_family = PF_INET;

            os.str("");

            aires = getaddrinfo(remoteEntry.server.c_str(), NULL, &ipinfo, &ipres);

            if(aires == 0)
            {
                getnameinfo(ipres->ai_addr, ipres->ai_addrlen, resip, sizeof(resip), NULL, 0, NI_NUMERICHOST);

                switch(ipres->ai_family)
                {
                    case AF_INET:
                    {
                        protocol = "4";
                    }
                    break;

                    case AF_INET6:
                    {
                        protocol = "6";
                    }
                    break;

                    default:
                    {
                        protocol = "??";
                    }
                    break;
                }

                os.str("");

                os << "Resolved remote OpenVPN server " << remoteEntry.server << " into IPv" << protocol << " " << resip;

                OPENVPN_LOG(os.str());;

                // replace profile's server with resolved IP
    
                pos = profile.find(remoteEntry.server);

                while(pos != std::string::npos)
                {
                    profile.replace(pos, remoteEntry.server.size(), resip);

                    pos = profile.find(remoteEntry.server, pos + strlen(resip));
                }
            }
            else
            {
                os.str("");

                os << "ERROR: Cannot resolve " << remoteEntry.server << " (" << gai_strerror(aires) << ")";

                OPENVPN_LOG(os.str());
            }
        }
    }

    if(ipres != NULL)
        freeaddrinfo(ipres);

    return profile;
}
