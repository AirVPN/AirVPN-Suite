#!/bin/sh

#
# Build dynamic binary for hummingbird
#
# Version 1.1 - ProMIND
#

if [ `uname` = "Darwin" ]; then
    echo "For macOS compilation, please use 'build-hummingbird-static.sh' script."

    exit 1
fi

BASE_NAME=hummingbird
SSL_LIB_TYPE=OPENSSL

INC_DIR=..
OPENVPN3=${INC_DIR}/openvpn3-airvpn
ASIO=${INC_DIR}/asio

SOURCES="src/hummingbird.cpp \
         src/localnetwork.cpp \
         src/dnsmanager.cpp \
         src/netfilter.cpp \
         src/wireguardclient.cpp \
         src/airvpntools.cpp \
         src/optionparser.cpp \
         src/base64.cpp \
         src/execproc.c \
         src/loadmod.c
        "

BIN_FILE=${BASE_NAME}

case $SSL_LIB_TYPE in
    OPENSSL)
        SSL_DEF=-DUSE_OPENSSL
        SSL_LIB_LINK="-lssl -lcrypto"
        break
        ;;

    MBEDTLS)
        SSL_DEF=-DUSE_MBEDTLS
        SSL_LIB_LINK="-lmbedtls -lmbedx509 -lmbedcrypto"
        break
        ;;

    *)
        echo "Unsupported SSL Library type ${SSL_LIB_TYPE}"
        exit 1
        ;;
esac

COMPILE="g++ -fwhole-program -Ofast -Wall -Wno-sign-compare -Wno-unused-parameter -std=c++17 -flto=4 -Wl,--no-as-needed -Wunused-local-typedefs -Wunused-variable -Wno-shift-count-overflow -pthread ${SSL_DEF} -DUSE_ASIO -DASIO_STANDALONE -DASIO_NO_DEPRECATED -I${ASIO}/asio/include -DHAVE_LZ4 -I${OPENVPN3} -I${OPENVPN3}/openvpn -I/usr/include/libxml2 ${SOURCES} ${SSL_LIB_LINK} -llz4 -lz -llzma -lzstd -lcryptopp -lcurl -o ${BIN_FILE}"

echo $COMPILE

$COMPILE

strip ${BIN_FILE}
